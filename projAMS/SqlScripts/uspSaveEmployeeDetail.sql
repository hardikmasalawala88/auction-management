use dbAMS 
go
/*
	Select * from dbo.EmployeeDetail order by IsActive
	uspSaveEmployeeDetail 0,'Prince',1,'Prince Palace',63476734,'12/31/2015','12/31/2015','Manager','xyz','harikm'
	uspSaveEmployeeDetail 0,'joshi keraan',0,'ved road,surat',63476734,'12/31/2015','12/31/2015','Employee','xyz','Abc@'
	delete  from dbo.EmployeeDetail where EmpId=3
	select * from dbo.EmployeeDetail where IsActive = 0 and EmpName like '% %'  or EmailId like '% %' 
	update EmployeeDetail set IsActive=0 where EmpId=6
	Select * from dbo.EmployeeDetail order by IsActive
*/

--
alter proc dbo.uspSaveEmployeeDetail
@EmpId	smallint,
@EmpName varchar(50),
@Gender	bit,
@Addr	varchar(100),
@ContactNo	varchar(15),
@Dob	date,	
@Doj	date,	
@Designation	varchar(15),
@EmpImage	varchar(100),
@EmailId	varchar(50)
with Encryption
as
declare @MaxEmpId smallint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveEmployeeDetail'
	Begin Tran @TransName
		set @MaxEmpId  = @EmpId 
		
		if(select COUNT(*) from dbo.EmployeeDetail
			where EmpId = @EmpId   ) > 0
		begin
			update dbo.EmployeeDetail
				set
					EmpName=@EmpName ,
					Gender=@Gender ,
					Addr=@Addr ,
					ContactNo=@ContactNo ,
					Dob=@Dob ,
					Doj=@Doj ,
					Designation=@Designation ,
					EmpImage=@EmpImage ,
					EmailId=@EmailId 
			
				where EmpId = @EmpId 
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment
			select @MaxEmpId  = ISNULL(MAX (EmpId),0)+1 from dbo.EmployeeDetail
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.EmployeeDetail (EmpId,EmpName,Gender,Addr,ContactNo,Dob,Doj,Designation,EmpImage,EmailId,IsActive )
			select @MaxEmpId,@EmpName,@Gender,@Addr,@ContactNo,@Dob,@Doj,@Designation,@EmpImage,@EmailId ,0
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxEmpId  EmployeeId
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

