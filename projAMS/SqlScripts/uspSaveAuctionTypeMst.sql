use dbAMS 
go
/*
	select * from dbo.AuctionTypeMst
	delete from dbo.AuctionTypeMst
	uspSaveAuctionTypeMst 0,'NORMAL AUCTION','It is like the treditional auction'
	uspSaveAuctionTypeMst 0,'EXPRESS AUCTION','Only for specif time auction is start'
	uspSaveAuctionTypeMst 0,'SEALDED AUCTION','Only one time bid and that is secrete'
	uspSaveAuctionTypeMst 0,'CHARITY AUCTION','Auction is running for charity'
	uspSaveAuctionTypeMst 0,'PACKAGE AUCTION','At lowest price you can get product'
	uspSaveAuctionTypeMst 0,'DEBUT AUCTION','Who lose in package auction they can able to take part in this auction.'
	delete from dbo.AuctionTypeMst
*/
--Create proc dbo.uspSaveAuctionTypeMst
alter proc dbo.uspSaveAuctionTypeMst
@AuctionTypeId	tinyint,          --paramiters
@AuctionTypeName varchar(30),		   --paramiters
@AuctionTypeDesc varchar(500)
with Encryption
as
declare @MaxAuctionTypeId tinyint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveAuctionTypeMst'
	Begin Tran @TransName
		set @MaxAuctionTypeId = @AuctionTypeId 
		
		if(select COUNT(*) from dbo.AuctionTypeMst 
			where AuctionTypeId = @AuctionTypeId ) > 0
		begin
			update dbo.AuctionTypeMst 
				set
					AuctionTypeName = @AuctionTypeName,
					AuctionTypeDesc = @AuctionTypeDesc
				where AuctionTypeId = @AuctionTypeId 
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment
			select @MaxAuctionTypeId = ISNULL(MAX (AuctionTypeId),0)+1 from dbo.AuctionTypeMst 
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.AuctionTypeMst (AuctionTypeId ,AuctionTypeName ,AuctionTypeDesc  )
			select @MaxAuctionTypeId ,@AuctionTypeName ,@AuctionTypeDesc 
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxAuctionTypeId AuctionTypeId,@AuctionTypeName  AucTypeName,@AuctionTypeDesc AucTypeDesc 
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

