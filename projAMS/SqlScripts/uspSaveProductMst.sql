use dbAMS 
go
/*
		
		uspSaveProductMst 4,0,1,'htc One M9',
							'Full Qualitative Camerawith 2 Camera 
							and with high defination  
							Front Camera with auto focus, octa cure processor',
							2,45000,'Owsome Display and camera','2 year warranty',
							'Nothing to say it is Owsome','New','dfdsfs1.jpg','dfdsfs2.jpg','dfdsfs3.jpg',
							'dfdsfs4.jpg',100,'Pickup from courier Office',1
		select * from ProductMst
		select * from ProductMainDetail		
		DELETE  FROM ProductMst
		DELETE  FROM ProductMainDetail
		select * from AuctionTypeMst
		select * from AuctionTypeItemDetail
		delete from ProductMst where productId=6
*/



alter proc dbo.uspSaveProductMst
@CategoryId	tinyint,
@ProductId	smallint,
@AuctionTypeId	tinyint,
--@ClientId	int,
--@EmpId smallint,
@ProductName	varchar(30),
@ProductDesc	varchar(1000),
@Qty	tinyint,
@Price	int,
@AdditionalInfo	varchar(1000),
@Warranty	varchar(100),
@FeaturesAndBenefits	varchar(1000),
@ProductCondition	varchar(10),
@Image1	varchar(100),
@Image2	varchar(100),
@Image3	varchar(100),
@Image4	varchar(100),
@ShippingCharge	smallint,
@ShippingDesc	varchar(200),
@IsConfirmed smallint
--@IsApproved	bit
with Encryption
as
declare @MaxProductId tinyint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveProdutMst'
	Begin Tran @TransName
		set @MaxProductId = @ProductId 
		
		if(select COUNT(*) from dbo.ProductMst 
			where  ProductId = @ProductId and CategoryId = @CategoryId ) > 0
		begin
		
			update dbo.ProductMst 
		
				set
					
					ProductName=@ProductName,
					ProductDesc=@ProductDesc,
					Qty=@Qty,
					Price=@Price,
					AdditionalInfo=@AdditionalInfo,
					Warranty=@Warranty,
					FeaturesAndBenefits=@FeaturesAndBenefits,
					ProductCondition=@ProductCondition,
					Image1=@Image1,
					Image2=@Image2,
					Image3=@Image3,
					Image4=@Image4,
					ShippingCharge=@ShippingCharge,
					ShippingDesc=@ShippingDesc
					--IsApproved=@IsApproved
				where ProductId=@ProductId and CategoryId = @CategoryId
						
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from ClientDetail
			select @MaxProductId = ISNULL(MAX (ProductId),0)+1 from dbo.ProductMst 
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.ProductMst(CategoryId,ProductId,AuctionTypeId,ProductName,ProductDesc,
										Qty,Price,AdditionalInfo,Warranty,FeaturesAndBenefits,
										ProductCondition,Image1,Image2,Image3,Image4,
										ShippingCharge,ShippingDesc)
			select @CategoryId,@MaxProductId ,@AuctionTypeId,@ProductName,@ProductDesc,
										@Qty,@Price,@AdditionalInfo,@Warranty,@FeaturesAndBenefits,
										@ProductCondition,@Image1,@Image2,@Image3,@Image4,
										@ShippingCharge,@ShippingDesc
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @CategoryId CategoryId , @MaxProductId ProductId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	