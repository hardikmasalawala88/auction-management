use dbAMS
go
alter proc dbo.uspGetPacketDetailClientSide
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetPacketDetailClientSide'
Begin Tran @TranName

select PacketId , PacketName,Bids,Price,Times,Duration,Convert(Varchar(2),Times)+' '+Duration as Timelimit from dbo.PacketDetail   
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
