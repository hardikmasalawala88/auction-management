use dbAMS 
go
/*
uspGetRecentBidder 1
select * from BidDetail

select ClientId,Max(BidAmt)as BidAmt from dbo.BidDetail where ProductId=1 Group By clientId

*/

alter proc dbo.uspGetRecentBidder
@ProductId smallint
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'GetRecentBidder'
	Begin Tran @TransName
		
		
		if(select COUNT(*) from dbo.BidDetail
			where ProductId=@ProductId ) > 0
		begin
			select top 5 cd.ClientName as BidderName ,BidAmt from dbo.BidDetail  bd  
			join ClientDetail cd  on cd.ClientId=bd.ClientId where ProductId=@ProductId order by  BidAmt desc
			
			set @ErrNo = @@ERROR 
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
		
			select  'No One Bid You Became 1st' as BidderName ,0 BidAmt
		end
		
		--when display alias name
	
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	