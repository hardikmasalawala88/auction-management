use dbAMS
go
/*
	select * from CategoryTypeMst
	Delete  from CategoryTypeMst where categoryTypeId=8
	
	select * from CategoryMst where CategoryTypeId=8 
	select * from ProductMst where CategoryId =1
	uspGetMenuProduct 1,'s'
*/

alter proc dbo.uspGetMenuProduct
@CategoryTypeId	tinyint,
@Category	varchar(30)
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetMenuProduct'
Begin Tran @TranName

select pm.ProductName,pm.ProductId,am.AuctionTypeName,aid.BuyNowAmt,pm.Image1,pm.Image2,cm.Category,
DATEDIFF(DAY,GETDATE(),aid.EndDt)as RDay,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND   ,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)),0),108) as RTime,
isnull(aid.BidMulti,0) as BidMulti
from ProductMst pm
join CategoryMst cm on 
		cm.CategoryId=pm.CategoryId 
join AuctionTypeItemDetail aid on 
		aid.AuctionTypeId=pm.AuctionTypeId and
		aid.CategoryId=pm.CategoryId and 
		aid.ProductId=pm.ProductId 
join AuctionTypeMst am on 
		am.AuctionTypeId=aid.AuctionTypeId
where  DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
and
cm.CategoryTypeId=@CategoryTypeId and cm.Category like '%'+@Category+'%'

--select * from CategoryMst
--select pm.ProductName,am.AuctionTypeName,
--aid.BuyNowAmt,pm.Image1,pm.Image2,cm.Category
--from ProductMst  pm
--join CategoryMst cm on cm.CategoryId=pm.CategoryId and cm.CategoryTypeId=1 and cm.Category like 'htc'
--join AuctionTypeItemDetail aid on aid.AuctionTypeId=pm.AuctionTypeId and
--aid.CategoryId=pm.CategoryId and 
--aid.ProductId=pm.ProductId 
--join AuctionTypeMst am on am.AuctionTypeId=aid.AuctionTypeId
--where pm.CategoryId=cm.CategoryId


--where DATEDIFF(DAY,aid.EndDt,GETDATE()) >=0  and
-- DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) <=0
--and cm.Category like @Category
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
/*	
	select * from CategoryMst
*/