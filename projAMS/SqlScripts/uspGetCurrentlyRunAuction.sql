use dbAMS
go
alter proc dbo.uspGetCurrentlyRunAuction
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetCurrentlyRunAuction'
Begin Tran @TranName

select am.AuctionTypeName,
DATEDIFF(DAY,GETDATE(),aid.StartDt) as StartDay,
DATEDIFF(DAY,aid.StartDt,aid.EndDt) as EndDay,
aid.StartDt ,aid.EndDt,CONVERT(varchar(8),aid.StartTime ) as StartTime,
CONVERT(varchar(8),aid.EndTime) as EndTime,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as t1,
aid.CategoryId,aid.ProductId,aid.AuctionTypeId,aid.BuyNowAmt ,pm.*
from ProductMst pm 
join AuctionTypeItemDetail aid 
on aid.AuctionTypeId=pm.AuctionTypeId and 
aid.CategoryId=pm.CategoryId and aid.ProductId=pm.ProductId 
join AuctionTypeMst am on am.AuctionTypeId=aid.AuctionTypeId
where DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
   
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
