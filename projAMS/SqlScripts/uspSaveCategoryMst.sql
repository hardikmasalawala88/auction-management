use dbAMS 
go
/*
	select ctm.CategoryType,cm.CategoryId,cm.Category from dbo.CategoryMst as cm,dbo.CategoryTypeMst as ctm where ctm.CategoryTypeId=cm.CategoryTypeId
		
	uspSaveCategoryMst 2,22,'Samsung25'
	select * from dbo.CategoryMst
	
	delete from dbo.CategoryMst 
	where 
	CategoryTypeId = 2 
	
	select *
	from dbo.CategoryMst 
	
	select *
	from dbo.CategoryMst cm
	join dbo.CategoryTypeMst ctm on ctm.CategoryTypeId =cm.CategoryTypeId 
*/

--create proc dbo.uspsaveCategoryMst

alter proc dbo.uspSaveCategoryMst
@CategoryTypeId	tinyint,
@CategoryId	tinyint,
@Category varchar(30)	   --paramiters
with Encryption
as
declare @MaxCategoryId tinyint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveCategoryMst'
	Begin Tran @TransName
		set @MaxCategoryId = @CategoryId 
		
		if(select COUNT(*) from dbo.CategoryMst 
			where CategoryId = @CategoryId   ) > 0
		begin
		
			update dbo.CategoryMst 
				set
					Category =@Category					
				where CategoryId = @CategoryId 
						
				 
				
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment
			select @MaxCategoryId = ISNULL(MAX (CategoryId),0)+1 from dbo.CategoryMst 
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.CategoryMst(CategoryTypeId,CategoryId,Category )
			select @CategoryTypeId,@MaxCategoryId ,@Category 
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxCategoryId CategoryId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	