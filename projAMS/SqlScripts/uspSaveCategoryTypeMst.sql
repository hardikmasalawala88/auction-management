use dbAMS 
go
/*
	select * from dbo.CategoryTypeMst
	select * from productMainDetail
	delete from CategoryTypeMst where categoryTypeId = 9
	uspSaveCategoryTypeMst 1,'MOBILES'
	uspSaveCategoryTypeMst 2,'LAPTOPS & COMPUTER'
	uspSaveCategoryTypeMst 0,'ELECTRONICS'
	uspSaveCategoryTypeMst 0,'JEWELLERY'
	uspSaveCategoryTypeMst 0,'HISTORICAL'
	uspSaveCategoryTypeMst 0,'BOOKS & EDUCATION'
	uspSaveCategoryTypeMst 0,'AUTOMOTIVES'
	uspSaveCategoryTypeMst 0,'MORE'
	
	delete from dbo.CategoryTypeMst 
*/
alter proc dbo.uspSaveCategoryTypeMst
@CategoryTypeId	tinyint,          --paramiters
@CategoryType	varchar(30)		   --paramiters
with Encryption
as
declare @MaxCategoryTypeId tinyint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveCategoryTypeMst'
	Begin Tran @TransName
		set @MaxCategoryTypeId = @CategoryTypeId 
		
		if(select COUNT(*) from dbo.CategoryTypeMst 
			where CategoryTypeId = @CategoryTypeId ) > 0
		begin
			update dbo.CategoryTypeMst 
				set
					CategoryType =@CategoryType 
				where CategoryTypeId = @CategoryTypeId 
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment
			select @MaxCategoryTypeId = ISNULL(MAX (CategoryTypeId),0)+1 from dbo.CategoryTypeMst 
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.CategoryTypeMst (CategoryTypeId ,CategoryType )
			select @MaxCategoryTypeId ,@CategoryType
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxCategoryTypeId CategoryTypeId,@CategoryType  CatType
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

