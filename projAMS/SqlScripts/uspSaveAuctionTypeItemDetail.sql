use dbAMS 
go

/*
	 uspSaveAuctionTypeItemDetail 1,4,1,50,'2014/5/15','2015/6/11','5:10:25','6:15:20',200,2,,5
uspSaveAuctionTypeItemDetail	 5,1,1,0,2015/03/12,2015/03/16,'10:50','02:10',15000

uspSaveAuctionTypeItemDetail	 5,1,1,0,'2015/03/16','2015/03/16','10:50','02:10',15000
uspSaveAuctionTypeItemDetail 2,1,2,25000,'2015-04-02','2015-04-04','06:30','06:35',25000
2,,1,8,|@MinAmt|SINT|55000|@StartDt|DATE|2015-04-24|@EndDt|DATE|2015-04-27|@StartTime|VCHR|14:00|@EndTime|VCHR|21:00|@BuyNowAmt|INT|60000
		
	select *,aid.startdt,aid.StartDt,aid.EndDt,aid.StartTime,aid.EndTime,aid.BuyNowAmt from ProductMst pm 
	join AuctionTypeItemDetail  aid on aid.AuctionTypeId=pm.AuctionTypeId and 
										aid.CategoryId=pm.CategoryId and
										aid.ProductId=pm.ProductId  
	
		
		select * from BidDetail 
		select * from AuctionTypeItemDetail
		select * from ProductMainDetail
		update AuctionTypeItemDetail set EndTime='21:00:00' where CategoryId=23 and ProductId=3
		
delete From BidDetail
delete from AuctionTypeItemDetail
select * from AuctionTypeItemDetail
*/


alter proc dbo.uspSaveAuctionTypeItemDetail
@AuctionTypeId	tinyint,
@CategoryId		tinyint,
@ProductId		smallint,
@MinAmt			int,
@StartDt		date,
@EndDt			date,
@StartTime		varchar(12),
@EndTime		varchar(12),
@BuyNowAmt		int
--@BidCnt			int
--@CharityACBank	varchar(50),
--@CharityACName	varchar(100),
--@CharityACNO	varchar(50),
--@BidMulti		tinyint

with Encryption
as
declare @MaxAuctionTypeId int ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveAuctionTypeItemDetail'
	Begin Tran @TransName
		set @MaxAuctionTypeId = @AuctionTypeId 
		
		if(select COUNT(*) from dbo.AuctionTypeItemDetail
			where @AuctionTypeId = @AuctionTypeId and CategoryId=@CategoryId and ProductId	=@ProductId) > 0 
		begin
		
			update dbo.AuctionTypeItemDetail
		
				set
					AuctionTypeId=@AuctionTypeId,
					CategoryId=@CategoryId,
					ProductId=@ProductId,
					MinAmt=@MinAmt,
					StartDt=@StartDt,
					EndDt=@EndDt,
					StartTime=@StartTime,
					EndTime=@EndTime,
					BuyNowAmt=@BuyNowAmt
					--BidCnt=@BidCnt
					--CharityACBank=@CharityACBank,
					--CharityACName=@CharityACName,
					--CharityACNO=@CharityACNO,
					--BidMulti=@BidMulti
				
				where AuctionTypeId=@AuctionTypeId and CategoryId=@CategoryId and ProductId	=@ProductId
										
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from Auctiontypeitemdetail
			--select @MaxAuctionTypeId = ISNULL(MAX (AuctionTypeId),0)+1 from dbo.AuctionTypeItemDetail 
				
				--insert start
			insert into dbo.AuctionTypeItemDetail(AuctionTypeId,CategoryId,ProductId,MinAmt,StartDt,EndDt,StartTime,EndTime,BuyNowAmt)
			select @AuctionTypeId,@CategoryId,@ProductId,@MinAmt,@StartDt,@EndDt,@StartTime,@EndTime,@BuyNowAmt
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	--select @CategoryId CategoryId , @ProductId ProductId,@ClientId ClientId,@EmpId EmpId 
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
