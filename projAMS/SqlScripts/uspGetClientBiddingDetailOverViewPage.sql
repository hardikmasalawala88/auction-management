use dbAMS 
go
/*
uspGetClientBiddingDetailOverViewPage 1
*/
alter proc dbo.uspGetClientBiddingDetailOverViewPage
@ClientId int
with Encryption
as
--Show Packet Purchase Detail
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetClientBiddingDetailOverViewPage'
Begin Tran @TranName
 

Select BidAmt,Convert(Varchar(20),bd.BidDate,7) BidDate,Convert(Varchar(8),bd.BidTime) BidTime,pm.ProductName 
from BidDetail bd
join ProductMst pm on pm.ProductId=bd.ProductId
where bd.ClientId=@ClientId 
Order By bd.BidTime,bd.BidDate Desc

	--Show Packet Purchase Detail
--select *  From  PackagePurchase

	
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0