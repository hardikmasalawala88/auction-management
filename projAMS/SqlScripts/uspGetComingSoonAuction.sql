use dbAMS
go
/*
uspGetComingSoonAuction
select * from AuctionTypeMst
*/
alter proc dbo.uspGetComingSoonAuction
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetComingSoonAuction'
Begin Tran @TranName

select top 4 pm.ProductName,am.AuctionTypeName,pm.Image1,
DATEDIFF(DAY,GETDATE(),aid.StartDt)as RDay,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as RTime,
DATEDIFF(HOUR,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime))  as TotalH,--* CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND   ,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as TotalH,
DATEDIFF(MINUTE,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime))  as TotalM,
DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime))  as TotalS,
DATEDIFF(DAY,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime))  as TotalD,
aid.StartDt ,aid.EndDt,CONVERT(varchar(8),aid.StartTime ) as StartTime,
CONVERT(varchar(8),aid.EndTime) as EndTime,
aid.CategoryId,aid.ProductId,aid.AuctionTypeId,aid.BuyNowAmt
from ProductMst pm 
join AuctionTypeItemDetail aid 
on aid.AuctionTypeId=pm.AuctionTypeId and 
aid.CategoryId=pm.CategoryId and 
aid.ProductId=pm.ProductId 
join AuctionTypeMst am on am.AuctionTypeId=aid.AuctionTypeId
where DATEDIFF(DAY,GETDATE(),aid.StartDt) >=0 and
DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)) >=0 and
DATEDIFF(DAY,GETDATE(),aid.StartDt) < 7
order by aid.EndDt Desc
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
/*	
	DECLARE @StartTime DATETIME = getdate()  --'2011-09-23 15:00:00'
       --,@EndTime   DATETIME = '2011-09-23 17:54:02'
       ,@EndTime   DATETIME = '2014-09-23 17:54:02'

SELECT CONVERT(VARCHAR(8), DATEADD(SECOND, DATEDIFF(SECOND,@StartTime, @EndTime),0), 108) as ElapsedTime
SELECT  DATEDIFF(DAY,@EndTime,@StartTime )

CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as t1,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as t2,

select aid.CategoryId,aid.ProductId,aid.AuctionTypeId,CONVERT(varchar(10),aid.StartDt,105)  as StartDt,CONVERT(varchar(10),aid.EndDt,105)  as EndDt,
aid.StartTime,aid.EndTime,aid.BuyNowAmt from ProductMst pm 
	join AuctionTypeItemDetail aid on aid.AuctionTypeId=pm.AuctionTypeId and 
		aid.CategoryId=pm.CategoryId and aid.ProductId=pm.ProductId 									
											
											--where DATEDIFF(DAY,aid.StartDt,aid.EndDt) >0 and DATEDIFF(DAY,GETDATE(),aid.StartDt)>0


*/