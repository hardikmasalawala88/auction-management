/*
uspgetProductSpecification 1
*/
use dbAMS
go

alter proc dbo.uspgetProductSpecification
@ProductId smallint
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetProductSpecification'
Begin Tran @TranName

select pm.*,DATEDIFF(DAY,GETDATE(),aid.EndDt )as RDay,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt ) + ' ' + CONVERT(varchar(10), aid.EndTime )),0),108) as RTime 
,atm.AuctionTypeName,
CONVERT(varchar(10),aid.StartDt,105) as StartDt,
CONVERT(varchar(10),aid.EndDt,105)  as EndDt,
aid.StartTime,aid.EndTime,aid.BuyNowAmt,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)  as TotalST
from ProductMst pm 
	join AuctionTypeItemDetail  aid on aid.AuctionTypeId=pm.AuctionTypeId and 
										aid.CategoryId=pm.CategoryId and
										aid.ProductId=pm.ProductId 
	join  AuctionTypeMst atm on atm.AuctionTypeId=pm.AuctionTypeId
 where pm.ProductId=@ProductId
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0