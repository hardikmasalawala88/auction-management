--uspVerifyEmail 'VivekSoni@gm.com'

use dbAMS 
go

alter procedure dbo.uspVerifyEmail
@EmailId varchar(50)
with encryption
as
declare @TranName varchar(20), @Errno int
	set @TranName ='uspVerifyEmailId'
	Begin Tran @TranName
		select COUNT (*) cnt 
		from dbo.EmployeeDetail  where EmailId like @EmailId
					
		set @Errno =@@ERROR
		if @Errno <>0 goto Err
	
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
	
	
	