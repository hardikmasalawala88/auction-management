USE dbAMS
GO
/*
		select * from EmployeeDetail
		Select * From EmployeeDetail where EmpId =1
		select * from ClientDetail
		select EmailId,CONVERT(VARCHAR,Passwd) Passwd,Designation Desig, EmpId,EmpName from dbo.EmployeeDetail
		delete from EmployeeDetail where EmpId =2 
		delete from EmployeeDetail where EmpId =2 
		select EmailId,CONVERT(VARCHAR,Passwd) Passwd,'Client' Desig,ClientId,ClientName from dbo.ClientDetail
		uspLogin 'Harikm73@gmail.com','748596'
		update EmployeeDetail set Passwd=CONVERT(binary,'keran##########') where EmpId=2
*/
alter proc dbo.uspLogin
@EmailId varchar(50),
@Passwd varchar(15)
With Encryption
as	
	set @Passwd =@Passwd +REPLICATE('#',15-LEN(@Passwd ))
	
	
	If(select COUNT(*)
		from EmployeeDetail
		where EmailId  = @EmailId  and Passwd =convert(binary,@Passwd) and IsActive = 1 ) = 1
		begin 
				select EmailId,Passwd,Designation Desig, EmpId,EmpName,EmpImage  from dbo.EmployeeDetail 
				 where EmailId  = @EmailId and Passwd =convert(binary,@Passwd) and IsActive=1
		end
		Else If(select COUNT(*)
		from ClientDetail
		where EmailId  = @EmailId and Passwd =convert(binary,@Passwd) and IsActive = 1 ) = 1
		
		begin 
			select EmailId,Passwd,'Client' Desig,ClientId,ClientName,ClientImage  from dbo.ClientDetail
			where EmailId  = @EmailId and Passwd =convert(binary,@Passwd) and IsActive=1
		end
		