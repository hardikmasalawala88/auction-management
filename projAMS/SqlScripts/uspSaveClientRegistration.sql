use dbAMS 
go
/*
	Select * from dbo.EmployeeDetail order by IsActive
	uspSaveClientRegistration 0,'Keran Joshi','Datt nagar','7485128964edjhvwehdf','kern@gm.com',jas74185
	update ClientDetail set IsActive=1 where ClientId=1
	delete  from dbo.ClientDetail 
	select EmailId,CONVERT(VARCHAR,Passwd) Passwd,'Client' Desig,ClientId,ClientName from dbo.ClientDetail
	select * from dbo.ClientDetail
*/

--
alter proc dbo.uspSaveClientRegistration
@ClientId int,
@ClientName varchar(100),
@ClientAddr	varchar(100),
@ContactNo	varchar(15),
@EmailId	varchar(50),
@Passwd varchar(15) 
with Encryption
as
declare @MaxClientId int ,@ErrNo tinyint , @TransName varchar(30)
	set @passwd=@passwd+REPLICATE('#',15-LEN(@passwd))
	set @TransName = 'SaveClientRegistration'
	Begin Tran @TransName
		set @MaxClientId  = @ClientId 		
		--if(select COUNT(*) from dbo.ClientDetail
		--	where ClientId = @ClientId   ) > 0
		--begin
		--	update dbo.ClientDetail
		--		set
		--		ClientName =@ClientName,
		--		ClientAddr=@ClientAddr,
		--		ContactNo=@ContactNo,
		--		EmailId=@EmailId
		--		where ClientId =@ClientId
		--		set @ErrNo = @@ERROR --return error number of last transact-sql
		--	if @ErrNo <> 0 goto Err 	
				
		--end
		--else
		begin
			--start auto incrment
			select @MaxClientId  = ISNULL(MAX (ClientId),0)+1 from dbo.ClientDetail
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.ClientDetail (ClientId,ClientName,ClientAddr,ContactNo,EmailId,Passwd,IsActive)
			select @MaxClientId ,@ClientName,@ClientAddr,@ContactNo ,@EmailId,CONVERT(BINARY,@Passwd),1   
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxClientId  ClientId
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

