/*
uspDefaultCurrent
*/



use dbAMS
go


alter proc dbo.uspDefaultCurrent
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetProductFullDetail'
Begin Tran @TranName

select top 4 pm.*,DATEDIFF(DAY,GETDATE(),aid.EndDt )as RDay,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt ) + ' ' + CONVERT(varchar(10), aid.EndTime )),0),108) as RTime 
,CONVERT(varchar(10),aid.StartDt,105) as StartDt,CONVERT(varchar(10),aid.EndDt,105)  as EndDt,aid.StartTime,aid.EndTime,aid.BuyNowAmt
from ProductMst pm 
	join AuctionTypeItemDetail  aid on aid.AuctionTypeId=pm.AuctionTypeId and 
	aid.CategoryId=pm.CategoryId and
	aid.ProductId=pm.ProductId
	where DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
   									
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
	