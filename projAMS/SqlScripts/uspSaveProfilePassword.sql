/*
select EmailId,CONVERT(VARCHAR,Passwd) Passwd,Designation Desig, EmpId,EmpName from dbo.EmployeeDetail
select EmailId,CONVERT(VARCHAR,Passwd) Passwd,'Client' Desig,ClientId,ClientName from dbo.ClientDetail
uspSaveProfilePassword '1','son','soni1'
soni5452MN#####
*/
use dbAMS 
go

alter procedure dbo.uspSaveProfilePassword
@Id varchar(50),
@Passwd	 varchar(15),
@NewPasswd varchar(15)
with encryption
as
declare @TranName varchar(20), @Errno int
	set @TranName ='uspProfilePassword'
	Begin Tran @TranName
		set @Passwd =@Passwd +REPLICATE('#',15-LEN(@Passwd ))
		set @NewPasswd =@NewPasswd +REPLICATE('#',15-LEN(@NewPasswd ))
		
		
		if(select COUNT (*)  
		from dbo.EmployeeDetail  
		where EmpId like @Id and Passwd = convert(binary,@Passwd) )=1
		begin 
				update dbo.EmployeeDetail 
				set Passwd= convert(binary,@NewPasswd)
				where EmpId like @Id and Passwd = convert(binary,@Passwd)
				select 1 
		end
		Else If(select COUNT (*)  
		from dbo.ClientDetail  where ClientId like @Id and Passwd = convert(binary,@Passwd) ) = 1
		
		begin 
			update dbo.ClientDetail 
				set Passwd=convert(binary,@NewPasswd)
				where ClientId like @Id and Passwd = convert(binary,@Passwd)
				select 1
		end
		else
		Begin
			select 0
		End
		set @Errno =@@ERROR
		if @Errno <>0 goto Err
		
		
	
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
	
	
	