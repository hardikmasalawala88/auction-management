use dbAMS
go

alter proc uspChangeThePasswd
@emailId varchar(50),
@passwd varchar(15),
@newPasswd varchar(15)
With Encryption
as	
	set @passwd=@passwd+REPLICATE('#',15-LEN(@passwd))		
	set @newPasswd=@newPasswd+REPLICATE('#',15-LEN(@newPasswd))		
	print @newPasswd
	update EmployeeDetail
		set passwd=CONVERT(binary,@newPasswd)
	 	where emailId=@emailId and passwd=convert(binary,@passwd)

	select @emailId
	goto success
success:
--	@passwd="System######"
--	set @newPasswd=@newPasswd+REPLICATE('#',12-LEN(@newPasswd))		
--	@newpasswd="soft.net"+REPLICATE('#',12-LEN("soft.net"))		
--	@newpasswd="soft.net####"
