use dbAMS 
go
/*
uspGetClientPacketPurchaseDetail 1
*/
alter proc dbo.uspGetClientPacketPurchaseDetail
@ClientId int
with Encryption
as
--Show Packet Purchase Detail
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetClientPacketPurchaseDetail'
Begin Tran @TranName
 

Select row_number() over (order by PurchDt) 'No.',
	Convert(Varchar(20),PurchDt,6) as PurchaseDate,
	TotalBid as Total,
	RemaingBid as Remaining,
	Amount as Price 
	from PackagePurchase
	where  ClientId=@ClientId
	--Show Packet Purchase Detail
--select *  From  PackagePurchase

	
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0