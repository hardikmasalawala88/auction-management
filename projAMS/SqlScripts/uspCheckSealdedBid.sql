use dbAMS 
go
/*
		select * from ProductMst
		select * from CategoryTypeMst
		select * from CategoryMst
		select * from ProductMainDetail		
		DELETE  FROM ProductMst
		DELETE  FROM ProductMainDetail
		select * from AuctionTypeMst
		select * from AuctionTypeItemDetail
		delete from BidDetail where BidId=2
		select * from BidDetail
uspCheckSealdedBid 1,1,1
*/



alter proc dbo.uspCheckSealdedBid
@CategoryId tinyint,
@ProductId smallint,
@ClientId	int
--@BidAmt	int,
--@IsBuyNowAmt	bit,
--@BuyQty	tinyint
--@IsApproved	bit
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'CheckSealdedBid'
	Begin Tran @TransName
		--set @MaxBidId = @BidId 
		
		if(select COUNT(*) from dbo.BidDetail
			where  ClientId=@ClientId and ProductId = @ProductId  and CategoryId =@CategoryId) > 0
		begin
		
				select ClientId ,ProductId,CategoryId 
				from BidDetail 
				where  ClientId=@ClientId and ProductId = @ProductId  and CategoryId =@CategoryId
			
				set @ErrNo = @@ERROR --return error number of last transact-sql
				if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			select '0' clientId ,'0' ProductId,'0' CategoryId 
			
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
		end	
		--when display alias name
	--select @CategoryId CategoryId , @ProductId ProductId,@ClientId ClientId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	