use dbAMS
go
/*
		select * from PackagePurchase
		delete from PackagePurchase where PackagePurchaseId=2
	uspSavePackagePurchase	0,'Maestro Card','Bank Of Baroda','748596as8s85','741258963221',1,1
*/

--
Alter proc dbo.uspSavePackagePurchase
@PackagePurchaseId smallint,
@PaymentType	varchar(20),
@BankName	varchar(50),
@BankAc		varchar(50),
@CardNo		varchar(50),
@ClientId  int ,
@PacketId tinyint
with Encryption
as
declare @MaxPackagePurchaseId smallint ,@ErrNo tinyint , @TransName varchar(30),
			@TotalBid	smallint,@RemaingBid	smallint,@Amount Smallint
	set @TransName = 'SavePackagePurchase'
	Begin Tran @TransName
		begin
			--start auto incrment
			select @MaxPackagePurchaseId  = ISNULL(MAX(PackagePurchaseId),0)+1 from dbo.PackagePurchase
			select @TotalBid=(select Bids from PacketDetail where PacketId=@PacketId)
			select @Amount = (select Price from PacketDetail where PacketId=@PacketId)
			set @RemaingBid=@TotalBid
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.PackagePurchase (PackagePurchaseId,PurchDt,TotalBid,
							RemaingBid,PaymentType,BankName,BankAc,Amount,CardNo,ClientId,PacketId)
			select @MaxPackagePurchaseId ,GETDATE(),@TotalBid,
							@RemaingBid,@PaymentType,@BankName,@BankAc,@Amount,@CardNo,@ClientId,@PacketId
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @MaxPackagePurchaseId  PackagePurchaseId
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

