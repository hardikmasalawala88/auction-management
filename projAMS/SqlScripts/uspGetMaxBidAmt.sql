use dbAMS 
go
/*
uspGetMaxBidAmt 15
select * from BidDetail

select ClientId,Max(BidAmt)as BidAmt from dbo.BidDetail where ProductId=1 Group By clientId

*/

alter proc dbo.uspGetMaxBidAmt
@ProductId smallint
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'GetMaxBidAmt'
	Begin Tran @TransName
		
		
		if(select COUNT(*) from dbo.BidDetail
			where ProductId=@ProductId ) > 0
		begin
			select ClientId,isnull(Max(BidAmt),0) as BidAmt 
			from dbo.BidDetail 
			where ProductId=@ProductId 
			Group By clientId 
			order by BidAmt desc
			set @ErrNo = @@ERROR 
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			select 0 BidAmt
		end
		--when display alias name
	
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	