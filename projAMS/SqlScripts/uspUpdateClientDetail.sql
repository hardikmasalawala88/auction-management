use dbAMS 
go
/*
	Select * from dbo.EmployeeDetail order by IsActive
	uspSaveClientRegistration 0,'Keran Joshi','Datt nagar','7485128964edjhvwehdf','kern@gm.com',jas74185
	update ClientDetail set IsActive=1 where ClientId=1
	delete  from dbo.ClientDetail 
	select EmailId,CONVERT(VARCHAR,Passwd) Passwd,'Client' Desig,ClientId,ClientName from dbo.ClientDetail
	select * from dbo.ClientDetail
*/

--
create proc dbo.uspUpdateClientDetail
@ClientId int,
@ClientName varchar(100),
@ClientAddr	varchar(100),
@Gender	bit,
@ContactNo	varchar(15),
@ShipAddr varchar(50)
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'UpdateClientDetail'
	Begin Tran @TransName
		
		if(select COUNT(*) from dbo.ClientDetail
			where ClientId = @ClientId   ) > 0
		begin
			update dbo.ClientDetail
				set
				ClientName =@ClientName,
				ClientAddr=@ClientAddr,
				ContactNo=@ContactNo,
				Gender=@Gender,
				ShippingAddr=@ShipAddr
				where ClientId =@ClientId
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		--when display alias name
	select @ClientId  ClientId
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

