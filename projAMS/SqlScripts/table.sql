use dbAMS 
go

--1.CategoryTypeMst
create table dbo.CategoryTypeMst 
(
	CategoryTypeId tinyint primary key,
	CategoryType varchar(30)not null
)

--2.CategoryMst
create table dbo.CategoryMst 
(
	CategoryTypeId tinyint references dbo.CategoryTypeMst(CategoryTypeId),
	CategoryId tinyint primary key,
	Category varchar(30) not null
)

--5.AuctionTypeMst

create table dbo.AuctionTypeMst
(
	AuctionTypeId tinyint primary key,
	AuctionTypeName varchar(30) not null,
	AuctionTypeDesc varchar(500)
)

--11.EmployeeDetail
create table dbo.EmployeeDetail
(
EmpId	smallint primary key,
EmpName	varchar(50) not null,
Gender	bit,
Addr	varchar(100),
ContactNo	varchar(15),
Dob	date,
Doj	date,
Designation	varchar(15),
EmpImage varchar(100),
EmailId	varchar(50) unique,
Passwd	Binary(15),
IsActive bit,
LeaveDt	date,
LoginDate date,
IsOnline bit
)

--9.ClientDetail
create table dbo.ClientDetail
(
ClientId int primary key,
ClientName varchar(100) not null,
Gender	bit,
ClientAddr	varchar(100),
ShippingAddr varchar(100),
ContactNo	varchar(15),
EmailId	varchar(50)unique,
ClientImage	varchar(100),
Passwd	Binary(15),
IsActive bit,
IsOnline bit,
)

insert into dbo.CategoryTypeMst (CategoryTypeId,CategoryType)
select 1,'Mobiles'
insert into dbo.CategoryTypeMst (CategoryTypeId,CategoryType)
select 2,'Laptops'
 --drop table dbo.CategoryTypeMst
 
select * from dbo.CategoryTypeMst 


--8.PacketDetail 
create table dbo.PacketDetail 
(
	PacketId	tinyint primary key,
	PacketName	varchar(50) not null,
	Bids	smallint,
	Price	int,
	Times	tinyint,
	Duration varchar(20)

)
drop table PacketDetail

alter table dbo.PacketDetail alter column Duration varchar(20)


--3.ProductMst
create table dbo.ProductMst
(
	CategoryId tinyint ,
	ProductId smallint ,
	AuctionTypeId tinyint references dbo.AuctionTypeMst(AuctionTypeId),
	ClientId int references dbo.ClientDetail(ClientId),
	EmpId smallint references dbo.EmployeeDetail(EmpId),
	ProductName varchar(30),
	ProductDesc varchar(1000),
	Qty tinyint,
	Price int,
	AdditionalInfo varchar(1000),
	Warranty varchar(100),
	FeaturesAndBenefits varchar(1000),
	ProductCondition varchar(10),
	Image1 varchar(100),
	Image2 varchar(100),
	Image3 varchar(100),
	Image4 varchar(100),
	ShippingCharge smallint,
	ShippingDesc varchar(200),
	IsApproved bit,
	IsConfirmed bit
	primary key (CategoryId,ProductId)
)
drop table ProductMst
drop table ProductMainDetail


--4.ProductMainDetail

create table dbo.ProductMainDetail
(
	CategoryId tinyint,
	ProductId smallint ,
	CompanyName varchar(50),
	ModelNo varchar(50),
	Color varchar(20),
	OS varchar(20),
	Processor varchar(20),
	OSversion varchar(20),
	RAM varchar(20),
	Sizeinch varchar(20),
	Resolution varchar(20),
	PrimaryCamera varchar(10),
	SecondaryCamera varchar(10),
	ScreenType varchar(30), 
	Connectivity varchar(30),
	Storage varchar(50),
	Speaker varchar(50),
	Battery varchar(50),
	Image1 varchar(200),
	Image2 varchar(200),
	Image3 varchar(200),
	Image4 varchar(200),
	Video varchar(200),
	foreign key (CategoryId,ProductId) references ProductMst(CategoryId,ProductId)
)
drop table ProductMainDetail


--6.AuctionTypeItemDetail

create table dbo.AuctionTypeItemDetail
(
	AuctionTypeId tinyint references AuctionTypeMst(AuctionTypeId),
	CategoryId tinyint,
	ProductId smallint,
	MinAmt smallmoney,
	StartDt date,
	EndDt date,
	StartTime time,
	EndTime time,
	BuyNowAmt money,
	BidCnt int,
	CharityACBank varchar(50),
	CharityACName varchar(100),
	CharityACNO varchar(50),
	BidMulti tinyint,
	foreign key (CategoryId,ProductId) references ProductMst(CategoryId,ProductId)
)	

--7.BidDetail
create table dbo.BidDetail
(
	BidId smallint ,
	CategoryId tinyint,
	ProductId smallint,
	BidDate date,
	BidTime time,
	BidAmt	int,
	IsBuyNowAmt	bit,
	BuyQty	tinyint,
	IsCancel	bit,
	foreign key (CategoryId,ProductId) references dbo.ProductMst(CategoryId,ProductId),
	ClientId int references dbo.ClientDetail(ClientId),
	PRIMARY KEY (CategoryId,ProductId,BidId)
)
drop table BidDetail
--10.PackagePurchase
create table dbo.PackagePurchase
(
	PackagePurchaseId smallint primary key,
	PurchDt		date,
	TotalBid	smallint,
	RemaingBid	smallint,
	PaymentType	varchar(20),
	BankName	varchar(50),
	BankAc		varchar(50),
	Amount		smallint,
	CardNo		varchar(50),
	ClientId  int references  dbo.ClientDetail(ClientId),
	PacketId tinyint references dbo.PacketDetail(PacketId)
)

--12.AutoBid
create table dbo.AutoBid
(
	CategoryId tinyint,
	ProductId	smallint,
	MaxBidAmt money,
	IncreaseBidAmt smallmoney,
	BidFreq	time,
	foreign key(CategoryId ,ProductId) references dbo.ProductMst(CategoryId ,ProductId),
	ClientId int references	dbo.ClientDetail(ClientId),
	primary key (CategoryId,ProductId,ClientId)	
)
	
--13.PaymentDetail
create table dbo.PaymentDetail
(
	CategoryId tinyint,
	ProductId	smallint,
	ClientId	int,
	PaymentId int primary key,
	PaymentType		varchar(20),
	BankName		varchar(50),
	BankAc			varchar(50),
	Amount			money,
	CardNo			varchar(20),
	IsShipping		bit,
	IsProductSend	bit,
	IsReturnPay		bit,
	foreign key(CategoryId,ProductId,ClientId) references dbo.AutoBid(CategoryId,ProductId,ClientId)
)

--17. WishlistDetail
create table dbo.WishlistDetail
(
	WishlistId smallint primary key,
	CategoryId tinyint,
	ProductId smallint,
	ClientId int references dbo.ClientDetail(ClientId),
	Wdate date,
	IsRemove bit,
	foreign key (CategoryId,ProductId) references dbo.ProductMst(CategoryId,ProductId),
)
drop table WishlistDetail

--15.ReturnDetail
create table dbo.ReturnDetail
(
	ReturnId smallint primary key,
	CategoryId tinyint,
	ProductId smallint,
	ReturnRemark varchar(100),
	IsReturnPayment bit,
	IsReplace	bit,
	IsReturn bit,
	Comment	varchar(500),
	foreign key (CategoryId,ProductId) references dbo.ProductMst(CategoryId,ProductId),
	ClientId int references dbo.ClientDetail(ClientId)
)

--16.ReturnPaymentDetail
create table dbo.ReturnPaymentDetail
(
	ReturnPayId smallint primary key,
	ReturnDt date,
	BankName varchar(50),
	BankAc	 varchar(50),	
	Amount	 money,
	ChequeNo varchar(15),
	ReturnId smallint references dbo.ReturnDetail(ReturnId)
)

--14.ShippingDetail
create table dbo.ShippingDetail
( 
	ShipId int primary key,
	CategoryId tinyint,
	ProductId	smallint,
	ShipDt date,
	ShippingStatus varchar(30),
	ShippingDetail varchar(100),
	EmpId	smallint references dbo.EmployeeDetail(EmpId),
	ClientId int references dbo.ClientDetail(ClientId),
	foreign key (CategoryId,ProductId) references dbo.ProductMst(CategoryId,ProductId)
)		
	









