use dbAMS 
go
/*
			select  ISNULL(MAX (BidId),0)+1 from dbo.BidDetail 
			where ProductId = 2 and CategoryId =2
		select * from ProductMst
		select * from ProductMainDetail		
		DELETE  FROM ProductMst
		DELETE  FROM ProductMainDetail
		select * from AuctionTypeMst
		select * from AuctionTypeItemDetail
		delete from BidDetail where BidId=2
		select * from BidDetail
		uspSaveBidDetail 0,1,1,1,2000,False,5
		@BidId|SINT|0|@CategoryId|TINT|1|@ProductId|SINT|1|@Client|INT|1|@BidAmt|INT|20000|@IsBuyNowAmt|BIT|False|@BuyQty|TINT|5
*/



alter proc dbo.uspSaveBuyNowDetail
@BidId smallint,
@CategoryId tinyint,
@ProductId smallint,
@ClientId	int,
@IsBuyNowAmt	bit,
@BuyQty	tinyint
--@IsApproved	bit
with Encryption
as
declare @MaxBidId tinyint ,@ErrNo tinyint , @TransName varchar(30),
@BidDate date,@BidTime varchar(8),@BidCnt int
	set @TransName = 'SaveBuyNowDetail'
	Begin Tran @TransName
		set @MaxBidId = @BidId 
		set @BidDate= CONVERT(varchar(10),GETDATE(),20)
		set	@BidTime= CONVERT(varchar(8),GETDATE(),108)
		
		if(select COUNT(*) from dbo.BidDetail
			where  BidId = @BidId and ProductId = @ProductId and CategoryId =@CategoryId) > 0
		begin
		
			--update dbo.BidDetail 
		
			--	set
					
			--	where ProductId=@ProductId and BidId = @BidId
						
			set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from ClientDetail
			select @MaxBidId = ISNULL(MAX (BidId),0)+1 from dbo.BidDetail where ProductId = @ProductId and CategoryId =@CategoryId
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.BidDetail(BidId,CategoryId,ProductId,ClientId,BidDate,BidTime,IsBuyNowAmt,BuyQty)
			select @MaxBidId,@CategoryId,@ProductId,@ClientId,@BidDate,@BidTime,@IsBuyNowAmt,@BuyQty
			
				
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
		end	
		--when display alias name
	select @CategoryId CategoryId , @ProductId ProductId,@MaxBidId BidId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	