use dbAMS 
go
/*
		uspGetCurrentWinner 1

*/

alter proc dbo.uspGetCurrentWinner
@ClientId int
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'GetCurrentWinner'
	Begin Tran @TransName
		
		
		if(select COUNT(*) from dbo.ClientDetail
			where ClientId=@ClientId ) > 0
		begin
			select ClientName,ClientImage,ClientId from dbo.ClientDetail where ClientId=@ClientId
			set @ErrNo = @@ERROR 
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from ClientDetail
			select 'No One Bid ' ClientName,'null' ClientImage,'null'  ClientId
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
		end	
		--when display alias name
	
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	