use dbAMS
go
/*
uspsaveEmpConvertPasswd 1,'Hari1234As'
select * from dbo.EmployeeDetail
*/
alter proc dbo.uspsaveEmpConvertPasswd
@EmpId SmallInt,
@Passwd varchar(15)
with encryption
as
declare @TranName varchar(20), @Errno int
	set @TranName ='SaveEmpConvertPasswd'
	set @passwd=@passwd+REPLICATE('#',15-LEN(@passwd))

	Begin Tran @TranName
		
			
			
			update dbo.EmployeeDetail
				set Passwd=CONVERT(binary,@Passwd)
				where EmpId=@EmpId
	
		
	Select @Passwd Passwd,@EmpId EmpId
		set @Errno =@@ERROR
		if @Errno <>0 goto Err
		
		
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
