use dbAMS 
go
/*
	select * from AutoBid
	select * 
	@CategoryId|TINT|3|@productId|INT|15|@ClientId|INT|2|@MAmount|MNY|55000|@IncreaseBidAmt|SMNY|500|@BidFreq|VCHR|20
uspSetAutoBid 3,14,1,55000,500,'00:00:40'
*/

alter proc dbo.uspSetAutoBid
@CategoryId tinyint,
@ProductId int,
@ClientId int,
@MAmount money,
@IncreaseBidAmt smallmoney,
@BidFreq varchar(8)
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
set @TransName = 'AutoBid'
Begin Tran @TransName

	if(select COUNT(*) from dbo.AutoBid
		where CategoryId = @CategoryId and ProductId=@ProductId and ClientId=@ClientId  ) > 0
	begin
		update dbo.AutoBid
			set MaxBidAmt=@MAmount,
				IncreaseBidAmt=@IncreaseBidAmt,
				BidFreq=Convert(varchar(8),@BidFreq)					
		where CategoryId = @CategoryId and ProductId=@ProductId and ClientId=@ClientId
	
		set @ErrNo = @@ERROR 
		if @ErrNo <> 0 goto Err 	
			
	end
	else
	begin
			--insert start
		insert into dbo.AutoBid(CategoryId,ProductId,ClientId,MaxBidAmt,IncreaseBidAmt,BidFreq)
		select @CategoryId,@ProductId,@ClientId,@MAmount,@IncreaseBidAmt,Convert(varchar(8),@BidFreq)						
		 
		set @ErrNo = @@ERROR 		
		if @ErrNo <> 0 goto Err 	
		
	end	
	--when display alias name
select @CategoryId CategoryId,@productId ProductId,@ClientId ClientId,@MAmount MaxBidAmt,@IncreaseBidAmt IncreaseBidAmt,@BidFreq BidFreq

goto success
Err:
rollback tran @TransName
return @ErrNo
Success:
Commit tran @TransName
return 0
