use dbAMS
go

/*
	uspUpdatePassword 'kern@gm.com','kem123'
	select * from ClientDetail
*/
alter proc uspUpdatePassword
@EmailId varchar(50),
@Passwd varchar(15)

With Encryption
as	
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveChangePassword'
	Begin Tran @TransName
	set @Passwd =@Passwd +REPLICATE('#',15-LEN(@Passwd ))		
	--set @newPasswd=@newPasswd+REPLICATE('#',15-LEN(@newPasswd))		
	--print @newPasswd
	print @Passwd
	--	update EmployeeDetail
	--	set passwd=CONVERT(binary,@newPasswd)
	-- 	where emailId=@emailId and passwd=convert(binary,@passwd)
	 	
	 	------------------------------------------------------------
	If(select COUNT(*)
		from EmployeeDetail
		where EmailId  like @EmailId  and IsActive = 1 ) = 1 --This Query for check record is only one
		
		begin 
				update EmployeeDetail
				set Passwd=CONVERT(binary,@passwd)
	 			where EmailId =@emailId  
	 	
			--select EmailId,Passwd,Designation Desig, EmpId,EmpName from dbo.EmployeeDetail 
			-- where emailId like @emailId and Passwd =convert(binary,@Passwd) and IsActive=1
		end
		Else If(select COUNT(*)
		from ClientDetail
		where EmailId  like @EmailId  and IsActive = 1) = 1
		begin
				update ClientDetail
				set Passwd=CONVERT(binary,@Passwd )
	 			where EmailId =@EmailId  
	 	 
			--select EmailId,Passwd,'Client' Desig,ClientId,ClientName from dbo.ClientDetail
			--where emailId like @emailId and Passwd =convert(binary,@Passwd) and IsActive=1
		end
	
		------------------------------------------------------------
	select @EmailId ,@Passwd 
	goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
