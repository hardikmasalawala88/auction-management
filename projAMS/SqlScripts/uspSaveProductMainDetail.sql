use dbAMS 
go
/*
		
		uspSaveProductMainDetail 4,1,'htc','htc One M9',
							'Santorini White','Android','Snapdragon 810 Octa Core','Android 5.1','3 GB',5,
							5000,13,13,'IPS Display','Connective BKJDJ','32 gb inbuilt','35mm jack','2900MaH',
							'dfdsfs1.jpg','dfdsfs2.jpg','dfdsfs3.jpg',
							'dfdsfs4.jpg','Pickup.mp4'
		select * from ProductMst
		select * from ProductMainDetail
		select * from AuctionTypeItemDetail
		select * from EmployeeDetail
		delete from ProductMst
		delete from ProductMainDetail
		delete from AuctionTypeItemDetail	
		
*/

--create proc dbo.uspsaveCategoryMst

alter proc dbo.uspSaveProductMainDetail
	@CategoryId tinyint,
	@ProductId smallint ,
	@CompanyName varchar(50),
	@ModelNo varchar(50),
	@Color varchar(20),
	@OS varchar(20),
	@Processor varchar(20),
	@OSversion varchar(20),
	@RAM varchar(20),
	@Sizeinch varchar(20),
	@Resolution varchar(20),
	@PrimaryCamera varchar(10),
	@SecondaryCamera varchar(10),
	@ScreenType varchar(30), 
	@Connectivity varchar(30),
	@Storage varchar(50),
	@Speaker varchar(50),
	@Battery varchar(50),
	@Image1 varchar(200),
	@Image2 varchar(200),
	@Image3 varchar(200),
	@Image4 varchar(200),
	@Video varchar(200)
	with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SaveProdutMainDetail'
	Begin Tran @TransName
		
		
		if(select COUNT(*) from dbo.ProductMainDetail 
			where  ProductId = @ProductId and CategoryId = @CategoryId ) > 0
		begin
		
			update dbo.ProductMainDetail 
		
				set
					CategoryId=@CategoryId,
					ProductId=@ProductId,
					CompanyName=@CompanyName,
					ModelNo=@ModelNo,
					Color=@Color,
					OS=@OS,
					Processor=@Processor,
					OSversion=@OSversion,
					RAM=@RAM,
					Sizeinch=@Sizeinch,
					Resolution=@Resolution,
					PrimaryCamera=@PrimaryCamera,
					SecondaryCamera=@SecondaryCamera,
					ScreenType=@ScreenType,
					Connectivity=@Connectivity,
					Storage=@Storage,
					Speaker=@Speaker,
					Battery=@Battery,
					Image1=@Image1,
					Image2=@Image2,
					Image3=@Image3,
					Image4=@Image4,
					Video=@Video
			where ProductId=@ProductId and CategoryId = @CategoryId
						
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from ClientDetail
				--insert start
			insert into dbo.ProductMainDetail(CategoryId,ProductId,CompanyName,ModelNo,Color,OS,Processor,
											OSversion,RAM,Sizeinch,Resolution,PrimaryCamera,SecondaryCamera,
											ScreenType,Connectivity,Storage,Speaker,Battery,
											Image1,Image2,Image3,Image4,Video)
			select @CategoryId,@ProductId,@CompanyName,@ModelNo,@Color,@OS,@Processor,
											@OSversion,@RAM,@Sizeinch,@Resolution,@PrimaryCamera,@SecondaryCamera,
											@ScreenType,@Connectivity,@Storage,@Speaker,@Battery,
											@Image1,@Image2,@Image3,@Image4,@Video
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
			
		end	
		--when display alias name
	select @CategoryId CategoryId , @ProductId ProductId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	