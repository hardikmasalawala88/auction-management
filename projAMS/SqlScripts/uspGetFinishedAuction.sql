use dbAMS
go

alter proc dbo.uspGetFinishedAuction
with encryption
as
Declare @TranName varchar(20) ,@Errno int
set @TranName = 'GetFinishedAuction'
Begin Tran @TranName

select top 4  pm.ProductName,am.AuctionTypeName,aid.EndDt,CONVERT(varchar(8),aid.EndTime) as EndTime,
aid.CategoryId,aid.ProductId,aid.AuctionTypeId,aid.BuyNowAmt,pm.Image1,cd.ClientName,cd.ClientImage,bd.BidAmt
from ProductMst pm 
join AuctionTypeItemDetail aid 
on aid.AuctionTypeId=pm.AuctionTypeId and 
aid.CategoryId=pm.CategoryId and 
aid.ProductId=pm.ProductId 
join AuctionTypeMst am on am.AuctionTypeId=aid.AuctionTypeId
join BidDetail bd on bd.ProductId=pm.ProductId
join ClientDetail cd on cd.ClientId=bd.ClientId
where DATEDIFF(DAY,aid.EndDt,GETDATE()) >=0  and
 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) <=0
 and bd.BidDate=(Select top 1  BidDate from BidDetail where productId=pm.ProductId order by BidTime,BidDate desc)
 and bd.BidTime=(Select top 1  BidTime from BidDetail where productId=pm.ProductId order by BidTime,BidDate desc)
 and cd.ClientId=(Select top 1  ClientId from BidDetail where productId=pm.ProductId order by BidTime,BidDate desc)
and bd.BidId=(Select top 1  BidId from BidDetail where productId=pm.ProductId order by BidTime,BidDate desc)
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
/*	
	DECLARE @StartTime DATETIME = getdate()  --'2011-09-23 15:00:00'
       --,@EndTime   DATETIME = '2011-09-23 17:54:02'
       ,@EndTime   DATETIME = '2014-09-23 17:54:02'

SELECT CONVERT(VARCHAR(8), DATEADD(SECOND, DATEDIFF(SECOND,@StartTime, @EndTime),0), 108) as ElapsedTime
SELECT  DATEDIFF(DAY,@EndTime,@StartTime )

CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as t1,
CONVERT(VARCHAR(20), DATEADD(SECOND, DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime)),0),108) as t2,

select aid.CategoryId,aid.ProductId,aid.AuctionTypeId,CONVERT(varchar(10),aid.StartDt,105)  as StartDt,CONVERT(varchar(10),aid.EndDt,105)  as EndDt,
aid.StartTime,aid.EndTime,aid.BuyNowAmt from ProductMst pm 
	join AuctionTypeItemDetail aid on aid.AuctionTypeId=pm.AuctionTypeId and 
		aid.CategoryId=pm.CategoryId and aid.ProductId=pm.ProductId 									
											
											--where DATEDIFF(DAY,aid.StartDt,aid.EndDt) >0 and DATEDIFF(DAY,GETDATE(),aid.StartDt)>0
*/