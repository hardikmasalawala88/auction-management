use dbAMS 
go
/*
		uspAutoBid 14,1
		select * from AutoBid
		select * from BidDetail where ClientId=2 order By BidTime,BidDate,BidAmt 
		delete from BidDetail where BidId>=3
		select * from BidDetail 
		uspAutoBid
				select * from PackagePurchase

		select * From AuctionTypeItemDetail
		select * From ProductMst
		update ProductMst
		select * From AuctionTypeItemDetail

select * from #tbl
*/

alter proc dbo.uspAutoBid
with Encryption
as
declare @ErrNo tinyint , @TransName varchar(30),
		@MaxProductBidAmt money,@ClientAffortableAmt money,
		@ClientIncreaseAmt money,@FetchCategoryId varchar(2),@FetchAuctionTypeId tinyint,
		@maxClientId Int,@MaxBidId int,@FetchClientId int,@FetchProductId smallint,@Cnt int,@inc int,
		@BidFreq date,@BidFreq1 date,@LBidTime date,@RemBid smallint,@BidMulti tinyint,
		@latestPackgePurchseDate date,@BidCnt int,@Duration smallint,@Days smallint,@packetId Tinyint,@Rdays smallint
	set @inc=1
	set @TransName = 'AutoBid'
	Begin Tran @TransName
		--If Tmp table has data then deleted
		IF OBJECT_ID ('temp.dbo.#tbl') IS NOT NULL
		DROP TABLE #tbl;
		--Identity used for genrate incremented id for temp table
		select IDENTITY(int,1,1) cnt,ab.ProductId,ab.ClientId 
							into #tbl
							from AutoBid  ab join AuctionTypeItemDetail aid 
							on aid.ProductId=ab.ProductId and 
							aid.CategoryId=ab.CategoryId 
							join AuctionTypeMst am on am.AuctionTypeId=aid.AuctionTypeId
							where DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
							 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
							 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
							 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
	
		select @Cnt=(select COUNT(*) from #tbl)--How many data in temp table
		--1 by 1 data fetch from temp table 
		while(@inc <= @Cnt )
		Begin	
				select 	@FetchProductId = (select productId from #tbl where cnt=@inc)	
					--print @FetchProductId
				select 	@FetchClientId = (select ClientId from #tbl where cnt=@inc)	   
					--print @FetchClientId
				select @MaxProductBidAmt = (select  Isnull(Max(BidAmt),0) from BidDetail where ProductId =@FetchProductId  )
					--print @MaxProductBidAmt
				select @maxClientId = (select ClientId  from BidDetail where ProductId =@FetchProductId  and BidAmt =@MaxProductBidAmt)
					--print @maxClientId
				select @FetchCategoryId=(select CategoryId  from ProductMst where ProductId = @FetchProductId)
				select @FetchAuctionTypeId=(select AuctionTypeId  from ProductMst where ProductId =@FetchProductId)
				if(@maxClientId != @FetchClientId )--Same client max bid check
				begin
							 --any auto bid set on this product for specified client
							if(select COUNT(*) from dbo.AutoBid
								where ClientId=@FetchClientId and ProductId=@FetchProductId ) > 0
							begin
									set @Rdays=0
									-- Total day return
									select @Duration=( select top 1 Duration =
										  CASE Duration
											 WHEN 'Day' THEN 1
											 WHEN 'Week' THEN 7
											 WHEN 'Month' THEN 30
											 WHEN 'Year' THEN 365
											 ELSE 0
										  END
									from PackagePurchase pp join PacketDetail pd on pd.PacketId=pp.PacketId
									where pp.ClientId=@FetchClientId
									order by PurchDt Desc)
									--His packet id return
									select @packetId=( select top 1 pp.PacketId
									from PackagePurchase pp join PacketDetail pd on pd.PacketId=pp.PacketId
									where pp.ClientId=@FetchClientId
									order by PurchDt Desc) 
									--How long packet exists 
									Select @Days= (select @Duration*Times 
									from PacketDetail where PacketId=@packetId)

									--Remaining days get

									set @Rdays=(select @Days-DATEDIFF(DAY,PurchDt,GETDATE()) as Rday from PackagePurchase where ClientId=@FetchClientId and @Days-DATEDIFF(DAY,PurchDt,GETDATE())>0)
									if(@Rdays >0 )
									Begin						--select top 1 Convert(Varchar(10),BidDate)+' '+Convert(Varchar(10),BidTime,108) from BidDetail where ClientId=1 and ProductId=15 order By BidTime,BidDate,BidAmt
											set @LBidTime = (select top 1 Convert(Varchar(10),BidDate)+' '+Convert(Varchar(10),BidTime,108) from BidDetail where ClientId=@FetchClientId and ProductId=@FetchProductId order By BidTime,BidDate,BidAmt )
											set @BidFreq =(select Convert(Varchar(10),GETDATE(),20)+' '+Convert(Varchar(10),BidFreq,108) from AutoBid where ClientId= @FetchClientId  and ProductId=@FetchProductId )
											set @BidFreq1 =(select Convert(Varchar(10),GETDATE(),20)+' 00:00:00' from AutoBid where ClientId= @FetchClientId  and ProductId=@FetchProductId )
										
											
											--at specific time check bidding of user who set auto bid
											if(DATEDIFF(second,@LBidTime,GETDATE()) >= DATEDIFF(second,@BidFreq1,@BidFreq))
											Begin
										
													
													select @ClientIncreaseAmt = (select IncreaseBidAmt from AutoBid where ClientId=@FetchClientId and ProductId=@FetchProductId)	
													select @ClientAffortableAmt= (select MaxBidAmt from AutoBid where ClientId=@FetchClientId and ProductId=@FetchProductId)	
												--is affortable for user or not
												if(@MaxProductBidAmt+@ClientIncreaseAmt <= @ClientAffortableAmt)
												Begin
													select @MaxBidId=ISNULL(Max(BidId),0)+1 from  BidDetail where ProductId = @FetchProductId and CategoryId =@FetchCategoryId
													--select 'AutoBid perform'
													--Check For Package Auction
													if(@FetchAuctionTypeId=5 or @FetchAuctionTypeId=6)
													Begin
															--Fetch Remaining Bid
															set @RemBid = (Select top 1 RemaingBid from PackagePurchase where ClientId=@FetchClientId order by PurchDt Desc)
															--Fetch Bid Multiply
															set @BidMulti = (select BidMulti from AuctionTypeItemDetail where CategoryId=@FetchCategoryId and ProductId=@FetchProductId and AuctionTypeId=@FetchAuctionTypeId)
															----Fetch Latest Purchase Packge Date
															set @latestPackgePurchseDate= (Select top 1 PurchDt from PackagePurchase where ClientId=@FetchClientId order by PurchDt Desc)
														if(@BidMulti<=@RemBid)
														Begin
																--Deduct Bid in Client Packge
																update PackagePurchase set RemaingBid=RemaingBid-@BidMulti where ClientId=@FetchClientId and PurchDt=@latestPackgePurchseDate
																--insert Bid in BidDetail table
																insert into BidDetail(CategoryId,ProductId,ClientId,BidId,BidDate,BidTime,BidAmt)
																select @FetchCategoryId,@FetchProductId,@FetchClientId,
																@MaxBidId,CONVERT(Date,GETDATE()),CONVERT(Time,GETDATE()),
																@MaxProductBidAmt+@ClientIncreaseAmt
																
																set @BidCnt= (select isnull(BidCnt,0)
																				from AuctionTypeItemDetail
																				where  CategoryId=@FetchCategoryId and ProductId=@FetchProductId)
																	update AuctionTypeItemDetail 
																	set BidCnt=@BidCnt+1 			
																	where CategoryId=@FetchProductId and ProductId=@FetchProductId
																	set @ErrNo = @@ERROR 		
																	if @ErrNo <> 0 goto Err 	
														End
														else
														Begin
																goto Increment
														End
													End	
												End	
								End
									End
									else
									Begin
										GoTo Increment
									End					
							End
				End
Increment:
				set @inc=@inc+1 
		End		
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
