use dbAMS 
go
/*
	Create proc dbo.uspSavePacketDetail
	delete from dbo.PacketDetail where PacketId=3
select * from PacketDetail
uspSavePacketDetail 0,'Promo',500,700,1,Week
uspSavePacketDetail 0,'Pack1',200,800,2,Day
uspSavePacketDetail 0,'Pack2',500,1500,1,Week
uspSavePacketDetail 0,'Pack3',1000,3000,10,Day
uspSavePacketDetail 0,'Pack4',2000,5600,20,Day
uspSavePacketDetail 0,'Pack5',5000,14000,3,Month
uspSavePacketDetail 0,'Pack6',10000,28000,1,Year
*/
alter proc dbo.uspSavePacketDetail
@PacketId tinyint,          --paramiters
@PacketName	varchar(50),		--paramiters
@Bids smallint,			--paramiters
@Price smallmoney,			--paramiters
@Times tinyint,			--paramiters
@Duration varchar(20)				--paramiters
with Encryption
as
declare @MaxPacketId tinyint ,@ErrNo tinyint , @TransName varchar(30)
	set @TransName = 'SavePacketDetail'
	Begin Tran @TransName
		set @MaxPacketId=@PacketId 
		
		if(select COUNT(*) from dbo.PacketDetail 
			where PacketId=@PacketId)>0
		begin
			update dbo.PacketDetail
				set
					PacketName=@PacketName, 
					Bids=@Bids,
					Price=@Price,
					Times=@Times,
					Duration=@Duration
				
				where PacketId=@PacketId 
				
				set @ErrNo=@@ERROR --return error number of last transact-sql
				
				if @ErrNo<>0 goto Err 			
		end
		else
		begin
			--start auto incrment
			select @MaxPacketId=ISNULL(MAX(PacketId),0)+1 from dbo.PacketDetail 
			set @ErrNo =@@ERROR 
			if @ErrNo<>0 goto Err 	
				--insert start
			insert into dbo.PacketDetail(PacketId,PacketName,Bids,Price,Times,Duration)
			select @MaxPacketId,@PacketName,@Bids,@Price,@Times,@Duration 
			set @ErrNo=@@ERROR 		
			if @ErrNo<>0 goto Err 	
			
		end	
		--when display alias name
	select @MaxPacketId PacketId,@PacketName PacketName,@Bids Bids,@Price Price,@Times Times,@Duration Duration 
		
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0

