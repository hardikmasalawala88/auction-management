use dbAMS 
go
/*
		
		uspSaveProductMst 4,0,1,'htc One M9',
							'Full Qualitative Camerawith 2 Camera 
							and with high defination  
							Front Camera with auto focus, octa cure processor',
							2,45000,'Owsome Display and camera','2 year warranty',
							'Nothing to say it is Owsome','New','dfdsfs1.jpg','dfdsfs2.jpg','dfdsfs3.jpg',
							'dfdsfs4.jpg',100,'Pickup from courier Office',1
		select * from ProductMst
		select * from CategoryTypeMst
		select * from CategoryMst
		select * from ProductMainDetail		
		DELETE  FROM ProductMst
		DELETE  FROM ProductMainDetail
		select * from AuctionTypeMst
		select * from AuctionTypeItemDetail
		delete from BidDetail where BidId=2
		select * from BidDetail
		uspSaveBidDetail 0,1,1,1,2000,False,5
		@BidId|SINT|0|@CategoryId|TINT|1|@ProductId|SINT|1|@Client|INT|1|@BidAmt|INT|20000|@IsBuyNowAmt|BIT|False|@BuyQty|TINT|5
*/



alter proc dbo.uspSaveBidDetail
@BidId smallint,
@CategoryId tinyint,
@ProductId smallint,
@ClientId	int,
@BidAmt	int,
@IsBuyNowAmt	bit,
@BuyQty	tinyint
--@IsApproved	bit
with Encryption
as
declare @MaxBidId tinyint ,@ErrNo tinyint , @TransName varchar(30),
@BidDate date,@BidTime varchar(8),@BidCnt int
	set @TransName = 'SaveBidDetail'
	Begin Tran @TransName
		set @MaxBidId = @BidId 
		set @BidDate= CONVERT(varchar(10),GETDATE(),20)
		set	@BidTime= CONVERT(varchar(8),GETDATE(),108)
		
		if(select COUNT(*) from dbo.BidDetail
			where  BidId = @BidId and ProductId = @ProductId  and CategoryId =@CategoryId) > 0
		begin
		
			update dbo.BidDetail 
		
				set
					ClientId=@ClientId,
					BidAmt=@BidAmt
					--IsApproved=@IsApproved
				where ProductId=@ProductId and BidId = @BidId
						
				set @ErrNo = @@ERROR --return error number of last transact-sql
			if @ErrNo <> 0 goto Err 	
				
		end
		else
		begin
			--start auto incrment select * from ClientDetail
			select @MaxBidId = ISNULL(MAX (BidId),0)+1 from dbo.BidDetail where ProductId = @ProductId and CategoryId =@CategoryId 
			set @ErrNo =@@ERROR 
			if @ErrNo <> 0 goto Err 	
				--insert start
			insert into dbo.BidDetail(BidId,CategoryId,ProductId,ClientId,BidDate,BidTime,BidAmt)
			select @MaxBidId,@CategoryId,@ProductId,@ClientId,@BidDate,@BidTime,@BidAmt
			
				set @BidCnt= (select isnull(BidCnt,0)
							from AuctionTypeItemDetail
							where  CategoryId=@CategoryId and ProductId=@ProductId)
					update AuctionTypeItemDetail 
					set BidCnt=@BidCnt+1 			
					where CategoryId=@CategoryId and ProductId=@ProductId
			set @ErrNo = @@ERROR 		
			if @ErrNo <> 0 goto Err 	
		end	
		--when display alias name
	select @CategoryId CategoryId , @ProductId ProductId,@MaxBidId BidId
	
goto success
Err:
	rollback tran @TransName
	return @ErrNo
Success:
	Commit tran @TransName
	return 0
	
	