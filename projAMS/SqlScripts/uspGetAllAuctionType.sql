use dbAMS 
go
/*
uspGetAllAuctionType 1,'HTC',1
select * from BidDetail
select * from AuctionTypeItemDetail

1	NORMAL AUCTION
2	EXPRESS AUCTION
3	SEALDED AUCTION
4	CHARITY AUCTION
5	PACKAGE AUCTION
6	DEBUT AUCTION
select ClientId,Max(BidAmt)as BidAmt from dbo.BidDetail where ProductId=1 Group By clientId

*/

alter proc dbo.uspGetAllAuctionType
@CategoryTypeId	tinyint,
@Category	varchar(30),
@AuctionTypeId smallint
with Encryption
as
declare @ErrNo tinyint , @TranName varchar(30)
	set @TranName = 'GetAllAuctionType'
	Begin Tran @TranName
		if(select COUNT(*) from ProductMst pm
			join CategoryMst cm on 
					cm.CategoryId=pm.CategoryId 
			join AuctionTypeItemDetail aid on 
					aid.AuctionTypeId=pm.AuctionTypeId and
					aid.CategoryId=pm.CategoryId and 
					aid.ProductId=pm.ProductId 
			join AuctionTypeMst am on 
					am.AuctionTypeId=aid.AuctionTypeId
			where  DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
			 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
			 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
			 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
			and
			cm.CategoryTypeId=@CategoryTypeId and cm.Category like @Category and pm.AuctionTypeId=@AuctionTypeId
		) > 0
		begin
					select pm.ProductName,pm.ProductId, am.AuctionTypeName,aid.BuyNowAmt,pm.Image1,pm.Image2,cm.Category
			from ProductMst pm
			join CategoryMst cm on 
					cm.CategoryId=pm.CategoryId 
			join AuctionTypeItemDetail aid on 
					aid.AuctionTypeId=pm.AuctionTypeId and
					aid.CategoryId=pm.CategoryId and 
					aid.ProductId=pm.ProductId 
			join AuctionTypeMst am on 
					am.AuctionTypeId=aid.AuctionTypeId
			where  DATEDIFF(DAY,GETDATE(),aid.EndDt) >=0  and
			 DATEDIFF(SECOND,GETDATE(),CONVERT(varchar(10), aid.EndDt) + ' ' + CONVERT(varchar(10), aid.EndTime)) >=0 and
			 DATEDIFF(DAY,aid.StartDt,GETDATE()) >=0  and
			 DATEDIFF(SECOND,CONVERT(varchar(10), aid.StartDt) + ' ' + CONVERT(varchar(10), aid.StartTime),GETDATE()) >=0
			and
			cm.CategoryTypeId=@CategoryTypeId and cm.Category like @Category and pm.AuctionTypeId=@AuctionTypeId
		End
		else
		Begin 
		
			select 'Product Not Available' ProductName,'Product Not Available' AuctionTypeName,
					'Product Not Available' BuyNowAmt,'Product Not Available'  Image1,'Product Not Available' Image2,'Product Not Available' Category
		End
set @Errno=@@ERROR
if @Errno<>0 goto err 
					
goto Success
Err:
	rollback tran @TranName
	return @ErrNo
Success:
	commit tran @TranName
	return 0
