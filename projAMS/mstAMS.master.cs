﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class mstAMS : System.Web.UI.MasterPage
{
    public string path = "";
    balSetAutoBid balObjAutoBid = new balSetAutoBid();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        //if (Session["MstSite"] != null)
        //{
        //    this.MasterPageFile = Session["MstSite"].ToString();
        //}
        //else
        //{
        //    Response.Redirect("../StaticPage/Login.aspx");
        //}


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (File.Exists(Server.MapPath("Web.config")))
            {
                path = "";
            }
            else if (File.Exists(Server.MapPath("../web.config")))
            {
                path = "../";
            }
            else if (File.Exists(Server.MapPath("../../web.config")))
            {
                path = "../../";
            }
            if (Session["LUID"] != null)
            {
                pnlLogIn.Visible = false;
                pnlMyAccount.Visible = true;
                lblGuest.Text = Session["LUName"].ToString()+" ";
            }
            else
            {
                pnlLogIn.Visible = true;
                pnlMyAccount.Visible = false;
            }
        }
    }
    protected void lbtnLogOut_Click(object sender, EventArgs e)
    {
        Session["LEmailId"] = null;
        Session["LPasswd"] = null;
        Session["LDesig"] = null;
        Session["LUID"] = null;
        Session["LUName"] = null;
        Session["MstSite"] = null;

        if (Session["LUID"] != null)
        {
            pnlLogIn.Visible = false;
            pnlMyAccount.Visible = true;
        }
        else
        {
            pnlLogIn.Visible = true;
            pnlMyAccount.Visible = false;
        }
        Response.Redirect("~/Default.aspx");
    }
    protected void lbtnLogin_Click(object sender, EventArgs e)
    {
        //string str = HttpContext.Current.Request.Url.AbsolutePath;
        //Session["RecentPage"] = str;
        Response.Redirect("StaticPage/Login.aspx");
    }
    protected void lbtnReg_Click(object sender, EventArgs e)
    {
        Response.Redirect("StaticPage/Login.aspx");
    }
    protected void lbtnUprofile_Click(object sender, EventArgs e)
    {
        Session["MstSite"] = "~/DynamicPage/mstClient.master";
        Response.Redirect("DynamicPage/DefaultClient.aspx");
    }
    protected void lbtnUsetting_Click(object sender, EventArgs e)
    {
        Session["MstSite"] = "~/DynamicPage/mstClient.master";
        Response.Redirect("DynamicPage/AEUserProfile.aspx");
    }

    ////protected void timermstAMS_Tick(object sender, EventArgs e)
    ////{
    ////    balObjAutoBid.CallAutoBid();
    //}
}
