﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master" AutoEventWireup="true" CodeFile="EmployeeReg.aspx.cs" EnableEventValidation="false" Inherits="DynamicPage_EmployeeReg" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
        <%--     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
        <h3 class="page-title">Employee Registration
                    <small>Add or Edit</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="DefaultEmployee.aspx">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Employee</a>
                </li>
            </ul>

        </div>
        <!-- END PAGE HEADER-->
        <%--Employee Registration Grid--%>
        <asp:Panel ID="PanelEntryEmployeeGrid" runat="server">

            <div class="portlet box red-intense ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Employee Registration Detail 
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>



                <div class="portlet-body">
                    <div class="portlet-title tools">

                        <div class="form-actions top" align="Middle">

                            <asp:Label ID="lblEmployeeName" 
                                runat="server" 
                                Text="Employee Name :" />
                            <i class="tooltips twitter-typeahead" data-original-title="Write Proper Name">
                                <asp:TextBox ID="txtSearchEmployee"
                                    runat="server"
                                    placeholder="Enter your Name"
                                    CssClass="form-control input-inline tt-hint twitter-typeahead" />
                            </i>

                                       <asp:FilteredTextBoxExtender ID="FTEBtxtSearchEmployee" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtSearchEmployee">
                                            </asp:FilteredTextBoxExtender>

                            <%--                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" 
                                runat="server" 
                                TargetControlID="txtSearchEmployee"
                                
                                />--%>

                            <asp:Button ID="btnSearchEmployee"
                                runat="server"
                                Text="Search"
                                class="btn blue" OnClick="btnSearchEmployee_Click" />
                            <asp:Button ID="btnAddNewEmp"
                                runat="server"
                                Text="Add New Employee"
                                class="btn default"
                                align="right"
                                OnClick="btnAddNewEmp_Click" />

                        </div>
                        <br />
                        <%--Grid view Start--%>
                        <asp:GridView ID="GVEmployeeReg1" runat="server"
                            AllowPaging="True" ShowFooter="True"
                            PageSize="10"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False"
                            GridLines="None"
                            PagerStyle-VerticalAlign="Middle"
                            HeaderStyle-ForeColor="Black"
                            CellPadding="4"
                            OnRowEditing="GVEmployeeReg1_RowEditing">

                            <Columns>

                                <asp:TemplateField HeaderText="Employee Id" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployeeId"
                                            runat="server"
                                            Text='<%#Eval("EmpId") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Employee Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployeeName"
                                            runat="server" Text='<%#Eval("EmpName") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Contact No." ControlStyle-Height="20px" ItemStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo"
                                            runat="server"
                                            Text='<%#Eval("ContactNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email-Id" ControlStyle-Height="20px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmailId"
                                            runat="server"
                                            Text='<%#Eval("EmailId") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Edit" ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgBtnEdit" runat="server"
                                            ImageUrl="~/DynamicPage/img/edit-grid.png"
                                            AlternateText="Edit"
                                            Height="25px"
                                            Width="30px"
                                            CausesValidation="false"
                                            ToolTip="Edit"
                                            CommandArgument='<%#Eval("EmpId") %>'
                                            CommandName="Edit"
                                            OnCommand="ImgBtnEdit_Command" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle ForeColor="Black" />
                            <PagerStyle VerticalAlign="Middle" />
                        </asp:GridView>

                    </div>

                    <%--Grid view Body END--%>
                </div>
            </div>
        </asp:Panel>



        <asp:Panel ID="PanelRegistrationForm" runat="server" Visible="False">

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet box red-intense">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Employee Registration
                            </div>

                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div id="Div1" class="form-horizontal form-bordered">
                                <div class="form-body">

                                    <div class="form-group">
                                        <asp:Label class="col-lg-3 control-label" runat="server" ID="lblEmpId">Employee Id:</asp:Label>
                                        <div class="col-lg-5">
                                            <asp:TextBox ID="txtEmpId"
                                                class="form-control input-circle-right"
                                                runat="server"
                                                ReadOnly="true" />
                                        </div>
                                    </div>
                                    <%--Emp Name--%>
                                    <div class="form-group">
                                        <label id="lblEmpName" class="col-lg-3 control-label">Employee Name:</label>
                                        <div class="col-lg-5">

                                            <asp:TextBox ID="txtEmpName"
                                                runat="server"
                                                placeholder="Enter fullname"
                                                CssClass="form-control input-circle-right"> </asp:TextBox>

                                            <asp:FilteredTextBoxExtender ID="FTBEtxtEmpName" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtEmpName">
                                            </asp:FilteredTextBoxExtender>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                ControlToValidate="txtAddress"
                                                ErrorMessage="* Please Enter Employee Name"
                                                CssClass="has-error"
                                                Display="Dynamic" />
                                        </div>
                                    </div>
                                    <%--Gender--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Gender :</label>
                                        <div class="col-lg-5">

                                            <label>
                                                <asp:RadioButton runat="server"
                                                    ID="rdbMale"
                                                    Text="Male"
                                                    Checked="true"
                                                    GroupName="grp1" />

                                            </label>
                                            <label>
                                                <asp:RadioButton runat="server"
                                                    ID="rdbFemale"
                                                    Text="Female"
                                                    GroupName="grp1" />
                                            </label>


                                        </div>

                                    </div>
                                    <%--Address--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Address :</label>
                                        <div class="col-lg-5">

                                            <asp:TextBox ID="txtAddress" runat="server"
                                                placeholder="Enter permannet address"
                                                CssClass="focused form-control input-circle-right"
                                                TextMode="MultiLine"> </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RfvAddress" runat="server"
                                                ControlToValidate="txtAddress"
                                                ErrorMessage="* Please Enter your Address"
                                                class="help-block"
                                                Display="Dynamic"> </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <%--Contact No--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Contact Number :</label>
                                        <div class="col-lg-5">
                                            <asp:TextBox ID="txtContactNo"
                                                runat="server"
                                                placeholder="Enter contact no."
                                                MaxLength="12"
                                                class=" form-control input-circle-right">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RfvConNo"
                                                runat="server"
                                                ControlToValidate="txtContactNo"
                                                ErrorMessage="* Please Enter Contact Number."
                                                class="help-block"
                                                Display="Dynamic" />
                                            <asp:FilteredTextBoxExtender ID="ftbeContactNo"
                                                runat="server"
                                                FilterType="Numbers"
                                                TargetControlID="txtContactNo" />
                                        </div>
                                    </div>
                                    <%--Date Of Join--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Date Of Joinin :</label>
                                        <div class="col-lg-5">
                                            <asp:TextBox ID="txtDOJ"
                                                runat="server"
                                                MaxLength="10"
                                                placeholder="Enter Date Of Joinin"
                                                CssClass="focused form-control input-circle-right">
                                            </asp:TextBox>
                                            <asp:CalendarExtender ID="CalEDOJ"
                                                runat="server"
                                                TargetControlID="txtDOJ"
                                                Format="dd-MM-yyyy"
                                                Enabled="True"
                                                Animated="true"
                                                PopupPosition="Left">
                                            </asp:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RfvDOJ"
                                                runat="server"
                                                ControlToValidate="txtDOJ"
                                                ErrorMessage="* Please Select Date Of Joining"
                                                class="help-block"
                                                Display="Dynamic" />
                                            <asp:FilteredTextBoxExtender ID="ftbeDOJ"
                                                runat="server"
                                                FilterType="Custom,Numbers"
                                                ValidChars="/-"
                                                TargetControlID="txtDOJ" />

                                        </div>
                                    </div>
                                    <%--Date Of Birth--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Date Of Birth :</label>
                                        <div class="col-lg-5">
                                            <asp:TextBox ID="txtDOB"
                                                runat="server"
                                                MaxLength="10"
                                                placeholder="Enter Date Of Birth"
                                                CssClass="focused form-control input-circle-right">
                                            </asp:TextBox>
                                            <asp:CalendarExtender ID="RfvDOB"
                                                runat="server"
                                                TargetControlID="txtDOB"
                                                Format="dd-MM-yyyy"
                                                Enabled="True"
                                                Animated="true"
                                                PopupPosition="Left">
                                            </asp:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                runat="server"
                                                ControlToValidate="txtDOB"
                                                ErrorMessage="* Please Select Date Of Birth"
                                                CssClass="LblErrorMsg"
                                                Display="Dynamic" />
                                            <asp:FilteredTextBoxExtender ID="ftbeDOB"
                                                runat="server"
                                                FilterType="Custom,Numbers"
                                                ValidChars="/-"
                                                TargetControlID="txtDOB" />
                                        </div>
                                    </div>
                                    <%--Designation--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Designation :</label>
                                        <div class="col-lg-5">
                                           <%-- <asp:TextBox ID="txtDesignation"
                                                runat="server"
                                                placeholder="Enter Designation"
                                                CssClass="focused form-control input-circle-right"
                                                MaxLength="15">
                                            </asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtDesignation">
                                            </asp:FilteredTextBoxExtender>

                                            <asp:RequiredFieldValidator ID="rfvDesig"
                                                runat="server"
                                                ControlToValidate="txtDesignation"
                                                ErrorMessage="* Please Enter Designation."
                                                CssClass="LblErrorMsg"
                                                Display="Dynamic"> </asp:RequiredFieldValidator>--%>
                                            <asp:DropDownList ID="ddlDesig" runat="server" CssClass="focused form-control input-circle-right">

                                                <asp:ListItem Text="Admin" Value="Admin">Admin</asp:ListItem>
                                                <asp:ListItem Text="Employee" Value="Employee">Employee</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <%--EmailId--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Email Id :</label>
                                        <div class="col-lg-5">
                                            <asp:TextBox ID="txtEmailId"
                                                runat="server"
                                                placeholder="Enter EmailId"
                                                CssClass="focused form-control input-circle-right" />
                                            <asp:Label ID="lblECheck" runat="server" />
                                            <asp:RequiredFieldValidator ID="RfvEmailId"
                                                runat="server"
                                                ControlToValidate="txtEmailId"
                                                ErrorMessage="* Please Enter Email Id"
                                                CssClass="LblErrorMsg"
                                                Display="Dynamic"> </asp:RequiredFieldValidator>

                                            <asp:RegularExpressionValidator ID="RgeEmailId"
                                                runat="server"
                                                ControlToValidate="txtEmailId"
                                                ErrorMessage="* Not Valid E-Mail Address"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                CssClass="LblErrorMsg"
                                                Display="Dynamic"> </asp:RegularExpressionValidator><br />
                                            <asp:Button ID="btnCheckEmail"
                                                runat="server"
                                                Text="Check"
                                                CssClass="btn purple"
                                                OnClick="btnCheckEmail_Click" />
                                        </div>
                                    </div>


                                    <%--Employee Image--%>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" id="Label2">Employee Image </label>
                                        <div class="col-lg-5 profile-userpic">
                                            <asp:Image ID="imgProfile"
                                                runat="server"
                                                class="img-responsive"
                                                Height="200"
                                                Width="200" />
                                            <asp:FileUpload ID="fuImg"
                                                runat="server"
                                                CssClass="form-control input-circle-right " />
                                            <asp:Button ID="btnUpload"
                                                runat="server"
                                                CssClass="btn purple"
                                                Text="Upload"
                                                OnClick="btnUpload_Click" />
                                        </div>
                                    </div>
                                    <%--Submit Cancel--%>
                                    <div class="form-actions fluid">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <asp:Button ID="BtnSubmit"
                                                    runat="server"
                                                    Text="Submit"
                                                    CssClass="btn purple"
                                                    OnClick="btnSubmit_Click" />

                                                <asp:Button ID="BtnCancel"
                                                    runat="server"
                                                    Text="Cancel"
                                                    CssClass="btn purple"
                                                    CausesValidation="false"
                                                    OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <asp:Label ID="lblMsg"
                            runat="server"
                            CssClass="lblCenter" />
                    </div>
                    <!-- BEGIN PORTLET-->

                </div>
            </div>
        </asp:Panel>





        <%--         </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>

