﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master" AutoEventWireup="true" CodeFile="AEUserProfile.aspx.cs" Inherits="DynamicPage_AEUserProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- END STYLE CUSTOMIZER -->
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">User Account <small>Update Your Profile</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Pages</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">User Account</a>
                </li>
            </ul>

        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row margin-top-20">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <asp:Image ImageUrl="../images/DefultUser.jpg" ID="imgUser" runat="server" class="img-responsive" alt="" />
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                <asp:Label ID="lbluname" runat="server" Text="Marcus Doe" />
                            </div>
                            <div class="profile-usertitle-job">
                                <asp:Label ID="lblDesig" runat="server" Text="Marcus Doe" />
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li>
                                    <a href="AEUserOverview.aspx">
                                        <i class="icon-home"></i>
                                        Overview </a>
                                </li>
                                <li class="active">
                                    <a href="AEUserProfile.aspx">
                                        <i class="icon-settings"></i>
                                        Account Settings </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <asp:LinkButton runat="server" ID="lbtnPersonalInfo" OnClick="lbtnPersonalInfo_Click" CausesValidation="false">
                                            <%--<a href="#tab_1_1" data-toggle="tab">--%>Personal Info</asp:LinkButton>
                                        </li>
                                        <li class="active">
                                            <asp:LinkButton ID="lbtnChangeProfilePicture" runat="server" OnClick="lbtnChangeProfilePicture_Click" CausesValidation="false">
                                           <%-- <a href="#tab_1_2" data-toggle="tab">--%>Change Profile Picture</asp:LinkButton>
                                        </li>
                                        <li id="tabChangePwd" class="active">
                                            <asp:LinkButton ID="lbtnChangePassword" runat="server" OnClick="lbtnChangePassword_Click" CausesValidation="false">
                                            <%--<a href="#tab_1_3" data-toggle="tab">--%>Change Password</asp:LinkButton>
                                        </li>
                                        <%-- <li>
                                            <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                        </li>--%>
                                    </ul>
                                </div>

                                <div class="portlet-body">

                                    <div class="tab-content">
                                        <!-- PERSONAL INFO TAB -->
                                        <asp:Panel runat="server" ID="pnlProfile">
                                            <div class="tab-pane active" id="tab_1_1">
                                                <%--  <form role="form" action="#">--%>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        First Name
                                                    </label>

                                                    <asp:TextBox ID="TxtName"
                                                        runat="server"
                                                        Placeholder="Enter your Full Name"
                                                        CssClass="focused form-control input-circle-right" />

                                                    <asp:FilteredTextBoxExtender ID="FTBETxtName" runat="server"
                                                        FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="TxtName">
                                                    </asp:FilteredTextBoxExtender>

                                                    <asp:RequiredFieldValidator ID="RFVName" runat="server"
                                                        ControlToValidate="TxtName"
                                                        ErrorMessage="* Please Enter Name"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Address</label>
                                                    <asp:TextBox ID="TxtAdd"
                                                        runat="server"
                                                        TextMode="MultiLine"
                                                        Placeholder="Enter your Full Address"
                                                        CssClass="focused form-control input-circle-right" />

                                                    <asp:FilteredTextBoxExtender ID="FTEBTxtAdd" runat="server"
                                                        FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="TxtAdd">
                                                    </asp:FilteredTextBoxExtender>

                                                    <asp:RequiredFieldValidator ID="RFVAddress" runat="server"
                                                        ControlToValidate="TxtAdd"
                                                        ErrorMessage="* Please Enter Address"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Gender</label>
                                                    <asp:DropDownList ID="DDLGender" runat="server" CssClass="focused form-control input-circle-right" Width="200px">

                                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>


                                                <%-- <div class="form-group">
                                                        <label class="control-label">
                                                            Gender</label>
                                                            Male<asp:RadioButton ID="RadioButton1" runat="server" />
                                                            Female<asp:RadioButton ID="RadioButton2" runat="server" />
                                                    </div>--%>

                                                <div class="form-group">
                                                    <label class="control-label">Contact Number</label>
                                                    <asp:TextBox ID="Txtcon"
                                                        runat="server"
                                                        Placeholder="+1 646 580 DEMO (6284)"
                                                        CssClass="focused form-control input-circle-right" />

                                                    <asp:FilteredTextBoxExtender ID="FTEBTxtcon" runat="server"
                                                        FilterType="Numbers"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="Txtcon">
                                                    </asp:FilteredTextBoxExtender>

                                                    <asp:RequiredFieldValidator ID="RFVCon" runat="server"
                                                        ControlToValidate="TxtCon"
                                                        ErrorMessage="* Please Enter Contact Number"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Email Id</label>
                                                    <asp:TextBox ID="TxtEmail"
                                                        runat="server"
                                                        Width="400px"
                                                        Placeholder="Enter your Email Id"
                                                        CssClass="focused form-control input-circle-right" />

                                                    <asp:FilteredTextBoxExtender ID="FTEBTxtEmail" runat="server"
                                                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="TxtEmail">
                                                    </asp:FilteredTextBoxExtender>

                                                    <asp:RequiredFieldValidator ID="RFVEmail" runat="server"
                                                        ControlToValidate="TxtEmail"
                                                        ErrorMessage="* Please Enter Email Id"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label class="control-label" runat="server" ID="lblShipAdd" Visible="false">Shipping Address</asp:Label>
                                                    <asp:TextBox ID="TxtShip"
                                                        runat="server"
                                                        Visible="false"
                                                        TextMode="MultiLine"
                                                        Placeholder="Enter your Shipping Address"
                                                        CssClass="focused form-control input-circle-right" />

                                                    <asp:FilteredTextBoxExtender ID="FTEBTxtShip" runat="server"
                                                        FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="TxtShip">
                                                    </asp:FilteredTextBoxExtender>

                                                    <asp:RequiredFieldValidator ID="RFVShip" runat="server"
                                                        ControlToValidate="TxtShip"
                                                        ErrorMessage="* Please Enter Shipping Address"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="lblDesignation" class="control-label"></asp:Label>
                                                    <asp:TextBox ID="TxtDesig"
                                                        runat="server"
                                                        Width="400px"
                                                        Placeholder="Enter your Designation"
                                                        CssClass="focused form-control input-circle-right" Text="User" />

                                                    <asp:FilteredTextBoxExtender ID="FTEBTxtDesig" runat="server"
                                                        FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                        FilterMode="ValidChars"
                                                        ValidChars=" "
                                                        TargetControlID="TxtDesig">
                                                    </asp:FilteredTextBoxExtender>


                                                    <asp:RequiredFieldValidator ID="RFVDesig" runat="server"
                                                        ControlToValidate="TxtDesig"
                                                        ErrorMessage="* Please Enter Designation"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="lblDOB" class="control-label" Text="Date of Birth" />

                                                    <asp:TextBox ID="TxtDOB"
                                                        runat="server"
                                                        MaxLength="10"
                                                        Width="400px"
                                                        placeholder="Enter Date Of Birth"
                                                        CssClass="focused form-control input-circle-right">
                                                    </asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1"
                                                        runat="server"
                                                        TargetControlID="txtDOB"
                                                        Format="dd-MM-yyyy"
                                                        Enabled="True"
                                                        Animated="true"
                                                        PopupPosition="Left">
                                                    </asp:CalendarExtender>

                                                    <asp:RequiredFieldValidator ID="RFVDOB"
                                                        runat="server"
                                                        ControlToValidate="txtDOB"
                                                        ErrorMessage="* Please Select Date Of Birth"
                                                        CssClass="LblErrorMsg"
                                                        Display="Dynamic" />
                                                    <asp:FilteredTextBoxExtender ID="ftbeDOB"
                                                        runat="server"
                                                        FilterType="Custom,Numbers"
                                                        ValidChars="/-"
                                                        TargetControlID="txtDOB" />
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="lblDOJ" class="control-label" Text="Date of Join" />

                                                    <asp:TextBox ID="TxtDOJ"
                                                        runat="server"
                                                        MaxLength="10"
                                                        Width="400px"
                                                        placeholder="Enter Date Of Join"
                                                        CssClass="focused form-control input-circle-right">
                                                    </asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender2"
                                                        runat="server"
                                                        TargetControlID="txtDOJ"
                                                        Format="dd-MM-yyyy"
                                                        Enabled="True"
                                                        Animated="true"
                                                        PopupPosition="TopLeft">
                                                    </asp:CalendarExtender>

                                                    <asp:RequiredFieldValidator ID="RfvDOJ"
                                                        runat="server"
                                                        ControlToValidate="txtDOJ"
                                                        ErrorMessage="* Please Select Date Of Joining"
                                                        class="help-block"
                                                        Display="Dynamic" />

                                                    <asp:FilteredTextBoxExtender ID="ftbeDOJ"
                                                        runat="server"
                                                        FilterType="Custom,Numbers"
                                                        ValidChars="/-"
                                                        TargetControlID="txtDOJ" />
                                                </div>


                                                <div class="form-group">
                                                    <asp:Button runat="server" ID="btnSavePersonalInfo" Text="Save" class="btn green-haze" OnClick="btnSavePersonalInfo_Click" />
                                                </div>


                                                <%--</form>--%>
                                            </div>
                                        </asp:Panel>
                                        <!-- END PERSONAL INFO TAB -->
                                        <!-- CHANGE AVATAR TAB -->
                                        <asp:Panel runat="server" ID="pnlImg" Visible="false">
                                            <div class="tab-pane active" id="tab_1_2">

                                                <%--<form action="#" role="form">--%>
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <%--<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    
                                                                </div>--%>
                                                        <%-- <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
                                                                <%--<img  src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                    <asp:Image ImageUrl=""
                                                                        runat="server" ID="ImgChangeProfileImage" alt="" Height ="100" Width ="190"/>
                                                                </div>--%>
                                                        <div>
                                                            <%-- <span class="btn default btn-file">
                                                                        <span class="fileinput-new">Select image </span>
                                                                        <span class="fileinput-exists">Change </span>--%>
                                                            <%--<input type="file" name="...">--%>
                                                            <%--<asp:FileUpload ID="FUImgChangeProfileImage" runat="server" />--%>
                                                            <asp:FileUpload ID="FUImgChangeProfileImage" runat="server" />
                                                            <%--</span>--%>
                                                            <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">


                                                        <span>
                                                            <asp:Label runat="server" ID="lblPmsg" /></span>
                                                    </div>
                                                </div>
                                                <div class="margin-top-10">
                                                    <asp:Button runat="server" ID="btnSubmitChangeProfile" class="btn green-haze" Text="Submit" OnClick="btnSubmitChangeProfile_Click"></asp:Button>
                                                    <a href="#" class="btn default">Cancel </a>
                                                </div>
                                                <%--</form>--%>
                                            </div>
                                        </asp:Panel>
                                        <!-- END CHANGE AVATAR TAB -->
                                        <!-- CHANGE PASSWORD TAB -->
                                        <asp:Panel runat="server" ID="pnlChangePwd" Visible="false">
                                            <div class="tab-pane active">
                                                <%--<form action="#">--%>
                                                <div class="form-group">
                                                    <label class="control-label">Current Password</label>
                                                    <%--<input type="password" class="form-control" />--%>
                                                    <asp:TextBox runat="server" ID="txtCurrentPasswd" class="form-control" TextMode="Password" />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                        ControlToValidate="txtCurrentPasswd"
                                                        ErrorMessage="Enter you current password"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />

                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">New Password</label>
                                                    <%--<input type="password" class="form-control" />--%>
                                                    <asp:TextBox runat="server" ID="txtNewPasswd1" class="form-control" TextMode="Password" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                        ControlToValidate="txtNewPasswd1"
                                                        ErrorMessage="Not blank new password"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Confirm New Password</label>
                                                    <%--<input type="password" class="form-control" />--%>
                                                    <asp:TextBox runat="server" ID="txtNewPasswd2" class="form-control" TextMode="Password" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                        ControlToValidate="txtNewPasswd2"
                                                        ErrorMessage="Not empty field"
                                                        CssClass="has-error"
                                                        Display="Dynamic" />
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                                                        ControlToValidate="txtNewPasswd2"
                                                        ControlToCompare="txtNewPasswd1"
                                                        ErrorMessage="Password and Confirm password doesn't match">

                                                    </asp:CompareValidator>
                                                    <asp:Label runat="server" ID="lblPwdMsg" Text="" />
                                                </div>
                                                <div class="margin-top-10">
                                                    <asp:Button runat="server" ID="btnChangePasswd" class="btn green-haze" Text="Change Password" OnClick="btnChangePasswd_Click" />
                                                    <asp:Button runat="server" ID="btnChangeCancel" class="btn default" Text="Cancel" />
                                                </div>
                                                <%--</form>--%>
                                            </div>
                                        </asp:Panel>
                                        <!-- END CHANGE PASSWORD TAB -->

                                        <!-- PRIVACY SETTINGS TAB -->

                                        <!-- END PRIVACY SETTINGS TAB -->
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>

    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
    <script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
    <script src="assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features
            Profile.init(); // init page demo
        });
    </script>
    <!-- END JAVASCRIPTS -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37564768-1', 'keenthemes.com');
        ga('send', 'pageview');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37564768-1', 'keenthemes.com');
        ga('send', 'pageview');
    </script>

</asp:Content>

