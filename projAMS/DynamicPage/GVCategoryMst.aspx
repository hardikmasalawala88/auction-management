﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="GVCategoryMst.aspx.cs" Inherits="DynamicPage_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <h3 class="page-title">Sub Category Detail
                    <small>View , Add & Edit</small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx" >Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Sub Category Detail</a>
                        </li>
                    </ul>
                    
                </div>
                <!-- END PAGE HEADER-->




                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>View Sub Category 
                            
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="table_responsive.html#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>


                    <div class="portlet-body">

                        <div class="select2-hidden-accessible">
                            <div class="row ">
                                <div class="col-md-6 col-sm-12">
                                    <label>
                                        <%--select2-drop select2-display-none select2-with-searchbox select2-drop-active--%>
                                        Main Category
                                        <asp:DropDownList ID="ddlCategoryType"
                                            class="  form-control input-inline select2-offscreen select2-display-none"
                                            runat="server"
                                            OnSelectedIndexChanged="ddlCategoryType_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </label>
                                </div>
                            </div>
                        </div>





                        <div class="portlet-title tools">
                            <%--Grid view Start--%>
                            <asp:GridView ID="GVCategoryMst1" runat="server"
                                AllowPaging="True"
                                ShowFooter="True"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4"
                                ViewStateMode="Enabled"
                                ShowHeaderWhenEmpty="True"
                                HeaderStyle-CssClass="hidden-xs sorting"
                                EditRowStyle-CssClass="hidden-xs sorting"
                                OnPageIndexChanging="GVCategoryMst1_PageIndexChanging"
                                OnRowCancelingEdit="GVCategoryMst1_RowCancelingEdit"
                                OnRowCommand="GVCategoryMst1_RowCommand"
                                OnRowEditing="GVCategoryMst1_RowEditing"
                                OnRowUpdating="GVCategoryMst1_RowUpdating">

                                <Columns>

                                    <asp:TemplateField HeaderText="SubCategoryId" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryId"
                                                runat="server"
                                                Text='<%#Eval("CategoryId") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAdd"
                                                runat="server"
                                                Text="New SubCategory">
                                            </asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SubCategory" HeaderStyle-HorizontalAlign="Center" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory"
                                                runat="server"
                                                Text='<%#Eval("Category")%>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCategory" 
                                                runat="server" 
                                                Text='<%#Eval("Category")%>'>
                                            </asp:TextBox>

                                            <asp:FilteredTextBoxExtender ID="FTBEtxtCategory" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom,numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtCategory">
                                            </asp:FilteredTextBoxExtender>
                                           
                                            
                                            <asp:RequiredFieldValidator ID="rfvCategory"
                                                runat="server"
                                                ErrorMessage="Can't be Blank"
                                                ControlToValidate="txtCategory"
                                                Display="Dynamic"
                                                Text="*Can't be Blank"
                                                ValidationGroup="EditValid">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAddCategory" CssClass=" form-control" runat="server">
                                            </asp:TextBox>

                                             <asp:FilteredTextBoxExtender ID="FTBEtxtAddCategory" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtAddCategory">
                                            </asp:FilteredTextBoxExtender>
                                           
                                            <asp:RequiredFieldValidator ID="rfvAddCategory"
                                                runat="server"
                                                ErrorMessage="Can't be Blank"
                                                ControlToValidate="txtAddCategory"
                                                Display="Dynamic"
                                                Text="*Can't be Blank">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Edit" ItemStyle-Width="130px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBtnEdit"
                                                runat="server"
                                                ImageUrl="~/DynamicPage/img/edit-grid.png"
                                                AlternateText="Update"
                                                Height="25px"
                                                Width="30px"
                                                CausesValidation="false"
                                                ToolTip="Edit"
                                                CommandName="Edit" />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <asp:ImageButton ID="btnUpdate"
                                                ImageUrl="~/DynamicPage/img/update.png"
                                                Height="25px"
                                                Width="30px"
                                                ToolTip="Update"
                                                runat="server"
                                                CommandName="Update"
                                                ValidationGroup="EditValid" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:ImageButton ID="btnCancel"
                                                ImageUrl="~/DynamicPage/img/cancel.png"
                                                Height="25px"
                                                Width="30px"
                                                ToolTip="Cancle"
                                                runat="server"
                                                CommandName="Cancel"
                                                CausesValidation="false" />
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:Button ID="btnAddRecord"
                                                runat="server"
                                                Text="Add"
                                                CssClass="btn-default"
                                                CommandName="Add"></asp:Button>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <%--Grid view End--%>
                        </div>
                    </div>
                   <%-- <asp:ValidationSummary ID="ValidationSummary1" runat="server" />--%>
                    <asp:Label ID="lblMsg" runat="server" />
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

