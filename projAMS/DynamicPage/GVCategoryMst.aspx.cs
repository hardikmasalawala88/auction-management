﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


public partial class DynamicPage_Default : System.Web.UI.Page
{
    boCategoryMst boObj = new boCategoryMst();
    balCategoryMst balObj = new balCategoryMst();

    DataSet ds = new DataSet();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            FillddlCT();
            ddlCategoryType.Items.Insert(0, "All");
            FillGrid();           
        }

    }
    protected void FillGrid()
    {

        ds = balObj.Load();
        if (ds.Tables[0].Rows.Count == 0)
        {
            ds.Tables[0].Rows.Add(0, 0, "");
            GVCategoryMst1.DataSource = ds;
            GVCategoryMst1.DataBind();
            GVCategoryMst1.Rows[0].Visible = false;
        }
        else
        {
            GVCategoryMst1.DataSource = ds;
            GVCategoryMst1.DataBind();
        }


    }
    protected void FillddlCT()
    {

            ddlCategoryType.DataSource = balObj.FillddlCategoryType();
            ddlCategoryType.DataTextField = "CategoryType";
            ddlCategoryType.DataValueField = "CategoryTypeId";
            ddlCategoryType.DataBind();

    }

    protected void GVCategoryMst1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //for updateing

        Label CategoryId = (Label)GVCategoryMst1.Rows[e.RowIndex].FindControl("lblCategoryId");
        TextBox TxtCategory = (TextBox)GVCategoryMst1.Rows[e.RowIndex].FindControl("txtCategory");

        boObj.CategoryTypeId = Convert.ToInt16(ddlCategoryType.SelectedValue);
        boObj.CategoryId = Convert.ToInt16(CategoryId.Text);
        boObj.Category = TxtCategory.Text;

        if (balObj.Insert(boObj))
        {
            lblMsg.Text = "Successfuly Save.";
        }
        else
        {
            lblMsg.Text = "Unsuccessfuly Save.";
        }
        GVCategoryMst1.EditIndex = -1;
        //FillGrid();
        ddlCategoryType_SelectedIndexChanged(sender, e);
    }
    protected void GVCategoryMst1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //which row you need to update that ID give
        GVCategoryMst1.EditIndex = e.NewEditIndex;
        ddlCategoryType_SelectedIndexChanged(sender, e);
        //FillGrid();
    }
    protected void GVCategoryMst1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        //when you press cancel then data not change for that it use
        GVCategoryMst1.EditIndex = -1;
        ddlCategoryType_SelectedIndexChanged(sender, e);
    }
    protected void GVCategoryMst1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Row Command give command what to do add or other

        if (e.CommandName.Equals("Add"))
        {
            TextBox TxtCategory = (TextBox)GVCategoryMst1.FooterRow.FindControl("txtAddCategory");
            if (ddlCategoryType.SelectedValue.ToString() != "All")
            {
                boObj.CategoryTypeId = Convert.ToInt16(ddlCategoryType.SelectedValue.ToString());
                boObj.CategoryId = 0;
                boObj.Category = TxtCategory.Text;

                //insert is method in balCategoryTypeMst.cs 
                if (balObj.Insert(boObj))
                {
                    lblMsg.Text = "Successfuly Save.";
                }
                else
                {
                    lblMsg.Text = "Unsuccessfuly Save.";
                }
                ddlCategoryType_SelectedIndexChanged(sender, e);
            }
            else
            {
                lblMsg.Text = "* Please select Any Category";
            }
        }

    }
    protected void GVCategoryMst1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVCategoryMst1.PageIndex = e.NewPageIndex;
        FillGrid();
        ddlCategoryType_SelectedIndexChanged(sender, e);
    }

    protected void ddlCategoryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ds = balObj.SearchCategoryType(ddlCategoryType.SelectedValue.ToString());
        if (ds.Tables[0].Rows.Count == 0)
        {
            ds.Tables[0].Rows.Add(0, 0, "");
            GVCategoryMst1.DataSource = ds;
            GVCategoryMst1.DataBind();
            GVCategoryMst1.Rows[0].Visible = false;
        }
        else
        {
            GVCategoryMst1.DataSource = ds;
            GVCategoryMst1.DataBind();
        }
    }

 
}
