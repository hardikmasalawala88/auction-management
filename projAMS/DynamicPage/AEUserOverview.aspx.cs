﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class DynamicPage_AEOverview : System.Web.UI.Page
{
    balEmployeeDetail balEmpObj = new balEmployeeDetail();
    boEmployeeDetail boEmpObj = new boEmployeeDetail();
    boClientRegistration boClientObj = new boClientRegistration();
    balClientRegistration balClientObj = new balClientRegistration();


    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        //Session["LEmailId"] = ds.Tables[0].Rows[0][0].ToString();
        //Session["LPasswd"] = ds.Tables[0].Rows[0][1].ToString();
        //Session["LDesig"] = ds.Tables[0].Rows[0][2].ToString();
        //Session["LUID"] = ds.Tables[0].Rows[0][3].ToString();
        //Session["LUName"] = ds.Tables[0].Rows[0][4].ToString();
        if (Session["LUID"] == null)
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }

        if (!Page.IsPostBack)
        {
            lbluname.Text = Session["LUName"].ToString();
            lblDesig.Text = Convert.ToString(Session["LDesig"]);
            //Admin Data Update
            if (Convert.ToString(Session["LDesig"]) == "Admin")
            {
                boEmpObj.EmpId = Convert.ToInt32(Session["LUID"]);
                ds = balEmpObj.getData(boEmpObj);
                if (ds.Tables[0].Rows[0]["EmpImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                }
            }
            else if (Convert.ToString(Session["LDesig"]) == "Employee")
            {
                //Employee Data Update
                boEmpObj.EmpId = Convert.ToInt32(Session["LUID"]);
                ds = balEmpObj.getData(boEmpObj);
                // ds.Tables[0].Rows[0]["EmpId"].ToString();
                if (ds.Tables[0].Rows[0]["EmpImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                }
            }
            else if (Convert.ToString(Session["LDesig"]) == "Client")
            {
                //Client Data Update
                boClientObj.ClientId = Convert.ToInt32(Session["LUID"]);
                ds = balClientObj.getData(boClientObj);
                if (ds.Tables[0].Rows[0]["ClientImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["ClientImage"].ToString();
                }
                FillGVPacketDetail();
                FillGVBiddingInfo();
            }
        }

    }

    protected void FillGVPacketDetail()
    {
        boClientObj.ClientId = Convert.ToInt32(Session["LUID"]);
        DataSet ds = new DataSet();
        ds = balClientObj.FillClientPackageDetail(boClientObj);
        gvPackagePurchase.DataSource = ds;
        gvPackagePurchase.DataBind();
        lblValue1.Text = ds.Tables[0].Rows[0]["Remaining"].ToString();

        lblValue2.Text = (Convert.ToInt16(ds.Tables[0].Rows[0]["Total"]) - Convert.ToInt16(ds.Tables[0].Rows[0]["Remaining"])).ToString();
    }
    protected void FillGVBiddingInfo()
    {
        boClientObj.ClientId = Convert.ToInt32(Session["LUID"]);
        DataSet ds = new DataSet();
        ds = balClientObj.FillClientBiddingDetail(boClientObj);
        gvBiddingInfo.DataSource = ds;
        gvBiddingInfo.DataBind();
        //lblValue1.Text = ds.Tables[0].Rows[0]["Remaining"].ToString();

        //lblValue2.Text = (Convert.ToInt16(ds.Tables[0].Rows[0]["Total"]) - Convert.ToInt16(ds.Tables[0].Rows[0]["Remaining"])).ToString();
    }

    protected void gvBiddingInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //nothing write on for GridView paging
        gvBiddingInfo.PageIndex = e.NewPageIndex;
        FillGVBiddingInfo();
    }
}