﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="GVProductApprove.aspx.cs" Inherits="DynamicPage_GVProductApprove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <h3 class="page-title">Product Approval
                    <small>For Start Auction </small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Product Approval</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->

                <div class="portlet box red-intense ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Product Approval 
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>



                    <div class="portlet-body">
                        <div class="portlet-title tools">

                            <div class="form-actions top" align="Middle">

                                <asp:Label ID="lblProductName" runat="server" Text="Product Name :" />
                                <i class="tooltips twitter-typeahead" data-original-title="Write Proper Name">

                                    <asp:TextBox ID="txtSearchProduct"
                                        runat="server"
                                        placeholder="Enter your Product Name"
                                        CssClass="form-control input-inline tt-hint twitter-typeahead" />
                                </i>



                                <asp:Button ID="btnSearchProduct"
                                    runat="server"
                                    Text="Search Product"
                                    class="btn blue" />


                            </div>
                            <br />


                            <asp:GridView ID="GVProductApprove" runat="server"
                                AllowPaging="True" ShowFooter="True"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4">

                                <Columns>

                                    <asp:TemplateField HeaderText="Approval" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="70px">
                                        <ItemStyle CssClass="gridViewRowLabel" />

                                        <ControlStyle Height="25px"></ControlStyle>
                                        <ItemTemplate>


                                            <asp:CheckBox ID="chkProductApproval"
                                                runat="server"
                                                AutoPostBack="True"
                                                OnCheckedChanged="chkProductApproval_CheckedChanged"
                                                Checked='<%#Eval("IsApproved") %>'
                                                CssClass='<%#Eval("ProductId") %>' />
                                        

                                        </ItemTemplate>
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Product Id" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductId"
                                                runat="server"
                                                Text='<%#Eval("ProductId") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Product Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName"
                                                runat="server"
                                                Text='<%#Eval("ProductName") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Auction" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName"
                                                runat="server"
                                                Text='<%#Eval("AuctionTypeName") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="View" ControlStyle-Height="30px">
                                        <ItemTemplate>
                                            <div class="form-actions">
                                                <asp:Button ID="Button1" class="btn blue" runat="server" Text="View" />

                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--                                    <asp:TemplateField HeaderText="Approval" HeaderStyle-CssClass="gridViewHead"
                                        ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle CssClass="gridViewRowLabel" />
                                        <ControlStyle Height="25px" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkApproval"
                                                runat="server"
                                                AutoPostBack="True"
                                                OnCheckedChanged="chkApproval_CheckedChanged" />
                                        </ItemTemplate>
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <HeaderStyle ForeColor="Black" />
                                <PagerStyle VerticalAlign="Middle" />
                            </asp:GridView>


                        </div>

                        <%--Grid view Body END--%>
                    </div>
                    <asp:Label ID="lblMsg"
                        runat="server"
                        CssClass="lblCenter" />
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>

