﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class DynamicPage_mstEmployee : System.Web.UI.MasterPage
{
    balEmployeeDetail balEmpObj = new balEmployeeDetail();
    boEmployeeDetail boEmpObj = new boEmployeeDetail();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["LUName"] != null)
        {
            lblUname.Text = "Hi , " + Session["LUName"].ToString();
            //imgUserProfile.ImageUrl = "~/Upload/" + Session["UImg"].ToString();
            //imgUserProfile.ImageUrl = "~/Upload/img220315191644.jpg";
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");

        }
        
    }
    protected void lbtnLogOut_Click(object sender, EventArgs e)
    {
        Session["LEmailId"] = null;
        Session["LPasswd"] = null;
        Session["LDesig"] = null;
        Session["LUID"] = null;
        Session["LUName"] = null;
        Session["MstSite"] = null;
        Response.Redirect("../StaticPage/Login.aspx");
    }
}
