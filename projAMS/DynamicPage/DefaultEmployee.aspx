﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master"  AutoEventWireup="true" CodeFile="DefaultEmployee.aspx.cs" Inherits="DynamicPage_DefaultEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  
      <div class="page-content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

          	<h3 class="page-title">
			Employee Dashboard 
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Dashboard</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt" data-placement="top" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block"></span>&nbsp; <i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE HEADER-->


      
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>
</asp:Content>

