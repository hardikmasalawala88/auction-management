﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master" AutoEventWireup="true" CodeFile="PacketDetail.aspx.cs" Inherits="DynamicPage_PacketDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="page-content">

        <h3 class="page-title">Packet Detail
                   <small>View , Add & Edit </small> 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                         <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultEmployee.aspx"  >Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Packet Detail</a>
                        </li>
                    </ul>
                    
                </div>
                <!-- END PAGE HEADER-->

   <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Packet Detail
                            
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="table_responsive.html#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>


                    <div class="portlet-body">


                    <div class="portlet-title tools">
                        
                        <asp:GridView ID="gvPacketDetail" runat="server"
                            AllowPaging="True" ShowFooter="True"
                            PageSize="10"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False"
                            GridLines="None"
                            PagerStyle-VerticalAlign="Middle"
                            HeaderStyle-ForeColor="Black"
                            CellPadding="4" ViewStateMode="Enabled"
                            ShowHeaderWhenEmpty="True"
                            OnRowCommand="gvPacketDetail_RowCommand"
                            OnRowUpdating="gvPacketDetail_RowUpdating"
                            OnRowEditing="gvPacketDetail_RowEditing"
                            OnRowCancelingEdit="gvPacketDetail_RowCancelingEdit"
                            OnPageIndexChanging="gvPacketDetail_PageIndexChanging"
                           >

                            <AlternatingRowStyle />
                            <Columns>
                                <asp:TemplateField HeaderText="PacketId" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPacketId" runat="server" Text='<%#Eval("PacketId") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                   <%-- <FooterTemplate>
                                        <asp:Label ID="lblAdd" runat="server" Text="ENTER NEW AUCTION TYPE ">
                                        </asp:Label>
                                    </FooterTemplate>--%>
                                    <ControlStyle Height="20px" />
                                    <HeaderStyle CssClass="gridViewHead" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PacketName" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPacketName" runat="server" Text='<%#Eval("PacketName") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtPacketName" 
                                            runat="server" 
                                            Text='<%#Eval("PacketName") %>'
                                             CssClass=" form-control" >
                                        </asp:TextBox>

                                         <asp:FilteredTextBoxExtender ID="FTBEtxtPacketName" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtPacketName">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvPacketName" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtPacketName" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank " ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtAddPacketName" 
                                            CssClass=" form-control" 
                                            runat="server"
                                            placeholder="PACKET NAME"
                                            >
                                        </asp:TextBox>
                                      <asp:FilteredTextBoxExtender ID="FTBEtxtPacketName" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtAddPacketName">
                                            </asp:FilteredTextBoxExtender>
                                        
                                        <asp:RequiredFieldValidator ID="rfvAddPacketName" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtAddPacketName" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                    <HeaderStyle CssClass="gridViewHead" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bids" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBids" 
                                            runat="server" 
                                            Text='<%#Eval("Bids") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TxtBids" 
                                            runat="server" 
                                            Text='<%#Eval("Bids") %>'
                                            CssClass="form-control">
                                        </asp:TextBox>

                                         <asp:FilteredTextBoxExtender ID="FTBEtxtBid" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtBids">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvTxtBids" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtBids" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank" 
                                            ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TxtAddBids" 
                                            runat="server" 
                                            CssClass="form-control"
                                            placeholder="BIDS" >
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTBEtxtBid" runat="server"
                                                FilterType="Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtAddBids">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvAddBids" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtAddBids" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                    <HeaderStyle CssClass="gridViewHead" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrice" 
                                            runat="server" 
                                            Text='<%#Eval("Price") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TxtPrice" 
                                            runat="server" 
                                            Text='<%#Eval("Price") %>'
                                            CssClass="form-control">
                                        </asp:TextBox>

                                         <asp:FilteredTextBoxExtender ID="FTBETxtPrice" runat="server"
                                                FilterType="Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtPrice">
                                            </asp:FilteredTextBoxExtender>
                                       
                                                                            
                                         <asp:RequiredFieldValidator ID="rfvTxtPrice" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtPrice" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank" 
                                            ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TxtAddPrice" 
                                            runat="server" 
                                            CssClass="form-control"
                                            placeholder="PRICE" >
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTBETxtPrice" runat="server"
                                                FilterType="Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtAddPrice">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvAddPrice" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtAddPrice" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                    <HeaderStyle CssClass="gridViewHead" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Times" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" 
                                            runat="server" 
                                            Text='<%#Eval("Times") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TxtTimes" 
                                            runat="server" 
                                            Text='<%#Eval("Times") %>'
                                            CssClass="form-control">
                                        </asp:TextBox>

                                        <asp:FilteredTextBoxExtender ID="FTBETxtTimes" runat="server"
                                                FilterType="Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtTimes">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvTxtTimes" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtTimes" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank" 
                                            ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TxtAddTimes" 
                                            runat="server" 
                                            CssClass="form-control"
                                            placeholder="Time" >
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTBETxtTimes" runat="server"
                                                FilterType="Numbers"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtAddTimes">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvAddTimes" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtAddTimes" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                    <HeaderStyle CssClass="gridViewHead" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDuration" 
                                            runat="server" 
                                            Text='<%#Eval("Duration") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TxtDuration" 
                                            runat="server" 
                                            Text='<%#Eval("Duration") %>'
                                            CssClass="form-control">
                                        </asp:TextBox>


                                         <asp:FilteredTextBoxExtender ID="FTBETxtDuration" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtDuration">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvTxtDuration" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtDuration" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank" 
                                            ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TxtAddDuration" 
                                            runat="server" 
                                            CssClass="form-control"
                                            placeholder="DURATION" >
                                        </asp:TextBox>
                                       <asp:FilteredTextBoxExtender ID="FTBETxtDuration" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="TxtAddDuration">
                                            </asp:FilteredTextBoxExtender>
                                       
                                        <asp:RequiredFieldValidator ID="rfvAddDuration" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="TxtAddDuration" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                    <HeaderStyle CssClass="gridViewHead" />
                                </asp:TemplateField>




                                <asp:TemplateField HeaderText="Edit" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgBtnEdit" 
                                            runat="server" 
                                            ImageUrl="~/DynamicPage/img/edit-grid.png"
                                            AlternateText="Edit" 
                                            Height="25px" 
                                            Width="30px" 
                                            CausesValidation="false" 
                                            ToolTip="Edit" 
                                            CommandName="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton ID="btnUpdate" 
                                            ImageUrl="~/DynamicPage/img/update.png" 
                                            ToolTip="Update" 
                                            AlternateText="Update"
                                            runat="server" 
                                            CommandName="Update" 
                                            ValidationGroup="EditValid" />
                                        <asp:ImageButton ID="btnCancel" 
                                            ImageUrl="~/DynamicPage/img/cancel.png" 
                                            ToolTip="Cancle" 
                                            runat="server"
                                            AlternateText="Cancel" 
                                            CommandName="Cancel" 
                                            CausesValidation="false" />
                                    </EditItemTemplate>
                                    <FooterTemplate>

                                        <asp:Button ID="btnAddRecord" 
                                            runat="server" 
                                            Text="Add" 
                                            CommandName="Add"
                                            OnClick="btnAddRecord_Click"></asp:Button>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle VerticalAlign="Middle" />
                                    <ControlStyle Height="25px"></ControlStyle>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle ForeColor="Black" />
                            <PagerStyle VerticalAlign="Middle" />
                        </asp:GridView>
                        <asp:Label ID="lblMsg" runat="server" />
                    </div>


                </div>
      <%--  </contenttemplate>
    </asp:updatepanel>--%>
    </div>
</asp:Content>

