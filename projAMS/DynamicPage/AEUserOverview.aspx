﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master" AutoEventWireup="true" CodeFile="AEUserOverview.aspx.cs" Inherits="DynamicPage_AEOverview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="page-content" style="min-height: 857px">

        <h3 class="page-title">User Account <small>Detail about Auction</small>
        </h3>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row margin-top-20">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <asp:Image ID="imgUser" runat="server" ImageUrl="../images/DefultUser.jpg" class="img-responsive" alt="" />
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                <asp:Label runat="server" ID="lbluname" />
                            </div>
                            <div class="profile-usertitle-job">
                                <asp:Label runat="server" ID="lblDesig" />
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="AEUserProfileOverView.aspx">
                                        <i class="icon-home"></i>
                                        Overview </a>
                                </li>
                                <li>
                                    <a href="AEUserProfile.aspx">
                                        <i class="icon-settings"></i>
                                        Account Settings </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    <div class="portlet light">
                        <!-- STAT -->
                        <div class="row list-separated profile-stat">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title">
                                    <asp:Label runat="server" ID="lblRunning" Text="50000" />
                                </div>
                                <div class="uppercase profile-stat-text">
                                    Running Auction
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title">
                                    <asp:Label runat="server" ID="lblUPComming" Text="50000" />
                                </div>
                                <div class="uppercase profile-stat-text">
                                    UP Comming
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title">
                                    <asp:Label runat="server" ID="lblFinished" Text="50000" />
                                </div>
                                <div class="uppercase profile-stat-text">
                                    Finished Auction
                                </div>
                            </div>
                        </div>
                        <!-- END STAT -->
                        <asp:Panel runat="server" ID="pnlPackage">
                            <div>
                                <h4 class="profile-desc-title">Purchase Package </h4>
                                <span class="profile-desc-text">If your running with low bid then quickly purchase package. </span>
                                <div class="margin-top-20 profile-desc-link">

                                    <a href="../StaticPage/GVBuyPackge.aspx">BuyPackage</a>
                                </div>

                            </div>
                        </asp:Panel>
                    </div>
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN PORTLET -->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption caption-md">
                                        <i class="icon-bar-chart theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Biding Information</span>

                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row number-stats margin-bottom-30">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="stat-left">
                                                <div class="stat-chart">
                                                    <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                    <div id="sparkline_bar">
                                                    </div>
                                                </div>
                                                <div class="stat-number">
                                                    <div class="title">
                                                        <asp:Label runat="server" ID="lblName1" Text="Remaining Bid" />
                                                    </div>
                                                    <div class="number">
                                                        <asp:Label runat="server" ID="lblValue1" Text="10000000" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="stat-right">
                                                <div class="stat-chart">
                                                    <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                    <div id="sparkline_bar2">
                                                    </div>
                                                </div>
                                                <div class="stat-number">
                                                    <div class="title">
                                                        <asp:Label runat="server" ID="lblName2" Text="Bid Deducted" />
                                                    </div>
                                                    <div class="number">
                                                        <asp:Label runat="server" ID="lblValue2" Text="10000" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="table-scrollable table-scrollable-borderless">--%>
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvBiddingInfo" OnPageIndexChanging="gvBiddingInfo_PageIndexChanging"
                                                runat="server"
                                                PageSize="5"
                                                BorderWidth ="2"
                                                BorderColor ="Green" 
                                                CssClass="table table-striped table-bordered table-hover"
                                                AllowPaging="true">
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <%--<table class="table table-hover table-light">
                                            <thead>
                                                <tr class="uppercase">
                                                    <th colspan="2">MEMBER
                                                    </th>
                                                    <th>Earnings
                                                    </th>
                                                    <th>CASES
                                                    </th>
                                                    <th>CLOSED
                                                    </th>
                                                    <th>RATE
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="fit">
                                                        <img class="user-pic" src="assets/admin/layout3/img/avatar4.jpg">
                                                    </td>
                                                    <td>
                                                        <a href="#" class="primary-link">Brain</a>
                                                    </td>
                                                    <td>$345
                                                    </td>
                                                    <td>45
                                                    </td>
                                                    <td>124
                                                    </td>
                                                    <td>
                                                        <span class="bold theme-font">80%</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fit">
                                                        <img class="user-pic" src="assets/admin/layout3/img/avatar5.jpg">
                                                    </td>
                                                    <td>
                                                        <a href="#" class="primary-link">Nick</a>
                                                    </td>
                                                    <td>$560
                                                    </td>
                                                    <td>12
                                                    </td>
                                                    <td>24
                                                    </td>
                                                    <td>
                                                        <span class="bold theme-font">67%</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fit">
                                                        <img class="user-pic" src="assets/admin/layout3/img/avatar6.jpg">
                                                    </td>
                                                    <td>
                                                        <a href="#" class="primary-link">Tim</a>
                                                    </td>
                                                    <td>$1,345
                                                    </td>
                                                    <td>450
                                                    </td>
                                                    <td>46
                                                    </td>
                                                    <td>
                                                        <span class="bold theme-font">98%</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fit">
                                                        <img class="user-pic" src="assets/admin/layout3/img/avatar7.jpg">
                                                    </td>
                                                    <td>
                                                        <a href="#" class="primary-link">Tom</a>
                                                    </td>
                                                    <td>$645
                                                    </td>
                                                    <td>50
                                                    </td>
                                                    <td>89
                                                    </td>
                                                    <td>
                                                        <span class="bold theme-font">58%</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>--%>
                                    <%--</div>--%>
                                </div>
                            </div>
                            <!-- END PORTLET -->
                        </div>

                        <div class="col-md-6">
                            <!-- BEGIN PORTLET -->
                            <div class="portlet light">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Package Purchase Detail</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <!--BEGIN TABS-->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1_1">
                                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 320px;">
                                                <div class="scroller" style="height: 320px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2" data-initialized="1">

                                                    <asp:GridView runat="server"
                                                        ID="gvPackagePurchase"
                                                        CssClass="table table-striped table-bordered table-hover">
                                                    </asp:GridView>

                                                </div>
                                                <div class="slimScrollBar" style="background-color: rgb(215, 220, 226); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 151.25553914327918px; background-position: initial initial; background-repeat: initial initial;"></div>
                                                <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <!--END TABS-->
                                </div>
                            </div>
                            <!-- END PORTLET -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <!-- BEGIN Portlet PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-paper-plane font-yellow-casablanca"></i>
                                    <span class="caption-subject bold font-yellow-casablanca uppercase">Order Detail </span>
                                    <span class="caption-helper"></span>
                                </div>
                                <div class="inputs">
                                    <div class="portlet-input input-inline input-medium">
                                        <div class="input-group">
                                            <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control input-circle-left" placeholder="search..." />
                                            <%--<input class="form-control input-circle-left" placeholder="search..." type="text">--%>
                                            <span class="input-group-btn">
                                                <asp:Button runat="server" ID="btnSearch" Text="GO" CssClass="btn btn-circle-right btn-default" />
                                                <%--<button class="btn btn-circle-right btn-default" type="submit">Go!</button>--%>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <asp:GridView ID="gvOrderDetail" runat="server"></asp:GridView>
                            </div>
                        </div>
                        <!-- END Portlet PORTLET-->
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <%--

    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Profile.init(); // init page demo
    });
</script>
<!-- END JAVASCRIPTS -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-37564768-1', 'keenthemes.com');
    ga('send', 'pageview');
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-37564768-1', 'keenthemes.com');
    ga('send', 'pageview');
</script>--%>
</asp:Content>

