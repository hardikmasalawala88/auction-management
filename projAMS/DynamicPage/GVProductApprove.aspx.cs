﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using DAL;


public partial class DynamicPage_GVProductApprove : System.Web.UI.Page
{
    balProductApprove balObjProdApprove = new balProductApprove();
    boAddProduct boObj = new boAddProduct();
    DataSet ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {

            fillGrid();
        }
    }
    protected void fillGrid()
    {
        GVProductApprove.DataSource = balObjProdApprove.GetProductApprove();
        GVProductApprove.DataBind();
    }
    protected void chkProductApproval_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            CheckBox chk = (CheckBox)sender;
            int eId = Convert.ToInt32(chk.CssClass.ToString());
            Session["ProductId"] = eId;
            boObj.ProductId = Convert.ToInt32(eId);
            if (chk.Checked == true)
            {
                boObj.IsApproved = Convert.ToInt16(true);

                ds = new DataSet();
                Boolean success = balObjProdApprove.Approval(boObj);
                if (success == true)
                {
                    //ds = new DataSet();
                    //ds = balObjProdApprove.getEmail(boObj);
                    //string EmpName = ds.Tables[0].Rows[0][1].ToString();
                    //string email = ds.Tables[0].Rows[0][0].ToString();
                                       
                    //string t = "Dear " + EmpName + ",<br />" + "<br />"
                    //    + "       Welcome To Our WorldAuction , We Received Your Application Of Sell your Product.<br />"
                    //    + "It is Our Pleasure To Inform You That Your Application Is Approved And" + "<br />"
                    //    + "Now Your Product is on Bidding List "
                    //    + "Thank You For Joining Our Team.";

                    //clsUtility.SendMail(email, "Your Product Detail", t);
                   


                    ////FillGrid();
                    //lblMsg.Text = "Sent Message";
                }
            }

            else
            {
               
            }
        }
        catch { }

    }
}