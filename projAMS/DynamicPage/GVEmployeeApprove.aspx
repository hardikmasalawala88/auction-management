﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="GVEmployeeApprove.aspx.cs" Inherits="DynamicPage_GVEmployeeApprove" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
        x<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <h3 class="page-title">Employee Approval
                    <small>For activating Employee </small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Employee</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->

                <div class="portlet box red-intense ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Employee Approval 
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>



                    <div class="portlet-body">
                        <div class="portlet-title tools">

                            <div class="form-actions top" align="Middle">

                                <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name :" />
                                <i class="tooltips twitter-typeahead" data-original-title="Write Proper Name">
                                    
                                    <asp:TextBox ID="txtSearchEmployee"
                                        runat="server"
                                        placeholder="Enter your Name"
                                        CssClass="form-control input-inline tt-hint twitter-typeahead" />
                                </i>
                                <asp:filteredtextboxextender id="FTBtxtSearchEmployee" runat="server"
                                                filtertype="lowercaseletters,uppercaseletters,custom"
                                                filtermode="validchars"
                                                validchars=" "
                                                targetcontrolid="txtSearchEmployee">
                                            </asp:filteredtextboxextender>
                                       

                                <asp:Button ID="btnSearchEmployee"
                                    runat="server"
                                    Text="Search"
                                    class="btn blue" OnClick="btnSearchEmployee_Click" />


                            </div>
                            <br />
                            <%--Grid view Start--%>
                            <%--     <asp:GridView ID="GVEmployeeReg1" runat="server"
                        AllowPaging="True" ShowFooter="True"
                        PageSize="10"
                        CssClass="table table-striped table-bordered table-hover"
                        AutoGenerateColumns="False"
                        GridLines="None"
                        PagerStyle-VerticalAlign="Middle"
                        HeaderStyle-ForeColor="Black"
                        CellPadding="4" 
                        OnPageIndexChanging="GVEmployeeReg1_PageIndexChanging" 
                        OnSelectedIndexChanging="GVEmployeeReg1_SelectedIndexChanging" >

                        <Columns>

                            <asp:TemplateField HeaderText="Employee Id" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeId"
                                        runat="server"
                                        Text='<%#Eval("EmpId") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Employee Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName"
                                        runat="server" Text='<%#Eval("EmpName") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Contact No." ControlStyle-Height="20px" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactNo"
                                        runat="server"
                                        Text='<%#Eval("ContactNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email-Id" ControlStyle-Height="20px">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailId"
                                        runat="server"
                                        Text='<%#Eval("EmailId") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Approval" HeaderStyle-CssClass="gridViewHead"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemStyle CssClass="gridViewRowLabel" />
                                <ItemTemplate>
                                    <asp:Label ID="lblApproval" runat="server" Text='<%#Eval("IsActive") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ControlStyle Height="25px"></ControlStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkApproval" 
                                        runat="server" 
                                        
                                        AutoPostBack="True" 
                                        OnCheckedChanged="chkApproval_CheckedChanged" 
                                        Checked='<%#Eval("IsActive") %>' />
                                </ItemTemplate>
                                <ControlStyle Height="25px"></ControlStyle>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle ForeColor="Black" />
                        <PagerStyle VerticalAlign="Middle" />
                    </asp:GridView>--%>

                            <asp:GridView ID="GVEmployeeReg1" runat="server"
                                AllowPaging="True" ShowFooter="True"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4">

                                <Columns>

                                    <asp:TemplateField HeaderText="Approval" HeaderStyle-CssClass="gridViewHead"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="70px">
                                        <ItemStyle CssClass="gridViewRowLabel" />
                                        <%--                                <ItemTemplate>
                                    <asp:Label ID="lblApproval" runat="server" Text='<%#Eval("IsActive") %>'>
                                    </asp:Label>
                                </ItemTemplate>--%>
                                        <ControlStyle Height="25px"></ControlStyle>
                                        <ItemTemplate>


                                            <asp:CheckBox ID="chkApproval"
                                                runat="server"
                                                AutoPostBack="True"
                                                OnCheckedChanged="chkApproval_CheckedChanged"
                                                Checked='<%#Eval("IsActive") %>'
                                                CssClass='<%#Eval("EmpId") %>' />


                                        </ItemTemplate>
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Id" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeId"
                                                runat="server"
                                                Text='<%#Eval("EmpId") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeName"
                                                runat="server"
                                                Text='<%#Eval("EmpName") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Contact No." ControlStyle-Height="20px" ItemStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContactNo"
                                                runat="server"
                                                Text='<%#Eval("ContactNo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email-Id" ControlStyle-Height="20px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmailId"
                                                runat="server"
                                                Text='<%#Eval("EmailId") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ControlStyle-Height="30px">
                                        <ItemTemplate>
                                            <div class="form-actions">
                                               <asp:Button ID="Button1" class="btn blue" runat="server" Text="View" />
                                                
                                            </div>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--                                    <asp:TemplateField HeaderText="Approval" HeaderStyle-CssClass="gridViewHead"
                                        ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle CssClass="gridViewRowLabel" />
                                        <ControlStyle Height="25px" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkApproval"
                                                runat="server"
                                                AutoPostBack="True"
                                                OnCheckedChanged="chkApproval_CheckedChanged" />
                                        </ItemTemplate>
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <HeaderStyle ForeColor="Black" />
                                <PagerStyle VerticalAlign="Middle" />
                            </asp:GridView>


                        </div>

                        <%--Grid view Body END--%>
                    </div>
                    <asp:Label ID="lblMsg"
                        runat="server"
                        CssClass="lblCenter" />
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>

