﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class GVCategoryTypeMst : System.Web.UI.Page
{
    boCategoryTypeMst boObj = new boCategoryTypeMst();
    balCategoryTypeMst balObj = new balCategoryTypeMst();
    DataSet ds = new DataSet();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();
            //if (Session["Uid"] == null)
            //{
            //    Response.Redirect("LogIn.aspx");
            //}
            //else
            //{
            //    FillGrid();
            //}
        }
    }
    protected void FillGrid()
    {
       
        GVCategoryTypeMst1.DataSource = balObj.Load();
        GVCategoryTypeMst1.DataBind();
    }
    protected void GVCategoryTypeMst1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //when record more than page then automatically go in next page
        GVCategoryTypeMst1.PageIndex = e.NewPageIndex;
        FillGrid();
    }
    protected void GVCategoryTypeMst1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
       //when you press cancel then data not change for that it use
        GVCategoryTypeMst1.EditIndex = -1;
        FillGrid();
    }
    protected void GVCategoryTypeMst1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Row Command give command what to do add or other

        if (e.CommandName.Equals("Add"))
        {
           //text box object is created txtAddColor is footer textbox that valu fetch and assign in object TxtColorName
            TextBox TxtCategoryType = (TextBox)GVCategoryTypeMst1.FooterRow.FindControl("txtAddCategoryType");


            boObj.CategoryTypeId = 0;
            boObj.CategoryType = TxtCategoryType.Text;

            //insert is method in balCategoryTypeMst.cs 
            if (balObj.Insert(boObj))
            {
                lblMsg.Text = "Successfuly Save.";
            }
            else
            {
                lblMsg.Text = "Unsuccessfuly Save.";
            }
            FillGrid();
        }


    }
    protected void GVCategoryTypeMst1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //which row you need to update that ID give
        GVCategoryTypeMst1.EditIndex = e.NewEditIndex;
        FillGrid ();
    }
    protected void GVCategoryTypeMst1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //for updateing
        Label CategoryTypeId = (Label)GVCategoryTypeMst1.Rows [e.RowIndex].FindControl("lblCategoryTypeId");
        TextBox TxtCategoryType = (TextBox)GVCategoryTypeMst1.Rows[e.RowIndex].FindControl("txtCategoryType");

        boObj.CategoryTypeId = Convert.ToInt16(CategoryTypeId.Text);
        boObj.CategoryType = TxtCategoryType.Text;

        //insert is method in balCategoryTypeMst.cs 
        if (balObj.Insert(boObj))
        {
            lblMsg.Text = "Successfuly Save.";
        }
        else
        {
            lblMsg.Text = "Unsuccessfuly Save.";
        }
        GVCategoryTypeMst1.EditIndex = -1;
        FillGrid();
    }
}
