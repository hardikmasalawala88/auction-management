﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstEmployee.master" AutoEventWireup="true" CodeFile="AddNewProduct.aspx.cs" Inherits="DynamicPage_AddNewProduct" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" type="text/css" href="assets/Clock/bootstrap-clockpicker.min.css" />



    <div class="page-content">


        <div class="portlet light bordered ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-equalizer font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Add Your Product</span>
                    <span class="caption-helper">For selling write perfact descrption of your product </span>
                </div>
                <div class="tools">
                    <a href="#" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="#" class="reload"></a>
                    <a href="#" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <%--  <form action="#" class="form-horizontal">--%>
                <div class="form-body">
                    <h3 class="form-section">Basic Product Info</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblProductName"
                                    class="control-label col-md-3"
                                    Text="Product Name" />
                                <div class="col-md-9">

                                    <asp:TextBox runat="server"
                                        ID="txtProductName"
                                        class="form-control input-circle-right"
                                        placeholder="Enter Product Name" />

                                    <span class="help-block">For Ex. HTC Desire Eye </span>

                                    <asp:FilteredTextBoxExtender ID="FTBEtxtProductName" runat="server"
                                        FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtProductName">
                                    </asp:FilteredTextBoxExtender>

                                    <asp:RequiredFieldValidator ID="RFVName" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtProductName"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter your product name">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblPrice"
                                    class="control-label col-md-3"
                                    Text="Price " />

                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtPrice"
                                        class="form-control input-circle-right"
                                        placeholder="Final Price " />

                                    <span class="help-block">Enter price in RS. </span>


                                    <asp:FilteredTextBoxExtender ID="FTBEtxtPrice" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtPrice">
                                    </asp:FilteredTextBoxExtender>

                                    <asp:RequiredFieldValidator ID="RFVPrice" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtPrice"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Price">
                                    </asp:RequiredFieldValidator>
                                </div>

                            </div>
                            <!--/span-->
                        </div>
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="Label1"
                                    class="control-label col-md-3"
                                    Text="Product Condition" />

                                <div class="col-md-9">

                                    <asp:DropDownList ID="ddlProductCondition" runat="server" class="select2me form-control input-circle-right">
                                        <asp:ListItem Value="Old">Old</asp:ListItem>
                                        <asp:ListItem Value="New">New</asp:ListItem>
                                        <asp:ListItem Value="Historical">Historical</asp:ListItem>
                                    </asp:DropDownList>

                                    <span class="help-block">For used product condition is old </span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblQty"
                                    class="control-label col-md-3"
                                    Text="Quantity " />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtQty"
                                        class="form-control input-circle-right"
                                        placeholder="Number of product you have " />


                                    <asp:FilteredTextBoxExtender ID="FTBEtxtQty" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtQty">
                                    </asp:FilteredTextBoxExtender>

                                    <asp:RequiredFieldValidator ID="RFVQty" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtQty"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Quantity">

                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblShippingCharge"
                                    class="control-label col-md-3"
                                    Text="Shipping Charges " />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtShippingCharge"
                                        class="form-control input-circle-right"
                                        placeholder="Enter amount" />

                                    <span class="help-block">Enter Amount in Rs. </span>


                                    <asp:FilteredTextBoxExtender ID="FTBEtxtShippingCharge" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtShippingCharge">
                                    </asp:FilteredTextBoxExtender>

                                    <asp:RequiredFieldValidator ID="RFVShipping" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtShippingCharge"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Shipping Charge">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="ShippingDesc"
                                    class="control-label col-md-3"
                                    Text="Shipping Description" />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtShippingDesc"
                                        class="form-control input-circle-right"
                                        TextMode="MultiLine"
                                        placeholder="Short Description about Shipping " />

                                    <asp:FilteredTextBoxExtender ID="FTBEtxtShippingDesc" runat="server"
                                        FilterType="LowercaseLetters,custom,Numbers,UppercaseLetters"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtShippingDesc">
                                    </asp:FilteredTextBoxExtender>

                                    <asp:RequiredFieldValidator ID="RFVShippingDesc" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtShippingDesc"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Shipping Description">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="Label2"
                                    class="control-label col-md-3"
                                    Text="Main Category Type" />

                                <div class="col-md-9">

                                    <asp:DropDownList ID="ddlMainCategoryId" runat="server"
                                        class="select2me form-control input-circle-right"
                                        AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlMainCategoryId_SelectedIndexChanged" />

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblCategoryId"
                                    class="control-label col-md-3"
                                    Text="Category Type" />

                                <div class="col-md-9">

                                    <asp:DropDownList ID="ddlCategoryId" runat="server"
                                        class="select2me form-control input-circle-right"
                                        AutoPostBack="True">
                                    </asp:DropDownList>


                                    <span class="help-block">For EX. HTC Desire Eye in mobile Category </span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblProductDesc"
                                    class="control-label col-md-3"
                                    Text="Product Description" />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtProductDesc"
                                        class="form-control input-circle-right"
                                        TextMode="MultiLine"
                                        placeholder="Short Detail About Product" />

                                    <%--<asp:FilteredTextBoxExtender ID="FTBEtxtProductDesc" runat="server"
                                        FilterType="LowercaseLetters,custom,Numbers,UppercaseLetters"
                                        FilterMode="ValidChars"
                                        ValidChars=" -@$%^&*()"
                                        TargetControlID="txtProductDesc">
                                    </asp:FilteredTextBoxExtender>--%>


                                    <asp:RequiredFieldValidator ID="RFVProduct" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtProductDesc"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Product Description">
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>

                    <asp:Panel runat="server" ID="pnlProductMainDetail" Visible="false">
                        <h3 class="form-section">About Product</h3>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblAdditionalInfo"
                                        class="control-label col-md-3"
                                        Text="Additional Information" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtAdditionalInfo"
                                            class="form-control input-circle-right"
                                            TextMode="MultiLine"
                                            placeholder="More Information about product" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblWarranty"
                                        class="control-label col-md-3"
                                        Text="Warranty" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtWarranty"
                                            class="form-control input-circle-right"
                                            TextMode="MultiLine"
                                            placeholder="Warrenty of product" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblFeaturesAndBenefits"
                                        class="control-label col-md-3"
                                        Text="Features And Benefits" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtFeaturesAndBenefits"
                                            class="form-control input-circle-right"
                                            TextMode="MultiLine"
                                            placeholder="Features And Benefits of product" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblCompanyName"
                                        class="control-label col-md-3"
                                        Text="Company Name" />
                                    <div class="col-md-9">

                                        <asp:DropDownList ID="ddlCompanyName" runat="server" class="select2me form-control input-circle-right">
                                            <asp:ListItem Value="Apple">Apple</asp:ListItem>
                                            <asp:ListItem Value="Sumsung">Sumsung</asp:ListItem>
                                            <asp:ListItem Value="HTC">HTC</asp:ListItem>
                                            <asp:ListItem Value="Nokia">Nokia</asp:ListItem>
                                            <asp:ListItem Value="LG">LG</asp:ListItem>
                                            <asp:ListItem Value="Asus">Asus</asp:ListItem>
                                            <asp:ListItem Value="Intex">Intex</asp:ListItem>
                                            <asp:ListItem Value="Gionee">Gionee</asp:ListItem>
                                            <asp:ListItem Value="Motorola">Motorola</asp:ListItem>
                                            <asp:ListItem Value="Appo">Appo</asp:ListItem>
                                            <asp:ListItem Value="Vivo">Vivo</asp:ListItem>
                                            <asp:ListItem Value="Others">Others</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblModelNo"
                                        class="control-label col-md-3"
                                        Text="Model Name" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtModelNo"
                                            class="form-control input-circle-right"
                                            placeholder="Model Name For Ex. Dell Vostro" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblColor"
                                        class="control-label col-md-3"
                                        Text="Color" />
                                    <div class="col-md-9">
                                        <asp:DropDownList ID="ddlColor" runat="server" class="select2me form-control input-circle-right">
                                            <asp:ListItem Value="White">White</asp:ListItem>
                                            <asp:ListItem Value="Red">Red</asp:ListItem>
                                            <asp:ListItem Value="Green">Green</asp:ListItem>
                                            <asp:ListItem Value="Black">Black</asp:ListItem>
                                            <asp:ListItem Value="SentoriniWhite">SentoriniWhite</asp:ListItem>
                                            <asp:ListItem Value="Golden">Golden</asp:ListItem>
                                            <asp:ListItem Value="Silver">Silver</asp:ListItem>
                                            <asp:ListItem Value="Orange">Orange</asp:ListItem>
                                            <asp:ListItem Value="Yellow">Yellow</asp:ListItem>
                                            <asp:ListItem Value="Pink">Pink</asp:ListItem>
                                            <asp:ListItem Value="Purple">Purple</asp:ListItem>
                                            <asp:ListItem Value="Blue">Blue</asp:ListItem>
                                            <asp:ListItem Value="Gray">Gray</asp:ListItem>


                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblOS"
                                        class="control-label col-md-3"
                                        Text="OSName" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtOS"
                                            class="form-control input-circle-right"
                                            TextMode="MultiLine"
                                            placeholder="Name of Operating System" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblProcessor"
                                        class="control-label col-md-3"
                                        Text="Processor" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server" ID="txtProcessor"
                                            class="form-control input-circle-right"
                                            placeholder="Processor of device" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblOSVersion"
                                        class="control-label col-md-3"
                                        Text="OSVersion" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtOSVersion"
                                            class="form-control input-circle-right"
                                            placeholder="Version of OS" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblRAM"
                                        class="control-label col-md-3"
                                        Text="RAM" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtRAM"
                                            class="form-control input-circle-right"
                                            TextMode="MultiLine"
                                            placeholder="Enter Ram" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblSizeInch"
                                        class="control-label col-md-3"
                                        Text="Device Size" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtSizeInch"
                                            class="form-control input-circle-right"
                                            placeholder="Size of your device" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblResolution"
                                        class="control-label col-md-3"
                                        Text="Resolution" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtResolution"
                                            class="form-control input-circle-right"
                                            placeholder="Device Resoloution " />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblPrimaryCamera"
                                        class="control-label col-md-3"
                                        Text="PrimaryCamera" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtPrimaryCamera"
                                            class="form-control input-circle-right"
                                            placeholder="Front Camera if exists" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblSecondaryCamera"
                                        class="control-label col-md-3"
                                        Text="SecondaryCamera" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtSecondaryCamera"
                                            class="form-control input-circle-right"
                                            placeholder="Rear Camera" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblScreenType"
                                        class="control-label col-md-3"
                                        Text="ScreenType" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtScreenType"
                                            class="form-control input-circle-right"
                                            placeholder="Screen Type of device" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblConnectivity"
                                        class="control-label col-md-3"
                                        Text="Connectivity" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtConnectivity"
                                            class="form-control input-circle-right"
                                            placeholder="For EX. Blutooth,Wifi" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblStorage"
                                        class="control-label col-md-3"
                                        Text="Storage" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtStorage"
                                            class="form-control input-circle-right"
                                            placeholder="Memory Capacity" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblSpeaker"
                                        class="control-label col-md-3"
                                        Text="Speaker" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtSpeaker"
                                            class="form-control input-circle-right"
                                            placeholder="About Music System" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblBattery"
                                        class="control-label col-md-3"
                                        Text="Battery" />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtBattery"
                                            class="form-control input-circle-right"
                                            placeholder="Battery Capablity" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!--/row-->

                </div>
                <asp:Panel runat="server" ID="pnlAuctionDetail">
                    <h3 class="form-section">Auction Detail Set</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblAuctionTypeId"
                                    class="control-label col-md-3"
                                    Text="AuctionType " />
                                <div class="col-md-9">
                                    <asp:DropDownList ID="ddlAuctionTypeId"
                                        runat="server"
                                        class="select2me form-control input-circle-right input-medium"
                                        AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlAuctionTypeId_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblMinAmt"
                                    class="control-label col-md-3"
                                    Text="Minimum Amount" />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtMinAmt"
                                        class="form-control input-circle-right input-medium"
                                        placeholder="Intial amount for auction" />


                                    <asp:FilteredTextBoxExtender ID="FTEBtxtMinAmt" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtMinAmt">
                                    </asp:FilteredTextBoxExtender>



                                    <asp:RequiredFieldValidator ID="RFVMinAmt" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtMinAmt"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Minimum Amount">
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UPdate" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <asp:Label runat="server"
                                            ID="lblStartDt"
                                            class="control-label col-md-3"
                                            Text="Start Date " />
                                        <div class="col-md-9">
                                            <asp:Label runat="server" ID="lblVStartDt"
                                                class="form-control input-circle-right input-medium" Visible="false" />
                                            <asp:TextBox runat="server"
                                                ID="txtStartDt"
                                                MaxLength="10"
                                                AutoPostBack="true"
                                                class="form-control input-circle-right input-medium"
                                                placeholder="When You want to start auction"
                                                 />
                                            <%--   <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        FilterMode="ValidChars"
                                        ValidChars=""
                                        TargetControlID="txtStartDt">
                                    </asp:FilteredTextBoxExtender>--%>

                                            <asp:CalendarExtender ID="calStartDt"
                                                runat="server"
                                                TargetControlID="txtStartDt"
                                                Format="dd-MM-yyyy">
                                            </asp:CalendarExtender>
                                            <asp:RegularExpressionValidator
                                                ID="RegularExpressionValidator1"
                                                runat="server"
                                                ControlToValidate="txtStartDt"
                                                ValidationExpression="^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$"
                                                ErrorMessage="Please Enter Valid Date"></asp:RegularExpressionValidator>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Label runat="server"
                                            ID="lblEndDt"
                                            class="control-label col-md-3"
                                            Text="End Date" />

                                        <div class="col-md-9">
                                            <asp:Label runat="server" ID="lblVEndDt"
                                                class="form-control input-circle-right input-medium" Visible="false" />
                                            <asp:TextBox runat="server" ID="txtEndDt"
                                                MaxLength="10"
                                                class="form-control input-circle-right input-medium"
                                                placeholder="When You want to finish auction" />
                                            <asp:CalendarExtender ID="calEndDt" runat="server"
                                                TargetControlID="txtEndDt"
                                                Format="dd-MM-yyyy" />
                                            <asp:CompareValidator runat="server" ControlToValidate="txtEndDt"
                                                ControlToCompare="txtStartDt"
                                                Operator="GreaterThan"
                                                ErrorMessage="Please Select Date Greter then start date" />
                                            <asp:RegularExpressionValidator
                                                ID="RegularExpressionValidator2"
                                                runat="server"
                                                ControlToValidate="txtEndDt"
                                                ValidationExpression="^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$"
                                                ErrorMessage="Please Enter Valid Date"></asp:RegularExpressionValidator>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblStartTime"
                                    class="control-label col-md-3"
                                    Text="Start Time" />
                                <div class="col-md-9 ">
                                    <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                        <asp:Label runat="server" ID="lblVStartTime"
                                            class="form-control input-circle-right input-medium" Visible="false" />
                                        <asp:TextBox runat="server"
                                            ID="txtStartTime"
                                            class="form-control input-circle-right input-medium"
                                            placeholder="Start time of auction " />


                                        <%--                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                            FilterMode="ValidChars"
                                            ValidChars=" "
                                            TargetControlID="txtStartTime">
                                        </asp:FilteredTextBoxExtender>--%>
                                    </div>


                                    <%--  <asp:TextBox runat="server" ID="txtStartTime"
                                                class="single-input form-control"
                                                placeholder="Auction start time" />--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblEndTime"
                                    class="control-label col-md-3"
                                    Text="End Time" />
                                <div class="col-md-9">
                                    <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                        <asp:Label runat="server" ID="lblVEndTime"
                                            class="form-control input-circle-right input-medium" Visible="false" />
                                        <asp:TextBox runat="server"
                                            ID="txtEndTime"
                                            class="form-control input-circle-right input-medium"
                                            placeholder="Ending time of auction" />

                                        <%--   <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                            FilterMode="ValidChars"
                                            ValidChars=" "
                                            TargetControlID="txtEndTime">
                                        </asp:FilteredTextBoxExtender>--%>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblBuyNowAmt"
                                    class="control-label col-md-3"
                                    Text="Buy Now Amount " />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtBuyNowAmt"
                                        class="form-control input-circle-right input-medium"
                                        placeholder="For Directly Buying" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtBuyNowAmt">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtBuyNowAmt"
                                        ForeColor="Red"
                                        ErrorMessage="Please Enter Amount for directly Buy">
                                    </asp:RequiredFieldValidator>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server"
                                    ID="lblBidMulti"
                                    class="control-label col-md-3"
                                    Text="Bid "
                                    Visible="false" />
                                <div class="col-md-9">
                                    <asp:TextBox runat="server"
                                        ID="txtBidMulti"
                                        class="form-control input-circle-right input-medium"
                                        placeholder="For EX. 1Bid=3Bid"
                                        Visible="false"
                                        MaxLength="1" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                        FilterType="Numbers"
                                        FilterMode="ValidChars"
                                        ValidChars=" "
                                        TargetControlID="txtBidMulti">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic"
                                        runat="server"
                                        ControlToValidate="txtBuyNowAmt"
                                        ForeColor="Red"
                                        ErrorMessage="Please enter how many bid deduct ">
                                    </asp:RequiredFieldValidator>

                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="pnlCharity" Visible="false">
                        <h3 class="form-section">Charity Auction Detail</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblCharityACBank"
                                        class="control-label col-md-3"
                                        Text="Charity Bank Name " />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtCharityACBank"
                                            class="form-control input-circle-right input-medium"
                                            placeholder="Bank Name" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                            FilterType="Numbers"
                                            FilterMode="ValidChars"
                                            ValidChars=" "
                                            TargetControlID="txtBidMulti">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic"
                                            runat="server"
                                            ControlToValidate="txtCharityACBank"
                                            ForeColor="Red"
                                            ErrorMessage="Please enter Charity Bank Name  ">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblCharityACName"
                                        class="control-label col-md-3"
                                        Text="Charity A/C Name " />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtCharityACName"
                                            class="form-control input-circle-right input-medium"
                                            placeholder="" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                            FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                            FilterMode="ValidChars"
                                            ValidChars=" "
                                            TargetControlID="txtCharityACName">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic"
                                            runat="server"
                                            ControlToValidate="txtCharityACName"
                                            ForeColor="Red"
                                            ErrorMessage="Please enter Charity AC Name  ">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server"
                                        ID="lblCharityACNO"
                                        class="control-label col-md-3"
                                        Text="Charity A/C No." />
                                    <div class="col-md-9">
                                        <asp:TextBox runat="server"
                                            ID="txtCharityACNO"
                                            class="form-control input-circle-right input-medium"
                                            placeholder="" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                            FilterType="Numbers"
                                            FilterMode="ValidChars"
                                            ValidChars=" "
                                            TargetControlID="txtCharityACNO">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic"
                                            runat="server"
                                            ControlToValidate="txtCharityACNO"
                                            ForeColor="Red"
                                            ErrorMessage="Please enter Charity AC Number  ">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <h3 class="form-section">Products Images & Video</h3>
                <div class="row">
                    <div class="col-md-6">
                        <asp:FileUpload ID="FUImg1" runat="server" />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            runat="server"
                            ControlToValidate="FUImg1"
                            ForeColor="Red"
                            Display="Dynamic"
                            ErrorMessage="Please Choose Image ">
                        </asp:RequiredFieldValidator>

                    </div>

                    <div class="col-md-6">
                        <asp:FileUpload ID="FUImg2" runat="server" />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            runat="server"
                            ControlToValidate="FUImg2"
                            ForeColor="Red"
                            Display="Dynamic"
                            ErrorMessage="Please Choose Image ">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="FUImg3" runat="server" />



                    </div>
                    <div class="col-md-6">
                        <asp:FileUpload ID="FUImg4" runat="server" />


                    </div>
                </div>

                <asp:Panel runat="server" ID="pnlProductMainDetailImg" Visible="false">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:FileUpload ID="FUImg5" runat="server" />
                        </div>
                        <div class="col-md-6">
                            <asp:FileUpload ID="FUImg6" runat="server" />
                        </div>
                        <div class="col-md-6">
                            <asp:FileUpload ID="FUImg7" runat="server" />
                        </div>
                        <div class="col-md-6">
                            <asp:FileUpload ID="FUImg8" runat="server" />
                        </div>
                        <div class="col-md-6">
                            <asp:FileUpload ID="FUVideo" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label runat="server" ID="lblMsg" Text="" />
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <asp:Button runat="server" ID="btnSubmit" class="btn btn-circle blue" Text="Submit" OnClick="btnSubmit_Click" />

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <%--</form>--%>
                <!-- END FORM-->
            </div>
        </div>
    </div>


    <script type="text/javascript" src="assets/Clock/jquery.min.js"></script>
    <script type="text/javascript" src="assets/Clock/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/Clock/bootstrap-clockpicker.min.js"></script>
    <script type="text/javascript">

        $('.clockpicker').clockpicker()
            .find('input').change(function () {
                console.log(this.value);
                placement: 'top';
                align: 'left';

            });

    </script>

</asp:Content>

