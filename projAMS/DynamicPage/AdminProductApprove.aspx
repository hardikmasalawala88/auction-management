﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="AdminProductApprove.aspx.cs" Inherits="DynamicPage_AdminProductApprove" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="page-content">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
          
                <h3 class="page-title">Admin Product Approve
                    <small>For activating Admin </small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Admin</a>
                        </li>
                    </ul>
                </div>

                  <!-- END PAGE HEADER-->

                <div class="portlet box red-intense ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Admin Approval 
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>

                    
                    <div class="portlet-body">
                        <div class="portlet-title tools">

                              <div class="form-actions top" align="Middle">

                                <asp:Label ID="lblProductName" runat="server" Text="Product Name :" />
                                <i class="tooltips twitter-typeahead" data-original-title="Write Proper Name">
                                    
                                    <asp:TextBox ID="txtSearchProduct"
                                        runat="server"
                                        placeholder="Enter your Name"
                                        CssClass="form-control input-inline tt-hint twitter-typeahead" />
                                </i>
                                <asp:filteredtextboxextender id="FTBtxtSearchProduct" runat="server"
                                                filtertype="lowercaseletters,uppercaseletters,custom"
                                                filtermode="validchars"
                                                validchars=" "
                                                targetcontrolid="txtSearchProduct">
                                            </asp:filteredtextboxextender>
                                       

                                <asp:Button ID="btnSearchEmployee"
                                    runat="server"
                                    Text="Search"
                                    class="btn blue"/>
                            </div>

                            <asp:GridView ID="GridView1" runat="server"
                                      AllowPaging="True" ShowFooter="True"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4">

                                <Columns>

                                    <asp:TemplateField HeaderText="Product Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName"
                                                runat="server"
                                                Text='<%#Eval("ProductName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Category Name" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryName"
                                                runat="server"
                                                Text='<%#Eval("CategoryName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Start Time" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartTime"
                                                runat="server"
                                                Text='<%#Eval("StartTime") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                         <asp:TemplateField HeaderText="End Time" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndTime"
                                                runat="server"
                                                Text='<%#Eval("EndTime") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         
                                    <asp:TemplateField HeaderText="Start Date" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate"
                                                runat="server"
                                                Text='<%#Eval("StartDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                         <asp:TemplateField HeaderText="End Date" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate"
                                                runat="server"
                                                Text='<%#Eval("EndDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                               </asp:GridView>
                    </div>
                        </div>
            </div>                     
        </ContentTemplate>
          </asp:UpdatePanel>
     </div>
</asp:Content>

