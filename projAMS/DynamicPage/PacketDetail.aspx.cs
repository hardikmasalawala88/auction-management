﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class DynamicPage_PacketDetail : System.Web.UI.Page
{
    boPacketDetail boObj = new boPacketDetail();
    balPacketDetail balObj = new balPacketDetail();
    DataSet ds = new DataSet();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }
        

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();
            //if (Session["Uid"] == null)
            //{
            //    Response.Redirect("LogIn.aspx");
            //}
            //else
            //{
            //    FillGrid();
            //}
        }
    }
    protected void FillGrid()
    {
        gvPacketDetail.DataSource = balObj.Load();
        gvPacketDetail.DataBind();
    }
    
    protected void btnAddRecord_Click(object sender, EventArgs e)
    {

    }
    protected void gvPacketDetail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        if (e.CommandName.Equals("Add"))
        {
            //text box object is created txtAddColor is footer textbox that valu fetch and assign in object TxtColorName
           // Label PacketId = (Label)gvPacketDetail.FooterRow.FindControl("lblPacketId");
            TextBox TxtPacketName = (TextBox)gvPacketDetail.FooterRow.FindControl("txtAddPacketName");
            TextBox TxtBids = (TextBox)gvPacketDetail.FooterRow.FindControl("txtAddBids");
            TextBox TxtPrice = (TextBox)gvPacketDetail.FooterRow.FindControl("txtAddPrice");
            TextBox TxtTime = (TextBox)gvPacketDetail.FooterRow.FindControl("txtAddTimes");
            TextBox TxtDuration = (TextBox)gvPacketDetail.FooterRow.FindControl("txtAddDuration");
            


            boObj.PacketId = 0;
            boObj.PacketName = TxtPacketName.Text;
            boObj.Bids = Convert.ToInt16(TxtBids.Text);
            boObj.Price = Convert.ToInt16(TxtPrice.Text);
            boObj.Times = Convert.ToInt16(TxtTime.Text);
            boObj.Duration = TxtDuration.Text;

            
          

           
            //insert is method in balCategoryTypeMst.cs 
            if (balObj.Insert(boObj))
            {
                lblMsg.Text = "Successfuly Save.";
            }
            else
            {
                lblMsg.Text = "Unsuccessfuly Save.";
            }
            FillGrid();
        }
    
    }

    protected void gvPacketDetail_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //for updateing
        Label PacketId = (Label)gvPacketDetail.Rows[e.RowIndex].FindControl("lblPacketId");
        TextBox TxtPacketName = (TextBox)gvPacketDetail.Rows[e.RowIndex].FindControl("txtPacketName");
        TextBox TxtBids = (TextBox)gvPacketDetail.Rows[e.RowIndex].FindControl("txtBids");
        TextBox TxtPrice = (TextBox)gvPacketDetail.Rows[e.RowIndex].FindControl("txtPrice");
        TextBox TxtTime = (TextBox)gvPacketDetail.Rows[e.RowIndex].FindControl("txtTimes");
        TextBox TxtDuration = (TextBox)gvPacketDetail.Rows[e.RowIndex].FindControl("txtDuration");



        boObj.PacketId = Convert.ToInt16(PacketId.Text);
        boObj.PacketName = TxtPacketName.Text;
        boObj.Bids = Convert.ToInt32(TxtBids.Text);
        boObj.Price = Convert.ToInt16(TxtPrice.Text);
        boObj.Times = Convert.ToInt16(TxtTime.Text);
        boObj.Duration = TxtDuration.Text;

        //insert is method in balCategoryTypeMst.cs 
        if (balObj.Insert(boObj))
        {
            lblMsg.Text = "Successfuly Save.";
        }
        else
        {
            lblMsg.Text = "Unsuccessfuly Save.";
        }
        gvPacketDetail.EditIndex = -1;
        FillGrid();
    }
    protected void gvPacketDetail_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //which row you need to update that ID give
        gvPacketDetail.EditIndex = e.NewEditIndex;
        FillGrid();
    }

    protected void gvPacketDetail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
         //when you press cancel then data not change for that it use
        gvPacketDetail.EditIndex = -1;
        FillGrid();
    }

    protected void gvPacketDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
       {
            //when record more than page then automatically go in next page
            gvPacketDetail.PageIndex = e.NewPageIndex;
            FillGrid();
       }
}