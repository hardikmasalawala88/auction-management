﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DynamicPage_mstClient : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
            if (Session["LUName"] != null)
            {
                lblUname.Text = "Hi , " + Session["LUName"].ToString();
                //imgUserProfile.ImageUrl = "~/Upload/" + Session["UImg"].ToString();
            }
            else
            {
                Response.Redirect("../StaticPage/Login.aspx");

            }
        
    }
    protected void lbtnLogOut_Click(object sender, EventArgs e)
    {
        Session["LEmailId"] = null;
        Session["LPasswd"] = null;
        Session["LDesig"] = null;
        Session["LUID"] = null;
        Session["LUName"] = null;
        Session["MstSite"] = null;
        Response.Redirect("../StaticPage/Login.aspx");
    }
}
