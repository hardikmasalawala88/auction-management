﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="OrderView.aspx.cs" Inherits="DynaminPage_OrderView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN STYLE CUSTOMIZER -->
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR </span>
                    <ul>
                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"></li>
                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"></li>
                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"></li>
                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"></li>
                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"></li>
                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout </span>
                    <select class="layout-option form-control input-sm">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header </span>
                    <select class="page-header-option form-control input-sm">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Top Menu Dropdown</span>
                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                        <option value="light" selected="selected">Light</option>
                        <option value="dark">Dark</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar Mode</span>
                    <select class="sidebar-option form-control input-sm">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar Menu </span>
                    <select class="sidebar-menu-option form-control input-sm">
                        <option value="accordion" selected="selected">Accordion</option>
                        <option value="hover">Hover</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar Style </span>
                    <select class="sidebar-style-option form-control input-sm">
                        <option value="default" selected="selected">Default</option>
                        <option value="light">Light</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar Position </span>
                    <select class="sidebar-pos-option form-control input-sm">
                        <option value="left" selected="selected">Left</option>
                        <option value="right">Right</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer </span>
                    <select class="page-footer-option form-control input-sm">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- END STYLE CUSTOMIZER -->
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">Order View <small>view order details</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="DefaultAdmin.aspx" >Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">eCommerce</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Order View</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                        Actions <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a href="#">Action</a>
                        </li>
                        <li>
                            <a href="#">Another action</a>
                        </li>
                        <li>
                            <a href="#">Something else here</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">Separated link</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i>Order #12313232 <span class="hidden-480">| Dec 27, 2013 7:16:25 </span>
                        </div>
                        <div class="actions">
                            <a href="#" class="btn default yellow-stripe">
                                <i class="fa fa-angle-left"></i>
                                <span class="hidden-480">Back </span>
                            </a>
                            <div class="btn-group">
                                <a class="btn default yellow-stripe dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                    <span class="hidden-480">Tools </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#">Export to Excel </a>
                                    </li>
                                    <li>
                                        <a href="#">Export to CSV </a>
                                    </li>
                                    <li>
                                        <a href="#">Export to XML </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">Print Invoice </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs nav-tabs-lg">
                                <li class="active">
                                    <a href="#tab_1" data-toggle="tab">Details </a>
                                </li>
                                <li>
                                    <a href="#tab_2" data-toggle="tab">Invoices <span class="badge badge-success">4 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab_3" data-toggle="tab">Credit Memos </a>
                                </li>
                                <li>
                                    <a href="#tab_4" data-toggle="tab">Shipments <span class="badge badge-danger">2 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab_5" data-toggle="tab">History </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow-crusta box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Order Details
                                                    </div>
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="fa fa-pencil"></i>Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Order #:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            12313232 <span class="label label-info label-sm">Email confirmation was sent </span>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Order Date & Time:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            Dec 27, 2013 7:16:25 PM
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Order Status:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="label label-success">Closed </span>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Grand Total:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            $175.25
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Payment Information:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            Credit Card
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue-hoki box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Customer Information
                                                    </div>
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="fa fa-pencil"></i>Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Customer Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            Jhon Doe
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Email:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            jhon@doe.com
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            State:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            New York
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Phone Number:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            12234389
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet green-meadow box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Billing Address
                                                    </div>
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="fa fa-pencil"></i>Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-12 value">
                                                            Jhon Done<br>
                                                            #24 Park Avenue Str<br>
                                                            New York<br>
                                                            Connecticut, 23456 New York<br>
                                                            United States<br>
                                                            T: 123123232<br>
                                                            F: 231231232<br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet red-sunglo box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Shipping Address
                                                    </div>
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="fa fa-pencil"></i>Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-12 value">
                                                            Jhon Done<br>
                                                            #24 Park Avenue Str<br>
                                                            New York<br>
                                                            Connecticut, 23456 New York<br>
                                                            United States<br>
                                                            T: 123123232<br>
                                                            F: 231231232<br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="portlet grey-cascade box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Shopping Cart
                                                    </div>
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="fa fa-pencil"></i>Edit </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Product
                                                                    </th>
                                                                    <th>Item Status
                                                                    </th>
                                                                    <th>Original Price
                                                                    </th>
                                                                    <th>Price
                                                                    </th>
                                                                    <th>Quantity
                                                                    </th>
                                                                    <th>Tax Amount
                                                                    </th>
                                                                    <th>Tax Percent
                                                                    </th>
                                                                    <th>Discount Amount
                                                                    </th>
                                                                    <th>Total
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#">Product 1 </a>
                                                                    </td>
                                                                    <td>
                                                                        <span class="label label-sm label-success">Available
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>2
                                                                    </td>
                                                                    <td>2.00$
                                                                    </td>
                                                                    <td>4%
                                                                    </td>
                                                                    <td>0.00$
                                                                    </td>
                                                                    <td>691.00$
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#">Product 1 </a>
                                                                    </td>
                                                                    <td>
                                                                        <span class="label label-sm label-success">Available
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>2
                                                                    </td>
                                                                    <td>2.00$
                                                                    </td>
                                                                    <td>4%
                                                                    </td>
                                                                    <td>0.00$
                                                                    </td>
                                                                    <td>691.00$
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#">Product 1 </a>
                                                                    </td>
                                                                    <td>
                                                                        <span class="label label-sm label-success">Available
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>2
                                                                    </td>
                                                                    <td>2.00$
                                                                    </td>
                                                                    <td>4%
                                                                    </td>
                                                                    <td>0.00$
                                                                    </td>
                                                                    <td>691.00$
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <a href="#">Product 1 </a>
                                                                    </td>
                                                                    <td>
                                                                        <span class="label label-sm label-success">Available
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>345.50$
                                                                    </td>
                                                                    <td>2
                                                                    </td>
                                                                    <td>2.00$
                                                                    </td>
                                                                    <td>4%
                                                                    </td>
                                                                    <td>0.00$
                                                                    </td>
                                                                    <td>691.00$
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="well">
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Sub Total:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $1,124.50
                                                    </div>
                                                </div>
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Shipping:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $40.50
                                                    </div>
                                                </div>
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Grand Total:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $1,260.00
                                                    </div>
                                                </div>
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Total Paid:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $1,260.00
                                                    </div>
                                                </div>
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Total Refunded:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $0.00
                                                    </div>
                                                </div>
                                                <div class="row static-info align-reverse">
                                                    <div class="col-md-8 name">
                                                        Total Due:
                                                    </div>
                                                    <div class="col-md-3 value">
                                                        $1,124.50
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="pending">Pending</option>
                                                <option value="paid">Paid</option>
                                                <option value="canceled">Canceled</option>
                                            </select>
                                            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_invoices">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%">
                                                        <input type="checkbox" class="group-checkable">
                                                    </th>
                                                    <th width="5%">Invoice&nbsp;#
                                                    </th>
                                                    <th width="15%">Bill To
                                                    </th>
                                                    <th width="15%">Invoice&nbsp;Date
                                                    </th>
                                                    <th width="10%">Amount
                                                    </th>
                                                    <th width="10%">Status
                                                    </th>
                                                    <th width="10%">Actions
                                                    </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_invoice_no">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_invoice_bill_to">
                                                    </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_invoice_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_invoice_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <input type="text" class="form-control form-filter input-sm" name="order_invoice_amount_from" placeholder="From" />
                                                        </div>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_invoice_amount_to" placeholder="To" />
                                                    </td>
                                                    <td>
                                                        <select name="order_invoice_status" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="pending">Pending</option>
                                                            <option value="paid">Paid</option>
                                                            <option value="canceled">Canceled</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i>Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="datatable_credit_memos">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%">Credit&nbsp;Memo&nbsp;#
                                                    </th>
                                                    <th width="15%">Bill To
                                                    </th>
                                                    <th width="15%">Created&nbsp;Date
                                                    </th>
                                                    <th width="10%">Status
                                                    </th>
                                                    <th width="10%">Actions
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_4">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="datatable_shipment">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%">Shipment&nbsp;#
                                                    </th>
                                                    <th width="15%">Ship&nbsp;To
                                                    </th>
                                                    <th width="15%">Shipped&nbsp;Date
                                                    </th>
                                                    <th width="10%">Quantity
                                                    </th>
                                                    <th width="10%">Actions
                                                    </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_shipment_no">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_shipment_ship_to">
                                                    </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_shipment_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_shipment_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <input type="text" class="form-control form-filter input-sm" name="order_shipment_quantity_from" placeholder="From" />
                                                        </div>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_shipment_quantity_to" placeholder="To" />
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i>Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_5">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="datatable_history">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="25%">Datetime
                                                    </th>
                                                    <th width="55%">Description
                                                    </th>
                                                    <th width="10%">Notification
                                                    </th>
                                                    <th width="10%">Actions
                                                    </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td>
                                                        <div class="input-group date datetime-picker margin-bottom-5" data-date-format="dd/mm/yyyy hh:ii">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date datetime-picker" data-date-format="dd/mm/yyyy hh:ii">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_history_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="order_history_desc" placeholder="To" />
                                                    </td>
                                                    <td>
                                                        <select name="order_history_notification" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="pending">Pending</option>
                                                            <option value="notified">Notified</option>
                                                            <option value="failed">Failed</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i>Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>

</asp:Content>

