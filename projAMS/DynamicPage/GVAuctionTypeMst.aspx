﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="GVAuctionTypeMst.aspx.cs" Inherits="DynamicPage_GVAuctionTypeMst" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">

        <h3 class="page-title">Auction Type Detail
                    <small>View , Add & Edit </small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx" >Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">AuctionTypeDetail</a>
                        </li>
                    </ul>
                    
                </div>
                <!-- END PAGE HEADER-->

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Auction Methods
                            
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="table_responsive.html#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>


                    <div class="portlet-body">


                    <div class="portlet-title tools">
                        <%--Grid view Start--%>
                        <asp:GridView ID="GVAuctionTypeMst1" runat="server"
                            AllowPaging="True" ShowFooter="True"
                            PageSize="10"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False"
                            GridLines="None"
                            PagerStyle-VerticalAlign="Middle"
                            HeaderStyle-ForeColor="Black"
                            CellPadding="4" ViewStateMode="Enabled"
                            ShowHeaderWhenEmpty="True"
                            OnPageIndexChanging="GVAuctionTypeMst1_PageIndexChanging"
                            OnRowCancelingEdit="GVAuctionTypeMst1_RowCancelingEdit"
                            OnRowCommand="GVAuctionTypeMst1_RowCommand"
                            OnRowEditing="GVAuctionTypeMst1_RowEditing"
                            OnRowUpdating="GVAuctionTypeMst1_RowUpdating">

                            <AlternatingRowStyle />
                            <Columns>
                                <asp:TemplateField HeaderText="AuctionTypeId" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuctionTypeId" runat="server" Text='<%#Eval("AuctionTypeId") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblAdd" runat="server" Text="ENTER NEW AUCTION TYPE ">
                                        </asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AuctionType" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuctionTypeName" runat="server" Text='<%#Eval("AuctionTypeName") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAuctionTypeName"
                                             runat="server" 
                                            Text='<%#Eval("AuctionTypeName") %>'>
                                        </asp:TextBox>

                                        <asp:FilteredTextBoxExtender ID="FTBEtxtAuctionTypeName" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtAuctionTypeName">
                                            </asp:FilteredTextBoxExtender>
                                           
                                        <asp:RequiredFieldValidator ID="rfvAuctionTypeName" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtAuctionTypeName"
                                             Display="Dynamic"
                                             Text="*Can't be Blank"
                                             ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtAddAuctionTypeName" 
                                            CssClass=" form-control" 
                                            runat="server"
                                            placeholder="AUCTION TYPE NAME"
                                            >
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTBEtxtAddAuctionTypeName" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtAddAuctionTypeName">
                                            </asp:FilteredTextBoxExtender>


                                        <asp:RequiredFieldValidator ID="rfvAddCategoryType" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtAddAuctionTypeName" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AuctionTypeDescription" ControlStyle-Height="40px" HeaderStyle-CssClass="gridViewHead">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuctionTypeDesc" 
                                            runat="server" 
                                            Text='<%#Eval("AuctionTypeDesc") %>'
                                            Height="20px">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAuctionTypeDesc" 
                                            runat="server" Text='<%#Eval("AuctionTypeDesc") %>'>
                                        </asp:TextBox>

                                      <asp:FilteredTextBoxExtender ID="FTBEtxtAuctionTypeDesc" runat="server"
                                       FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                       FilterMode="ValidChars"
                                       ValidChars=" "
                                       TargetControlID="txtAuctionTypeDesc">
                                       </asp:FilteredTextBoxExtender>
                         
                                        <asp:RequiredFieldValidator ID="rfvTXTAuctionTypeDesc" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtAuctionTypeDesc" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank" 
                                            ValidationGroup="EditValid">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtAddAuctionTypeDesc" 
                                            runat="server" 
                                            CssClass="form-control"
                                            placeholder="AUCTION DESCRIPTION" >
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTBEtxtAddAuctionTypeDesc" runat="server"
                                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                FilterMode="ValidChars"
                                                ValidChars=" "
                                                TargetControlID="txtAddAuctionTypeDesc">
                                            </asp:FilteredTextBoxExtender>

                                        <asp:RequiredFieldValidator ID="rfvAddAuctionTypeDesc" 
                                            runat="server" 
                                            ErrorMessage="Can't be Blank"
                                            ControlToValidate="txtAddAuctionTypeDesc" 
                                            Display="Dynamic" 
                                            Text="*Can't be Blank">
                                        </asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                    <ControlStyle Height="20px"></ControlStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgBtnEdit" 
                                            runat="server" 
                                            ImageUrl="~/DynamicPage/img/edit-grid.png"
                                            AlternateText="Edit" 
                                            Height="25px" 
                                            Width="30px" 
                                            CausesValidation="false" 
                                            ToolTip="Edit" 
                                            CommandName="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton ID="btnUpdate" 
                                            ImageUrl="~/DynamicPage/img/update.png" 
                                            ToolTip="Update" 
                                            AlternateText="Update"
                                            runat="server" 
                                            CommandName="Update" 
                                            ValidationGroup="EditValid" />
                                        <asp:ImageButton ID="btnCancel" 
                                            ImageUrl="~/DynamicPage/img/cancel.png" 
                                            ToolTip="Cancle" 
                                            runat="server"
                                            AlternateText="Cancel" 
                                            CommandName="Cancel" 
                                            CausesValidation="false" />
                                    </EditItemTemplate>
                                    <FooterTemplate>

                                        <asp:Button ID="btnAddRecord" 
                                            runat="server" 
                                            Text="Add" 
                                            CommandName="Add"></asp:Button>
                                    </FooterTemplate>
                                    <ItemStyle VerticalAlign="Middle" />
                                    <ControlStyle Height="25px"></ControlStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblMsg" runat="server" />
                    </div>


                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div> 
</asp:Content>

