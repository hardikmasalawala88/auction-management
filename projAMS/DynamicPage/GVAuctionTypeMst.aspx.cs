﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class DynamicPage_GVAuctionTypeMst : System.Web.UI.Page
{
    boAuctionTypeMst boObj = new boAuctionTypeMst();
    balAuctionTypeMst balObj = new balAuctionTypeMst();
    DataSet ds = new DataSet();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();
            //if (Session["Uid"] == null)
            //{
            //    Response.Redirect("LogIn.aspx");
            //}
            //else
            //{
            //    FillGrid();
            //}
        }
    }
    protected void FillGrid()
    {
       
        GVAuctionTypeMst1 .DataSource = balObj.Load();
        GVAuctionTypeMst1 .DataBind();
    }
    protected void GVAuctionTypeMst1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //for updateing
        Label AuctionTypeId = (Label)GVAuctionTypeMst1.Rows[e.RowIndex].FindControl("lblAuctionTypeId");
        TextBox TxtAuctionTypeName = (TextBox)GVAuctionTypeMst1.Rows[e.RowIndex].FindControl("txtAuctionTypeName");
        TextBox TxtAuctionTypeDesc = (TextBox)GVAuctionTypeMst1.Rows[e.RowIndex].FindControl("txtAuctionTypeDesc");

        boObj.AuctionTypeId = Convert.ToInt16(AuctionTypeId.Text);
        boObj.AuctionTypeName = TxtAuctionTypeName.Text;
        boObj.AuctionTypeDesc = TxtAuctionTypeDesc.Text;

        //insert is method in balCategoryTypeMst.cs 
        if (balObj.Insert(boObj))
        {
            lblMsg.Text = "Successfuly Save.";
        }
        else
        {
            lblMsg.Text = "Unsuccessfuly Save.";
        }
        GVAuctionTypeMst1.EditIndex = -1;
        FillGrid();
    }
    protected void GVAuctionTypeMst1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //which row you need to update that ID give
        GVAuctionTypeMst1.EditIndex = e.NewEditIndex;
        FillGrid();
    }
    protected void GVAuctionTypeMst1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        //when you press cancel then data not change for that it use
        GVAuctionTypeMst1.EditIndex = -1;
        FillGrid();
    }
    protected void GVAuctionTypeMst1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
         //Row Command give command what to do add or other

        if (e.CommandName.Equals("Add"))
        {
            //text box object is created txtAddColor is footer textbox that valu fetch and assign in object TxtColorName
            TextBox TxtAuctionTypeName = (TextBox)GVAuctionTypeMst1.FooterRow.FindControl("txtAddAuctionTypeName");
            TextBox TxtAuctionTypeDesc = (TextBox)GVAuctionTypeMst1.FooterRow.FindControl("txtAddAuctionTypeDesc");

            boObj.AuctionTypeId = 0;
            boObj.AuctionTypeName = TxtAuctionTypeName.Text;
            boObj.AuctionTypeDesc = TxtAuctionTypeDesc.Text;
            //insert is method in balCategoryTypeMst.cs 
            if (balObj.Insert(boObj))
            {
                lblMsg.Text = "Successfuly Save.";
            }
            else
            {
                lblMsg.Text = "Unsuccessfuly Save.";
            }
            FillGrid();
        }

    }
    protected void GVAuctionTypeMst1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //when record more than page then automatically go in next page
        GVAuctionTypeMst1.PageIndex = e.NewPageIndex;
        FillGrid();
    }
}