﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class DynamicPage_AEUserProfile : System.Web.UI.Page
{
    string path, fname;
    FileInfo fInfo;

    boClientRegistration boClientObj = new boClientRegistration();
    balClientRegistration balClientObj = new balClientRegistration();

    balEmployeeDetail balEmpObj = new balEmployeeDetail();
    boEmployeeDetail boEmpObj = new boEmployeeDetail();
    boAddProduct boAddObj = new boAddProduct();

    //string sql;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["LUID"] != null)
        {
            if (Session["MstSite"] != null)
            {
                this.MasterPageFile = Session["MstSite"].ToString();
            }
            else
            {
                Response.Redirect("../StaticPage/Login.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        //Session["LEmailId"] = ds.Tables[0].Rows[0][0].ToString();
        //Session["LPasswd"] = ds.Tables[0].Rows[0][1].ToString();
        //Session["LDesig"] = ds.Tables[0].Rows[0][2].ToString();
        //Session["LUID"] = ds.Tables[0].Rows[0][3].ToString();
        //Session["LUName"] = ds.Tables[0].Rows[0][4].ToString();
        if (Session["LUID"] == null)
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            lbluname.Text = Session["LUName"].ToString();
            lblDesig.Text = Convert.ToString(Session["LDesig"]);
            //Admin Data Update
            if (Convert.ToString(Session["LDesig"]) == "Admin")
            {
                boEmpObj.EmpId = Convert.ToInt32(Session["LUID"]);
                ds = balEmpObj.getData(boEmpObj);
                // ds.Tables[0].Rows[0]["EmpId"].ToString();
                TxtName.Text = ds.Tables[0].Rows[0]["EmpName"].ToString();
                if (ds.Tables[0].Rows[0]["Gender"].ToString() == "True")
                {
                    DDLGender.SelectedValue = "Male";
                }
                else
                {
                    DDLGender.SelectedValue = "Female";
                }
                //DDLGender.SelectedValue = ds.Tables[0].Rows[0]["Gender"].ToString();
                TxtAdd.Text = ds.Tables[0].Rows[0]["Addr"].ToString();
                Txtcon.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                TxtDOB.Text = ds.Tables[0].Rows[0]["Dob"].ToString();
                TxtDOJ.Text = ds.Tables[0].Rows[0]["Doj"].ToString();
                TxtDesig.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
                //ImgProfile.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                //Session["ImgLoc1"] = ds.Tables[0].Rows[0]["EmpImage"].ToString();
                TxtEmail.Text = ds.Tables[0].Rows[0]["EmailId"].ToString();
                if (ds.Tables[0].Rows[0]["EmpImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                }
                TxtEmail.Enabled = false;
                TxtDesig.Enabled = false;
                lblDesignation.Visible = false;
                lblShipAdd.Visible = false;
                TxtShip.Visible = false;
                //sql = "Select * From EmployeeDetail where EmpId =" + Session["LUID"].ToString();

            }
            else if (Convert.ToString(Session["LDesig"]) == "Employee")
            {
                //Employee Data Update
                boEmpObj.EmpId = Convert.ToInt32(Session["LUID"]);
                ds = balEmpObj.getData(boEmpObj);
                // ds.Tables[0].Rows[0]["EmpId"].ToString();
                TxtName.Text = ds.Tables[0].Rows[0]["EmpName"].ToString();
                if (ds.Tables[0].Rows[0]["Gender"].ToString() == "True")
                {
                    DDLGender.SelectedValue = "Male";
                }
                else
                {
                    DDLGender.SelectedValue = "Female";
                }
                //DDLGender.SelectedValue = ds.Tables[0].Rows[0]["Gender"].ToString();
                TxtAdd.Text = ds.Tables[0].Rows[0]["Addr"].ToString();
                Txtcon.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                TxtDOB.Text = ds.Tables[0].Rows[0]["Dob"].ToString();
                TxtDOJ.Text = ds.Tables[0].Rows[0]["Doj"].ToString();
                TxtDesig.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
                //ImgProfile.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                //Session["ImgLoc1"] = ds.Tables[0].Rows[0]["EmpImage"].ToString();
                TxtEmail.Text = ds.Tables[0].Rows[0]["EmailId"].ToString();
                if (ds.Tables[0].Rows[0]["EmpImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();
                }
                TxtEmail.Enabled = false;
                TxtDesig.Enabled = false;
                TxtDesig.Visible = false;
                lblShipAdd.Visible = false;
                TxtShip.Visible = false;


                //sql = "Select * From EmployeeDetail where EmpId =" + Session["LUID"].ToString();
            }
            else if (Convert.ToString(Session["LDesig"]) == "Client")
            {
                //Client Data Update
                boClientObj.ClientId = Convert.ToInt32(Session["LUID"]);
                ds = balClientObj.getData(boClientObj);
                // ds.Tables[0].Rows[0]["EmpId"].ToString();
                
                TxtName.Text = ds.Tables[0].Rows[0]["ClientName"].ToString();
                if (ds.Tables[0].Rows[0]["Gender"].ToString() == "True")
                {
                    DDLGender.SelectedValue = "Male";
                }
                else
                {
                    DDLGender.SelectedValue = "Female";
                }
                //DDLGender.SelectedValue = ds.Tables[0].Rows[0]["Gender"].ToString();
                TxtAdd.Text = ds.Tables[0].Rows[0]["ClientAddr"].ToString();
                Txtcon.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                TxtShip.Text = ds.Tables[0].Rows[0]["ShippingAddr"].ToString();
                //ImgProfile.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["ClientImage"].ToString();
                //Session["ImgLoc1"] = ds.Tables[0].Rows[0]["ClientImage"].ToString();
                TxtEmail.Text = ds.Tables[0].Rows[0]["EmailId"].ToString();
                if (ds.Tables[0].Rows[0]["ClientImage"].ToString().Length > 0)
                {
                    imgUser.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["ClientImage"].ToString();
                }
                TxtEmail.Enabled = false;
                TxtDesig.Enabled = false;
                TxtDesig.Visible = false;
                lblDOB.Visible = false;
                lblDOJ.Visible = false;
                TxtDOB.Visible = false;
                TxtDOJ.Visible = false;
                lblShipAdd.Visible = true;
                TxtShip.Visible = true;
                //lblDesig.Visible = false;
                TxtDesig.Visible = false;
            }
        }
    }


    protected void BtnSave_Click(object sender, EventArgs e)
    {


        //Admin Data Update
        if (Convert.ToString(Session["LDesig"]) == "Admin")
        {
            boEmpObj.EmpId = Convert.ToInt32(Session["LUID"].ToString());
            boEmpObj.EmpName = TxtName.Text;
            boEmpObj.Addr = TxtAdd.Text;
            if (DDLGender.SelectedValue == "Male")
            {
                //male.Checked;
                boEmpObj.Gender = 1;
            }
            else
            {
                // female.Checked;
                boEmpObj.Gender = 0;
            }
            boEmpObj.Dob = Convert.ToString(TxtDOB.Text);
            boEmpObj.ContactNo = Txtcon.Text;
            boEmpObj.EmailId = TxtEmail.Text;
            //boEmpObj.Designation = txtDesignation.Text;
            boEmpObj.Designation = TxtDesig.Text;
            boEmpObj.Doj = Convert.ToString(TxtDOJ.Text);
            boEmpObj.IsActive = Convert.ToInt16(true);
            boEmpObj.EmpImage = Session["ImgLoc1"].ToString();
            balEmpObj.Insert(boEmpObj);

        }
        else if (Convert.ToString(Session["LDesig"]) == "Employee")
        {
            boEmpObj.EmpId = Convert.ToInt32(Session["LUID"].ToString());
            boEmpObj.EmpName = TxtName.Text;
            boEmpObj.Addr = TxtAdd.Text;
            if (DDLGender.SelectedValue == "Male")
            {
                //male.Checked;
                boEmpObj.Gender = 1;
            }
            else
            {
                // female.Checked;
                boEmpObj.Gender = 0;
            }
            boEmpObj.Dob = Convert.ToString(TxtDOB.Text);
            boEmpObj.ContactNo = Txtcon.Text;
            boEmpObj.EmailId = TxtEmail.Text;
            //boEmpObj.Designation = txtDesignation.Text;
            boEmpObj.Designation = TxtDesig.Text;
            boEmpObj.Doj = Convert.ToString(TxtDOJ.Text);
            boEmpObj.IsActive = Convert.ToInt16(true);
            boEmpObj.EmpImage = Session["ImgLoc1"].ToString();
            balEmpObj.Insert(boEmpObj);

        }
        else if (Convert.ToString(Session["LDesig"]) == "Client")
        {
            boClientObj.ClientId = Convert.ToInt32(Session["LUID"].ToString());
            boClientObj.ClientName = TxtName.Text;
            boClientObj.ClientAddr = TxtAdd.Text;
            boClientObj.ShippingAddr = TxtAdd.Text;
            boClientObj.Passwd = Convert.ToString(Session["LPasswd"]);
            if (DDLGender.SelectedValue == "Male")
            {
                //male.Checked;
                boClientObj.Gender = 1;
            }
            else
            {
                // female.Checked;
                boEmpObj.Gender = 0;
            }
            boClientObj.ContactNo = Txtcon.Text;
            boClientObj.EmailId = TxtEmail.Text;
            //boEmpObj.Designation = txtDesignation.Text;
            boClientObj.IsActive = Convert.ToInt16(true);
            if (TxtShip.Text.Length == 0)
            {
                TxtShip.Text = TxtAdd.Text;
            }
            boClientObj.ShippingAddr = TxtShip.Text;
            boClientObj.ClientImage = Session["ImgLoc1"].ToString();
            balClientObj.Insert(boClientObj);

        }
    }
    
   

    protected void lbtnChangeProfilePicture_Click(object sender, EventArgs e)
    {
        pnlProfile.Visible = false;
        pnlChangePwd.Visible = false;
        pnlImg .Visible =true ;

    }
    protected void lbtnChangePassword_Click(object sender, EventArgs e)
    {
        pnlImg.Visible = false;
        pnlProfile.Visible = false;
        pnlChangePwd.Visible = true;
        
    }
    protected void lbtnPersonalInfo_Click(object sender, EventArgs e)
    {
        pnlImg.Visible = false;
        pnlChangePwd.Visible = false;
        pnlProfile.Visible = true;
    }
   
    protected void btnChangePasswd_Click(object sender, EventArgs e)
    {
        Boolean f1;
        //DataSet ds = new DataSet();

        if (Convert.ToString(Session["LDesig"]) == "Admin")
        {
            boEmpObj.EmpId = Convert.ToInt32(Session["LUID"].ToString());
            boEmpObj.EmpName = TxtName.Text;
            boEmpObj.Passwd = txtCurrentPasswd.Text;
            string pwd = txtNewPasswd1.Text;
            f1 = balEmpObj.ChangeProfilePasswd(boEmpObj, pwd);
            if (f1 == true)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Succssfully Update');", true);
                lblPwdMsg.Text = "Password successfully updated";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Password not Update');", true);
                lblPwdMsg.Text = "Password not updated";
            }
        }
        else if (Convert.ToString(Session["LDesig"]) == "Employee")
        {
            boEmpObj.EmpId = Convert.ToInt32(Session["LUID"].ToString());
            boEmpObj.EmpName = TxtName.Text;
            boEmpObj.Passwd = txtCurrentPasswd.Text;
            string pwd = txtNewPasswd1.Text;
            f1 = balEmpObj.ChangeProfilePasswd(boEmpObj, pwd);
            if (f1 == true)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Succssfully Update');", true);
                lblPwdMsg.Text = "Password successfully updated";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Password not Update');", true);
                lblPwdMsg.Text = "Password not updated";
            }
        }
        else if (Convert.ToString(Session["LDesig"]) == "Client")
        {
            boClientObj.ClientId = Convert.ToInt32(Session["LUID"].ToString());
            boClientObj.ClientName = TxtName.Text;
            boClientObj.Passwd = txtCurrentPasswd.Text;
            string pwd = txtNewPasswd1.Text;
            f1 = balClientObj.ChangeProfilePasswd(boClientObj, pwd);
            if (f1 == true)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Succssfully Update');", true);
                lblPwdMsg.Text = "Password successfully updated";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Not Update');", true);
                lblPwdMsg.Text = "Password not updated";
            }
        }
        
    }

    protected void btnSubmitChangeProfile_Click(object sender, EventArgs e)
    {
        if (FUImgChangeProfileImage.HasFile)
        {
            imgUpload(FUImgChangeProfileImage);
            if (Convert.ToString(Session["LDesig"]) == "Employee" || Convert.ToString(Session["LDesig"]) == "Admin")
            {
                boAddObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));
                boAddObj.Image1 = Convert.ToString(Session["ImgLoc1"]);
                balClientObj.setImage(boAddObj);
            }
            else
            {
                boAddObj.ClientId = (Convert.ToInt32(Session["LUID"].ToString()));
                boAddObj.Image1 = Convert.ToString(Session["ImgLoc1"]);
                balClientObj.setImage(boAddObj);

            }
            imgUser.ImageUrl = "~/Upload/" + Convert.ToString(Session["ImgLoc1"]);

        }
    }

    protected void imgUpload(FileUpload FUImg)
    {

        if (FUImg.HasFile)
        {
            fname = FUImg.FileName;
            fInfo = new FileInfo(fname);
            string ext = fInfo.Extension.ToLower();

            if (ext == ".jpeg" || ext == ".jpg" || ext == ".png")
            {
                path = Server.MapPath("~/Upload/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                fname = Convert.ToString(Session["LUID"]) + "img" + DateTime.Now.ToString("ddMMyyyyHHmmssffff") + fInfo.Extension;
                FUImg.SaveAs(Server.MapPath("~/Upload/") + fname);
                Session["ImgLoc1"] = fname;
            }
            else
            {
                lblPmsg.Text = "Please Select .jpeg / .jpg / .png File";
            }
        }
        else
        {
            lblPmsg.Text = "Please select Image";
        }
    }




    protected void btnSavePersonalInfo_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["LDesig"]) == "Employee" || Convert.ToString(Session["LDesig"]) == "Admin")
        {
            boEmpObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));
            boEmpObj.EmpName = TxtName.Text;
            boEmpObj.Addr = TxtAdd.Text;
            if (DDLGender.SelectedValue == "Male")
            {
                boEmpObj.Gender = Convert.ToInt16(true);
            }
            else
            {
                boEmpObj.Gender = Convert.ToInt16(false);
            }
            boEmpObj.ContactNo = Txtcon.Text;
            boEmpObj.Dob = TxtDOB.Text;
            boEmpObj.Doj = TxtDOJ.Text;
            balEmpObj.updateEmpDetail(boEmpObj);
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Succssfully Update');", true);
        }
        else
        {
            boClientObj.ClientId = (Convert.ToInt32(Session["LUID"].ToString()));
            boClientObj.ClientName = TxtName.Text;
            boClientObj.ClientAddr = TxtAdd.Text;
            if (DDLGender.SelectedValue == "Male")
            {
                boClientObj.Gender = Convert.ToInt16(true);
            }
            else
            {
                boClientObj.Gender = Convert.ToInt16(false);
            }
            boClientObj.ContactNo = Txtcon.Text;
            balClientObj.UpdateClientDetail(boClientObj);
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Succssfully Update');", true);

        }
       

    }
}
