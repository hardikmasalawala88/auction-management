﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class DynamicPage_AddNewProduct : System.Web.UI.Page
{
    string path, fname;

    FileInfo fInfo;
    balAddProduct balAddObj = new balAddProduct();
    boAddProduct boAddObj = new boAddProduct();

    balAuctionTypeItemDetail balAuctionTDObj = new balAuctionTypeItemDetail();
    boAuctionTypeItemDetail boAuctionTDObj = new boAuctionTypeItemDetail();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        calStartDt.StartDate = DateTime.Now;
        //calEndDt.StartDate = DateTime.Now.AddDays((DateTime)calStartDt.SelectedDate - (DateTime)DateTime.Now.Date);
        //calEndDt.StartDate = DateTime.Now.AddDays(DateTime.Now.Subtract((TimeSpan)calStartDt.StartDate));
        if (!IsPostBack)
        {
            
            FillddlCategoryTypeId();
            ddlMainCategoryId.SelectedIndex = 3;
            FillddlCategoryId(Convert.ToInt32(ddlMainCategoryId.SelectedValue));
            FillddlAuctionType();

        }




    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //Add new product Submit
        if (Convert.ToString(Session["LDesig"]) == "Employee" || Convert.ToString(Session["LDesig"]) == "Admin")
        {
            boAddObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));

        }
        else
        {
            boAddObj.ClientId = (Convert.ToInt32(Session["LUID"].ToString()));

        }
        //boAddObj.ProductId = 0;
        boAddObj.ProductName = txtProductName.Text;
        boAddObj.Price = Convert.ToInt32(txtPrice.Text);
        boAddObj.ProductCondition = ddlProductCondition.SelectedItem.ToString();
        boAddObj.Qty = Convert.ToInt16(txtQty.Text);
        boAddObj.ShippingCharge = Convert.ToInt32(txtShippingCharge.Text);
        boAddObj.ShippingDesc = txtShippingDesc.Text;
        boAddObj.CategoryId = Convert.ToInt16(ddlCategoryId.SelectedValue.ToString());
        boAddObj.ProdcutDesc = txtProductDesc.Text.ToString();
        if (FUImg1.HasFile)
        {
            imgUpload(FUImg1);
            boAddObj.Image1 = Convert.ToString(Session["ImgLoc1"]);
        }
        else
        {
            boAddObj.Image1 = "NULL";
        }
        if (FUImg2.HasFile)
        {
            imgUpload(FUImg2);
            boAddObj.Image2 = Convert.ToString(Session["ImgLoc1"]);
        }
        else
        {
            boAddObj.Image2 = "NULL";
        }
        if (FUImg3.HasFile)
        {
            imgUpload(FUImg3);
            boAddObj.Image3 = Convert.ToString(Session["ImgLoc1"]);
        }
        else
        {
            boAddObj.Image3 = "NULL";
        }
        if (FUImg4.HasFile)
        {
            imgUpload(FUImg4);
            boAddObj.Image4 = Convert.ToString(Session["ImgLoc1"]);
        }
        else
        {
            boAddObj.Image4 = "NULL";
        }
        //boAddObj.Image1
        if (ddlCategoryId.SelectedValue == "1" || ddlCategoryId.SelectedValue == "2")
        {
            //if category is mobile or computer then this properties set
            boAddObj.AdditionalInfo = txtAdditionalInfo.Text;
            boAddObj.Warranty = txtWarranty.Text;
            boAddObj.FeaturesAndBenefits = txtFeaturesAndBenefits.Text;
            boAddObj.CompanyName = ddlCompanyName.SelectedItem.ToString();
            boAddObj.Color = ddlColor.SelectedItem.ToString();
            boAddObj.ModelNo = txtModelNo.Text;
            boAddObj.OS = txtOS.Text;
            boAddObj.OSversion = txtOSVersion.Text;
            boAddObj.Processor = txtProcessor.Text;
            boAddObj.RAM = txtRAM.Text;
            boAddObj.Sizeinch = txtSizeInch.Text;
            boAddObj.Resolution = txtResolution.Text;
            boAddObj.PrimaryCamera = txtPrimaryCamera.Text;
            boAddObj.SecondaryCamera = txtSecondaryCamera.Text;
            boAddObj.ScreenType = txtScreenType.Text;
            boAddObj.Connectivity = txtConnectivity.Text;
            boAddObj.Storage = txtStorage.Text;
            boAddObj.Speaker = txtSpeaker.Text;
            boAddObj.Battery = txtBattery.Text;
            if (FUImg5.HasFile)
            {
                imgUpload(FUImg5);
                boAddObj.Image5 = Convert.ToString(Session["ImgLoc1"]);
            }
            else
            {
                boAddObj.Image5 = "NULL";
            }
            if (FUImg6.HasFile)
            {
                imgUpload(FUImg6);
                boAddObj.Image6 = Convert.ToString(Session["ImgLoc1"]);
            }
            else
            {
                boAddObj.Image6 = "NULL";
            }
            if (FUImg7.HasFile)
            {
                imgUpload(FUImg7);
                boAddObj.Image7 = Convert.ToString(Session["ImgLoc1"]);
            }
            else
            {
                boAddObj.Image7 = "NULL";
            }
            if (FUImg8.HasFile)
            {
                imgUpload(FUImg8);
                boAddObj.Image8 = Convert.ToString(Session["ImgLoc1"]);
            }
            else
            {
                boAddObj.Image8 = "NULL";
            }
            if (FUVideo.HasFile)
            {
                vidUpload(FUVideo);
                boAddObj.Video = Convert.ToString(Session["VidLoc1"]);
            }
            else
            {
                boAddObj.Video = "NULL";
            }

        }
        boAddObj.AuctionTypeId = Convert.ToInt16(ddlAuctionTypeId.SelectedValue);
        boAuctionTDObj.AuctionTypeId = Convert.ToInt16(ddlAuctionTypeId.SelectedValue);
        boAuctionTDObj.CategoryId = Convert.ToInt16(ddlCategoryId.SelectedValue.ToString());
        boAuctionTDObj.StartDt = Convert.ToString(txtStartDt.Text);
        boAuctionTDObj.EndDt = Convert.ToString(txtEndDt.Text);
        boAuctionTDObj.StartTime = txtStartTime.Text;
        if (ddlAuctionTypeId.SelectedValue == "6" || ddlAuctionTypeId.SelectedValue == "5")
        {
            boAuctionTDObj.MinAmt = 0;
        }
        else 
        {
            boAuctionTDObj.MinAmt = Convert.ToInt32(txtMinAmt.Text);
        }
        boAuctionTDObj.EndTime = txtEndTime.Text;
        boAuctionTDObj.BuyNowAmt = Convert.ToInt32(txtBuyNowAmt.Text);
        if (ddlAuctionTypeId.SelectedValue == "5")
        {
            boAuctionTDObj.BidMulti = Convert.ToInt16(txtBidMulti.Text);
        }

        balAddObj.Insert(boAddObj, boAuctionTDObj);

        boAuctionTDObj.ProductId = boAuctionTDObj.ProductId;
        balAuctionTDObj.Insert(boAuctionTDObj);

        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data inserted');", true);



    }
    protected void FillddlCategoryId(int MCId)
    {
        ddlCategoryId.DataSource = balAddObj.fillDDLCategoryId(MCId);
        ddlCategoryId.DataTextField = "Category";
        ddlCategoryId.DataValueField = "CategoryId";
        ddlCategoryId.DataBind();

    }
    protected void FillddlCategoryTypeId()
    {
        ddlMainCategoryId.DataSource = balAddObj.fillDDLCategoryTypeId();
        ddlMainCategoryId.DataTextField = "CategoryType";
        ddlMainCategoryId.DataValueField = "CategoryTypeId";
        ddlMainCategoryId.DataBind();

    }
    protected void FillddlAuctionType()
    {
        if (Convert.ToString(Session["LDesig"]) == "Employee" || Convert.ToString(Session["LDesig"]) == "Admin")
        {
            ddlAuctionTypeId.DataSource = balAddObj.fillDDLAuctionTypeEmp();

        }
        else
        {
            ddlAuctionTypeId.DataSource = balAddObj.fillDDLAuctionTypeClient();

        }


        ddlAuctionTypeId.DataTextField = "AuctionTypeName";
        ddlAuctionTypeId.DataValueField = "AuctionTypeId";
        ddlAuctionTypeId.DataBind();
    }



    //Select Auction Type
    protected void ddlAuctionTypeId_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAuctionTypeId.SelectedValue == "1")//Normal Auction
        {

            lblMinAmt.Visible = true;
            txtMinAmt.Visible = true;
            lblBidMulti.Visible = false;
            txtBidMulti.Visible = false;
            
            lblVStartTime.Visible = false;
            lblVEndTime.Visible = false;
            lblVStartDt.Visible = false;
            lblVEndDt.Visible = false;

            txtStartDt.Visible = true;
            txtEndDt.Visible = true;
            txtStartTime.Visible = true;
            txtEndTime.Visible = true;

            pnlCharity.Visible = false;
            txtBuyNowAmt.Focus();

        }
        if (ddlAuctionTypeId.SelectedValue == "2")//Express Auction
        {
            txtBuyNowAmt.Focus();
            lblMinAmt.Visible = true;
            txtMinAmt.Visible = true;
            lblBidMulti.Visible = false;
            txtBidMulti.Visible = false;
            pnlCharity.Visible = false;
            var date = DateTime.Now.Date;
            var nextSunday = date.AddDays(7 - (int)date.DayOfWeek);

            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + nextSunday.ToString("MM/dd/yyyy") + "');", true);
            txtStartDt.Text = nextSunday.ToString("dd/MM/yyyy");
            txtEndDt.Text = nextSunday.ToString("dd/MM/yyyy");
            lblVStartDt.Text = nextSunday.ToString("dd/MM/yyyy");
            lblVStartDt.Visible = true;
            txtStartDt.Visible = false;
            lblVEndDt.Text = nextSunday.ToString("dd/MM/yyyy");
            lblVEndDt.Visible = true;
            txtEndDt.Visible = false;

            txtStartTime.Text = "09:00";
            txtEndTime.Text = "15:00";
            lblVStartTime.Text = "09:00";
            lblVStartTime.Visible = true;
            txtStartTime.Visible = false;
            lblVEndTime.Text = "15:00";
            lblVEndTime.Visible = true;
            txtEndTime.Visible = false;

        }
        if (ddlAuctionTypeId.SelectedValue == "3")//Sealded Auction
        {
            txtBuyNowAmt.Focus();
            lblMinAmt.Visible = true;
            txtMinAmt.Visible = true;
            lblBidMulti.Visible = false;
            txtBidMulti.Visible = false;
            lblVStartTime.Visible = false;
            lblVEndTime.Visible = false;
            lblVStartDt.Visible = false;
            lblVEndDt.Visible = false;

            txtStartDt.Visible = true;
            txtEndDt.Visible = true;
            txtStartTime.Visible = true;
            txtEndTime.Visible = true;

            pnlCharity.Visible = false;
        }
        if (ddlAuctionTypeId.SelectedValue == "4")//Charity Auction
        {
            txtBuyNowAmt.Focus();
            lblMinAmt.Visible = true;
            txtMinAmt.Visible = true;
            pnlCharity.Visible = true;
            lblBidMulti.Visible = false;
            txtBidMulti.Visible = false;

            txtStartDt.Visible = true;
            txtEndDt.Visible = true;
            txtStartTime.Visible = true;
            txtEndTime.Visible = true;
            
            lblVStartTime.Visible = false;
            lblVEndTime.Visible = false;
            lblVStartDt.Visible = false;
            lblVEndDt.Visible = false;
        }
        if (ddlAuctionTypeId.SelectedValue == "5")//Packge Auction
        {
            txtBuyNowAmt.Focus();
            lblMinAmt.Visible = false;
            txtMinAmt.Visible = false;
            lblBidMulti.Visible = true;
            txtBidMulti.Visible = true;
            pnlCharity.Visible = false;

            txtStartDt.Visible = true;
            txtEndDt.Visible = true;
            txtStartTime.Visible = true;
            txtEndTime.Visible = true;

            lblVStartTime.Visible = false;
            lblVEndTime.Visible = false;
            lblVStartDt.Visible = false;
            lblVEndDt.Visible = false;
        }
        if (ddlAuctionTypeId.SelectedValue == "6")//Debut Auction
        {
            lblMinAmt.Visible = false;
            txtMinAmt.Visible = false;
            txtBuyNowAmt.Focus();
            lblBidMulti.Visible = true;
            txtBidMulti.Visible = true;
            pnlCharity.Visible = false;

            txtStartDt.Visible = true;
            txtEndDt.Visible = true;
            txtStartTime.Visible = true;
            txtEndTime.Visible = true;

            lblVStartTime.Visible = false;
            lblVEndTime.Visible = false;
            lblVStartDt.Visible = false;
            lblVEndDt.Visible = false;
        }

    }

    //FileUpload Butten
    protected void imgUpload(FileUpload FUImg)
    {

        if (FUImg.HasFile)
        {
            fname = FUImg.FileName;
            fInfo = new FileInfo(fname);
            string ext = fInfo.Extension.ToLower();

            if (ext == ".jpeg" || ext == ".jpg" || ext == ".png")
            {
                path = Server.MapPath("~/Upload/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                fname = Convert.ToString(Session["LUID"]) + "img" + DateTime.Now.ToString("ddMMyyyyHHmmssffff") + fInfo.Extension;
                FUImg.SaveAs(Server.MapPath("~/Upload/") + fname);
                Session["ImgLoc1"] = fname;
            }
            else
            {
                lblMsg.Text = "Please Select .jpeg / .jpg / .png File";
            }
        }
        else
        {
            lblMsg.Text = "Please select Image";
        }
    }
    protected void vidUpload(FileUpload FUImg)
    {

        if (FUImg.HasFile)
        {
            fname = FUImg.FileName;
            fInfo = new FileInfo(fname);
            string ext = fInfo.Extension.ToLower();

            if (ext == ".mp4" || ext == ".3gp" || ext == ".mkv")
            {
                path = Server.MapPath("~/Upload/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                fname = Convert.ToString(Session["LUID"]) + "vid" + DateTime.Now.ToString("ddMMyyyyHHmmssffff") + fInfo.Extension;
                FUImg.SaveAs(Server.MapPath("~/Upload/") + fname);
                Session["VidLoc1"] = fname;
            }
            else
            {
                lblMsg.Text = "Please Select .mp4 / .3gp / .mkv File";
            }
        }
        else
        {
            lblMsg.Text = "Please select Video";
        }
    }




    protected void ddlMainCategoryId_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillddlCategoryId(Convert.ToInt32(ddlMainCategoryId.SelectedValue));

        if (ddlMainCategoryId.SelectedValue == "1" || ddlMainCategoryId.SelectedValue == "2")
        {
            pnlProductMainDetail.Visible = true;
            pnlProductMainDetailImg.Visible = true;
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ddlCategoryId.SelectedValue + "');", true);

        }
        else
        {
            pnlProductMainDetail.Visible = false;
            pnlProductMainDetailImg.Visible = false;
        }
    }


}