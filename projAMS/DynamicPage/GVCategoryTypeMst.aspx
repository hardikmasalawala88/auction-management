﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DynamicPage/mstAdmin.master" AutoEventWireup="true" CodeFile="GVCategoryTypeMst.aspx.cs" Inherits="GVCategoryTypeMst" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-content">
          <h3 class="page-title">Category Detail 
                    <small>View , Add & Edit </small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="DefaultAdmin.aspx" >Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">CategoryDetail</a>
                        </li>
                    </ul>
                    
                </div>
                <!-- END PAGE HEADER-->


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>




                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>View of Category 
                            
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>

                        </div>
                    </div>


                    <div class="portlet-body">

                        <div class="portlet-title tools">

                            <asp:GridView ID="GVCategoryTypeMst1" runat="server"
                                AllowPaging="True" ShowFooter="True"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4" ViewStateMode="Enabled"
                                ShowHeaderWhenEmpty="True"
                                OnPageIndexChanging="GVCategoryTypeMst1_PageIndexChanging"
                                OnRowCancelingEdit="GVCategoryTypeMst1_RowCancelingEdit"
                                OnRowCommand="GVCategoryTypeMst1_RowCommand"
                                OnRowEditing="GVCategoryTypeMst1_RowEditing"
                                OnRowUpdating="GVCategoryTypeMst1_RowUpdating">
                                <AlternatingRowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="Category Id"
                                        ControlStyle-Height="20px"
                                        ItemStyle-Width="250px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryTypeId"
                                                runat="server"
                                                Text='<%#Eval("CategoryTypeId") %>' />

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAdd"
                                                runat="server"
                                                class="control-label"
                                                Text="Enter New Master Category Type : " />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Category" ControlStyle-Height="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryType"
                                                runat="server"
                                                Text='<%#Eval("CategoryType") %>'
                                                Height="20px" />

                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:TextBox ID="txtCategoryType"
                                                runat="server"
                                                Text='<%#Eval("CategoryType") %>' />

                                           <asp:filteredtextboxextender id="FTBtxtCategoryType" runat="server"
                                                filtertype="lowercaseletters,uppercaseletters,custom"
                                                filtermode="validchars"
                                                validchars=" "
                                                targetcontrolid="txtCategoryType">
                                            </asp:filteredtextboxextender>
                                       
                                            
                                            
                                            
                                             <asp:RequiredFieldValidator ID="rfvCategoryType"
                                                runat="server"
                                                ErrorMessage="Can't be Blank"
                                                ControlToValidate="TxtCategoryType"
                                                Display="Dynamic"
                                                Text="*Can't be Blank"
                                                ValidationGroup="EditValid" />

                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAddCategoryType"
                                                runat="server"
                                                CssClass=" form-control" />

                                            <asp:filteredtextboxextender id="FTBtxtCategoryType" runat="server"
                                                filtertype="lowercaseletters,uppercaseletters,custom"
                                                filtermode="validchars"
                                                validchars=" "
                                                targetcontrolid="txtAddCategoryType">
                                            </asp:filteredtextboxextender>
                                       

                                            <asp:RequiredFieldValidator ID="rfvAddCategoryType"
                                                runat="server"
                                                ErrorMessage="Can't be Blank"
                                                ControlToValidate="txtAddCategoryType"
                                                Display="Dynamic"
                                                Text="*Can't be Blank" />

                                        </FooterTemplate>
                                        <ControlStyle Height="20px" />

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-Width="130px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBtnEdit"
                                                runat="server"
                                                ImageUrl="~/DynamicPage/img/edit-grid.png"
                                                AlternateText="Edit"
                                                ImageAlign="Middle"
                                                Height="25px"
                                                Width="30px"
                                                CausesValidation="false"
                                                ToolTip="Edit"
                                                CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="btnUpdate"
                                                ImageUrl="~/DynamicPage/img/update.png"
                                                ToolTip="Update"
                                                AlternateText="Update"
                                                runat="server"
                                                CommandName="Update"
                                                ValidationGroup="EditValid" />&nbsp;&nbsp;
                                            <asp:ImageButton ID="btnCancel"
                                                ImageUrl="~/DynamicPage/img/cancel.png"
                                                ToolTip="Cancle"
                                                runat="server"
                                                AlternateText="Cancel"
                                                CommandName="Cancel"
                                                CausesValidation="false" />
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:Button ID="btnAddRecord"
                                                runat="server"
                                                Text="Add"
                                                CssClass="btn-default"
                                                CommandName="Add" />
                                        </FooterTemplate>
                                        <ItemStyle VerticalAlign="Middle" />
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                <%--    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />--%>
                    <asp:Label ID="lblMsg" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div> 
</asp:Content>

