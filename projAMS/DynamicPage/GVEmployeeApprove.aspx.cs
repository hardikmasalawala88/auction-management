﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using DAL;

public partial class DynamicPage_GVEmployeeApprove : System.Web.UI.Page
{

    boEmployeeDetail boObj = new boEmployeeDetail();
    balEmployeeDetail balObj = new balEmployeeDetail();
    DataSet ds = new DataSet();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();
            //if (Session["Uid"] == null)
            //{
            //    Response.Redirect("LogIn.aspx");
            //}
            //else
            //{
            //    FillGrid();
            //}
            lblMsg.Text = "";
        }
    }

    protected void FillGrid()
    {

        GVEmployeeReg1.DataSource = balObj.Load(); ;
        GVEmployeeReg1.DataBind();

    }
    protected void GVEmployeeReg1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVEmployeeReg1.PageIndex = e.NewPageIndex;
        GVEmployeeReg1.DataSource = balObj.Load();
        GVEmployeeReg1.DataBind();
    }
    protected void chkApproval_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            CheckBox chk = (CheckBox)sender;
            int eId = Convert.ToInt32(chk.CssClass.ToString());
            Session["EmpId"] = eId;
            boObj.EmpId = Convert.ToInt32(eId);
            if (chk.Checked == true)
            {
                boObj.IsActive = Convert.ToInt16(true);

                ds = new DataSet();
                Boolean success = balObj.Approval(boObj);
                if (success == true)
                {
                    ds = new DataSet();
                    ds = balObj.getEmail(boObj);
                    boObj.EmailId = ds.Tables[0].Rows[0][0].ToString();
                    boObj .EmpName =ds .Tables [0].Rows [0][1].ToString ();
                    string email = ds.Tables[0].Rows[0][0].ToString();
                 
                    string passwd = email.Substring(0, 4);
                    string str1 = balObj.GetPassword();
                    passwd += str1;
                    string t = "Dear "+ boObj .EmpName   + ",<br />" + "<br />" 
                        +"       Welcome To Our WorldAuction , We Received Your Application Of Joing to Our Team And " + "<br />"
                        + "It is Our Pleasure To Inform You That Your Application Is Approved And" + "<br />"
                        + "Now You Can able To LogIn To Your Account By Following Unique UserName" + "<br />"
                        + "And Password." + "<br />" + "<br />" 
                        + "Email Id : " + email + "<br />"
                        + "Password : " + passwd + "<br />" + "<br />"
                        + "Thank You For Joining Our Team.";

                    clsUtility.SendMail(email, "Your Password", t);
                    boObj.Passwd = passwd;
                    balObj.PassBinary(boObj);


                     //FillGrid();
                    lblMsg.Text = "Sent Message";
                   
                    
                    //PAssword COde
                    //ds = new DataSet();
                    //ds = balObj.setpasswd(boObj);
                    //string passwd = Convert.ToString(ds.Tables[0].Rows[0][1].ToString());
                    //int pwd = passwd.IndexOf("#");
                    //if (pwd != -1)
                    //{
                    //    // objMail = new Mail();
                    //string pwd = balObj.GetRandomAlphaNumeric();                   
                    //    int len = passwd.Length;
                    //    int l = len = pwd;
                    //    passwd = passwd.Substring(0, l);
                    //    string t = "Welcome To Our Auction World" + "<br />" + "Email Id :" + email + "<br />" + "Password :" + passwd;
                    //    l.SendMail(email, "Your Password", t);
                    //    FillGrid();
                    //    lblMsg.Text = "Sent Message";
                    //}
                    //else
                    //{

                    //    //lblMsg.Text = "<br />" + "Email Id :" + email + "<br />" + "Password :" + passwd;
                    //    string t = "Welcome To Auction World" + "<br />" + "Email Id :" + email + "<br />" + "Password :" + passwd;
                    //    clsUtility.SendMail(email, "Your Password", t);

                    //    FillGrid();
                    //    lblMsg.Text = "Sent Message";
                    //    // lblMsg.Text = "Sent Message";
                    //}


                }
            }

            else
            {
                if (balObj.LeaveDate(boObj))
                {
                    lblMsg.Text = "Block SuccessFully";
                }
            }
        }
        catch { }


    }

    protected void btnSearchEmployee_Click(object sender, EventArgs e)
    {
        GVEmployeeReg1.DataSource = balObj.AdminSearchData(txtSearchEmployee.Text);
        GVEmployeeReg1.DataBind();
    }

    protected void GVEmployeeReg1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GVEmployeeReg1.PageIndex = e.NewSelectedIndex;
        GVEmployeeReg1.DataSource = balObj.Load();
        GVEmployeeReg1.DataBind();

    }
}
