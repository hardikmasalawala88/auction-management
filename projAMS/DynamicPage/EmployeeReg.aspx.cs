﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class DynamicPage_EmployeeReg : System.Web.UI.Page
{
    boEmployeeDetail boObj = new boEmployeeDetail();
    balEmployeeDetail balObj = new balEmployeeDetail();
    DataSet ds = new DataSet();
    //string mEmpId;
    string path, fname;
    FileInfo fInfo;
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //string s = (string)Session["User"];
        if (Session["MstSite"] != null)
        {
            this.MasterPageFile = Session["MstSite"].ToString();
        }
        else
        {
            Response.Redirect("../StaticPage/Login.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();

        }

    }
    protected void FillGrid()
    {

        GVEmployeeReg1.DataSource = balObj.LoadApprove();
        GVEmployeeReg1.DataBind();
       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //Boolean Success;


        boObj.EmpId = Convert.ToInt32(txtEmpId.Text.Length) == 0 ? 0 : Convert.ToInt32(txtEmpId.Text);
        boObj.EmpName = txtEmpName.Text;
        boObj.Addr = txtAddress.Text;
        if (rdbMale.Checked == true)
        {
            //rbmale.Checked;
            boObj.Gender = 1;
        }
        else
        {
            // rbfemale.Checked;
            boObj.Gender = 0;
        }
        boObj.Dob = Convert.ToString(txtDOB.Text);
        boObj.ContactNo = txtContactNo.Text;
        boObj.EmailId = txtEmailId.Text;
        //boObj.Designation = txtDesignation.Text;
        boObj.Designation = ddlDesig.SelectedValue;
        boObj.Doj = Convert.ToString(txtDOJ.Text);
        boObj.IsActive = Convert.ToInt16(false);
        boObj.EmpImage = Session["ImgLoc1"].ToString();



        //if (Session["ImgLoc1"].ToString() == "0")
        //{
        //    if (Session["ImgLocE1"].ToString() == "")  // where Session["ImgLocE1"]:declare on edit btn click...
        //    {
        //        if (fuImg.HasFile)
        //        {
        //            fname = fuImg.FileName;
        //            fInfo = new FileInfo(fname);
        //            string ext = fInfo.Extension.ToLower();
        //            if (ext == ".jpeg" || ext == ".jpg" || ext == ".png")
        //            {

        //                boObj.EmpImage = SaveImg(fInfo);
        //            }
        //            else
        //            {
        //                lblMsg.Text = "Please Select .jpeg / .jpg / .png File";
        //            }
        //        }
        //    }
        //    else
        //    {
        //        boObj.EmpImage = Session["ImgLocE1"].ToString();
        //    }
        //}
        //else
        //{
        //    boObj.EmpImage = Session["ImgLoc1"].ToString();
        //    if (Session["ImgLocE1"] != null && File.Exists(Server.MapPath("~/Upload/" + Session["ImgLocE1"])))
        //        File.Delete(Server.MapPath("~/Upload/" + Session["ImgLocE1"]));
        //}
        if (txtEmpId.Text.Length == 0)
        {
            btnCheckEmail_Click(sender, e);
        }
        balObj.Insert(boObj);
        FillGrid();
        PanelRegistrationForm.Visible = false;
        PanelEntryEmployeeGrid.Visible = true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        PanelRegistrationForm.Visible = false;
        PanelEntryEmployeeGrid.Visible = true;
    }

    public void ClearPanelRegistrationForm()
    {
        txtAddress.Text = "";
        txtContactNo.Text = "";
        //txtDesignation.Text = "";
        txtDOB.Text = "";
        txtDOJ.Text = "";
        txtEmailId.Text = "";
        txtEmpId.Text = "";
        txtEmpName.Text = "";
        imgProfile.ImageUrl = "";

    }

    protected void btnAddNewEmp_Click(object sender, EventArgs e)
    {
        ClearPanelRegistrationForm();

        PanelEntryEmployeeGrid.Visible = false;
        PanelRegistrationForm.Visible = true;

    }
    protected void ImgBtnEdit_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {

            PanelEntryEmployeeGrid.Visible = false;
            PanelRegistrationForm.Visible = true;
            txtEmpId.Visible = false;
            lblEmpId.Visible = false;
            boObj.EmpId = Convert.ToInt32(e.CommandArgument.ToString());
            ds = balObj.getData(boObj);

            txtEmpId.Text = ds.Tables[0].Rows[0]["EmpId"].ToString();
            txtEmpName.Text = ds.Tables[0].Rows[0]["EmpName"].ToString();

            if (ds.Tables[0].Rows[0]["Gender"].ToString() == "True")
            {
                rdbMale.Checked = true;
            }
            else
            {
                rdbFemale.Checked = true;
            }

            txtAddress.Text = ds.Tables[0].Rows[0]["Addr"].ToString();
            txtContactNo.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
            txtDOB.Text = ds.Tables[0].Rows[0]["Dob"].ToString();
            txtDOJ.Text = ds.Tables[0].Rows[0]["Doj"].ToString();
            ddlDesig.SelectedValue= ds.Tables[0].Rows[0]["Designation"].ToString();
            imgProfile.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["EmpImage"].ToString();

            Session["ImgLoc1"] = ds.Tables[0].Rows[0]["EmpImage"].ToString();
            txtEmailId.Text = ds.Tables[0].Rows[0]["EmailId"].ToString();
        }
    }
    protected void GVEmployeeReg1_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }




    //checked Email
    protected void btnCheckEmail_Click(object sender, EventArgs e)
    {
        boObj.EmailId = txtEmailId.Text;
        ds = balObj.verifyEmail(boObj);
        string count = ds.Tables[0].Rows[0][0].ToString();

        //lblemsg.Visible = true;
        if (count == "1")
        {
            lblECheck.Text = "Email Id Already Exists";
        }
        else
        {
            lblECheck.Text = "Email Approved";
        }
        PanelEntryEmployeeGrid.Visible = false;
        PanelRegistrationForm.Visible = true;


    }

    //FileUpload Butten
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fuImg.HasFile)
        {
            fname = fuImg.FileName;
            fInfo = new FileInfo(fname);
            string ext = fInfo.Extension.ToLower();

            if (ext == ".jpeg" || ext == ".jpg" || ext == ".png")
            {
                // int fileSize = FileUpload1.PostedFile.ContentLength;
                string logopath = SaveImg(fInfo);
                boObj.EmpImage = logopath;
                imgProfile.ImageUrl = "~/Upload/" + logopath;
                Session["ImgLoc1"] = logopath;

            }
            else
            {
                lblMsg.Text = "Please Select .jpeg / .jpg / .png File";
            }
            PanelRegistrationForm.Visible = true;
            PanelEntryEmployeeGrid.Visible = false;
        }
    }

    //SaveImage Function
    protected string SaveImg(FileInfo fInfo)
    {
        path = Server.MapPath("~/Upload/");
        createdir(path);
        fname = "img" + DateTime.Now.ToString("ddMMyyHHmmss") + fInfo.Extension;
        path = path + fname;
        fuImg.SaveAs(path);
        return fname;
    }

    //Create Upload DIrectiry If Not Exist
    protected void createdir(string f2)
    {
        if (!Directory.Exists(f2))
        {
            Directory.CreateDirectory(f2);
        }
    }

    protected void btnSearchEmployee_Click(object sender, EventArgs e)
    {
        GVEmployeeReg1.DataSource = balObj.SearchData(txtSearchEmployee.Text);
        GVEmployeeReg1.DataBind();
    }


}