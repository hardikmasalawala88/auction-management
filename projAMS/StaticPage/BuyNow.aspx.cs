﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class StaticPage_BuyNow : System.Web.UI.Page
{
    boAddProduct boObjAddProd = new boAddProduct();
    balProductFullDetail balObjFullD = new balProductFullDetail();
    boClientRegistration boClientObj = new boClientRegistration();
    balClientRegistration balClientObj = new balClientRegistration();
    boProductBid boObjPBid = new boProductBid();
    balProductBid balObjPB = new balProductBid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillBuyNowData();
        }

    }
    protected void FillBuyNowData()
    {
        if (Session["LUID"] != null)
        {

            if (Session["showD"] != null)
            {
                boObjAddProd.ProductId = Convert.ToInt32(Session["showD"]);

                //lblProductName.Text = Session["showD"].ToString();
                DataSet ds = new DataSet();
                ds = balObjFullD.getProductSpec(boObjAddProd);
                Session["pId"] = ds.Tables[0].Rows[0]["ProductId"].ToString();
                Session["cId"] = ds.Tables[0].Rows[0]["CategoryId"].ToString();
                Session["Qtys"] = ds.Tables[0].Rows[0]["Qty"].ToString();
                LblVpn.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
                LblVpq.Text =ds.Tables[0].Rows[0]["Qty"].ToString();
                LblVbna.Text = ds.Tables[0].Rows[0]["Price"].ToString();
                LblVsd.Text = ds.Tables[0].Rows[0]["ShippingDesc"].ToString();
                LblVsc.Text = ds.Tables[0].Rows[0]["ShippingCharge"].ToString();
                //lblProductName.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
                //lblBuyNowAmt.Text = ds.Tables[0].Rows[0]["Price"].ToString();
                if (ds.Tables[0].Rows[0]["Image1"].ToString() != "NULL")
                {
                    Img1.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image1"].ToString();
                }
                else { Img1.Visible = false; }

                

                //NImg1.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image1"].ToString();
               

                //string f1 = ds.Tables[0].Rows[0]["FeaturesAndBenefits"].ToString();
                //if (f1.Length > 20)
                //{
                //    f1 = f1.Insert(20, "<br />");
                //}
                //lblFeatures.Text = f1;
                //lblProductCondition.Text = ds.Tables[0].Rows[0]["ProductCondition"].ToString();
                //lblProductDesc.Text = ds.Tables[0].Rows[0]["ProductDesc"].ToString();
                //lblAdditionalInfo.Text = ds.Tables[0].Rows[0]["AdditionalInfo"].ToString();
                //lblShippingCharge.Text = ds.Tables[0].Rows[0]["ShippingCharge"].ToString();
                //lblShippingDesc.Text = ds.Tables[0].Rows[0]["ShippingDesc"].ToString();
                //lblWarrenty.Text = ds.Tables[0].Rows[0]["Warranty"].ToString();
                //lblAuctionName.Text = ds.Tables[0].Rows[0]["AuctionTypeName"].ToString();
                //lblQty.Text = ds.Tables[0].Rows[0]["Qty"].ToString();
                //lblPrice.Text = ds.Tables[0].Rows[0]["Price"].ToString();
                if (Convert.ToString(Session["LDesig"]) == "Client")
                {
                    //Client Data Update
                    boClientObj.ClientId = Convert.ToInt32(Session["LUID"]);
                    ds = balClientObj.getData(boClientObj);
                    // ds.Tables[0].Rows[0]["EmpId"].ToString();

                    LblVbn.Text = ds.Tables[0].Rows[0]["ClientName"].ToString();
                 
                    //DDLGender.SelectedValue = ds.Tables[0].Rows[0]["Gender"].ToString();
                    LblVba.Text = ds.Tables[0].Rows[0]["ClientAddr"].ToString();
                    //Txtcon.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                    //TxtShip.Text = ds.Tables[0].Rows[0]["ShippingAddr"].ToString();
                    //TxtEmail.Text = ds.Tables[0].Rows[0]["EmailId"].ToString();
                }
            }
        }
        else
        {
            //redirect to login
            Response.Redirect("~/StaticPage/Login.aspx");
        }
    }

    protected void btnBuyNow_Click(object sender, EventArgs e)
    {

        if (Session["LUID"] != null)
        {
            //if(Convert.ToInt16(Session["cId"])=="PackageAuction")

            boObjPBid.ProductId = Convert.ToInt32(Session["showD"]);
            boObjPBid.CategoryId = Convert.ToInt16(Session["cId"]);
            boObjPBid.ClientId = Convert.ToInt32(Session["LUID"]);
            boObjPBid.IsBuyNowAmt = true;
            boObjPBid.BuyQty = Convert.ToInt16(Session["Qtys"]);
            //boObjAutoBid.IncreaseBidAmt = Convert.ToInt32(txtIBidAmt.Text);
            //boObjAutoBid.BidFreq = "00:" + txtMBidFreq.Text + ":" + txtSBidFreq.Text;//Combine Minute Second
            //boObjAutoBid.Amount = Convert.ToInt32(txtMaxBidAmt.Text);

            balObjPB.BuyNowBtn(boObjPBid);
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Product Buy Successfully');", true);
           
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Without login You Can not Buy Product');", true);
        }
    }
}