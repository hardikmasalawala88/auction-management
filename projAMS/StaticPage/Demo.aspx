﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="Demo.aspx.cs" Inherits="StaticPage_Demo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-4" style="padding-top: 15em">
        <div class="cart-total-box">
            <div class="cart-head">
                <h2 class="cart-product">ENTER PERSONAL DETAIL</h2>
            </div>
            <ul>
                <li>
                    <asp:Label runat="server" ID="lblPaymentType" Text="Payment Type" />
                </li>
            </ul>
            <table>
                <tr>


                    <td style="padding-left: 75px;">
                        <asp:RadioButton runat="server" ID="rdbVisaCard" GroupName="Card" Text="VISA Card" />
                    </td>
                    <td style="padding-left: 80px;">
                        <asp:RadioButton runat="server" ID="rdbMasterCard" GroupName="Card" Text="Master Card" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 75px;">
                        <asp:RadioButton runat="server" ID="rdbMaestro" GroupName="Card" Text="Maestro Card" />
                    </td>
                    <td style="padding-left: 80px;">
                        <asp:RadioButton runat="server" ID="rdbPayPal" GroupName="Card" Text="Pay Pal" />
                    </td>
                </tr>
            </table>
            <ul>
                <li></li>
                <li>
                    <asp:Label ID="lblBankName" runat="server" Text="Bank Name" />
                </li>
                <li>
                    <asp:TextBox runat="server" ID="txtBankName" placeholder="Valid Bank Name" />
                </li>

                <li>
                    <asp:Label ID="lblBankAccount" runat="server" Text="Bank Account" />
                </li>
                <li>
                    <asp:TextBox runat="server" ID="txtBankAccount" placeholder="Valid Bank Account" />
                </li>

                <li>
                    <asp:Label ID="lblCardName" runat="server" Text="Card Number" />
                </li>
                <li>
                    <asp:TextBox runat="server" ID="txtCardNumber" placeholder="Valid Card Numbert" />
                </li>

                <li>
                    <asp:Button ID="btnSubmit" runat="server" Text="Purchase" Width="100px" cssclass="btn default "/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="100px" Style="margin-left: 20px" cssclass="btn default"/>
                </li>

               
            </ul>

        </div>
    </div>
</asp:Content>

