﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="FinishedProduct.aspx.cs" Inherits="StaticPage_FinishedProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="block" id="inner-head" style="padding-top: 15em;">
        <div class="fixed-img sec-bg4"></div>
        <div class="container">
            <h1>Finshed Auctions</h1>
        </div>
    </section>


    <section class="block">
        <div class="container">

            <div class="row">
                
                <asp:DataList ID="DLFinishedProduct" runat="server"
                    RepeatColumns="4"
                    RepeatDirection="Horizontal"
                    Width="900px"
                    CellPadding="20">
                    <ItemTemplate>



                        <div class="latest-categories">
                            <asp:Image runat="server" ID="ImgFinished" Height="250" Width="280" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' />


                            <ul>
                                <li>
                                    <i>
                                        <asp:Image runat="server" ID="ImgWinner" ImageUrl='<%#Bind("ClientImage","~/Upload/{0}") %>'
                                            Style="width: 40px; height: 40px; border-radius: 150px; -webkit-border-radius: 150px; -moz-border-radius: 150px; margin-bottom: 5px; box-shadow: 0 0 8px rgba(0, 0, 0, .8); -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8); -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);" />
                                    </i>
                                    <asp:Label runat="server" ID="lblWinner" Text='<%#Eval("ClientName") %>' Font-Size="20px" />
                                </li>

                                <li><i class="fa fa-check"></i>
                                    <p>
                                        <asp:Label runat="server" ID="lblFProductName" Text='<%#Eval("ProductName") %>' />
                                    </p>
                                </li>

                                <li><i class="fa fa-check"></i>
                                    <p>
                                        <asp:Label runat="server" ID="lblAuctionName" Text='<%#Eval("AuctionTypeName") %>' />
                                    </p>
                                </li>
                                <li><i class="fa fa-shopping-cart"></i>
                                    <p>
                                        SellingPrice : 
                                            <asp:Label runat="server" ID="lblFinishedPrice" Text='<%#Eval("BidAmt") %>' />
                                    </p>
                                </li>

                            </ul>
                        </div>
                        <%--</div>--%>
                    </ItemTemplate>
                </asp:DataList>
       
                
            </div>
        </div>
    </section>

</asp:Content>

