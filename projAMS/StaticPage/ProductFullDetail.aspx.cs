﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class StaticPage_ProductFullDetail : System.Web.UI.Page
{
    balProductFullDetail balObjFullD = new balProductFullDetail();

    balProductBid balObjPB = new balProductBid();
    boProductBid boObjPBid = new boProductBid();

    balSetAutoBid balObjAutoBid = new balSetAutoBid();
    boSetAutoBid boObjAutoBid = new boSetAutoBid();


    boAddProduct boObjAddProd = new boAddProduct();
    balDefaultProductView balObjDP = new balDefaultProductView();
    boClientRegistration boObjClientR = new boClientRegistration();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillData();
            getMaxBidAmt();
            lblMsg.Text = "";
        }
    }
    protected void fillData()
    {
        if (Session["showD"] != null)
        {
            boObjAddProd.ProductId = Convert.ToInt32(Session["showD"]);

            //lblProductName.Text = Session["showD"].ToString();
            DataSet ds = new DataSet();
            ds = balObjFullD.getProductSpec(boObjAddProd);
            Session["pId"] = ds.Tables[0].Rows[0]["ProductId"].ToString();
            HFProductId.Value = ds.Tables[0].Rows[0]["ProductId"].ToString();
            Session["cId"] = ds.Tables[0].Rows[0]["CategoryId"].ToString();
            Session["Qtys"] = ds.Tables[0].Rows[0]["Qty"].ToString();
            lblProductName.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
            lblBuyNowAmt.Text = ds.Tables[0].Rows[0]["Price"].ToString();
            if (ds.Tables[0].Rows[0]["Image1"].ToString() != "NULL")
            {
                Img1.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image1"].ToString();
            }
            else { NImg1.Visible = false; }

            if (ds.Tables[0].Rows[0]["Image2"].ToString() != "NULL")
            {
                Img2.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image2"].ToString();
            }
            else { NImg2.Visible = false; }

            if (ds.Tables[0].Rows[0]["Image3"].ToString() != "NULL")
            {
                Img3.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image3"].ToString();
            }
            else { NImg3.Visible = false; }

            if (ds.Tables[0].Rows[0]["Image4"].ToString() != "NULL")
            {
                Img4.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image4"].ToString();
            }
            else { NImg4.Visible = false; }

            NImg1.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image1"].ToString();
            NImg2.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image2"].ToString();
            NImg3.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image3"].ToString();
            NImg4.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["Image4"].ToString();
            
            lblFeatures.Text = ds.Tables[0].Rows[0]["FeaturesAndBenefits"].ToString();
            lblProductCondition.Text = ds.Tables[0].Rows[0]["ProductCondition"].ToString();
            lblProductDesc.Text = ds.Tables[0].Rows[0]["ProductDesc"].ToString();
            lblAdditionalInfo.Text = ds.Tables[0].Rows[0]["AdditionalInfo"].ToString();
            lblShippingCharge.Text = ds.Tables[0].Rows[0]["ShippingCharge"].ToString();
            lblShippingDesc.Text = ds.Tables[0].Rows[0]["ShippingDesc"].ToString();
            lblWarrenty.Text = ds.Tables[0].Rows[0]["Warranty"].ToString();
            lblAuctionName.Text = ds.Tables[0].Rows[0]["AuctionTypeName"].ToString();
            if (lblAuctionName.Text.Equals("SEALDED AUCTION"))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert( 'In SEALDED AUCTION You can Bid Only One Timenn');", true);
            }
            lblQty.Text = ds.Tables[0].Rows[0]["Qty"].ToString();
            lblPrice.Text = ds.Tables[0].Rows[0]["Price"].ToString();

            //var d1 = Convert.ToDateTime(ds.Tables[0].Rows[0]["TotalST"]);
            var sd = DateTime.Now.ToString("dd-MM-yyyy");
            if (Convert.ToDateTime(ds.Tables[0].Rows[0]["startDt"]) > Convert.ToDateTime(sd))//15-04-2015 //Check Start Date is Greater then Current Date 
            {
                 
                btnBid.Enabled = false;
                btnBuyNow.Enabled = false;
                txtBidAmt.Enabled = false;
                
                 ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert( 'Auction Start In Future so Bidding not Allow');", true);
                //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ds.Tables[0].Rows[0]["StartDt"] + "');", true);
            }
            else 
            {
                btnBid.Enabled = true; btnBuyNow.Enabled = true; txtBidAmt.Enabled = true;
            }

            if (Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDt"]) < Convert.ToDateTime(sd))//15-04-2015 //Check End Date is Less then Current Date 
            {
                btnBid.Enabled = false;
                btnBuyNow.Enabled = false;
                txtBidAmt.Enabled = false;

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert( 'Auction Already Finished so Bidding not Allow');", true);
                //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ds.Tables[0].Rows[0]["StartDt"] + "');", true);
            }
            //else
            //{
            //    btnBid.Enabled = true; btnBuyNow.Enabled = true; txtBidAmt.Enabled = true;
            //}
            if (lblAuctionName.Text == "PACKAGE AUCTION")
            {
                pnlAutoBid.Visible = true;
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }

    protected void fillTimerWinner()
    {
        if (Session["showD"] != null)
        {
            boObjAddProd.ProductId = Convert.ToInt32(Session["showD"]);

            //lblProductName.Text = Session["showD"].ToString();
            DataSet ds = new DataSet();
            ds = balObjFullD.getProductSpec(boObjAddProd);
            if (ds.Tables[0].Rows[0]["RDay"].ToString() != "0")
            {
                lblTimer.Text = ds.Tables[0].Rows[0]["RDay"].ToString() + " Day " + ds.Tables[0].Rows[0]["RTime"].ToString() + " Remaining";
            }
            else
            {
                lblTimer.Text = ds.Tables[0].Rows[0]["RTime"].ToString() + " Remaining";
            }


        }
    }

    protected DataSet fillDLItems()
    {
        DataSet ds = new DataSet();
        ds = balObjDP.getDataProductFullDetail();
        return ds;

    }

    protected void btnBid_Click(object sender, EventArgs e)
    {
        if (Session["LUID"] != null)
        {
            boObjPBid.ProductId = Convert.ToInt16(Session["pId"]);
            //DataSet ds = new DataSet();
            //boObjPBid.ClientId = Convert.ToInt32(Session["WCid"]);
            //ds = balObjFullD.getCurrentWinner(boObjPBid);
            if (Session["CWinnerId"] != Session["LUID"])
            {
                DataSet ds = new DataSet();
                ds = balObjPB.getMaxAmt(boObjPBid);
                if (Convert.ToInt32(txtBidAmt.Text) > Convert.ToInt32(ds.Tables[0].Rows[0]["BidAmt"]))
                {
                    boObjPBid.BidAmt = Convert.ToInt32(txtBidAmt.Text);
                    boObjPBid.CategoryId = Convert.ToInt16(Session["cId"]);
                    boObjPBid.ProductId = Convert.ToInt32(Session["pId"]);
                    boObjPBid.BidId = 0;
                    boObjPBid.BuyQty = Convert.ToInt16(Session["Qtys"]);
                    boObjPBid.BidAmt = Convert.ToInt32(txtBidAmt.Text);
                    boObjPBid.ClientId = Convert.ToInt32(Session["LUID"]);
                    if (lblAuctionName.Text.Equals("SEALDED AUCTION"))
                    {
                        boObjPBid.CategoryId = Convert.ToInt16(Session["cId"]);
                        boObjPBid.ProductId = Convert.ToInt32(Session["pId"]);
                        boObjPBid.ClientId = Convert.ToInt32(Session["LUID"]);
                        ds = balObjPB.CheckSealded(boObjPBid);
                        if (ds.Tables[0].Rows[0]["ClientId"].ToString() != "0")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('In SEALDED AUCTION You Can Bid Only One Time');", true);
                            return;
                        }
                    }
                    balObjPB.BidBtn(boObjPBid);
                    lblMsg.Text = "Bidded Succesfully";
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                    lblCurrentPrice.Text = Convert.ToInt32(ds.Tables[0].Rows[0]["BidAmt"]).ToString();
                    lblSave.Text = (Convert.ToInt32(lblPrice.Text) - Convert.ToInt32(lblCurrentPrice.Text)).ToString();
                    getMaxBidAmt();


                }
                else
                {
                    lblMsg.Text = "Enter Greter Amount";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                    txtBidAmt.Focus();
                    getMaxBidAmt();
                }
            }
            else 
            {
                lblMsg.Text = "You are Current Winner So Bid Not Perform";
            }
        }
        else
        {
            //redirect to login
            Response.Redirect("~/StaticPage/Login.aspx");

        }


    }

    protected void getMaxBidAmt()
    {
        boObjPBid.ProductId = Convert.ToInt16(Session["pId"]);
        DataSet ds = new DataSet();
        ds = balObjPB.getMaxAmt(boObjPBid);
        if (ds != null)
        {
            if (ds.Tables.Count != 0)
            {
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["BidAmt"]) != 0)
                {
                    lblMaxBidAmt.Text = Convert.ToInt32(ds.Tables[0].Rows[0]["BidAmt"]).ToString();
                    lblCurrentPrice.Text = lblMaxBidAmt.Text;
                    lblSave.Text = (Convert.ToInt32(lblPrice.Text) - Convert.ToInt32(lblCurrentPrice.Text)).ToString();

                    Session["WCid"] = Convert.ToInt32(ds.Tables[0].Rows[0]["ClientId"]);
                }
                CurrentWinner();
            }
        }
    }

    public void CurrentWinner()
    {
        DataSet ds = new DataSet();
        boObjPBid.ClientId = Convert.ToInt32(Session["WCid"]);
        ds = balObjFullD.getCurrentWinner(boObjPBid);
        lblClientName.Text = ds.Tables[0].Rows[0]["ClientName"].ToString();
        Session["CWinnerId"] = ds.Tables[0].Rows[0]["ClientId"].ToString();
        if (ds.Tables[0].Rows[0]["ClientImage"].ToString().Length > 0)
        {
            ImgClient.ImageUrl = "~/Upload/" + ds.Tables[0].Rows[0]["ClientImage"].ToString();
        }

    }

    public void FillGVRecentBid()
    {
        if (Session["showD"] != null)
        {
            boObjAddProd.ProductId = Convert.ToInt32(Session["showD"]);
            GVRecentBid.DataSource = balObjFullD.getRecentBidder(boObjAddProd);
            GVRecentBid.DataBind();
        }
        //CurrentWinner();

    }

    protected void timer1_Tick(object sender, EventArgs e)
    {
        //fillData();
        fillTimerWinner();
        FillGVRecentBid();
        CurrentWinner();
        getMaxBidAmt();
        balObjAutoBid.CallAutoBid();

    }

    protected void btnAutoBid_Click(object sender, EventArgs e)
    {
        lblBidType.Text = "AutoBid";
        pnlButton.Visible = false;
        pnlBid.Visible = true;
    }

    protected void btnSmartBid_Click(object sender, EventArgs e)
    {
        lblBidType.Text = "SmartBid";
        txtMBidFreq.Visible = false;
        txtSBidFreq.Visible = false;
        pnlButton.Visible = false;
        pnlBid.Visible = true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pnlBid.Visible = false;
        pnlButton.Visible = true;
        txtMBidFreq.Visible = true;
        txtSBidFreq.Visible = true;
    }

    //set autobid
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["LUID"] != null)
        {
            //if(Convert.ToInt16(Session["cId"])=="PackageAuction")

            boObjAutoBid.ProductId = Convert.ToInt32(Session["showD"]);
            boObjAutoBid.CategoryId = Convert.ToInt16(Session["cId"]);
            boObjAutoBid.ClientId = Convert.ToInt32(Session["LUID"]);
            boObjAutoBid.IncreaseBidAmt = Convert.ToInt32(txtIBidAmt.Text);
            boObjAutoBid.BidFreq = "00:" + txtMBidFreq.Text + ":" + txtSBidFreq.Text;//Combine Minute Second
            boObjAutoBid.Amount = Convert.ToInt32(txtMaxBidAmt.Text);

            if (lblBidType.Text == "AutoBid")
            {

                balObjAutoBid.InsertAutoBid(boObjAutoBid);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Autobid is Successfully Set');", true);
                txtSBidFreq.Text = string.Empty;
                txtMBidFreq.Text = string.Empty;
                txtMaxBidAmt.Text = string.Empty;
                txtIBidAmt.Text = string.Empty;
            }
            else if (lblBidType.Text == "SmartBid")
            {

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Without login You Can not set Autobid');", true);
        }
    }
  
    protected void btnBuyNow_Click(object sender, EventArgs e)
    {
        //btn for Buy product Now
        if (Session["LUID"] != null)
        {
           
                Session["showD"] = HFProductId.Value;
                Response.Redirect("~/StaticPage/BuyNow.aspx");
          
        }
        else
        {
            //redirect to login
            Response.Redirect("~/StaticPage/Login.aspx");
        }
    }
}