﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="ProductFullDetail.aspx.cs" Inherits="StaticPage_ProductFullDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <section class="block" id="inner-head" style="padding-top: 15em;">
        <div class="fixed-img sec-bg4"></div>
        <div class="container">
            <h1>
                <asp:Label ID="lblAuctionName" runat="server" /></h1>
        </div>
    </section>

    <section class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="single-product-tab ">
                                <div class="tab-content" id="single-post-content">
                                    <div id="single-post1" class="tab-pane fade in active">
                                        <asp:Image runat="server" ID="Img1" ImageUrl=""  />
                                        <%--<img src="images/single-post11.jpg" alt="" />--%>
                                    </div>
                                    <div id="single-post2" class="tab-pane fade">
                                        <asp:Image runat="server" ID="Img2" ImageUrl="" />
                                        <%--<img src="images/single-post22.jpg" alt="" />--%>
                                    </div>
                                    <div id="single-post3" class="tab-pane fade">
                                        <asp:Image runat="server" ID="Img3" ImageUrl="" />
                                        <%--<img src="images/single-post33.jpg" alt="" />--%>
                                    </div>
                                    <div id="single-post4" class="tab-pane fade">
                                        <asp:Image runat="server" ID="Img4" ImageUrl="" />
                                        <%--<img src="images/single-post33.jpg" alt="" />--%>
                                    </div>
                                </div>


                                <ul class="nav nav-tabs" id="single-post-tabs">


                                    <li class="active"><a data-toggle="tab" href="#single-post1">
                                        <asp:Image runat="server" ID="NImg1" ImageUrl="" Height="100" Width="80" />
                                        <%--<img src="images/single-post1.jpg" alt="" />--%></a></li>
                                    <li><a data-toggle="tab" href="#single-post2">
                                        <asp:Image runat="server" ID="NImg2" ImageUrl="" Height="100" Width="80" />
                                        <%--<img src="images/single-post2.jpg" alt="" />--%></a></li>
                                    <li><a data-toggle="tab" href="#single-post3">
                                        <asp:Image runat="server" ID="NImg3" ImageUrl="" Height="100" Width="80" />
                                        <%--<img src="images/single-post3.jpg" alt="" />--%></a></li>
                                    <li><a data-toggle="tab" href="#single-post4">
                                        <asp:Image runat="server" ID="NImg4" ImageUrl="" Height="100" Width="80" />
                                        <%--<img src="images/single-post3.jpg" alt="" />--%></a></li>

                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="single-post-desc">
                                <div class="single-post-head">
                                    <%--<ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li class="un-rate"><i class="fa fa-star"></i></li>
                                    </ul>--%>
                                    <span><i>BuyNow</i>₹<asp:Label runat="server" ID="lblBuyNowAmt" Text="" Font-Size="15px" /></span>
                                    <h3>
                                        <asp:Label runat="server" ID="lblProductName" Text="Product Name" />
                                        <%--<i>40%</i> OFF IN SI IN THE SUMMER--%></h3>
                                    <p>
                                        ProductCondition: <i>
                                            <asp:Label runat="server" ID="lblProductCondition" Text="ConditionProduct" /></i>
                                    </p>
                                    <p>
                                        Quantity : <i>
                                            <asp:Label runat="server" ID="lblQty" Text="0" /></i>
                                    </p>
                                </div>

                                <div class="cart-options">
                                </div>

                                <%--<a href="#" title="">Add to Compare</a>--%>
                                <section class="block" style="margin-top: -150px">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="cart-total-box">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                        </Triggers>
                                                        <ContentTemplate>
                                                            <asp:Timer runat="server" ID="timer1" Interval="850" OnTick="timer1_Tick" />

                                                            <div class="cart-head" style="background-color: white;">
                                                                <h2 class="cart-product">
                                                                    <asp:Label runat="server" ID="lblTimer" Text="Timer" ForeColor="Red" Font-Bold="true" /></h2>
                                                                </ul>
                                                            </div>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="cart-head">
                                                        <h2 class="cart-product">
                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                <ContentTemplate>

                                                                    <asp:Image runat="server" ID="ImgClient" ImageUrl="~/images/DefultUser.jpg"
                                                                        Style="width: 110px; height: 110px; border-radius: 150px; -webkit-border-radius: 150px; -moz-border-radius: 150px; margin-bottom: 5px; box-shadow: 0 0 8px rgba(0, 0, 0, .8); -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8); -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            CURRENT WINNER</h2>
                                                    </div>
                                                    <ul>
                                                        <%--<li>
                                                            <i><span></span>
                                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                    <ContentTemplate>

                                                                                                                                              <asp:Image runat="server" ID="ImgClientImg" ImageUrl="~/images/harik.JPG"
                                                                            Style="width: 175px; height: 115px; border-radius: 150px; -webkit-border-radius: 150px; -moz-border-radius: 150px;" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </i>

                                                        </li>--%>
                                                        <li><span>Name :</span>
                                                            <i>
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>

                                                                        <asp:Label runat="server" ID="lblClientName" Text="Name of winner" Font-Bold="true" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </i>

                                                        </li>
                                                        <li><span>Orignal Price :</span><i> ₹<asp:Label runat="server" ID="lblPrice" Text="0" /></i></li>
                                                        <li><span>Current Price :</span>
                                                            <i>
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        ₹
                                                                        <asp:Label runat="server" ID="lblCurrentPrice" Text="0" Font-Bold="true" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>

                                                            </i>

                                                        </li>
                                                    </ul>
                                                    <ul>
                                                        <li><span>You Save</span>
                                                            <i>
                                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                    <ContentTemplate>
                                                                        ₹<asp:Label runat="server" ID="lblSave" Text="0" ForeColor="Green" /><br />
                                                                        <asp:Label ID="Label1" runat="server" Text="*Shipping charge not included" ForeColor="Red" Font-Size="XX-Small" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>

                                                            </i>
                                                        </li>

                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

                    </div>


                    <div class="shop-tabs">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs shop-tab" id="myTab3">
                                <li class="active"><a data-toggle="tab" href="#tab-photos">Features And Benifits</a></li>
                                <li class=""><a data-toggle="tab" href="#reviews">Product Description</a></li>

                                <li class=""><a data-toggle="tab" href="#video">Additional Information</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent5">
                                <div id="tab-photos" class="tab-pane fade active in">
                                    <div class="tab-features">
                                        <asp:Label runat="server" ID="lblFeatures" Text="Features of product bind" />
                                    </div>
                                </div>
                                <div id="reviews" class="tab-pane fade">


                                    <div class="product-details">
                                        <asp:Panel runat="server" ID="pnlMLSpecification" CssClass="product-details">
                                        </asp:Panel>
                                        <br />
                                        <asp:Label runat="server" ID="lblProductDesc" Text="Description of product bind" /><br />

                                    </div>
                                </div>

                                <div id="video" class="tab-pane fade">
                                    <div class="shipping-return">
                                        <h3>Warrenty</h3>
                                        <asp:Label runat="server" ID="lblWarrenty" Text="Product Warrenty" />

                                        <asp:Panel runat="server" ID="pnlShipping">
                                            <h3>ShippingCharge</h3>
                                            <p>₹<asp:Label runat="server" ID="lblShippingCharge" Text="Charge" /></p>


                                            <h3>ShippingDesc</h3>
                                            <p>
                                                <asp:Label runat="server" ID="lblShippingDesc" Text="Desc shipping" />
                                            </p>
                                        </asp:Panel>


                                        <h3>Additional Information :</h3>
                                        <asp:Label runat="server" ID="lblAdditionalInfo" Text="Description of product bind" />


                                        <%-- <ul>
                                            <li class="left">
                                                <img src="images/shiping1.jpg" alt="" /><p>Phasellus egestas, nunc non consectetur hendrerit, risus mauris cursus velit, et condimentum nisi enim in eros. Nam ullamcorper neque non erat elementum vulputate. Nullam dignissim lobortis interdum. Donec nisi est, tempus eget dignissim vitae,</p>
                                            </li>
                                            <li class="right">
                                                <img src="images/shiping1.jpg" alt="" /><p>Phasellus egestas, nunc non consectetur hendrerit, risus mauris cursus velit, et condimentum nisi enim in eros. Nam ullamcorper neque non erat elementum vulputate. Nullam dignissim lobortis interdum. Donec nisi est, tempus eget dignissim vitae,</p>
                                            </li>
                                        </ul>--%>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-md-3" style="margin-top: -60px">
                    <div class="cart-total-box">

                        <div class="cart-head">
                            <h2 class="cart-product">Manually Bid</h2>
                        </div>
                        <br />
                        <ul>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <asp:Panel runat="server" ID="Panel3">
                                        <br />
                                        <asp:TextBox runat="server" ID="txtBidAmt" placeholder="Enter Bid Amount" ValidationGroup="ManualBid" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtBidAmt"
                                            Display="Dynamic"
                                            ForeColor="Red"
                                            ValidationGroup="ManualBid"
                                            ErrorMessage="Must Enter Amount">
                                            
                                        </asp:RequiredFieldValidator><br />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                            FilterType="Numbers"
                                            TargetControlID="txtBidAmt">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:Label runat="server" ID="lblMsg" Text="" /><br />
                                        Bid More Then  
                                        <asp:Label runat="server" ID="lblMaxBidAmt" Text="0" />

                                    </asp:Panel>
                                    <asp:Button runat="server" ID="btnBid" Text="Bid" 
                                        OnClick="btnBid_Click" 
                                        Height="30px" 
                                        Width="80px" 
                                        ValidationGroup="ManualBid" />
                                    <asp:Button runat="server" ID="btnBuyNow" Text="Buy Now" 
                                        Height="30px" 
                                        Width="80px" 
                                        CausesValidation="false" 
                                        OnClick="btnBuyNow_Click" 
                                       />
                                    <asp:HiddenField ID="HFProductId" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />



                        </ul>



                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlAutoBid" Visible ="false" >
                    <div class="col-md-3" style="margin-top: -40px">
                        <div class="cart-total-box">

                            <div class="cart-head">
                                <h2 class="cart-product">SetUp AutoBid</h2>
                            </div>

                            <ul>

                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel runat="server" ID="pnlButton">
                                            <asp:Button runat="server" ID="btnAutoBid" Text="AutoBid" OnClick="btnAutoBid_Click" CausesValidation="false" />
                                            <asp:Button runat="server" ID="btnSmartBid" Text="SmartBid" OnClick="btnSmartBid_Click" CausesValidation="false" />
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlBid" Visible="false">
                                            <asp:Label runat="server" ID="lblBidType" Text="" /><br />

                                            <asp:TextBox runat="server" ID="txtMaxBidAmt" placeholder="Maximum Affortable amount" Width="200px" /><br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                ControlToValidate="txtMaxBidAmt"
                                                Display="Dynamic"
                                                ForeColor="Red"
                                                ErrorMessage="Must Enter Amount">
                                            
                                            </asp:RequiredFieldValidator><br />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                FilterType="Numbers"
                                                TargetControlID="txtMaxBidAmt" />


                                            <asp:TextBox runat="server" ID="txtIBidAmt" placeholder="Increase Amount Each time" Width="200px" />
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                ControlToValidate="txtIBidAmt"
                                                Display="Dynamic"
                                                ForeColor="Red"
                                                ErrorMessage="Must Enter Amount">
                                            </asp:RequiredFieldValidator>

                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                FilterType="Numbers"
                                                TargetControlID="txtIBidAmt">
                                            </asp:FilteredTextBoxExtender>
                                            <br />
                                            <asp:TextBox runat="server" ID="txtMBidFreq" placeholder="00 Minutes" Width="100px" MaxLength="2" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                ControlToValidate="txtMBidFreq"
                                                Display="Dynamic"
                                                ForeColor="Red"
                                                ErrorMessage="Must Enter Minutes">
                                            </asp:RequiredFieldValidator>
                                            <asp:RangeValidator runat="server" ID="rv1"
                                                ControlToValidate="txtMBidFreq"
                                                Type="Integer"
                                                MaximumValue="59" Display="Dynamic"
                                                MinimumValue="0"
                                                ErrorMessage="please Enter Minute between 00-59" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                FilterType="Numbers"
                                                TargetControlID="txtMBidFreq">
                                            </asp:FilteredTextBoxExtender>

                                            <asp:TextBox runat="server" ID="txtSBidFreq" placeholder="00 Second" Width="100px" MaxLength="2" /><br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                                ControlToValidate="txtSBidFreq"
                                                Display="Dynamic"
                                                ForeColor="Red"
                                                ErrorMessage="Must Enter Second">
                                            </asp:RequiredFieldValidator>
                                            <asp:RangeValidator runat="server" ID="RangeValidator1"
                                                ControlToValidate="txtSBidFreq"
                                                Type="Integer"
                                                MaximumValue="59" Display="Dynamic"
                                                MinimumValue="0"
                                                ErrorMessage="please Enter Second between 00-59" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                FilterType="Numbers"
                                                TargetControlID="txtSBidFreq">
                                            </asp:FilteredTextBoxExtender>

                                            <br />

                                            <asp:Button runat="server" ID="btnSubmit" Text="Start" OnClick="btnSubmit_Click" />
                                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ul>


                        </div>
                    </div>
                </asp:Panel>
                <div class="col-md-3" style="margin-top: -10px">
                    <div class="cart-total-box">

                        <div class="cart-head">
                            <h2 class="cart-product">Recent Bidder</h2>
                        </div>

                        <ul>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel1">
                                        <asp:GridView ID="GVRecentBid"
                                            runat="server"
                                            PageSize="10"
                                            CssClass="table table-striped table-bordered table-hover">
                                        </asp:GridView>
                                    </asp:Panel>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ul>


                    </div>
                </div>


            </div>
        </div>
    </section>


</asp:Content>

