﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StaticPage_FinishedProduct : System.Web.UI.Page
{


    public string path = "";
    balDefaultProductView balObjDP = new balDefaultProductView();
    balSetAutoBid balObjSetAutoBid = new balSetAutoBid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillDLFinishedItem();

        }
    }
    protected void fillDLFinishedItem()
    {
        DLFinishedProduct.DataSource = balObjDP.getDataFinishedItemPage();
        DLFinishedProduct.DataBind();
    }

}