﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StaticPage_UpCommingProduct : System.Web.UI.Page
{
    balDefaultProductView balObjDP = new balDefaultProductView();
    balSetAutoBid balObjSetAutoBid = new balSetAutoBid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillDLUpcomingItem();
        }

    }
    protected void fillDLUpcomingItem()
    {
        dlUPComming.DataSource = balObjDP.getDataUpcomingItemPage();
        dlUPComming.DataBind();
    }

    protected void dlUPComming_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label timelabel = (Label)e.Item.FindControl("lblRTime");

            string RDay = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString());//21 Remainig Day
            string Rtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[4].ToString());//21 Remainig Time
            if (RDay != "0")
            {
                timelabel.Text = " " + RDay.ToString() + " Day Remainig to Start";
            }
            else
            {
                timelabel.Text = Rtime.ToString() + " Time Remainig to Start";
            }

        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        fillDLUpcomingItem();
    }
   
    protected void lbtnPIDAutoBid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "showD")
        {
            Session["showD"] = e.CommandArgument.ToString();
            Response.Redirect("~/StaticPage/ProductFullDetail.aspx");
        }
    }
}