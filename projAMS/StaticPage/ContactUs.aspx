﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
	
<section class="block" id="inner-head" style="padding-top: 15em">
	<div class="fixed-img sec-bg4"></div>
	<div class="container">	
		<h1>CONTACT US</h1>
	</div>
</section>

<section class="block">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="contact-details">
					<div class="heading1">
						<h2><i>CONTACT </i>US</h2>
						<span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
					</div>
					
					<p> We try our best to solve your query by our FAQ but still , If you have any queries or problems, do not hesitate to write to us.Our team will do our best, so as to answer to your question as fast as possible.</p>
					<div class="row">
						<div class="col-md-6">
							<ul class="social-btns">
								<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-youtube"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-github"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-rss"></i></a></li>
							</ul>
						</div>
						<div class="col-md-6 contact-info">
							<span><i class="fa fa-map-marker"></i>World Auction Pvt. Ltd , near prannath hospital </span>
							<span><i class="fa fa-envelope-o"></i>WorldAuction@gmail.com</span>
							<span><i class="fa fa-mobile"></i>8866587814</span>
						</div>
					</div>
					
					<%--<iframe height="400" src="https://maps.google.com/?ie=UTF8&amp;ll=36.527295,-93.515625&amp;spn=40.324354,86.572266&amp;t=m&amp;z=4&amp;output=embed"></iframe><br />--%>
                    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:400px;width:900px;"><div id="gmap_canvas" style="height:400px;width:875px;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.trivoo.net" id="get-map-data">www.trivoo.net</a></div><script type="text/javascript"> function init_map() { var myOptions = { zoom: 16, center: new google.maps.LatLng(21.2159816435613, 72.82282795026242), mapTypeId: google.maps.MapTypeId.ROADMAP }; map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions); marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(21.2159816435613, 72.82282795026242) }); infowindow = new google.maps.InfoWindow({ content: "<b>WorldAuction</b><br/>prannath hospital,ved road<br/>395004 surat" }); google.maps.event.addListener(marker, "click", function () { infowindow.open(map, marker); }); infowindow.open(map, marker); } google.maps.event.addDomListener(window, 'load', init_map);</script>
				</div>
				
				
				<div class="contact-form">
					<div class="heading1">
						<h2><i>CONTACT </i>FORM</h2>
						<span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
					</div>
					
					<div id="message"></div>
					<form method="post" action="ContactUs.aspx" name="contactform" id="contactform">
						<div class="row">
							<div class="col-md-4">
								<input name="name" class="input-style" id="name" type="text" placeholder="NAME" />
								<input name="email" type="text" id="email" class="input-style" placeholder="EMAIL" />
								<input class="input-style" type="text" placeholder="PHONE NUMBER" />
							</div>	
							<div class="col-md-11">
								<textarea name="comments"  id="comments" class="input-style" placeholder="YOUR MESSAGE"></textarea>
								<input type="submit" class="submit" id="submit" value="Submit" value="SEND MAIL" />
							</div>
						</div>	
					</form>
					
				</div>
				
				
			</div>
			
					
		</div>
	</div>
</section>
</asp:Content>

