﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="Faq.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <section class="block" style="padding-top: 15em">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">

				<div class="why-us">
					<div class="heading1">
						<h2><i>FAQ </i></h2>
						<span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
					</div>
                   <h1 style="color:darkred;text-align:center">Mostly Asked Questions</h1>
					<div id="toggle-widget" class="why-us-sec">
						<h2><i class="fa fa-location-arrow"></i>How many type of Auction provided by WorldAuction ?</h2>
						<div class="content">
							<p>
                               <ol>
                                  <h5 style="color:darkred"><li>Regular Auction</li></h5>
                                    <b>The Regular Auction</b> is a standard type of Auction at our Service. The rules of bidding at Auctions apply to all types of Auctions at our Service.
                                    <ul>  <li>The Starting price of each Auction is always 0.00USD. Each placed bid increases the price of an auctioning item and extends the time of the Auction by a few seconds.</li>
                                        <li>Time to place a bid is displayed at the Auction card. It may be: 60s, 30s, 15s, 10s, 6s or  5s</li>
                                        <li>The cost of each bid is always displayed in the Auction details at the green <i>Bid</i> button.</li>
                                        <li>The Auction lasts as long as there are at least two people bidding at an Auction. It ends when there is only one User left and no one outbids the user's offer.</li>
                                        <li>You can bid at the Auction manually, or you can use a special feature Autobid which places bids on your behalf even if you've logged out.</li>
                                        <li>Regular Auctions can start and end at any time of the day as the website operates twenty-four hours a day, seven days a week. Occasionally ongoing Auctions can be suspended as part of the maintenance and upgrading process to ensure proper functioning.</li>
                                  </ul>
                                   <h5 style="color:darkred"><li>Express Auction</li></h5>
                                      <b>The Express Auction</b> is a special kind of Auction that is designed more for adventurous Users who do not like waiting!<br>
                                        These Auctions have a higher than usual bidding price which shortens the time of the Auction and makes it more exciting. 
                                    <ul>
                                    <li>The cost per bid at an <b>Express Auction</b> is displayed on the "Bid" button and can be different from every <b>Express Auction</b>.
                                     For example: (BID x 18) means that by clicking on the "Bid" button of the Auction, 18 BIDs will be deducted from your Account. </li>
                                    <li>One Bid will increase the price of the item by the amount stated in the Auction details, for example <b>1 cents</b> ($ 0.010).</li>

                                </ul>
                                    <h5 style="color:darkred"><li>Debut Auction</li></h5>
                                    <ul>
                                        <li>The <b>Debut Auction</b> is an Auction for Users who have not won an item Auction yet (ex. perfumes, phones, tablets, etc.).</li> 
                                        <li>This is more of a "Traditional Auction" but with some restrictions, and therefore, the rules are the same as Regular Auctions where the initial price is $0.00 and the price increases by small incremental amounts. </li>

                                    </ul>
                                    <h5 style="color:darkred"><li>Package Auction</li></h5>
                                    <ul>
                                        <li>The cost of one bid is displayed on the <b>“Bid”</b> button, and it varies for each BID pack Auction.</li>
                                        <li><b>Placing one Bid</b> will increase the price by the constant amount stated in the Auction details.</li>
                                        <li>The bidding time is displayed on an Auction Card and can equal to: 60s, 30s, 15s, 10s, 6s or 5s.</li>
                                        <li>This Auction is counted into the BID Pack Auction limit. Within one day <b>(24 hours)</b>, a User can win only one BID pack, no matter how much the amount of the BID is.</li>
                                        </ul>
                                   <h5 style="color:darkred"><li>Seld(Secret) Auction</li></h5>
                                    <ul>
                                        <li>This type of auction requires bidders to place bids that are global unique bids.</li> 
                                        <li> That is, for a bid to be <b>eligible to win no other bidder can have made a bid for the same amount.</b></li> 
                                        <li>Bidders are generally able to place multiple bids and the number of current bids at each <b>amount is typically kept secret</b>.</li>
                                        <li>In a <b>highest unique bid</b> auction, the bid that is the <b>highest</b> and <b>unmatched</b> when the auction closes is the winning bid.</li> 
                                        <li>A maximum bid value is usually set at a much lower level than the actual value of the lot.</li>
                                        </ul>
                                   <h5 style="color:darkred"><li>Charity Auction</li></h5>
                                    <ul>
                                        <li><b>Donors</b> who want to sell items for a <b>charity</b>.</li>
                                        <li><b>Charity Auction</b> may claim a charitable contribution <b>deduction</b> for the excess of the purchase price paid for an item over its fair market value.</li>
                                        <li>The donor must be able to show, however, that he or she knew that the value of the item was less than the amount paid.</li>
                                        <li>For example, a charity may publish a catalog, given to each person who attends an auction, providing a good faith estimate of items that will be available for bidding.</li>
                                  </ul> 
                            </ol>  
                        </p>
					   </div>
						<h2><i class="fa fa-location-arrow"></i>Registration Detail</h2>
						<div class="content">
							<p> 
                                    <h5 style="color:darkred"><li>Who can set up an Account on WorldAuction.com ?</li></h5>
                                    <ul>
                                        <li>Only people at the age of majority can register to use WorldAuction.com.</li>
                                        <li>In our Service one User can have only one Account.</li> 
                                        <li>Registration is free of charge, however, in order to fully activate an Account and to be able to use all the features of the Service, you need to buy at least one BID pack to start you off.
                                        <il>The wide range of BID packs are tailored to your needs and are available in the Price List.</li>
                                    </ul>
                                     <h5 style="color:darkred"><li>What information is required in order to register?</li></h5> 
                                       <ul>
                                        <li>Keep in mind that you need to choose your Username carefully as you cannot change it after registration.</li>
                                        <li>Your personal info and residential address provided during siging up must match the information on your ID card (e.g. driving license, passport).If it is more convenient for your delivery address may be different, so you will be able to freely receive parcels.</li>
                                        <li>After winning your first item Auction, you will be requested to fill in the form with your delivery address.</li>
                                        <li>Please be careful whhen entering your personal info because you won't be able to change it after registration.</li>
                                        <li>If you are a new User of this website, please tick a proper chcekbox during signing in. New Users have access to special promotions not available for Users who had an Account previously. If you had an Account at WorldAuction.com, you also have to tick a proper checkbox.</li>
                                       </ul>
                                    <h5 style="color:darkred"><li>I'd like to close my Account. How do I do that?</li></h5> 
                                        <ul>
                                            <li>To close your Account you need to make sure that there is no outstanding balance on your Account.</li>
                                            <li>In order to close your Account; first, you will need to go to My Account then to "My Data" and below your personal info there is a tab labeled "Account closing". 
                                                By clicking on the tab you will be redirected to a page where you can close your Account.
                                            <li>Alternatively, you can use this link: Close Account. 
                                                Before clicking on the LINK, please make sure that you are logged in, otherwise the link may not work.</li>
                                        </ul> 
                            </p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>My Profile</h2>
						<div class="content">
							<p>
                                <h5 style="color:darkred"><li>How can I check how many BIDs I have on my Account?</li></h5>
                                                          <ul>
                                                              <li>Your BID balance is visible only when you are logged in. The number by the letter B on the black bar at the top of the screen is the number of BIDs on your Account.</li>
                                                              <li>You can also review all of your BID operations by clicking on tabs My Account -> Account History -> Summary.</li>
                                                          </ul>
                                <h5 style="color:darkred"><li>How can I complete my delivery address?</li></h5> 
                                                           <ul>
                                                              <li>After winning your first item Auction, you will be requested to fill out a form with your delivery address. Your delivery address may be different from the one of your invoice data. Wherever it is convenient for you to receive packages</li>
                                                           </ul> 
                                <h5 style="color:darkred"><li>I would like to change some of my personal information. How do I do that?</li></h5> 
                                                           <ul>
                                                              <li>If you have changed your address and/or your phone number, please contact our Customer Service. In order to confirm your settings, please include your previous address and the new address that will replace the previous one in your e-mail to our Customer Service.</li>
                                                              <li>Please keep in mind that changing your personal info is possible only in special circumstances.</li>
                                                           </ul>
                                <h5 style="color:darkred"><li>I've been asked to provide a scan of my ID card, why?</li></h5> 
                                                            <ul>
                                                              <li>In accordance with our Terms and Conditions that you accept while signing-in, the Service has the right to request you to provide us with a copy of your ID card so as to protect both us and you from frauds. This is a standard procedure which enables us to protect you and offer you a safe and secure service. The information you will provide to us is confidential and will be used only to verify your Account.</li>
                                                               <li>If you do not have an image scanner, you can upload a picture/photo of your document taken by a digital camera/a cell phone and upload with the file manager. We can accept files in the following formats: .jpg, .png, .bmp, .tiff, .pdf, .doc.</li>
                                                            </ul> 
                                
                            </p>
						</div>
                      <%--Start Information About at Buying BIDS--%>  
                        <h1 style="color:darkred;text-align:center">Information About at Buying BID'S</h1>

                        <h2><i class="fa fa-location-arrow"></i>I've tried to buy a new promo BID pack but it is no longer on the Price List</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Promo packs of 200 and 2000 BIDs which are 50% less are one-time offers hence 
                                        if you click on one of these packs and don't complete the payment, the 
                                        offer will disappear from the Price List until we've received 
                                        information from a payment operator about the mistake.<br>
                                        You can buy the 200 BID promo pack once every 24 hours so you can buy this BID pack again after 24 hours has passed.<br>
                                        The 2000 BID promo pack is a one-time offer for our new Users who have 
                                        not purchased any promo pack, therefore you will need to wait until the 
                                        payment is rendered void in our System before this promo BID pack 
                                        becomes available again. This procedure can take up to 14 working days 
                                        to unlock the promo BID pack. If you’re positive that you have not 
                                        completed the payment for the promo BID pack and you would like to try 
                                        it again, please contact Customer Service and explain why the payment 
                                        has not been completed</li>
                                </ul>
							</p>
						</div>
                        
                        <h2><i class="fa fa-location-arrow"></i>I've paid for a BID pack and I would like to bid now, but I still do not have my BIDs. What is the problem?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Purchased  BIDs will appear on your Account automatically, as soon as your payment is completed.</li>
                                    <li>Sometimes there are delays with payments. It usually only takes just
                                         a few minutes but depending on the payment method, it may take a few 
                                        days.</li>
                                    <li>So please remember to stock up before taking part in an 
                                        Auction. Having a sufficient supply of BIDs in your Account is 
                                        essential, as your recharge may take more time than you expected. Even 
                                        if the payment operator such as PayPal/Wire Card/Skrill has taken money 
                                        from your Account it does not mean that the amount has been accounted 
                                        for in our Service</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i>Can I pay for BIDs with a bank transfer?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Unfortunately bank transfers are unavailable on our site. You can pay for your BID 
                                        packs either with a credit/debit card, or with our major payment 
                                        operator PayPal.</li>
                                    <li>If you choose to pay with PayPal then you will be directed to the secure PayPal website to complete your payment. </li>
                                    <li>We do not store your payment details in any way. This is managed by 
                                        our partners as they achieved the highest level of compliance. </li>
                                    <li> If you choose to pay for BIDs with your credit or debit card, make sure your bank accepts Internet transactions. </li> 
                                    <li>We highly recommend creating an account on PayPal to ease the use of
                                         WorldAuction. Once you have a PayPal account it is easy to transfer funds 
                                        over to your PayPal account or to link it to your bank account. PayPal 
                                        account is completely free of charge, so if you are interested, make 
                                        sure to create one.</li>
                                </ul>
							</p>
						</div>

                       


                        <%--End Information About at Buying BIDS--%>  
                        <%--Start General Questions about WorldAuction--%>  
                        <h1 style="color:darkred;text-align:center">General Questions about WorldAuction</h1>
                                               
                        <h2><i class="fa fa-location-arrow"></i>What are BIDs and what do they do ?</h2>
						<div class="content">
							<p><ul><li>BIDs are
                                     virtual units enabling you to participate in Auctions. Thanks to those 
                                    units you may bid either manually or with the Autobid feature. What is 
                                    more, you may also use BIDs with other features in our Service, such as 
                                    chatting.</li>
                                    <li>Each time you place a bid, the set amount of BIDs are deducted from 
                                    your Account. The amount is set individually for each Auction and is 
                                    displayed on the <i>Bid</i> button (ex. <i>Bid x3</i> means that each placed bid will cost you 3 BIDs).</li>
                                    <li>Please note that BIDs are one-time use only, no matter if you win an Auction or not, they will not be returned to your Account. </li> 
                                    <li>At WorldAuction.com, BIDs are divided into two groups: returnable (BIDs 
                                    purchased or BIDs which have been exchanged from won items) and 
                                    non-returnable (promo BIDs or received in the form of redeemable code 
                                    received from the Service). During the Auction, returnable BIDs are 
                                    preferentially deducted from your Account. If you use the option “Buy it
                                     Now” at an Auction, you may receive returnable BIDs back into your 
                                    account but only those used in the Auction. BIDs that you get back from 
                                    “Buy It Now” are transferred into the non-returnable BIDs pool.</li>
							   </ul>
							</p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>What does x3 mean on BID button ?</h2>
						<div class="content">
							<p><ul>
                                <li>The number by the <i>Bid</i> button informs you how many BIDs will be deducted from your Account each time you place a bid at an Auction. The amount of BIDs that will be deducted from your Account is set individually and is displayed in the Auction Parameters
                                     (i.e. <i>Bid x3</i> means that for one bid, you will spend 3 BIDs)</li>
                                <li>As the amount of BIDs required varies from each Auction, please make sure to check the Bid button out before taking part in an Auction.</li>
                                </ul></p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>What is “Buy it Now” and how can I use it ?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>“Buy it Now” enables Users to purchase an auctioned item without participation in an Auction.<br></li>
                                    <li>You can select this option as long as it is displayed at the Auction page.</li>
                                    <li>Each User with a fully activated Account at WorldAuction.com Service may place an order.</li>
                                    <li>An item purchased with the “Buy it Now” option does not count toward the limits of won Auctions at WorldAuction.com.</li>
                                    <li>If you use the option  “Buy it Now”, you may get a return of used BIDs at an Auction back, or you may buy an item at a discounted price. 
                                        Only BIDs from the pool of returnable BIDs are returned.
                                        The discounted price is calculated in accordance with the amount of returnable BIDs you have spent at the Auction.</li>
                                </ul>
							</p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>I think BIDs have disappeared from my Account. Is it possible ?</h2>
						<div class="content">
							<p>
                                <ul><li>It is not possible for someone to take BIDs from your Account.
                                    They are your property and nobody apart from you has access to them.</li>
                                    Please carefully review how many BIDs you have spent at your last Auction and review if you set any Autobids. Using Autobid might be the reason for the decreasing number of BIDs on your Account. 
                                
                                    <li>You can also review your Account history by clicking on tabs:<i>My Account</i>
							</p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>When does the Auction finish? The time is about to run out, but it constantly extends at the last moment.</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Each Auction has its own individual timer which counts down the time left till the end of the Auction. Each time a new bid is received by a bidder or the Autobid feature is used, the timer resets! 
                                        At our Service, this time is called Bidding time which can be set up to e.g. 60 s, 30 s, 15s or 10 s - each Auction has its own individual outbidding time.
                                        This time may change throughout the duration of an Auction. 
                                        The current time to bid is always displayed in the Auction details. 
                                        The longer an Auction lasts, the shorter the time is to be Outbid. </li>
                                    <li>In practice, it is difficult to define the time when an Auction ends
                                         - it will last as long as there are other bidders and it ends at the 
                                        moment when no one outbids the current offer before the time runs out.</li>
                                    <li>The Service of WorldAuction.com does not interfere with the Remaining 
                                        time when Auctions finish. Only the Users decide when the Auction ends.
                                        You may review how many Users are bidding at a certain Auction by clicking on <i>Recent Bidders.</i> sign at an Auction page. </li>

                                </ul>
							</p>
						</div>
						<h2><i class="fa fa-location-arrow"></i>What is an Autobid and how do I set one up ?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>At WorldAuction.com there are two ways to bid: manually and by the use of the system - Autobid</li>
                                    <li>The System places bids at an Auction on your behalf even if you are 
                                        offline during the Auction. Autobid feature starts bidding just before 
                                        the end of Auction and in the order it was first set up. 
                                        The sign <i>Autobid</i> at the end of an Auction means that a user is bidding with Autobid feature.</li>
                                    <li>Autobid suspends bidding when all the set bids are placed.</li>
                                    <li>There is a minimum amount of bidding required to start and it is displayed in the Autobid settings (in Auction details).</li>
                                    <li>Note! Once set the Autobid feature cannot be turned off. The User may only change the parameter 
                                        settings, however, the number cannot be lower than the current minimum amount of 
                                        bidding displayed in the parameters.</li>
                                    <li>It is advisable to check if you have enough BIDs in your Account and 
                                        top off your Account before you start bidding at an Auction to avoid 
                                        any problems. You can set Autobid up even if you don’t have enough BIDs 
                                        in your Account, as you can recharge your Account later. Autobid starts 
                                        using Purchased BIDs as soon as your payment for the BIDs is confirmed.</li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>I've set my Autobid to 5 Bids, but it used more BIDs. How did that happen ?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>When you set an Autobid, you enter the number of Bids you want the Autobid to 
                                        place for you, not the number of BIDs that the Autobid will use. That's 
                                        why before setting the Autobid at an Auction, please review the cost of 
                                        each BID of the Auction. The <i>Bid</i> button displays the cost of each Bid (e.g. "Bid x9")</li>
                                    <li>If you set Autobid for 5 at <i>Bid x6</i>, 30 BIDs are required to 
                                        complete the set amount, and this amount will be deducted from your 
                                        Account (5 Bids x 6 BIDs = 30 BIDs).</li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>I've received a message that I have reached the limit, but I still want to bid. Why can't I ?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Within 30 days (720 hours exactly) a User may win up to five item Auctions.
                                        In other words there is a limit of five Auctions a user can win.
                                        Limits for each Auction is separated. The items purchased with the 
                                        option Buy it Now are not counted toward the Auction limits.</li>
                                    <li>A User can win only one BIDs pack per day (24 hours)</li>
                                    <li>The moment that a User reaches the limit(BIDs or an item), the 
                                        Service automatically blocks the User's access to bidding and setting up
                                         the Autobid feature. The User will regain access respectively 30 days 
                                        from each Auction or 24 hours regarding the BID pack Auction.</li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>Why can't I take part in the Debut Auction ?</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>The Debut Auction is a special Auction designed for Users who have not won an 
                                        Auction with an item (a Gift Card is also counted as an item). It's a 
                                        great opportunity to learn how Auctions work. Each user can win only one
                                         Debut Auction. If you used to have an Account, you cannot participate 
                                        in Debut Auctions again.</li>
                                    <li>Users who previously had an Account in the service cannot participate in Debut Auctions.</li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>I observed an Auction but I can't find in the Finished Auctions.</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>As we have thousands of ongoing Auctions at the time, we are unable 
                                        to list all Auctions on the <i>Finished Auctions</i> tab and 
                                        therefore we make a random selection for Users.</li>
                                    <li>If you would like to follow a particular Auction but you don't 
                                        wish to take part in it, you can click on the <i>Add to observed</i> 
                                        button at the Auction Page. </li>
                                    <li>Even after the bidding process has finished, you can access the Auction
                                        details by clicking on tabs: <i>My Account</i> -&gt; <i>Auctions</i> -&gt; <i>Observed.</i></li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>The Autobid started bidding and it's placed only one Bid. Why? If the minimum amount must be 5 Bids.</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>If during ongoing Auction a User runs out of BIDs, the Autobid will 
                                        suspend its bidding until the User tops up an Account again. In the case
                                         when the User didn't top up the Account and in the meantime the Auction
                                         ended it looks as if that User has set the Autobid for the lower amount
                                         of Bids. However, in the reality it's a so called “break” in Autobid 
                                        because without BIDs on the Account it wasn't possible for the Autobid 
                                        to run and the Auction ended.</li>
                                    <li>Autobid may also suspend bidding when a User have become a Winner of a 
                                        different Auction and as a result a User has reached the limit of won 
                                        Auctions (either item Auctions or BID pack Auctions). So it wasn't 
                                        possible to win that Auction.</li>
                                </ul>
							</p>
						</div>
                        <h2><i class="fa fa-location-arrow"></i>How do I leave a comment for an item Auction?</h2>
						<div class="content">
							<p>
                                <ul>Each User who has won an item Auction and has not exchanged the item for BIDs/cash may leave a comment(excluding Gift Cards).
                                    <li>After making a payment for an won item, in the ''Won'' tab you will 
                                        find the ''Comment' button. By clicking on it you will be redirected to a
                                         page where you can leave a comment and submit a photo.</li>
                                    Comments and photos to be submitted should meet the following requirements:
                                    <li>A photo has to be in .jpg format and the size can't be larger than 
                                        100 kB. The image will be resized to 195x143 pixel size and therefore it
                                         would be best for your photo to be of similar size.</li>
                                    <li>A photo should show an Auction Winner with the item or the item itself.</li>
                                    <li>The comment should not be offensive and it should not be against any rules stated in the Terms and Conditions of the Service.</li>
                                    <li>A user will receive additional BIDs for each accepted and published 
                                        comment. The user will be informed about the amount of BIDs when the 
                                        comment is accepted.</li>
                                    <li>Service will publish only comments containing both picture and a well written comment.</li>
                                    <li>The Operator of the Service decides whether the comment is acceptable or not.</li>
                                    <li>The Operator restricts the right not to publish a comment even if the comment has been submitted properly.</li>
                                    <li>Once submitted comment cannot be modified</li>
                                </ul>
							</p>
                            </div> 
                        <%--End General Questions about WorldAuction--%>  
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
</asp:Content>

