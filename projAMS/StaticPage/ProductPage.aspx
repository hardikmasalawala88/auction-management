﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="ProductPage.aspx.cs" Inherits="StaticPage_ProductPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="block" id="inner-head" style="padding-top: 15em;">
        <div class="fixed-img sec-bg4"></div>
        <div class="container">
            <h1>PRODUCT PAGE</h1>
        </div>
    </section>
    <asp:HiddenField ID="HFCategoryTypeId" runat="server" />
    <asp:HiddenField ID="HFCategory" runat="server" />
    <div class="col-md-2">
        <div class="categories-widget widget-body">
            <div class="heading-5">
                <h2><i class="fa fa-folder-open"></i>Auctions</h2>
            </div>


            <ul>

                <li><i></i>
                    <asp:LinkButton runat="server" ID="lbtnAuctionId1" Text="NORMAL AUCTION" OnClick="lbtnAuctionId1_Click" />
                </li>
                <li><i></i>
                    <asp:LinkButton runat="server" ID="lbtnAuctionId2" Text="EXPRESS AUCTION" OnClick="lbtnAuctionId2_Click" />
                </li>
                <li><i></i>
                    <asp:LinkButton runat="server" ID="lbtnAuctionId3" Text="SEALDED AUCTION" OnClick="lbtnAuctionId3_Click" />
                </li>
                <li><i></i>
                    <asp:LinkButton runat="server" ID="lbtnAuctionId4" Text="CHARITY AUCTION" OnClick="lbtnAuctionId4_Click" />
                </li>
                <li><i></i>
                    <asp:LinkButton runat="server" ID="lbtnAuctionId5" Text="PACKAGE AUCTION" OnClick="lbtnAuctionId5_Click" />
                </li>


            </ul>
        </div>

    </div>
    <div class="row">
        <asp:DataList ID="DLProduct1" runat="server" RepeatColumns="2"
            Width="750px" OnItemDataBound="DLProduct1_ItemDataBound">
            <ItemTemplate>
                <%--<div class="col-md-4">--%>
                <div class="best-seller seller3">
                    <asp:Image runat="server" ID="ImgProduct" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' Height="300px" Width="200px" />

                    <h3>
                        <asp:UpdatePanel runat="server" ID="TimePanel" UpdateMode="Always">

                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:Timer runat="server" ID="Timer1" Interval="850" OnTick="Timer1_Tick"></asp:Timer>
                                <asp:Label runat="server" ID="lblRTime" Text='<%#Bind("RTime") %>' />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                        <br />
                        <asp:Label runat="server" ID="lblAuction" Text='<%#Bind("AuctionTypeName") %>' />
                    </h3>
                    <asp:Label runat="server" ID="lblProductName" Font-Size="Large" Text='<%#Bind("ProductName") %>' />
                    <div class="w-best-seller">

                        <asp:Button runat="server" Text="Bid" ID="btnBid" Height="30" Width="130" Font-Size="Large" CssClass="btn-success" OnCommand="btnBid_Command" CommandName="Bid" CommandArgument='<%#Bind("ProductId") %>' />
                        <asp:Button ID="btnBuyNow" runat="server" Text="BuyNow" Height="30" Width="130" Font-Size="Large" CssClass="btn-success" OnCommand="btnBuyNow_Command" CommandName="BuyNow" CommandArgument='<%#Bind("ProductId") %>' />

                    </div>
                </div>
                <%--</div>--%>
            </ItemTemplate>
        </asp:DataList>
    </div>

</asp:Content>

