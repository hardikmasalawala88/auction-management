﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class StaticPage_GVBuyPackge : System.Web.UI.Page
{
    boPacketDetail boObj = new boPacketDetail();
    balPacketDetail balObjPacketDetail = new balPacketDetail();
    balPackgePurchase balObjPackagePurchase = new balPackgePurchase();
    boPackagePurchase boObjPackagePurchase = new boPackagePurchase();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillGrid();
            //if (Session["Uid"] == null)
            //{
            //    Response.Redirect("LogIn.aspx");
            //}
            //else
            //{
            //    FillGrid();
            //}
        }
    }
    protected void FillGrid()
    {
        gvPacketDetail.DataSource = balObjPacketDetail.Load();
        gvPacketDetail.DataBind();
    }

    protected void gvPacketDetail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Session["LUID"] != null)
        {
            if (e.CommandName.Equals("Add"))
            {
                ////text box object is created lblAddColor is footer textbox that valu fetch and assign in object TxtColorName
                Session["lblPacketId"] = e.CommandArgument.ToString();
                //Session["lblPacketId"] = (Label)gvPacketDetail.FooterRow.FindControl("ImgBtnEdit");
                Session["lblPacketName"] = (Label)gvPacketDetail.FooterRow.FindControl("lblPacketName");
                Session["lblBids"] = (Label)gvPacketDetail.FooterRow.FindControl("lblBids");
                Session["lblPrice"] = (Label)gvPacketDetail.FooterRow.FindControl("lblPrice");
                Session["lblTime"] = (Label)gvPacketDetail.FooterRow.FindControl("lblTime");
                Session["lblDuration"] = (Label)gvPacketDetail.FooterRow.FindControl("lblDuration");
                pnlPacketPurchase.Visible = true;
                pnlGVPacketDetail.Visible = false;

            }
        }
        else
        {
            Response.Redirect("~/StaticPage/Login.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pnlGVPacketDetail.Visible = true;
        pnlPacketPurchase.Visible = false;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        boObjPackagePurchase.BankAc = txtBankAccount.Text.ToString();
        boObjPackagePurchase.BankName = txtBankName.Text.ToString();
        boObjPackagePurchase.CardNo = txtCardNumber.Text.ToString();
        boObjPackagePurchase.ClientId = Convert.ToInt32(Session["LUID"]);
        boObjPackagePurchase.PackagePurchaseId = 0;
        boObjPackagePurchase.PacketId = Convert.ToInt16(Session["lblPacketId"]);
        if (rdbMaestro.Checked = true)
        {
            boObjPackagePurchase.PaymentType = rdbMaestro.Text;
        }
        else if (rdbMasterCard.Checked = true)
        {
            boObjPackagePurchase.PaymentType = rdbMasterCard.Text; 
        }
        else if (rdbPayPal.Checked = true)
        {
            boObjPackagePurchase.PaymentType = rdbPayPal.Text; 
        }
        else
        {
            boObjPackagePurchase.PaymentType = rdbVisaCard.Text; 
        }
        balObjPackagePurchase.Purchase(boObjPackagePurchase);
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert( 'Package Purchase Successfully');", true);
        Response.Redirect("../Default.aspx");

    }
   
}