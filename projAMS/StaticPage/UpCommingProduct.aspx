﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="UpCommingProduct.aspx.cs" Inherits="StaticPage_UpCommingProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="block remove-gap" id="shop-recent-product" style="padding-top: 15em;">
        <div class="container">
            <div class="heading1">
                <h2><i>UPCOMING</i>AUCTION</h2>

            </div>
            <div class="row">
                <asp:DataList runat="server" ID="dlUPComming"


                     OnItemDataBound="dlUPComming_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-4">
                            <div class="shop-recent-product">
                                <asp:Image runat="server" ID="img1Upcoming" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' />
                                <%--<img src="../images/recent-products.jpg" alt="" />--%>
                                <span><i>
                                    <asp:Label runat="server" ID="lblAuctionName" Text='<%#Bind("AuctionTypeName") %>' /></i></span>
                                <asp:UpdatePanel runat="server" ID="TimePanel" UpdateMode="Always">

                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Timer runat="server" ID="Timer1" Interval="850" OnTick="Timer1_Tick"></asp:Timer>
                                        <p >
                                            <asp:Label ID="lblRTime" runat="server" Text='<%#Bind("RTime") %>' Font-Size ="XX-Large"  /><br />
                                            <asp:Label ID="lblPname" runat="server" Text='<%#Bind("ProductName") %>' Font-Size ="XX-Large" />
                                        </p>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%--<p>

                            <asp:Label runat="server" ID="lblTimer" Font-Size="Large" Text="Remaing Time" /><br />
                            <asp:Label runat="server" ID="lblProductName" Font-Size="Large" Text="Product Name" />
                        </p>--%>
                                <ul>
                                    <li>
                                        <asp:LinkButton runat="server"
                                            ID="lbtnPIDAutoBid"
                                            Text="AutoBid"
                                            CommandArgument='<%#Bind("ProductId") %>'
                                            CommandName="showD"
                                            OnCommand="lbtnPIDAutoBid_Command" /></li>
                                    <li>
                                        <asp:LinkButton runat="server"
                                            ID="lbtnPIDViewDetail"
                                            Text="View Detail"
                                            CommandName="showD"
                                            OnCommand="lbtnPIDAutoBid_Command" /></li>
                                </ul>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <%--<div class="row">
                <asp:DataList runat="server" ID="dlUPComming" OnItemDataBound="dlUPComming_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-4">
                            <div class="shop-recent-product">
                                <asp:Image runat="server" ID="img1Upcoming" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' />
                                <%--<img src="../images/recent-products.jpg" alt="" />
                                <span><i>
                                    <p>
                                        <asp:Label runat="server" ID="lblAuctionName" Text='<%#Bind("AuctionTypeName") %>' />
                                    </p>
                                </i>
                                </span>

                                <asp:UpdatePanel runat="server" ID="TimePanel" UpdateMode="Always">

                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Timer runat="server" ID="Timer1" Interval="850" OnTick="Timer1_Tick"></asp:Timer>
                                        <p>
                                            <asp:Label ID="lblRTime" runat="server" Text='<%#Bind("RTime") %>' /><br />
                                            <asp:Label ID="lblPname" runat="server" Text='<%#Bind("ProductName") %>' Font-Size="Large" />
                                        </p>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <ul>
                                    <li>
                                        <asp:LinkButton runat="server"
                                            ID="lbtnPIDAutoBid"
                                            Text="AutoBid"
                                            CommandArgument='<%#Bind("ProductId") %>'
                                            CommandName="showD"
                                            OnCommand="lbtnPIDAutoBid_Command" /></li>
                                    <li>
                                        <asp:LinkButton runat="server"
                                            ID="lbtnPIDViewDetail"
                                            Text="View Detail"
                                            CommandName="showD"
                                            OnCommand="lbtnPIDAutoBid_Command" /></li>
                                </ul>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>--%>
        </div>
    </section>
</asp:Content>

