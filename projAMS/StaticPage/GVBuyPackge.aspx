﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="GVBuyPackge.aspx.cs" Inherits="StaticPage_GVBuyPackge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="block" id="inner-head" style="padding-top: 15em;">
                <div class="fixed-img sec-bg7"></div>
                <div class="container">
                    <h1>
                        <span id="ContentPlaceHolder1_lblAuctionName">Buy Package</span></h1>
                </div>
            </section>

            <div class="portlet box green " style="padding-top: 23em;">


                <div class="portlet-body">
                    <div class="portlet-title tools">
                        <asp:Panel runat="server" ID="pnlGVPacketDetail">
                            <asp:GridView ID="gvPacketDetail" runat="server"
                                PageSize="10"
                                CssClass="table table-striped table-bordered table-hover"
                                AutoGenerateColumns="False"
                                GridLines="None"
                                PagerStyle-VerticalAlign="Middle"
                                HeaderStyle-ForeColor="Black"
                                CellPadding="4" ViewStateMode="Enabled"
                                ShowHeaderWhenEmpty="True"
                                OnRowCommand="gvPacketDetail_RowCommand">

                                <AlternatingRowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="PacketId" ControlStyle-Height="20px" ItemStyle-HorizontalAlign="center"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPacketId" runat="server" Text='<%#Eval("PacketId") %>'>
                                            </asp:Label>
                                        </ItemTemplate>

                                        <ControlStyle Height="20px" />

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PacketName" ControlStyle-Height="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPacketName" runat="server" Text='<%#Eval("PacketName") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bids" ControlStyle-Height="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBids"
                                                runat="server"
                                                Text='<%#Eval("Bids") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price" ControlStyle-Height="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrice"
                                                runat="server"
                                                Text='<%#Eval("Price") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Timelimit" ControlStyle-Height="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTimeLimit"
                                                runat="server"
                                                Text='<%#Eval("Timelimit") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Times" ControlStyle-Height="40px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTime"
                                                runat="server"
                                                Text='<%#Eval("Times") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Duration" ControlStyle-Height="40px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration"
                                                runat="server"
                                                Text='<%#Eval("Duration") %>'
                                                Height="20px">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Height="20px"></ControlStyle>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Purchase" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="ImgBtnEdit"
                                                runat="server"
                                                CausesValidation="false"
                                                ToolTip="Buy Now"
                                                Text="Buy Now"
                                                CommandName="Add"
                                                CommandArgument='<%#Eval("PacketId") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" />
                                        <ControlStyle Height="25px"></ControlStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle ForeColor="#34AEC4" BackColor="SteelBlue" Font-Size="Medium" />
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlPacketPurchase" Visible="true">
                            <center>
                             <table>
                                 <Caption>Enter Personal Detail</Caption>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblPaymentType" Text="Payment Type" /></td>
                                    <td>
                                        <asp:RadioButton runat="server" ID="rdbVisaCard" GroupName="Card" Text="VISA Card" />
                                        <asp:RadioButton runat="server" ID="rdbMasterCard" GroupName="Card" Text="Master Card" />
                                        <asp:RadioButton runat="server" ID="rdbMaestro" GroupName="Card" Text="Maestro Card" />
                                        <asp:RadioButton runat="server" ID="rdbPayPal" GroupName="Card" Text="Pay Pal" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBankName" runat="server" Text="Bank Name" /></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtBankName" placeholder="Valid Bank Name" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBankAccount" runat="server" Text="Bank Account" /></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtBankAccount" placeholder="Valid Bank Account" /></td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:Label ID="lblCardName" runat="server" Text="Card Number" /></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtCardNumber" placeholder="Valid Card Numbert" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSubmit" runat="server" Text="Purchase" OnClick="btnSubmit_Click" /></td>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" /></td>
                                </tr>
                            </table>
                            </center>
                        </asp:Panel>
                        <asp:Label ID="lblMsg" runat="server" />
                    </div>


                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

