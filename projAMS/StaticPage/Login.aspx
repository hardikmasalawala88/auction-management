﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="DynamicPage_Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WorlAuction Login</title>
    <link rel="stylesheet" href="../Login/css/style.default.css" type="text/css" />
    <script type="text/javascript" src="../Login/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../Login/js/jquery-migrate-1.1.1.min.js"></script>
</head>
<body style="background-size: cover; background-image: url('../Login/bg_1.jpg');">

    <form runat="server" id="loginform" method="post">
        <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <%--Login Panel1--%>
                <asp:Panel runat="server" ID="Panel1" CssClass="bounce">
                    <div class="loginwrapper">

                        <h1 class="logintitle animate3 bounceIn" style="opacity: 0.7; color: #C43455">
                            <span class="iconfa-lock" style="color: #C43455"></span>Log In 
                            <span class="subtitle" style="color: #C43455">Here you can login !

                            </span>

                        </h1>
                        <p>
                            <asp:Label runat="server" ID="lblMsg" display="Dynamic" Text="" ForeColor="White" />
                        </p>
                        <p class="animate4 bounceInLeft" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtUserEmail"
                                placeholder="EmilId" />
                            <asp:RegularExpressionValidator runat="server"
                               ID="regEmailId"
                                ControlToValidate="txtUserEmail"
                                ErrorMessage="* Invalid EmailId"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic"
                                SetFocusOnError="true"
                                ForeColor="Red"
                                Font-Bold="true" /> 

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                ControlToValidate="txtUserEmail"
                                ErrorMessage="* Must Enter Email Id"
                                ForeColor="White"
                                CssClass="has-error"
                                Display="Dynamic" />
                        </p>

                        <p class="animate5 bounceInRight" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtUserPwd"
                                placeholder="Password"
                                MaxLength="15"
                                TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"
                                ControlToValidate="txtUserEmail"
                                ErrorMessage="* You not give password blank"
                                CssClass="has-error"
                                ForeColor="White"
                                Display="Dynamic" />
                        </p>

                        <p class="animate6 bounceInUp">
                            <asp:Button runat="server"
                                ID="btnPanel1Submit"
                                Text="Login"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                OnClick="btnPanel1Submit_Click" />
                        </p>

                        <p class="animate7 bounceInUp">
                            <asp:Button runat="server"
                                ID="btnPanel1ForgotPassword"
                                Text="Forgot Passwrod"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                CausesValidation="false"
                                OnClick="btnPanel1ForgotPassword_Click" />
                        </p>

                        <p class="animate8 bounceInUp">
                            <asp:Button runat="server"
                                ID="btnPanel1Register"
                                Text="Register"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                CausesValidation="false"
                                OnClick="btnPanel1Register_Click" />
                        </p>

                    </div>
                </asp:Panel>
                <%--Forget Password Panel--%>
                <asp:Panel runat="server" ID="Panel2" Visible="false" CssClass="fadeIn">
                    <div class="loginwrapper">

                        <h1 class="logintitle animate3 fadeInRightBig" style="opacity: 0.3; color: #C43455">
                            <span class="iconfa-lock" style="color: #C43455"></span>Forgot Password 
            <span class="subtitle" style="color: #C43455">If you loss your password Don't Worry Enter your Email!</span>

                        </h1>

                        <p class="animate4 fadeInLeftBig" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtEmailId"
                                placeholder="EmailId" />
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"
                                ControlToValidate="txtEmailId"
                                ErrorMessage="* Must Enter Email Id"
                                ForeColor="White"
                                CssClass="has-error"
                                Display="Dynamic" />

                             <asp:RegularExpressionValidator runat="server"
                               ID="RegularExpressionValidator1"
                                ControlToValidate="txtEmailId"
                                ErrorMessage="* Invalid EmailId"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic"
                                SetFocusOnError="true"
                                ForeColor="Red"
                                Font-Bold="true" /> 
                         
                          

                        <p class="animate5 fadeInRightBig" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnPanel2Submit"
                                Text="Submit"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                OnClick="btnPanel2Submit_Click" />
                        </p>

                        <p class="animate6 fadeInLeftBig" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnPanel2LogIn"
                                Text="LogIn"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                CausesValidation="false" 
                                OnClick="btnPanel2LogIn_Click" />
                        </p>

                    </div>
                </asp:Panel>
                <%--Register Panel--%>
                <asp:Panel runat="server" ID="Panel3" Visible="false" CssClass="flipInX">
                    <div class="loginwrapper">

                        <h1 class="logintitle animate3 flipInX" style="opacity: 0.7; color: #C43455">
                            <span class="iconfa-lock" style="color: #C43455"></span>Register 
                            <span class="subtitle" style="color: #C43455">Not Have account why wait enter your detail !</span>

                        </h1>

                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtClientName"
                                placeholder="Enter FullName" />
                            <asp:FilteredTextBoxExtender ID="FTBEtxtClientName" runat="server"
                                FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                FilterMode="ValidChars"
                                ValidChars=" "
                                TargetControlID="txtClientName">
                            </asp:FilteredTextBoxExtender>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txtClientName"
                                ErrorMessage="* Please Enter Client Name"
                                ForeColor="White"
                                CssClass="has-error"
                                Display="Dynamic" />
                        </p>
                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtClientAddr"
                                placeholder="Enter Permannent Address" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers"
                                FilterMode="ValidChars"
                                ValidChars=" ,-/"
                                TargetControlID="txtClientAddr">
                            </asp:FilteredTextBoxExtender>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtClientAddr"
                                ErrorMessage="* Please Enter Permananet"
                                CssClass="has-error"
                                ForeColor="White"
                                Display="Dynamic" />
                        </p>

                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtContactNo"
                                placeholder="Enter ContactNo" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                FilterType="Numbers"
                                TargetControlID="txtContactNo">
                            </asp:FilteredTextBoxExtender>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="txtContactNo"
                                ErrorMessage="* Please Enter Contact No"
                                CssClass="has-error"
                                ForeColor="White"
                                Display="Dynamic" />
                        </p>
                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtRGEmailId"
                                placeholder="Enter EmailId that cosider as your user name" />
                              <asp:RegularExpressionValidator runat="server"
                               ID="RegularExpressionValidator2"
                                ControlToValidate="txtRGEmailId"
                                ErrorMessage="* Invalid EmailId"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic"
                                SetFocusOnError="true"
                                ForeColor="Red"
                                Font-Bold="true" /> 
                           <%-- <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers"
                                FilterMode="ValidChars"
                                ValidChars="@_."
                                TargetControlID="txtRGEmailId">
                            </asp:FilteredTextBoxExtender>--%>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                ControlToValidate="txtRGEmailId"
                                ErrorMessage="* Please Enter Valid EmailId"
                                CssClass="has-error"
                                ForeColor="White"
                                Display="Dynamic" />
                        </p>
                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtPwd"
                                MaxLength="15"
                                placeholder="Enter Password"
                                TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                ControlToValidate="txtPwd"
                                ErrorMessage="* Please Enter Permananet"
                                ForeColor="White"
                                CssClass="has-error"
                                Display="Dynamic" />
                        </p>
                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtConfirmPwd"
                                MaxLength="15"
                                placeholder="Enter ConfirmPassword"
                                TextMode="Password" />
                            <asp:CompareValidator ID="CompareValidator1"
                                runat="server"
                                ForeColor="White"
                                ErrorMessage="Password and Confirm password must be same"
                                ControlToValidate="txtPwd"
                                Display="Dynamic"
                                ControlToCompare="txtConfirmPwd" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                ControlToValidate="txtConfirmPwd"
                                ErrorMessage="* Please Enter Permananet"
                                CssClass="has-error"
                                ForeColor="White"
                                Display="Dynamic" />
                        </p>
                        <p class="animate4 flipInX" style="opacity: 0.90">
                            <asp:TextBox runat="server" Visible="false"
                                ID="txtRgCode"
                                placeholder="Enter Code that send on your Email" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                FilterType="Numbers"
                                TargetControlID="txtRGCode">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                ControlToValidate="txtRGCode"
                                ErrorMessage="* Please Enter Valid Code"
                                ForeColor="White"
                                CssClass="has-error"
                                Display="Dynamic" />
                        </p>

                        <p class="animate5 flipInX" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnPanel3Submit"
                                Text="Submit"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                OnClick="btnPanel3Submit_Click" />
                        </p>
                        <p>
                            <asp:Button runat="server"
                                ID="btnPanel3ConfirmCode"
                                Text="Confirm"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                CausesValidation="false"
                                Visible="false"
                                OnClick="btnPanel3ConfirmCode_Click" />
                        </p>
                        <p class="animate6 flipInX" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnPanel3LogIn"
                                Text="LogIn"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                CausesValidation="false"
                                OnClick="btnPanel3LogIn_Click" />

                        </p>


                    </div>
                </asp:Panel>
                <%--Confirm Code Panel--%>
                <asp:Panel runat="server" ID="Panel4" Visible="false" CssClass="fadeIn">
                    <div class="loginwrapper">

                        <h1 class="logintitle animate3 fadeInRightBig" style="opacity: 0.7; color: #C43455">
                            <span class="iconfa-lock" style="color: #C43455"></span>Confirm Code 
            <span class="subtitle" style="color: #C43455">Conform Code which we send on your mail.</span>

                        </h1>

                        <p class="animate4 fadeInLeftBig" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtCode"
                                placeholder="Unique Code" />
                        </p>

                        <p class="animate5 fadeInRightBig" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnConform"
                                Text="Conform"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                OnClick="btnConform_Click" />
                        </p>

                    </div>
                </asp:Panel>
                <%--Change Password--%>
                <asp:Panel runat="server" ID="Panel5" Visible="false" CssClass="fadeIn">
                    <div class="loginwrapper">

                        <h1 class="logintitle animate3 fadeInRightBig" style="opacity: 0.7; color: #C43455">
                            <span class="iconfa-lock" style="color: #C43455"></span>Change Password 
            <span class="subtitle" style="color: #C43455">Change password and quickly rejoint our site.</span>

                        </h1>

                        <p class="animate4 fadeInLeftBig" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtNewPassword"
                                MaxLength="15"
                                placeholder="New Password" />
                        </p>

                        <p class="animate5 fadeInLeftBig" style="opacity: 0.90">
                            <asp:TextBox runat="server"
                                ID="txtNewConformPassword"
                                MaxLength="15"
                                placeholder="Conform Password" />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Password and Confirm password must be same"
                                ControlToValidate="txtNewPassword"
                                ForeColor="White"
                                Display="Dynamic"
                                ControlToCompare="txtNewConformPassword" />
                        </p>

                        <p class="animate6 fadeInRightBig" style="opacity: 0.7">
                            <asp:Button runat="server"
                                ID="btnChangePassword"
                                Text="ChangePassword"
                                CssClass="btn-danger"
                                BackColor="#C43455"
                                ForeColor="White"
                                OnClick="btnChangePassword_Click" />

                        </p>

                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        jQuery.noConflict();

        jQuery(document).ready(function () {

            var anievent = (jQuery.browser.webkit) ? 'webkitAnimationEnd' : 'animationend';
            jQuery('.loginwrap').bind(anievent, function () {
                jQuery(this).removeClass('animate2 bounceInDown');
            });

            jQuery('#username,#password').focus(function () {
                if (jQuery(this).hasClass('error')) jQuery(this).removeClass('error');
            });

            jQuery('#loginform button').click(function () {
                if (!jQuery.browser.msie) {
                    if (jQuery('#username').val() == '' || jQuery('#password').val() == '') {
                        if (jQuery('#username').val() == '') jQuery('#username').addClass('error'); else jQuery('#username').removeClass('error');
                        if (jQuery('#password').val() == '') jQuery('#password').addClass('error'); else jQuery('#password').removeClass('error');
                        jQuery('.loginwrap').addClass('animate0 fadeInRightBig').bind(anievent, function () {
                            jQuery(this).removeClass('animate0 fadeInRightBig');
                        });
                    } else {
                        jQuery('.loginwrapper').addClass('animate0 fadeOutUp').bind(anievent, function () {
                            jQuery('#loginform').submit();
                        });
                    }
                    return false;
                }
            });
        });
    </script>

</body>
</html>
