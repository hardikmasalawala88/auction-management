﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;

public partial class DynamicPage_Login : System.Web.UI.Page
{
    boClientRegistration boClientObj = new boClientRegistration();
    balClientRegistration balClientObj = new balClientRegistration();

    balEmployeeDetail balEmpObj = new balEmployeeDetail();
    boEmployeeDetail boEmpObj = new boEmployeeDetail();

    boLogin boObj = new boLogin();
    balLogin balObj = new balLogin();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUserEmail.Focus ();
    }
    protected void btnPanel1ForgotPassword_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        Panel2.Visible = true;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }
    protected void btnPanel1Register_Click(object sender, EventArgs e)
    {
        //Panel1 Regi Btn
        Panel1.Visible = false;
        Panel2.Visible = false;
        Panel3.Visible = true;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }
    protected void btnPanel2LogIn_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }
    protected void btnPanel3LogIn_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }
    protected void btnPanel1Submit_Click(object sender, EventArgs e)
    {

        //Main Login Butten
        boObj.EmailId = txtUserEmail.Text;
        boObj.Passwd = txtUserPwd.Text;

        ds = balObj.Login(boObj);
        if (ds.Tables.Count > 0)
        {
            Session["LEmailId"] = ds.Tables[0].Rows[0][0].ToString();
            Session["LPasswd"] = ds.Tables[0].Rows[0][1].ToString();
            Session["LDesig"] = ds.Tables[0].Rows[0][2].ToString();
            Session["LUID"] = ds.Tables[0].Rows[0][3].ToString();
            Session["LUName"] = ds.Tables[0].Rows[0][4].ToString();
            Session["UImg"] = ds.Tables[0].Rows[0][5].ToString();
            //lblmsg.Visible = true;
            if (Convert.ToString(Session["LDesig"]) == "Admin")
            {
                boEmpObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));
                balEmpObj.LastLoginEmployee(boEmpObj);
                Session["MstSite"] = "~/DynamicPage/mstAdmin.master";
                Response.Redirect("../DynamicPage/DefaultAdmin.aspx");

            }
            else if (Convert.ToString(Session["LDesig"]) == "Employee")
            {
                boEmpObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));
                balEmpObj.LastLoginEmployee(boEmpObj);
                Session["MstSite"] = "~/DynamicPage/mstEmployee.master";
                Response.Redirect("../DynamicPage/DefaultEmployee.aspx");

            }
            else if (Convert.ToString(Session["LDesig"]) == "Client")
            {
                boEmpObj.EmpId = (Convert.ToInt32(Session["LUID"].ToString()));
                //balEmpObj.LastLoginAdmin(boEmpObj);
                Session["MstSite"] = "~/mstAMS.master";

                //if (Session["RecentPage"] != null)
                //{

                //    string st = Session["RecentPage"].ToString();
                //    Response.Redirect(st);   
                //}
                //else
                //{
                    Response.Redirect("../Default.aspx");
                //}

            }
            else
            {
                lblMsg.Text = "Please check the EmailId & Password";
                //Response.Redirect("Login.aspx");
            }

        }
        else
        {
            lblMsg.Text = "Please check the EmailId & Password";
            //Response.Redirect("Login.aspx");
        }


    }
    protected void btnPanel2Submit_Click(object sender, EventArgs e)
    {
        //Forget Submit
        if (txtEmailId.Text == "")
        {
            txtEmailId.Focus();
        }
        else
        {
            ds = balObj.ForgetPassword(txtEmailId.Text);
            string email = ds.Tables[0].Rows[0][0].ToString();
            if (email == txtEmailId.Text)
            {
                //string passwd = Convert.ToString(ds.Tables[0].Rows[0][1].ToString());
                Session["FGEmailId"] = email;
                string str1 = balClientObj.GetCode();

                string t = "Dear User,<br />" + "<br />"
                    + "For Account Recovery Please enter Below unique code then chage the password   " + "<br />"
                    + "And Login with new password for access your account " + "<br />"
                    + "Unique Code : " + str1 + "<br />" + "<br />";

                Session["Code"] = str1;
                clsUtility.SendMail(email, "WorldAuction  Unique Code", t);

                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = true;
                Panel5.Visible = false;
            }
            else
            {
                btnPanel1Register_Click(sender, e);
            }

        }


    }
    protected void btnPanel3Submit_Click(object sender, EventArgs e)
    {
        Session["RGPwd"] = txtPwd.Text;
        btnPanel3Submit.Visible = false;
        btnPanel3ConfirmCode.Visible = true;
        txtRgCode.Visible = true;
        txtClientName.Enabled = false;
        txtClientAddr.Enabled = false;
        txtContactNo.Enabled = false;
        txtRGEmailId.Enabled = false;
        txtPwd.Enabled = false;
        txtConfirmPwd.Enabled = false;
        string str1 = balClientObj.GetCode();

        string t = "Dear " + txtClientName.Text + ",<br />" + "<br />"
            + "       Welcome To  WorldAuction , We Received Your Application Of Joing to Our Team And " + "<br />"
            + "For Complete your Registration to our site " + "<br />"
            + "you have to enter below unique code in your registration form" + "<br />"
            + "Unique Code : " + str1 + "<br />" + "<br />";

        Session["Code"] = str1;
        clsUtility.SendMail(txtRGEmailId.Text, "WorldAuction Registration Code", t);


    }
    protected void btnConform_Click(object sender, EventArgs e)
    {

        if (Convert.ToString(Session["Code"]) == txtCode.Text.ToString())
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = false;
            Panel5.Visible = true;
        }
        else
        {
            txtCode.Focus();
        }
    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        //Change Password Submit

        boObj.EmailId = Convert.ToString(Session["FGEmailId"]);
        boObj.Passwd = txtNewPassword.Text;
        balObj.UpdatePassword(boObj);//fuction contain procedure variable is pass

        Panel1.Visible = true;
        Panel2.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }

    //boObj.ClientName = txtClientName.Text;
    //     boObj.ClientAddr  = txtClientAddr.Text;
    //     boObj.ShippingAddr = txtShippingAddr.Text;
    //     boObj.ContactNo = txtContactNo.Text;
    //     boObj.EmailId = txtEmailId.Text;
    //     boObj.Passwd = txtPwd.Text;
    //     //boObj.Designation = txtDesignation.Text;
    //     ////boObj.Designation = ddlDesig.SelectedValue;
    //     //boObj.Doj = Convert.ToString(txtDOJ.Text);
    //     //boObj.IsActive = Convert.ToInt16(false);
    //     //boObj.EmpImage = Session["ImgLoc1"].ToString();


    //     //if (txtEmpId.Text.Length == 0)
    //     //{
    //     //    btnCheckEmail_Click(sender, e);
    //     //}
    //     balObj.Insert(boObj);
    protected void btnPanel3ConfirmCode_Click(object sender, EventArgs e)
    {

        if (Convert.ToString(Session["Code"]) == txtRgCode.Text.ToString())
        {
            boClientObj.ClientName = txtClientName.Text;
            boClientObj.ClientAddr = txtClientAddr.Text;
            boClientObj.ContactNo = txtContactNo.Text;
            boClientObj.EmailId = txtRGEmailId.Text;
            boClientObj.Passwd = Convert.ToString(Session["RGPwd"]);


            balClientObj.Insert(boClientObj);


            string t = "Dear " + txtClientName.Text + ",<br />" + "<br />"
                + "       Welcome To  WorldAuction ," + "<br />"
                + "You Successfully Registered in our website  " + "<br />"
                + "Enjoy Six type of auction facilities. Your User Id" + "<br />"
                + "User Id : " + boClientObj.EmailId + "<br />" + "<br />"
                + "Password is not display here for security reason.";
            clsUtility.SendMail(boClientObj.EmailId, "WorldAuction Registration Successfully", t);
            btnPanel3LogIn_Click(sender, e);

        }

    }
}