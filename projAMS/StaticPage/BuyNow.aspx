﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="BuyNow.aspx.cs" Inherits="StaticPage_BuyNow" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="padding-top: 10em">
        <div class="col-md-4" align="center">
            <div class="cart-total-box">
                <div class="cart-head">
                    <h2 class="cart-product">BUY NOW DETAILS</h2>
                </div>
                <ul>
                    <li>
                        <asp:Label ID="Lblpn" runat="server" Text="Product Name"></asp:Label>
                        <asp:Label ID="LblVpn" runat="server" Text="Samsung mobile"></asp:Label>
                    </li>

                    <li>
                        <asp:Label ID="Lblpq" runat="server" Text="Product Qty"></asp:Label>
                        <asp:Label ID="LblVpq" runat="server" Text="1"></asp:Label>
                    </li>

                    <li>
                        <asp:Label ID="Lblbna" runat="server" Text="Buy Now Amount"></asp:Label>
                        <asp:Label ID="LblVbna" runat="server" Text="30000"></asp:Label>
                    </li>

                    <li>
                        <asp:Label ID="Lblsc" runat="server" Text="Shipping Charge"></asp:Label>
                        <asp:Label ID="LblVsc" runat="server" Text="100"></asp:Label>
                    </li>

                    <li>
                        <asp:Label ID="Lblsd" runat="server" Text="Shipping Description"></asp:Label>
                        <asp:Label ID="LblVsd" runat="server" Text="As require"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Lblbn" runat="server" Text="Bidder Name"></asp:Label>
                        <asp:Label ID="LblVbn" runat="server" Text="Chirag"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Lblba" runat="server" Text="Bidder Address"></asp:Label>
                        <asp:Label ID="LblVba" runat="server" Text="Amroli"></asp:Label>
                    </li>

                </ul>
            </div>
        </div>

        <div class="col-md-4">
            <div class="cart-total-box">
                <div class="cart-head">
                    <h2 class="cart-product">PRODUCT IMAGE</h2>
                </div>
                <ul>
                    <li>
                        <asp:Label ID="Lblpi" runat="server" Text="Product Image"></asp:Label>
                    </li>
                    <li>
                        
                            <asp:Image ID="Img1" runat="server" Width="350" Height="300" />
                        
                    </li>
                    <li>
                        
                        <asp:Button runat="server" ID="btnBuyNow" Text="Buy Now" OnClick="btnBuyNow_Click"/>
                    </li>

                </ul>
            </div>
        </div>

    </div>


</asp:Content>

