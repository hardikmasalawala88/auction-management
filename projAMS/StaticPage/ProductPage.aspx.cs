﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class StaticPage_ProductPage : System.Web.UI.Page
{
    boCategoryMst boObjCategoryMst = new boCategoryMst();
    balProductPage balObjProductPage = new balProductPage();
    boAuctionTypeItemDetail boObjAuctionTypeItem = new boAuctionTypeItemDetail();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HFCategory.Value != null && HFCategoryTypeId.Value != null)
            {
                //Response.Write(HFCategoryTypeId.Value);
                //Response.Write(HFCategory.Value);
                this.HFCategoryTypeId.Value = Request.QueryString["CategoryTypeId"];
                this.HFCategory.Value = Request.QueryString["Category"];
                FillData();
                // FillFilterData();
            }
            else 
            {
                Response.Redirect("../Default.aspx"); 
            }

        }

    }
    protected void FillData()
    {
        try
        {
            DataSet ds = new DataSet();
            boObjCategoryMst.CategoryTypeId = Convert.ToInt32(HFCategoryTypeId.Value);
            boObjCategoryMst.Category = HFCategory.Value;

            ds = balObjProductPage.getAllProduct(boObjCategoryMst);
            if (ds != null)
            {
                DLProduct1.DataSource = ds;
                DLProduct1.DataBind();
            }
        }
        catch { }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        FillData();
    }
    protected void FillFilterData(int id)
    {
        try
        {
            DataSet ds = new DataSet();
            boObjCategoryMst.CategoryTypeId = Convert.ToInt32(HFCategoryTypeId.Value);
            boObjCategoryMst.Category = HFCategory.Value;

            boObjAuctionTypeItem.AuctionTypeId = Convert.ToInt16(id);

            ds = balObjProductPage.getFilterAllAuction(boObjCategoryMst, boObjAuctionTypeItem);
            if (ds != null)
            {
                DLProduct1.DataSource = ds;
                DLProduct1.DataBind();
            }
        }
        catch { }
    }

    protected void lbtnAuctionId1_Click(object sender, EventArgs e)
    {
        FillFilterData(1);
    }
    protected void lbtnAuctionId2_Click(object sender, EventArgs e)
    {
        FillFilterData(2);
    }
    protected void lbtnAuctionId3_Click(object sender, EventArgs e)
    {
        FillFilterData(3);
    }
    protected void lbtnAuctionId4_Click(object sender, EventArgs e)
    {
        FillFilterData(4);
    }
    protected void lbtnAuctionId5_Click(object sender, EventArgs e)
    {
        FillFilterData(5);
    }
    protected void btnBuyNow_Command(object sender, CommandEventArgs e)
    {
        if (Session["LUID"] != null)
        {
            if (e.CommandName == "BuyNow")
            {
                Session["showD"] = e.CommandArgument.ToString();
                Response.Redirect("~/StaticPage/BuyNow.aspx");
            }
        }
        else
        {
            //redirect to login
            Response.Redirect("~/StaticPage/Login.aspx");
        }
    }
    protected void btnBid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Bid")
        {
            Session["showD"] = e.CommandArgument.ToString();
            Response.Redirect("~/StaticPage/ProductFullDetail.aspx");
        }
    }
    protected void DLProduct1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label timelabel = (Label)e.Item.FindControl("lblRTime");

            string RDay = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[7].ToString());//21 Remainig Day
            string Rtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[8].ToString());//21 Remainig Time
            if (RDay != "0")
            {
                timelabel.Text = " " + RDay.ToString() + " Day Remainig";
            }
            else
            {
                timelabel.Text = Rtime.ToString();
            }

            Button ButtonBid = (Button)e.Item.FindControl("btnBid");
            string BidMulti = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[9].ToString());//21 Remainig Day
            string AuctoinType = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[2].ToString());
            if (AuctoinType == "PACKAGE AUCTION")
            {
                if (BidMulti != "0")
                {
                    ButtonBid.Text += " X " + BidMulti;
                }
            }
        }
    }
}