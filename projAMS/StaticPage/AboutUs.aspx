﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="aboutus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <section class="block" id="inner-head" style="padding-top: 15em">
        <div class="fixed-img sec-bg4"></div>
        <div class="container">
            <h1>ABOUT US</h1>
        </div>
    </section>

    <section class="block">
        <div class="container">
            <div class="title-head">
                <h2>WorldAuction is the India's First Auction Website in which <i>international Auction Concepts</i> are use. We also provide <i>various type of Auction</i> for Buyer and seller. We are always in fever of Consumer.</h2>
            </div>
        </div>
    </section>


    <section class="block remove-gap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="our-skills-sec">
                        <div class="heading1">
                            <h2><i>OUR </i>AUCTION FACILITIES</h2>
                            <span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
                        </div>

                        <ul>

                            <div class="col-md-4">
                                <div class="service-box red">
                                    <span><i class="fa fa-tag"></i></span>
                                    <h3>GENERAL</h3>
                                    <p>Highest bidder get the product as</p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="service-box orange">
                                    <span><i class="fa fa-leaf"></i></span>
                                    <h3>PACKAGE </h3>
                                    <p>See ballerina boy for as little as </p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="service-box blue">
                                    <span><i class="fa fa-github"></i></span>
                                    <h3>EXPRESS</h3>
                                    <p>See ballerina boy for as little as </p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="service-box green">
                                    <span><i class="fa fa-leaf"></i></span>
                                    <h3>DEBUT</h3>
                                    <p>See ballerina boy for as little as </p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="service-box brown">
                                    <span><i class="fa fa-github"></i></span>
                                    <h3>SEALD</h3>
                                    <p>See ballerina boy for as little as </p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="service-box d-pink">
                                    <span><i class="fa fa-tag"></i></span>
                                    <h3>CHARITY</h3>
                                    <p>See ballerina boy for as little as </p>
                                    <a href="#" title="">READ MORE</a>
                                    <i class="box-1"></i>
                                    <i class="box-2"></i>
                                    <i class="box-3"></i>
                                    <i class="box-4"></i>
                                </div>
                            </div>

                        </ul>
                    </div>
                </div>


                <div class="col-md-6">

                    <div class="why-us">
                        <div class="heading1">
                            <h2><i>WHY </i>US</h2>
                            <span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
                        </div>

                        <div id="toggle-widget" class="why-us-sec">
                            <h2 class="animated bounceIn"><i class="fa fa-location-arrow"></i>Why Buy Product From Here ?</h2>
                            <div class="content">
                                <p>We Provide 90% Discout On New Product Which is include in Package Auction.  </p>
                            </div>
                            <h2 class="animated bounceIn"><i class="fa fa-location-arrow"></i>Why WorldAuction is best in all auction sites ?</h2>
                            <div class="content">
                                <p>We have More than four type of auction. And with more manageable way.</p>
                            </div>
                            <h2 class="animated bounceIn"><i class="fa fa-location-arrow"></i>What is Interesting things in WorlAuction ?</h2>
                            <div class="content">
                                <p>We provide two most interesting auction method one of them is Seald auction and another is Package auction.</p>
                            </div>
                            <h2 class="animated bounceIn"><i class="fa fa-location-arrow"></i>How can Loser become winner of a auction?</h2>
                            <div class="content">
                                <p>In Debut Auction Customer have second chance for winning the auction who lose in Package auction.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="block">
        <div class="fixed-img sec-bg7"></div>
        <div class="container">
            <div class="row our-shop-section">
                <div class="col-md-8">
                    <div class="tab-content" id="myTabContent">
                        <div id="our-shop1" class="tab-pane fade in active our-shop-big">
                            <div class="our-shop-thumb">
                                <img src="../images/our-shop11.jpg" alt="" />
                                <span>One Time Bidding ?
                                </span>
                                <i class="shop-box1"></i>
                                <i class="shop-box2"></i>
                                <i class="shop-box3"></i>
                                <i class="shop-box4"></i>
                                <i class="shop-box5"></i>
                                <i class="shop-box6"></i>
                                <i class="shop-box7"></i>
                                <i class="shop-box8"></i>
                            </div>
                            <p>In sealded auction only one time you can bid. So it Saves your time and only place of one max bid you can winner of auction.  </p>
                        </div>
                        <div id="our-shop2" class="tab-pane fade our-shop-big">
                            <div class="our-shop-thumb">
                                <img src="../images/our-shop22.jpg" alt="" />
                                <span>Less investment gave more profit. </span>
                                <i class="shop-box1"></i>
                                <i class="shop-box2"></i>
                                <i class="shop-box3"></i>
                                <i class="shop-box4"></i>
                                <i class="shop-box5"></i>
                                <i class="shop-box6"></i>
                                <i class="shop-box7"></i>
                                <i class="shop-box8"></i>
                            </div>
                            <p>In our package auction you have to purchase package and you can able to take part in this most exiciting auction. This auction is mostly like by our customer because they get product by less than 99% . this is not fake , you take part in this auction and feel it and enjoy it. </p>
                        </div>
                        <div id="our-shop3" class="tab-pane fade our-shop-big">
                            <div class="our-shop-thumb">
                                <img src="../images/our-shop33.jpg" alt="" />
                                <span>If you offline still connected with us</span>
                                <i class="shop-box1"></i>
                                <i class="shop-box2"></i>
                                <i class="shop-box3"></i>
                                <i class="shop-box4"></i>
                                <i class="shop-box5"></i>
                                <i class="shop-box6"></i>
                                <i class="shop-box7"></i>
                                <i class="shop-box8"></i>
                            </div>
                            <p>We provide status of auction in which you participate. As well as we gave status of upcoming and finishing product and many more.</p>
                        </div>
                        <div id="our-shop4" class="tab-pane fade our-shop-big">
                            <div class="our-shop-thumb">
                                <img src="../images/our-shop44.jpg" alt="" />
                                <span>You not stop your self to Bid.</span>
                                <i class="shop-box1"></i>
                                <i class="shop-box2"></i>
                                <i class="shop-box3"></i>
                                <i class="shop-box4"></i>
                                <i class="shop-box5"></i>
                                <i class="shop-box6"></i>
                                <i class="shop-box7"></i>
                                <i class="shop-box8"></i>
                            </div>
                            <p>we provid more than one option. Simple and Smart AutoBid .Select which you like both exiciting and interesting. Both bidding type is unique and automatic.</p>
                        </div>
                    </div>
                </div>

                <ul class="nav nav-tabs" id="shop-tab1">
                    <li class="active col-md-2"><a data-toggle="tab" href="#our-shop1">
                        <img src="../images/our-shop1.jpg" alt="" /></a></li>
                    <li class="col-md-2"><a data-toggle="tab" href="#our-shop2">
                        <img src="../images/our-shop2.jpg" alt="" /></a></li>
                    <li class="col-md-2"><a data-toggle="tab" href="#our-shop3">
                        <img src="../images/our-shop3.jpg" alt="" /></a></li>
                    <li class="col-md-2"><a data-toggle="tab" href="#our-shop4">
                        <img src="../images/our-shop4.jpg" alt="" /></a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="block">
        <div class="container">
            <div class="heading1">
                <h2><i>OUR </i>TEAM</h2>
                <span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="our-skills2">
                        <img width="400" height="400" src="../images/vivek.jpg" alt="" />
                        <h3>Vivek Soni</h3>
                        <span>Creative Designer</span>
                        <ul>
                            <li><a href="https://www.facebook.com/vivek.soni.7505468" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://plus.google.com/107052916390892705810" title=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="our-skills2">
                        <img width="400" height="400" src="../images/harik.JPG" alt="" />
                        <h3>Hardik masalawala</h3>
                        <span>Web Developer</span>
                        <ul>
                            <li><a href="https://www.facebook.com/HrdikmasalawalaHD" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://plus.google.com/u/0/115103718089802587483" title=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://twitter.com/Harik48" title=""><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="our-skills2">
                        <img width="400" height="400" src="../images/keran.JPG" alt="" />
                        <h3>Joshi keraan</h3>
                        <span>Our Manager</span>
                        <ul>
                            <li><a href="https://www.facebook.com/kiran.joshi.7921" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://plus.google.com/u/0/109857811021821207457" title=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

