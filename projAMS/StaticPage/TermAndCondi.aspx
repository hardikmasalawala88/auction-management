﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="TermAndCondi.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <section class="block" style="padding-top: 15em">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">

				<div class="why-us">
                 	<div class="heading1">
						<h2><i>Terms and Conditions</i></h2>
						<span>BRAND FOR EVERY STYLE. FIND YOURS.
					</div>

                    	<div id="toggle-widget" class="why-us-sec">
                     	<h2><i class="fa fa-location-arrow"></i> 1. General provisions</h2>
						<div class="content">
							<p>
                                <ul>
                                        <li>These Terms and Conditions define the rules and conditions under which the Service is provided by the Operator for the price specified in the Price List, the rules and conditions of the participation in Buy it Now purchases and Auctions, as well the conditions of the sale of items offered by the Seller.</li>
                                        <li>These Terms and Conditions are an integral part of the Agreement , in particular of agreements for the purchase and sale of rights and items within the Service. These Terms and Conditions are always available on the Main Page.</li>
                                        <li>Logging into the Service indicates your acceptance of these Terms and Conditions pursuant to the rules these Terms and Conditions.</li>
                                        <li>When used in these Terms and Conditions the following terms shall have the following meanings:
                                        <ol style="list-style-type: none;">
                                        <li><b>Terms and Conditions</b> – this document;</li>
                                        <li><b>Operator</b> – Welmory Limited with its seats in Cyprus, Nicosia P.C. 1065 Arch. Makariou III, 2-4/703, established under No. HE 245903 under the laws of Cyprus and registered in the register held by the Ministry of Energy, Commerce, Industry and Tourism, Department of Registrar of Companies. The Company’s initial capital amounts to € 1,001,000.00 (one million and one thousand Euro);</li>
                                        <li><b>Seller </b>– a private individual, a legal entity or an organizational entity without legal personality which, after going through the verification process and signing relevant agreements with the Operator, received the status of the Seller from the Operator;</li>
                                        <li><b> Authorisation</b> – the process of agreement verification and signing with the Operator ended in that the Operator has granted the status of the Seller pursuant to the procedure specified in § 3 below;</li>
                                        <li><b>Service</b> – the on-line transaction services rendered by the Operator, under the wellbid.com and za10groszy.pl domains, where the Operator organizes Buy it Now purchases and Auctions and enables the purchase of the right to Bid or Bid with Autobid, as well as placing Bids and Bids with Autobid;</li>
                                        <li><b>Buy it Now purchase, also defined as the Buy it Now</b> – the sale of items conducted in accordance with these Terms and Conditions, fully defined in §4;</li>
                                        <li><b>Auction </b>– the sale of items conducted under these Terms and Conditions, fully specified in §5 and §5A;</li>
                                        <li><b> Bidding</b> – using the option of the Service enabling the indication of a wish to purchase a particular item for a particular price offered on an Auction and offering a higher price by the Incremental Bid Amount than the Current Price, enabling the conclusion of a sale agreement for the item being the object of the Auction;</li>
                                        <li><b>Autobid</b> – a bid performed automatically by the System, on behalf of the User and on his/her account without the participation of the User according to the Autobid Parameters set by the User;</li>
                                        <li><b>System</b> – the computer system providing the Service activity;</li>
                                        <li><b>BID, Returnable BID</b> – the right to a single Bid, a Bid by the Autobid or to other action specified in these Terms and Conditions, where Returnable BIDs are purchased from the Operator according to the Price List or purchased as a result of the item exchange. BIDs which are not Returnable BIDs are purchased in other cases specified in these Terms and Conditions, or on the Service websites, Help and the FAQ;</li>
                                        <li><b>Pool of BIDs</b> – an amount (a pool) of BIDs and Returnable BIDs at the User’s Account, which are used in Auctions offered by the Service; </li>
                                        <li><b>User</b> – a private individual with a full legal capacity who has reached the age of majority and registered with the Service who has correctly registered with the Service and has access to his/her Account. A person is identified by the first name, surname, and the date of birth;</li>
                                        <li><b>Related Accounts </b>- Accounts of two or more Users with at least one element in common:
                                        <ol style="list-style-type: none; padding-left: 40px;">
                                        <li>Residential address;</li>
                                        <li>Shipping address;</li>
                                        <li>Bank account/credit card used for payment for Returnable BIDs, Buy it Now or an Auction.
                                        <p>Accounts can also be related when the same computer has been used on those Accounts.</p></li>
                                        </ol>
                                        </li>
                                        <li><b>Price List</b> – information published by the Operator on one of the websites defining the conditions and the amount of payment required to purchase Returnable BIDs;</li>
                                        <li><b>Duration of Buy it Now</b> – a particular time at which the User may purchase an item that has been put up for the Buy it Now;</li>
                                        <li><b>Remaining Time to the End of the Auction</b> – time left to the end of the Auction excluding time essential for System maintenance and recesses in System operations;</li>
                                        <li><b>End of Auction</b> – the moment when the Remaining Time to the End of the Auction has ended as marked by the System displaying the word “Finished” on the Auction Website; </li>
                                        <li><b> Account</b> – a data managed by the Operator for the User under a unique name (User-name), such data comprising the User’s personal data and his/her Account history within the Service. The data is displayed in the “MY ACCOUNT” tab;</li>
                                        <li><b>Auction Parameters</b> – a set of individual data and features of a given type of Auction;</li>
                                        <li><b> Autobid Parameters</b> – the parameters that the User sets up in the Autobid on the “Autobid Settings” page according to the procedure described on that page;</li>
                                        <li><b>Incremental Bid Amount</b> – the highest offer made by a User at an Auction in progress;</li>
                                        <li><b>Confirmation</b> – email confirming of the end the Auction sent to the Auction Winner in the way </li>
                                        <li><b>Buy it Now or Auction Website</b> – the page where the Buy it Now or the Auction of a particular item takes place. It includes the name of the item, its technical description, the price of the item for the Buy it Now purchase given by the Seller in advance, as well as shipping fees and other costs typical of a given kind of purchase at the Buy it Now or an Auction;</li>
                                        <li><b>Reserve Price</b> – the initial price of the item offered by the Seller at the beginning of the Auction;</li>
                                        <li><b>Current Price</b> – the price offered by the Current Winner. Where no User has placed a Bid yet and no Bid has been made by the Autobid, the Current Price equals the Reserve Price;</li>
                                        <li><b>Purchase Price</b> – the final price defined by the Seller at which the User may purchase the item with the Buy it Now option or the price that the highest Bidder has placed as an offer for which he/she purchases the auctioned item;</li>
                                        <li><b>Buyer of the Buy it Now</b> – the User who has purchased an item with the Buy it Now option, pursuant to the rules specified in §4;</li>
                                        <li><b> Current Winner </b>– the User who will become the Auction Winner as long as no other User has made a Bid or Autobid before the Auction finishes, pursuant to the rules in §5;</li>
                                        <li><b>Auction Winner</b> – the User who has won the Auction pursuant to §5;</li>
                                        <li><b>Cookies</b> – a piece of information stored by the server on the User’s computer or other data terminal equipment in the text file. The computer’s server may read the cookie again while establishing connection as appropriate from that computer or other data terminal equipment.</li>
                                        </ol>
                                      </li>
                                </ul>
                             
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 2. Registration</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Registration in the Service is free of charge.

                                        <li>The registration in the Service requires the acceptance of these Terms and Conditions. By registering, the User declares that he/she has read and accepted the content of the most recent version of these Terms and Conditions and all the duties and responsibilities resulting from these Terms and Conditions;</li>
                                        <li>Only private individuals with a full legal capacity who have reached the age of majority have the right to register, provided that their agreement for the use of the Service was not previously terminated by the Operator because of the violation by them of law, the established custom, good practice or these Terms and Conditions;</li>
                                        <li>If the User whose agreement was previously terminated due to any of the reasons mentions in 1.2. above is found to register again, the Operator shall block his/her Account immediately and all BIDs on his/her Account will be forfeited as a penalty for repeated gross breach of the Terms and Conditions.</li>
                                        <li>In order to register it is necessary to complete the Registration Form by providing a email address, a username and password.</li>
                                        <li>All the personal data given must pertain to the person ordering the service and be true. In particular, the name, surname and residential address must comply with that stated in the identity card or another identity document. The Operator takes no liability for invalid data provided by Users at their Accounts. Amending the data after registration will only be possible in justified cases by the User by notifying a modification in writing and sending the notice to the address of the Seller, who will be able to alter the personal data as necessary.</li>
                                        <li>The System sends the confirmation of the registration to the email address given in the Registration Form.</li>
                                        <li>Upon registration of an Account in the Service, an agreement is concluded between the entity being registered and the Operator,<br>
                                        where the subject matter are the services provided by the Operator in the Service in accordance with the Terms and Conditions and the subject being registered gains the access to its Account (after giving the username and password) and becomes the User.</li>
                                        <li>The User may withdraw from within the first 14 days without stating a reason by sending a relevant statement either by email or letter. If the User has already taken part in promotions, purchased Returnable BIDs or performed any actions within the Service, he/she is not entitled to terminate the agreement.</li>
                                        <li>The User is not allowed to alter the username and the email address entered during registration.</li>
                                        <li>The username must be unique to each Account.</li>
                                        <li>The User can have only one Account. A person is identified by the name, surname and date of birth. Under extenuating circumstances the Operator may allow an exception to this rule only upon written request of the User.</li>
                                        <li>The User is not allowed to use other Users’ Accounts or let other persons use his/her Account. The Users of Related Accounts cannot participate in the same Auction.</li>
                                        <li>Accounts are non-transferable.</li>
                                        <li>In the Service there is no limit defining how many times the User may participate in the Buy it Now.</li>
                                        <li>In the Service there is a limit defining how many times one User can become an Auction Winner excluding BID packs. Further details are provided in Help (FAQ – General Questions about Service). The limit and current balance of won Auctions included in that limit can be reviewed after logging into the System.</li>
                                        <li>In the Service there is an additional limit defining how many times one User can become a BID Pack Auction Winner. Details are provided in Help (FAQ – General Questions about Service). The limit and current balance of won Auctions included in that limit can be reviewed after logging into the System.</li>
                                        <li>The Operator reserves the right to verify and check the information provided by persons during and after Registration. If any inaccuracy is found, the Operator reserves the right to block the User’s Account. This may take place, in particular, when any inaccuracy of the data of the entity being registered with the actual data is found.</li>
                                        <li>In the event of a change in the User’s data, the User is obliged to update such information immediately, otherwise the Account may be blocked by the Operator.</li>
                                        <li>All and any actions of the User using the Service shall be in compliance with all applicable law, and in accordance with these Terms and Conditions as well as the established custom and good practice. In particular, the User may not use the Service to provide any content that infringes the law or recognized moral standards, or act in cooperation with other Auction participants or a third party in a way that influences the result of an Auction. In particular, the Users of Related Accounts are not allowed to participate in the same Auctions. The User shall be notified should any examples of such behaviour occur. User actions contrary to these provisions can result in the blocking of the Account and cause termination of the agreement regarding the use of the Service, refusal of Confirmation and the penalty shall be imposed .</li>
                                        <li>The User should archive any information about transactions made through the Service on a data storage device for possible use in filing complaints. The failure to fulfil this obligation by the User constitutes that he/she accepts the archived transaction data provided by the Service.</li>
                                        <li>The User has the right to participate in competitions organized by the Operator whose rules will be defined in the Terms and Conditions of Competitions published on special sub-pages of the Service Website. The competitions are not the games of chance pursuant to the Gambling Act from 2005.</li>
                                        <li>The technical requirements necessary to use the Service include any graphical web browser with JavaScript enabled and encrypted connection using SSL protocol.</li>
                                </ul> 
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 3. Authorization For The Seller</h2>
						<div class="content">
							<p>
                                <ul>
                                <li>Only the Seller has the right to put the items up for Buy it Now or Auctions held by the Service.</li>
                                <li>The status of the Seller has been granted to a User who has received the Operator’s Authorization in accordance with the procedure specified below.</li>
                                <li>The rules of selection and Authorization of the Seller:
                                    <ul>
                                        <li>Filing of the Authorization application by sending it to the email address: company@welmory.com;</li>
                                        <li>Verification of the application by the Operator;</li>
                                        <li>Acceptance or refusal of the application;</li>
                                        <li>Conclusion of the cooperation agreement in the case when the application has been approved..</li>
                                    </ul>
                                </li>
                                        <li>The application specified mentioned above shall contain the data and information concerning the User, including at least:
                                        <ul>
                                                <li>The name, surname, residential address and the Identity Card serial number in the case when the User is the private individual or the business name, the address of the registered office, the number of the entry in the relevant register and the legal entity making the entry, the tax ID and business registry number when the User has the status of a legal entity or an organizational unit without legal personality;</li>
                                                <li>The User who has the status of a commercial law entity, an association or a foundation etc. has to enclose a current copy of court registration. A private individual running a business has to enclose a copy from the business register. A private individual who is not running a business has to enclose a copy of the Identity Card;</li>
                                                <li>Regardless of the legal status, a User who is a private individual, a legal person or an organization without legal personality, is obliged to enclose tax and social security clearance certificates with the Authorization application.</li>
                                        </ul>
                                        </li>
                                    <li>The Operator reserves the right to decline the Authorization application of a particular User without providing the explanation.</li>
                                    <li>Upon conclusion of the cooperation agreement between the Organizer and the User, the User receives the status of the Seller and has the right to put the items up for Buy it Now or on an Auction in accordance with the rules specified in the agreement.</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 4. Buy it Now</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>The Buy it Now is an option to purchase an item for a Purchase Price set by the Seller without the necessity to participate in an Auction. The User may use the Buy it Now option at any point of the Auction, as well as after the Remaining Time to the End of the Auction, at the Duration of Buy it Now. Whether the User has put an offer on the Buy it Now or not, the User may still participate in the Auction of this particular item.</li>
                                    <li>The item put up for sale in the Buy it Now shall be made available by the Operator in the Service on the Buy it Now Website and the Auction Website at the Duration of Buy it Now.</li>
                                    <li>Each User of the Service whose Account is not blocked may participate in the Buy it Now.</li>
                                    <li>The buyer who uses the Buy it Now option purchases the item in the way specified in this section by clicking the “BUY IT NOW” button on the Buy it Now Website. This means that the User concludes the sale agreement for the item with the Seller. The Operator informs the User, on behalf of the Seller, by sending an email with confirmation of the purchase, and the performance of the agreement depends on the fulfilment of the sale conditions specified in §7, including the payment of the Purchase Price by the User.</li>
                                    <li>The conditions of the sale agreement, shipping, warranty and the payment for the item mentioned above are specified in §7 of these Terms and Conditions.</li>
                                    <li>In accordance with the rules mentioned below, a User who has purchased an item with Buy it Now and at the same the User has taken part in the Auction acquires the right to purchase the item at discounted Purchase Price, with the restriction or to the return of the Returnable BIDs . The User is entitled to the mentioned rights provided that the User has fulfilled the obligation imposed and has not used the right specified  of these Terms and Conditions.</li>
                                    <li>The Purchase Price will be discounted by the settlement amount of Returnable BIDs which the User has placed at the Auction. The discounted Purchase Price cannot be lower than 50% of the Purchase Price specified in Buy it Now Website.</li>
                                    <li>The return mentioned  above shall occur in the following manner: the User shall receive BIDs which shall replenish pool of BIDs in the amount of Returnable BIDs which equals the amount of spent Returnable BIDs from participation in the Auction to clicking the Buy it Now option.</li>
                                    <li>If the User uses the Buy it Now option once again in the same Auction, the amount of Returnable BIDs shall equal the amount spent from the last Buy it Now purchase. In this case the Purchase Price will not be discounted.</li>
                                    <li>If the User has met all the obligations specified in our rule.In particular the Operator shall begin the recuperating procedure of Returnable BIDs, and the return shall begin within 45 days from the confirmation of an item receipt and, at the same time, the Operator shall replenish the User’s Pool of BIDs unless the User has terminated the sale agreement.</li>
                                    <li>The number of Buy it Now purchases by the User is indicated in User’s Account, in the “MY ACCOUNT”.</li>
                                    <li>It is forbidden to use any unauthorized software by the Operator by any User in order to participate. </li>
                                    <li>Employees and close relatives of the Operator are strictly forbidden to participate in the Buy it Now option.</li>
                                
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 5. Auctions</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Each Auction is conducted on the basis of the conditions specified in the Auction Parameters.</li>
                                    <li>Each Auction begins as soon as the Operator gives access to the Bidding function for a given Auction on the Auction Page.</li>
                                    <li>The Reserve Price is defined in the Auction Parameters.</li>
                                    <li>Bidding rules:</li> 
                                
                                    <ul>
                                    <li>The User places Bids using BIDs purchased on his/her Account by choosing the “BID” button located on the Auction Website or another website of the Service concerning the particular Auction;</li>
                                    <li>Bidding is possible only if the User has at least 1 BID or 1 Returnable BID on an Account;</li>
                                    <li>Bidding or Autobidding decreases the balance of BIDs at least by 1 BID or 1 Returnable BID in accordance with Auction Parameters, in the order specified in Help;</li>
                                    <li>After the start of the Auction the Service updates the Remaining Time to the End of the Auction with accuracy to one second;</li>
                                    <li>Each bid placed by the User extends the Remaining Time to the End of the Auction in such manner that it is made equal to the Current Time to Bid for this particular Auction, provided that the Remaining Time to the End of the Auction is shorter than the Bidding Time;</li>
                                    <li>The Current Price is displayed on the Auction Page;</li>
                                    <li>After each Bidding or Autobidding the Current Price of the item at the Auction is increased by the Incremental Bid Amount defined in the Auction Parameters;</li>
                                    <li>The User who bids the final and highest price becomes the Current Winner;</li>
                                    <li>The Auction finishes when there is a “Finished” sign on the Auction Page; this information does not, however, mean automatic Comfirmation;</li>
                                    <li>The Current Winner becomes the Auction Winner when the Auction finishes and the Current Price becomes the Purchase Price.</li>
                                    </ul> 
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 6. Fees</h2>
						<div class="content">
							<p>
                                <ul>
                                <li>The right to Bid and Autobid in the Auction specified in § 5 is payable. In order to use Service functions enabling the purchase of a particular item offered at the Auction, Returnable BIDs must be purchased. The price of the Returnable BIDs is included in the Price List. In addition to the cost of purchasing the BIDs. Paying for Bidding or Autobidding does not release the Winner of the Auction from the obligation to pay the Purchase Price.</li>
                                <li>In order to purchase Returnable BIDs, the User needs to log into the Service and click the “Replenish my Account” button, then choose the amount of Returnable BIDs to be purchases and the payment method, and then make the payment. From the moment of confirmation of the payment the User acquires the right to Bid and Bid with Autobid in the amount specified in the Price List.</li>
                                <li>Fees for the services provided by the Operator are paid by the User through recognised third party companies.</li>
                                <li>Purchased Returnable BIDs replenish the Pool of BIDs at the User’s Account.</li>
                                <li>Returnable BIDs and BIDs are valid for 180 days from the date of the purchase or their receipt. After the expiry of their validity, if they are unused, they are automatically removed from the Pool of BIDs on the User’s Account. In this case appeals procedures shall be denied.</li>
                                <li>BIDs and Returnable BIDs used by the User are withdrawn from that User’s Account automatically.</li>
                                <li>A User has no right to receive BIDs and Returnable BIDs back apart from the case specified in § 4.</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 7. Sale conditions</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>The sale agreement is concluded between the User and the Seller. The Operator is not a Party to that sale agreement and only enables the Users to make sale agreements with the Seller on the basis of the rules described in these Terms and Conditions.</li>
                                    <li>The Seller is required to confirm in writing the transfer of ownership of the item purchased or auctioned to the Buyer of the Buy it Now or to the Auction Winner no later than the completion of the payment of the Purchase Price by him/her i.e. upon confirmation of the payment from the System up to the amount of 500 Euro. The confirmation is sent only upon request from the Buyer of the Buy it Now or from the Auction Winner.
                                        <ol>
                                                <li>In order to deliver the item purchased or auctioned, the User must provide the necessary address immediately at “MY ACCOUNT,” in the “MY DATA” sub-tab, in the “SHIPPING DATA” form, not later than the completion of the payment. This information is immediately made available to the Seller by the Operator. In the event of a delay in providing this information neither the Operator nor the Seller shall be liable for any delay in the shipment of the item purchased with the Buy it Now option or at the Auction. </li>
                                                <li>The User, concluding the sale agreement with Buy It Now option, shall pay the Purchase Price within at the time of purchasing the item. Notwithstanding the User, concluding the sale agreement by Confirmation, shall complete the payment within 7 days from the Confirmation. The deadline may be extended upon the request from the User via e-mail with the consent of the Seller given in the same manner. Should the User fail to meet the deadline, the Seller shall have the right to withdraw from the sale agreement, about which the Service informs the User via email.</li>
                                        </ol>
                                    </li>
                                    <li>The item purchased or auctioned will be delivered by the Seller to the User or an authorized person at the address provided after 14 days  the Purchase Price is received by the Seller’s bank account. The Seller will inform the User about the dispatch of the item purchased by granting the status “sent” in the System. The User must meet the shipping costs.</li>
                                    <li>The package with the item purchased or auctioned can be sent by an entity acting in understanding with the Seller on the basis of a relevant agreement, which does not affect the rights and obligations of the User.</li>
                                    <li>The User after receiving the item must confirm the delivery.</li>
                                    <li>After receiving the item the User who have purchased an item with the option Buy it Now confirms the receipt of the delivery at the Service Website.</li>
                                    <li>The Seller is not liable for any damage of items incurred during delivery or for the actions of the courier/postal service.</li>
                                    <li>The Seller issues an invoice for the item according to information defined these Terms and Conditions or, when the above-mentioned information is not provided.</li>
                                    <li>The User has the right to terminate the sale agreement for the item without stating a reason within 14 days after receiving the item. The termination has to be made in writing and the object of sale must be returned to the Seller’s address without the signs of being used, at the User’s expense, no later than the date of expiry of the time limit mentioned above.</li>
                                    <li>The Seller has the right to terminate the sale agreement for the item purchased with the Buy it Now option, within the time limit set for the delivery of the item. The User must be informed by email. If the User has already paid the Purchase Price, the paid amount must be returned immediately.</li>
                                    <li>The Seller has the right to terminate the sale agreement for the item purchased as a result of the User becoming the Auction Winner, within the time limit set out  for the delivery of the item. The User must be informed by email. The User is entitled to compensation in the amount of the average market value of the item auctioned if the cause of the termination of the agreement was not the situation is described. If the User has paid the Purchase Price before termination of the sale agreement, the paid amount is subjected to immediate refund.</li>
                                    <li>The Purchase Price and the financial compensation is mentioned . transferred within 7 days to the bank account specified by the User.</li>
                                    <li>The User acquires the right to file a complaint about the item purchased at the Buy it Now or Auction in accordance with the provisions of the warranty document issued by the Manufacturer. All items sold by the Seller are delivered with the manufacturer’s warranty.</li>
                                    <li>Neither the Seller nor the Operator are liable for any incompatibility of the object of sale with the sale agreement if the User knew or should have known about such incompatibility by using reasonable judgement or when those defects occurred after the sale for reasons outside the Seller’s control. In particular, the Seller does not take liability under warranty for defects which occurred after the ownership of the item was transferred to the User.</li>
                                    <li>Complaints about the object of the sale agreement shall be send by email to the Seller at complaint@wellbid.com. The title of this email should include the word “COMPLAINT” and the mark of the Buy it Now or Auction.</li>
                                    <li>The Seller is obliged to respond within 14 days to the complaint by sending an email to the User.</li>
                                    <li>If the complaint requires additional information, the Seller will request further information and the time mentioned in the above point is extended as appropriate.</li>
                                    <li>The User’s warranty in relation to the complaint is valid for one year from the date of the sale, however, the User’s warranty for defects of the item expires if the User does not inform the seller about a defect within 2 months after the defect is detected.</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 8. Complaints</h2>
						<div class="content">
							<p>
                                    <ul>
                                        <li>If any service or other actions of the Operator are inconsistent with those defined in these Terms and Conditions, the User has the right to make a complaint.</li>
                                        <li>The Operator shall do its best to maintain the proper and uninterrupted operation of the Service.</li>
                                        <li>All information concerning technical breaks, errors and breakdowns and the consequences resulting from such for each User shall be published on the Main Page, and in the way detailed.</li>
                                        <li>The User’s Account may be blocked in cases specified in theses Terms and Conditions for the time necessary to verify the User’s actions in compliance with the Terms and Conditions, to prevent any possible damage to other Users, third parties and to the Operator and the apply sanctions described in these Terms and Conditions.</li>
                                        <li>A complaint can be submitted by email to the Operator’s address at complaint@welmory.com.</li>
                                        <li>The Operator is obliged to respond to the complaint within 14 days by sending an email to the User.</li>
                                        <li>If the complaint requires additional information, the Operator sends the complaint back to the Seller to provide such information.</li>
                                    </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 9. Confidentiality. Privacy policy. Use of the Service data</h2>
						<div class="content">
							<p>
                                <ul>
                                        <li>The Operator collects and processes personal data for all Users in accordance with law and the provisions of these Terms and Conditions, as necessary for the purposes of the performance and settlement of the services as part of the agreement.</li>
                                        <li>The Operator will not reveal any personal data of the User to a third party without the User’s express consent subject to the exceptions.</li>
                                        <li>The Operator may reveal personal data of the User to entities authorized to receive such data on the basis of the mandatory provisions of law.</li>
                                        <li>The Operator will provide the Seller with the Users’ personal data in order to implement and perform the provisions of these Terms and Conditions.</li>
                                        <li>The Operator will enable the Users to view and update their data as well as ensure its removal if the agreement is terminated.</li>
                                        <li>The Operator reserves the right to retain the User’s data until such time that any debt towards the Operator is paid by the User or until all circumstances connected with the violation of the Terms and Conditions are explained and the User’s liability established.</li>
                                        <li>The User agrees to receive newsletter but not longer than until the termination of the agreement this option is deselected in the “MY ACCOUNT” tab, the “MY DATA” sub-tab.</li>
                                        <li>The Operator declares that Cookies are saved in the User’s computer when the Service is used. Cookies will be saved with the User’s consent. If the User does not accept this process, the User must not use the Service.</li>
                                        <li>Cookies do not change any configurations on the User’s computer, other data terminal equipment or software installed in that equipment.</li>
                                        <li>The content of Cookies does not allow the identification of the User. With the help of Cookies the personal data is not stored. The identification is conducted impersonally and impartially which concerns only the data on how the Service is used.</li>
                                        <li>The Operator stores Cookies on the Users’ computers to:
                                            <ol>
                                                <li>Maintain the User’s connection (after logging-in);</li>
                                                <li>Verify the User’s identity;</li>
                                                <li>Adjust the Services to the needs of the Users better;</li>
                                                <li>Save the User’s individual set-up and preferences; </li>
                                                <li>Verify the correctness of the Buy it Now and Auction processes.</li>
                                            </ol>
                                        </li>
                                        <li>The Operator does not take liability for the contents of Cookies send by other websites to which the links are located at the Service Website.</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 10. Termination of the Agreement</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>Termination of the agreement
                                    <ul>
                                        <li>The Operator is entitled to terminate the agreement for important reasons at any time without stating a reason, with 14 days notice. The User is entitled to terminate the agreement at any time;</li>
                                        <li>The termination of the agreement by the Operator shall occur after the User is notified by email. If the termination occurs without notice, for important reasons, the Account is immediately blocked, but the User may still send a complaint. If the agreement is terminated with notice, the Account is blocked and closed after the last day of notice;</li>
                                        <li>The User can terminate the agreement by completing an appropriate form available at one of the pages of the Service and sending it to the email address of the Operator, thus starting the account closing process in accordance with the instructions in the “MY ACCOUNT” tab, the “MY DATA” sub-tab, by clicking on “Account Closing” or by using an instruction with the procedure explaining how to close the account located in sub-tab FAQ, in the case where Users have not purchased Returnable BIDs within 10 days of registration.<br>
                                        </li>
                                    </ul>
                                    </li>
                                    <li>After the User has completed the process in accordance the Operator sends to the User’s email address the information on the demand to close the account, containing the link to the website with the confirmation of the username and the User’s password. After confirmation of these details the agreement is terminated with immediate effect, unless:
                                        <ul>
                                        <li>The User participates in Auctions which began before the termination of the agreement, on the basis of the rules effective at the moment at which those Auctions began;</li>
                                        <li>The User has access to the Account only concerning the functions enabling the conclusion of the agreements between the Seller as a result of purchase with the Buy it Now option and the winning of the Auction ended before the agreement has been terminated or the Auction in progress at the time of closing the Account.</li>
                                        </ul>
                                    </li>
                                    <li>Terminating the agreement does not relieve both parties from the settlement of mutual obligations.</li>
                                </ul>
							</p>
						</div>

                        <h2><i class="fa fa-location-arrow"></i> 11. Final Provisions</h2>
						<div class="content">
							<p>
                                <ul>
                                    <li>The Operator has the right to introduce changes into these Terms and Conditions at any time. Updates will be posted on the Home Page, and the Users have the right and obligation to read these updated Terms and Conditions before the first log in after the changes. First logging-in constitutes the acceptance of changes in the updated Terms and Conditions.</li>
                                    <li>All and any changes hereof are implemented in the time specified by the Operator on the Home Page and the login page.</li>
                                    <li>The User may indicate refusal to accept the new Terms and Conditions within 7 days from being notified of the changes at the first time logging in by sending an email to resignation@wellbid.com, which is equivalent to the termination of the agreement for the use of the Operator’s services.</li>
                                    <li>If the User wants to refuse to accept these Terms and Conditions as specified in participation in the Buy it Now or Auctions, otherwise the refusal to accept these Terms and Conditions enters into force upon receipt by the Operator of the email with the refusal statement.</li>
                                    <li>The matters not provided herein shall be governed by the provisions of the Cypriot Civil Code and other relevant provisions of current Cypriot law.</li>
                                    <li>Any disputes between the parties shall be settled by a competent Cypriot civilian court of law.</li>
                                    <li>In the case of any disputes or occurrence of the language variations between English and other languages, the English version shall prevail.</li>
                                </ul>

							</p>
						</div>


                    
        </div> 
		

					
		
    </section>
    
    </span>
    
</asp:Content>

