﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/mstAMS.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <section >


        <div class="container">
            <div class="fixed-img sec-bg7"></div>


            <asp:DataList ID="DLItems"
                runat="server"
                RepeatColumns="4"
                RepeatDirection="Horizontal"
                Width="400px"
                CellPadding="20"
                CssClass="block" OnItemDataBound="DLItems_ItemDataBound">
                <%----%>
                <ItemTemplate>
                    <%-- <div class="row">--%>
                    <div class="col-md-12">
                        <div class="best-seller seller2 ">
                            <asp:Image ID="imgPhoto1"
                                runat="server"
                                Width="230"
                                Height="361px"
                                class="seller-still"
                                ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' />
                            <asp:Image ID="imgPhoto2"
                                runat="server"
                                Width="230"
                                Height="361px"
                                class="seller-hover"
                                ImageUrl='<%#Bind("image2","~/Upload/{0}") %>' />
                            <span>
                                <asp:UpdatePanel runat="server" ID="TimePanel" UpdateMode="Always">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Timer runat="server" ID="Timer1" Interval="850" OnTick="Timer1_Tick"></asp:Timer>
                                        <h3>
                                            <asp:Label runat="server" ID="Label1" />

                                        </h3>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </span>
                        </div>

                        <asp:Label ID="Label2" runat="server" Text="Price :" ForeColor="White" />
                        ₹  
                        <asp:Label runat="server" ID="Label3" Text='<%#Eval("Price") %>' ForeColor="White" /><br />
                        <h3>
                            <asp:LinkButton runat="server"
                                ID="lbtnPname"
                                Text='<%#Eval("ProductName") %>'
                                ForeColor="White"
                                CommandArgument='<%#Bind("ProductId") %>'
                                CommandName="showD"
                                OnCommand="lbtnPname_Command" />
                        </h3>
                        <br />

                    </div>
                </ItemTemplate>
            </asp:DataList>

        </div>
    </section>

    <section class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="promo">
                        <img src="images/service1.jpg" alt="" />
                        <span><i>SIX METHOD</i> Auction</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="promo yellow">
                        <img src="images/service2.jpg" alt="" />
                        <span><i>BIDDING </i>Three Way</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="promo purple">
                        <img src="images/service3.jpg" alt="" />
                        <span><i>NEW PRODUCT</i> Up To 50%</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="block">
        <div class="container">
            <div class="heading1">
                <h2><i>POPULAR </i>CATEGORIES</h2>
                <span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
            </div>
            <div class="shoping-categories">
                <div class="row">
                    <div class="col-md-4">
                        <div class="shop-categories-sec">
                            <img src="images/Default-MobileCollection.jpg" height="368" width="361" alt="" />
                            <div class="shop-categories">
                                <h2>MOBILE COLLECTION</h2>
                                <a href="#" title="">VIEW ALL</a>
                                <span><i class="fa fa-heart"></i>Latest Mobile  </span>
                                <span><i class="fa fa-th"></i>(300) items</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="shop-categories-sec">
                            <img src="images/Default-Computer-Collection.png" height="368" width="361" alt="" />
                            <div class="shop-categories">
                                <h2>COMPUTER  COLLECTION</h2>
                                <a href="#" title="">VIEW ALL</a>
                                <span><i class="fa fa-heart"></i>Great Offer 10% </span>
                                <span><i class="fa fa-th"></i>(250) items</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="shop-categories-sec">
                            <img src="images/Default-Historical-Collection.jpg" height="368" width="361" alt="" />
                            <div class="shop-categories">
                                <h2>HISTORY COLLECTION</h2>
                                <a href="#" title="">VIEW ALL</a>
                                <span><i class="fa fa-heart"></i>Old Product </span>
                                <span><i class="fa fa-th"></i>(400) items</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="block remove-gap">
        <div class="container">
            <div class="heading1">
                <h2><i>UP COMMING AUCTION</i></h2>
                <span>BRAND FOR EVERY STYLE.</span>
            </div>
            <div class="row">

                <asp:DataList ID="DLUpcomingItem"
                    runat="server"
                    RepeatDirection="Horizontal"
                    Width="10px"
                    CellPadding="20"
                    OnItemDataBound="DLUpcomingItem_ItemDataBound">
                    <ItemTemplate>
                        <%--<div class="col-md-4">--%>
                        <div class="best-seller most-sold">

                            <div class="sold-thumb">
                                <asp:Image runat="server" ID="img1Upcoming" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' Width ="400" Height="400"/>
                                <%--<a href="StaticPage/ProductFullDetail.aspx" title="">VIEW DETAILS</a>--%>
                                <asp:LinkButton runat="server"
                                    ID="lbtnUPCOMINGPID"
                                    Text="VIEW DETAIL"
                                    ForeColor="White"
                                    CommandArgument='<%#Bind("ProductId") %>'
                                    CommandName="showD"
                                    OnCommand="lbtnUPCOMINGPID_Command" />
                                <i class="box1"></i>
                                <i class="box2"></i>
                                <i class="box3"></i>
                                <i class="box4"></i>
                            </div>

                            <h3>
                                <asp:Label ID="lblPname" runat="server" Text='<%#Bind("ProductName") %>' />

                            </h3>

                            <ul class="tooltip-btn">
                                <%--<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
                                    <li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>--%>
                            </ul>
                            <%--<a href="" title="">DETAILS</a>--%>

                            <h6>NEW<%--<asp:Label ID="lblRTime" runat="server" Text='<%#Bind("RTime") %>' />--%></h6>
                            <asp:UpdatePanel runat="server" ID="TimePanel" UpdateMode="Always">
                                <ContentTemplate>
                                    <h2>
                                        <asp:Label ID="lblRTime" runat="server" Text='<%#Bind("RTime") %>' /></h2>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--$360</i>$460</span>--%>
                        </div>
                        <%--</div>--%>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>

    </section>

    <div class="load-more-sec">
        <a href=" <%=path%>StaticPage/UpCommingProduct.aspx" title="">More Up Comming Product</a>
    </div>
    <section class="block">
        <div class="container">
            <div class="heading1">
                <h2><i>Finished  </i>Auction</h2>
                <span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
            </div>
            <div class="row">
                <asp:DataList ID="DLFinishedProduct" runat="server"
                    RepeatColumns="4"
                    RepeatDirection="Horizontal"
                    Width="900px"
                    CellPadding="20">
                    <ItemTemplate>


                        
                        <div class="latest-categories">
                            <asp:Image runat="server" ID="ImgFinished" Height="250" Width="270" ImageUrl='<%#Bind("image1","~/Upload/{0}") %>' />
                        

                            <ul>
                                <li>
                                    <i>
                                        <asp:Image runat="server" ID="ImgWinner" ImageUrl='<%#Bind("ClientImage","~/Upload/{0}") %>'
                                            Style="width: 30px; height: 30px; border-radius: 150px; -webkit-border-radius: 150px; -moz-border-radius: 150px; margin-bottom: 5px; box-shadow: 0 0 8px rgba(0, 0, 0, .8); -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8); -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);" />
                                    </i>
                                    <asp:Label runat="server" ID="lblWinner" Text='<%#Eval("ClientName") %>' Font-Size="20px" />
                                </li>

                                <li><i class="fa fa-check"></i>
                                    <p>
                                        <asp:Label runat="server" ID="lblFProductName" Text='<%#Eval("ProductName") %>' />
                                    </p>
                                </li>

                                <li><i class="fa fa-check"></i>
                                    <p>
                                        <asp:Label runat="server" ID="lblAuctionName" Text='<%#Eval("AuctionTypeName") %>' />
                                    </p>
                                </li>
                                <li><i class="fa fa-shopping-cart"></i>
                                    <p>
                                        SellingPrice : 
                                            <asp:Label runat="server" ID="lblFinishedPrice" Text='<%#Eval("BidAmt") %>' />
                                    </p>
                                </li>

                            </ul>
                        </div>
                        <%--</div>--%>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>

    </section>

    <div class="load-more-sec">
        <a href=" <%=path%>StaticPage/FinishedProduct.aspx" title="">More Finished Product</a>
    </div>
</asp:Content>

