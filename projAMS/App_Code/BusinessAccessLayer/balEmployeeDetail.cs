﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;
//for use of string bulider in password
using System.Text;



/// <summary>
/// Summary description for balEmployeeDetail
/// </summary>
public class balEmployeeDetail
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    Random random;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    public balEmployeeDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "Select * from dbo.EmployeeDetail order by IsActive  ";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }

    public DataSet LoadApprove()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.EmployeeDetail WHERE IsActive = 0";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }

    public Boolean Insert(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmpId|SINT|" + boObj.EmpId + "|@EmpName|VCHR|" + boObj.EmpName + "|@Gender|TINT|" + boObj.Gender;
        parasql += "|@Addr|VCHR|" + boObj.Addr + "|@ContactNo|VCHR|" + boObj.ContactNo + "|@Dob|DATE|" + clsUtility.YMD(boObj.Dob) + "|@Doj|DATE|" + clsUtility.YMD(boObj.Doj);
        parasql += "|@Designation|VCHR|" + boObj.Designation + "|@EmpImage|VCHR|" + boObj.EmpImage + "|@EmailId|VCHR|" + boObj.EmailId ;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveEmployeeDetail", parasql);
        if (dalObj.success == true)
        {
            boObj.EmpId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }


    public DataSet getData(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);

        sql = "select EmpId,EmpName,Gender,Addr,ContactNo,";
        sql += "CONVERT(VARCHAR(10),Dob,105) Dob, CONVERT(VARCHAR(10),Doj,105) Doj,Designation,EmpImage,EmailId ";
        sql += " from dbo.EmployeeDetail EM ";
        sql += " where EmpId =" + boObj.EmpId;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;

    }

    public DataSet verifyEmail(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmailId|VCHR|" + boObj.EmailId; //+ "|@Designation|VCHR|" + boObj.Designation ;
        ds = new DataSet();
        ds = dalObj.GetResults("uspVerifyEmail", parasql);
        if (dalObj.success == true)
            return ds;
        else
            return null;


    }


    public DataSet SearchData(String name)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.EmployeeDetail where IsActive = 0 and EmpName like '%" + name + "%' ";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public DataSet AdminSearchData(String name)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.EmployeeDetail where  EmpName like '%" + name + "%'";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }

    public Boolean Approval(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "update EmployeeDetail";
        sql += " set IsActive=" + Convert.ToInt16(boObj.IsActive);
        sql += " where EmpId=" + boObj.EmpId;

        ds = new DataSet();

        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Boolean LeaveDate(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "update EmployeeDetail";
        sql += " set IsActive = 0 , LeaveDt=getdate()";
        sql += " where EmpId=" + boObj.EmpId;

        ds = new DataSet();
         
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public DataSet setpasswd(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@emailId|VCHR|" + boObj.EmailId;
        ds = new DataSet();
        ds = dalObj.GetResults("uspForgetPassword", parasql);

        if (dalObj.success == true)
            return ds;
        else
            return null;

    }
    public DataSet getEmail(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select EmailId,EmpName from EmployeeDetail Where EmpId=" + boObj.EmpId;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);

        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public Boolean LastLoginEmployee(boEmployeeDetail boEmpObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "Update dbo.EmployeeDetail set LoginDate=GETDATE() where EmpId=" + boEmpObj.EmpId;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return true;
        else
            return false;
    }
    public Boolean LastLoginAdmin(boEmployeeDetail boEmpObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "Update dbo.EmployeeDetail set LastLoginDt=GETDATE() where EmpId=" + boEmpObj.EmpId;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return true;
        else
            return false;
    }

    public string GetPassword()
    {
        StringBuilder builder = new StringBuilder();
        //builder.Append(RandomString(2, true));
        builder.Append(RandomNumber(1000, 9999));
        builder.Append(RandomString(2, false));
        return builder.ToString();
    }


    private int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    private string RandomString(int size, bool lowerCase)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
        if (lowerCase)
            return builder.ToString().ToLower();
        return builder.ToString();
    }

    public  Boolean  PassBinary(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmpId|SINT|" + boObj.EmpId + "|@Passwd|VCHR|" + boObj.Passwd;  //+ "|@Designation|VCHR|" + boObj.Designation ;
        ds = new DataSet();
        ds = dalObj.GetResults("uspsaveEmpConvertPasswd", parasql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public Boolean ChangeProfilePasswd(boEmployeeDetail boObj,string pwd)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@Id|VCHR|" + boObj.EmpId + "|@Passwd|VCHR|" + boObj.Passwd + "|@NewPasswd|VCHR|" + pwd;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveProfilePassword", parasql);

        if (ds.Tables[0].Rows[0][0].ToString() == "1")
        {
            return true;
        }
        //if (dalObj.success == true)
        //{
        //    //boObj.EmpId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
        //    return true;
        //}
        else
            return false;     
        
    }

    public Boolean updateEmpDetail(boEmployeeDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmpId|SINT|" + boObj.EmpId + "|@EmpName|VCHR|" + boObj.EmpName + "|@Gender|TINT|" + boObj.Gender;
        parasql += "|@Addr|VCHR|" + boObj.Addr + "|@ContactNo|VCHR|" + boObj.ContactNo + "|@Dob|DATE|" + clsUtility.YMD(boObj.Dob) + "|@Doj|DATE|" + clsUtility.YMD(boObj.Doj);
        

        ds = new DataSet();
        ds = dalObj.GetResults("uspUpdateEmployeeDetail", parasql);
        if (dalObj.success == true)
        {
            boObj.EmpId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
}


