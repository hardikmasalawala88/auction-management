﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balLogin
/// </summary>
public class balLogin
{

    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

    public balLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet Login(boLogin boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmailId|VCHR|" + boObj.EmailId + "|@Passwd|VCHR|" + boObj.Passwd;
        ds = new DataSet();
        ds = dalObj.GetResults("uspLogin", parasql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    //public Boolean Changepasswd(boLogin boObj, string newPwd)
    //{
    //    clsDal dalObj = new clsDal(constr);
    //    parasql = "@emailId|VCHR|" + boObj.EmailId;
    //    parasql += "|@passwd|VCHR|" + boObj.Passwd;
    //    parasql += "|@newPasswd|VCHR|" + newPwd;
    //    ds = new DataSet();
    //    ds = dalObj.GetResults("uspChangeThePasswd", parasql);
    //    if (dalObj.success == true)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}
    public DataSet ForgetPassword(string Email)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@emailId|VCHR|" + Email;
        ds = new DataSet();
        ds = dalObj.GetResults("uspForgetPassword", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }
    public DataSet UpdatePassword(boLogin boObj)//Client Admin Employee Update Forget password and password change
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@EmailId|VCHR|" + boObj.EmailId +"|@Passwd|VCHR|" + boObj.Passwd;
        ds = new DataSet();
        ds = dalObj.GetResults("uspUpdatePassword", parasql);//procedure is called
        if (dalObj.success == true)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }

}