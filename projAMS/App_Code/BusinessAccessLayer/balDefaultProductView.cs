﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using DAL;

/// <summary>
/// Summary description for balDefaultProductView
/// </summary>
public class balDefaultProductView
{
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

	public balDefaultProductView()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet getDataProductFullDetail()
    {
        clsDal dalObj = new clsDal(constr);
        DataSet ds = new DataSet();
        ds = dalObj.GetResults("uspDefaultCurrent");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet getDataUpcomingItem()
    {
        clsDal dalObj = new clsDal(constr);
        DataSet ds = new DataSet();
        ds = dalObj.GetResults("uspGetComingSoonAuction");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet getDataUpcomingItemPage()
    {
        clsDal dalObj = new clsDal(constr);
        DataSet ds = new DataSet();
        ds = dalObj.GetResults("uspGetComingSoonAuctionPage");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet getDataFinishedItem()
    {
        clsDal dalObj = new clsDal(constr);
        DataSet ds = new DataSet();
        ds = dalObj.GetResults("uspGetFinishedAuction");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet getDataFinishedItemPage()
    {
        clsDal dalObj = new clsDal(constr);
        DataSet ds = new DataSet();
        ds = dalObj.GetResults("uspGetFinishedAuctionPage");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
}