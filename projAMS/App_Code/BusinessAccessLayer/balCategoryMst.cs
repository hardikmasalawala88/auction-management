﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//=======================
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;


/// <summary>
/// Summary description for dalCategoryMst
/// </summary>
public class balCategoryMst
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

    public balCategoryMst()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
       
        sql = "select ctm.CategoryType,cm.CategoryId,cm.Category ";
        sql += "from dbo.CategoryMst cm ";
        sql += "join dbo.CategoryTypeMst ctm on ctm.CategoryTypeId =cm.CategoryTypeId";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }

    public DataSet FillddlCategoryType()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.CategoryTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }

    public Boolean Insert(boCategoryMst boObj)
    {
       
        clsDal dalObj = new clsDal(constr);
        parasql = "@CategoryTypeId|TINT|" + boObj.CategoryTypeId + "|@CategoryId|TINT|" + boObj.CategoryId + "|@Category|VCHR|" + boObj.Category;
        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveCategoryMst",parasql);
        if (dalObj.success == true)
        {
            boObj.CategoryId = Convert.ToInt16(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
        {
            return false;
        }
    }

    public DataSet SearchCategoryType(String CTID)
    {
        clsDal dalObj = new clsDal(constr);
        if (CTID == "All")
        {
            sql = "select * from dbo.CategoryMst";
        }
        else
        {
            sql = "select * from dbo.CategoryMst where CategoryTypeId=" + CTID;
        }
        
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
}