﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;//fro constr

/// <summary>
/// Summary description for balSetAutoBid
/// </summary>
public class balSetAutoBid
{
    string parasql = string.Empty;
    DataSet ds;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

    public balSetAutoBid()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Boolean InsertAutoBid(boSetAutoBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|INT|" + boObj.ProductId + "|@ClientId|INT|" + boObj.ClientId;
        parasql += "|@MAmount|MNY|" + boObj.Amount + "|@IncreaseBidAmt|SMNY|" + boObj.IncreaseBidAmt + "|@BidFreq|VCHR|" + boObj.BidFreq;
        ds = new DataSet();
        ds = dalObj.GetResults("uspSetAutoBid", parasql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
            return false;
        }
    public Boolean CallAutoBid()
    {
        clsDal dalObj = new clsDal(constr);
        dalObj.GetResults("uspAutoBid", parasql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
            return false;

    }
    
}