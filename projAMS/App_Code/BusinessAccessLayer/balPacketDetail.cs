﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//=======================
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balPacketDetail
/// </summary>
public class balPacketDetail
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

    public balPacketDetail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
        ds = new DataSet();
        ds = dalObj.GetResults("uspGetPacketDetailClientSide");//uspGetPacketDetailClientSide
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public Boolean Insert(boPacketDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@PacketId|TINT|" + boObj.PacketId + "|@PacketName|VCHR|" + boObj.PacketName + "|@Bids|SINT|" + boObj.Bids + "|@Price|SMNY|" + boObj.Price + "|@Times|TINT|" + boObj.Times + "|@Duration|VCHR|" + boObj.Duration; 

        ds = new DataSet();
        ds = dalObj.GetResults("uspSavePacketDetail", parasql);
        if (dalObj.success == true)
        {
            boObj.PacketId = Convert.ToInt16(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
}