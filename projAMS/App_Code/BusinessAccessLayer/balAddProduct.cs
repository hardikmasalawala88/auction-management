﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;//fro constr

/// <summary>
/// Summary description for balAddProduct
/// </summary>
public class balAddProduct
{
    private DataSet ds;
    string parasql = string.Empty;
    string parasqlProductMainDetail = string.Empty;
    string str;
    string sql = string.Empty;
    Random random;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
	public balAddProduct()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public Boolean Insert(boAddProduct  boObj ,boAuctionTypeItemDetail boAuctionObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId + "|@AuctionTypeId|TINT|" + boObj.AuctionTypeId;
        parasql += "|@ProductName|VCHR|" + boObj.ProductName + "|@ProductDesc|VCHR|" + boObj.ProdcutDesc + "|@Qty|TINT|" + boObj.Qty + "|@Price|INT|" + boObj.Price;
        parasql += "|@AdditionalInfo|VCHR|" + boObj.AdditionalInfo + "|@Warranty|VCHR|" + boObj.Warranty;
        parasql += "|@FeaturesAndBenefits|VCHR|" + boObj.FeaturesAndBenefits + "|@ProductCondition|VCHR|" + boObj.ProductCondition;
        parasql += "|@Image1|VCHR|" + boObj.Image1 + "|@Image2|VCHR|" + boObj.Image2 + "|@Image3|VCHR|" + boObj.Image3 + "|@Image4|VCHR|" + boObj.Image4 + "|@ShippingCharge|SINT|" + boObj.ShippingCharge + "|@ShippingDesc|VCHR|" + boObj.ShippingDesc + "|@IsConfirmed|SINT|" + boObj.IsConfirmed;
        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveProductMst", parasql);
        //product main detail
        boObj.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);
        boAuctionObj.ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]);

        if (boObj.CategoryId == 1 || boObj.CategoryId == 2)
        {
            
            parasqlProductMainDetail = "@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId + "|@CompanyName|VCHR|" + boObj.CompanyName + "|@ModelNo|VCHR|" + boObj.ModelNo + "|@Color|VCHR|" + boObj.Color;
            parasqlProductMainDetail += "|@OS|VCHR|" + boObj.OS + "|@Processor|VCHR|" + boObj.Processor + "|@OSversion|VCHR|" + boObj.OSversion + "|@RAM|VCHR|" + boObj.RAM + "|@Sizeinch|VCHR|" + boObj.Sizeinch + "|@Resolution|VCHR|" + boObj.Resolution + "|@PrimaryCamera|VCHR|" + boObj.PrimaryCamera + "|@SecondaryCamera|VCHR|" + boObj.SecondaryCamera;
            parasqlProductMainDetail += "|@ScreenType|VCHR|" + boObj.ScreenType + "|@Connectivity|VCHR|" + boObj.Connectivity + "|@Storage|VCHR|" + boObj.Storage + "|@Speaker|VCHR|" + boObj.Speaker + "|@Battery|VCHR|" + boObj.Battery + "|@Image1|VCHR|" + boObj.Image5 + "|@Image2|VCHR|" + boObj.Image6 + "|@Image3|VCHR|" + boObj.Image7 + "|@Image4|VCHR|" + boObj.Image8 + "|@Video|VCHR|" + boObj.Video;
            ds = new DataSet();
            ds = dalObj.GetResults("uspSaveProductMainDetail", parasqlProductMainDetail);
        
        }
        
        if (boObj.ClientId == 0)
        {
            str = "update productMst set EmpId = " + boObj.EmpId + " where CategoryId = " + boObj.CategoryId + " and ProductId = " + boObj.ProductId;
            setId(str);
        }
        else
        {
            str = "update productMst set ClientId = " + boObj.ClientId + " where CategoryId = " + boObj.CategoryId + " and ProductId = " + boObj.ProductId;
            setId(str);
        }
        if (dalObj.success == true)
        {
           // boObj.EmpId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
    
    public DataSet fillDDLCategoryId(int MCId)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.CategoryMst where CategoryTypeId="+MCId ;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public DataSet fillDDLCategoryTypeId()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.CategoryTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public DataSet fillDDLAuctionTypeEmp()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.AuctionTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public DataSet fillDDLAuctionTypeClient()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select top 5 * from dbo.AuctionTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public Boolean setId(string str)
    {
        clsDal dalObj = new clsDal(constr);
        ds = new DataSet();
        ds = dalObj.GetResults(str);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}