﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balProductPage
/// </summary>
public class balProductPage
{
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    private DataSet ds;
	public balProductPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getAllProduct(boCategoryMst boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@CategoryTypeId|SINT|" + boObj.CategoryTypeId + "|@Category|VCHR|" + boObj.Category;

        ds = new DataSet();
        ds = dalObj.GetResults("uspGetMenuProduct", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    //uspGetAllAuctionType
    public DataSet getFilterAllAuction(boCategoryMst boObj,boAuctionTypeItemDetail boObjATID)
    {
        clsDal dalObj = new clsDal(constr);
        
        parasql = "@CategoryTypeId|TINT|" + boObj.CategoryTypeId + "|@Category|VCHR|" + boObj.Category + "|@AuctionTypeId|SINT|"+ boObjATID.AuctionTypeId;
        ds = new DataSet();
        ds = dalObj.GetResults("uspGetAllAuctionType", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
}