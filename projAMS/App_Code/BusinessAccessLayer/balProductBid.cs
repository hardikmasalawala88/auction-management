﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balProductBid
/// </summary>
public class balProductBid
{
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    private DataSet ds;
	public balProductBid()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet BidBtn(boProductBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@BidId|SINT|" + boObj.BidId + "|@CategoryId|TINT|" +boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId +"|@ClientId|INT|" +boObj.ClientId;
        parasql += "|@BidAmt|INT|" + boObj.BidAmt + "|@IsBuyNowAmt|BIT|" + boObj.IsBuyNowAmt + "|@BuyQty|TINT|" + boObj.BuyQty;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveBidDetail", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }

    public DataSet getMaxAmt(boProductBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ProductId|SINT|" + boObj.ProductId;

        ds = new DataSet();
        ds = dalObj.GetResults("uspGetMaxBidAmt", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet BuyNowBtn(boProductBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@BidId|SINT|" + boObj.BidId + "|@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId + "|@ClientId|INT|" + boObj.ClientId;
        parasql +="|@IsBuyNowAmt|BIT|" + boObj.IsBuyNowAmt + "|@BuyQty|TINT|" + boObj.BuyQty;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveBuyNowDetail", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet CheckSealded(boProductBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "|@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId + "|@ClientId|INT|" + boObj.ClientId;
        ds = new DataSet();
        ds = dalObj.GetResults("uspCheckSealdedBid", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }

}