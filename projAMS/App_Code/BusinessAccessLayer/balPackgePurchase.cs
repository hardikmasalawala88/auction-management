﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;//fro constr

/// <summary>
/// Summary description for balPackgePurchase
/// </summary>
public class balPackgePurchase
{
    string parasql = string.Empty;
    DataSet ds;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();

    public balPackgePurchase()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Boolean Purchase(boPackagePurchase boObj)
    {

        //Int32 mPackagePurchaseId;
        //Int32 mTotalBid;
        //Int32 mRemaingBid;
        //String mPaymentType;
        //String mBankName;
        //String mBankAc;
        //Int32 mAmount;
        //String mCardNo;
        //int mClientId;
        //Int16 mPacketId;
        ////        @PackagePurchaseId smallint,
        ////@TotalBid	smallint,
        ////@RemaingBid	smallint,
        ////@PaymentType	varchar(20),
        ////@BankName	varchar(50),
        ////@BankAc		varchar(50),
        ////@Amount		smallint,
        ////@CardNo		varchar(50),
        ////@ClientId  int ,
        ////@PacketId tinyint
        clsDal dalObj = new clsDal(constr);
        parasql = "@PackagePurchaseId|SINT|" + boObj.PackagePurchaseId ;
        parasql += "|@PaymentType|VCHR|" + boObj.PaymentType + "|@BankName|VCHR|" + boObj.BankName + "|@BankAc|VCHR|" + boObj.BankAc;
        parasql += "|@CardNo|VCHR|" + boObj.CardNo + "|@ClientId|INT|" + boObj.ClientId + "|@PacketId|TINT|" + boObj.PacketId;
        ds = new DataSet();
        ds = dalObj.GetResults("uspSavePackagePurchase", parasql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
            return false;
    }
}