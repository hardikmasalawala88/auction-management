﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
/// <summary>
/// Summary description for balAuctionTypeItemDetail
/// </summary>
public class balAuctionTypeItemDetail
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    Random random;
    string str;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
	public balAuctionTypeItemDetail()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public Boolean Insert(boAuctionTypeItemDetail boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@AuctionTypeId|TINT|" + boObj.AuctionTypeId + "|@CategoryId|TINT|" + boObj.CategoryId + "|@ProductId|SINT|" + boObj.ProductId;
        parasql += "|@MinAmt|INT|" + boObj.MinAmt + "|@StartDt|DATE|" + clsUtility.YMD(boObj.StartDt) + "|@EndDt|DATE|" + clsUtility.YMD(boObj.EndDt )+ "|@StartTime|VCHR|" + boObj.StartTime + "|@EndTime|VCHR|" + boObj.EndTime;
        parasql += "|@BuyNowAmt|INT|" + boObj.BuyNowAmt  ;
        
        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveAuctionTypeItemDetail", parasql);
        if (boObj.AuctionTypeId == 4)
        {
            str = "update AuctionTypeItemDetail set CharityACBank =" + boObj.CharityACBank + " , CharityACName = " + boObj.CharityACName + " , CharityACNO  " + boObj.CharityACNo;
            str += "  where AuctionTypeId =" + boObj.AuctionTypeId + " and  CategoryId=" + boObj.CategoryId + " and ProductId=" + boObj.ProductId;
            setPro(str);
        }
        if (boObj.AuctionTypeId == 5)
        {
            str = "update AuctionTypeItemDetail set BidMulti =" + boObj.BidMulti ;
            str += "  where AuctionTypeId =" + boObj.AuctionTypeId + " and  CategoryId=" + boObj.CategoryId + " and ProductId=" + boObj.ProductId;
            setPro(str);
        }
        if (dalObj.success == true)
        {
            return true;
        }
        else
            return false;
    }

    public Boolean setPro(string str)
    {
        clsDal dalObj = new clsDal(constr);
        ds = new DataSet();
        ds = dalObj.GetResults(str);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}