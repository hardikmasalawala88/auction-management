﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balProductApprove
/// </summary>
public class balProductApprove
{
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    private DataSet ds;

	public balProductApprove()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet GetProductApprove()
    {
        clsDal dalObj = new clsDal(constr);
        ds = new DataSet();
        ds = dalObj.GetResults("uspGetProductApprove");
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public Boolean Approval(boAddProduct boObj)
    {
        clsDal dalObj = new clsDal(constr);
        sql = "update ProductMst set IsApproved=" + Convert.ToInt16(boObj.IsApproved);
        sql += " where ProductId=" + boObj.ProductId;

        ds = new DataSet();

        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
   
}