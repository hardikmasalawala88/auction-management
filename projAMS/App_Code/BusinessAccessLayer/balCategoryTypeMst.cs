﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//=======================
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;


/// <summary>
/// Summary description for balCategoryTypeMst
/// </summary>
public class balCategoryTypeMst
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    public balCategoryTypeMst()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.CategoryTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public Boolean Insert(boCategoryTypeMst boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@CategoryTypeId|TINT|" + boObj.CategoryTypeId + "|@CategoryType|VCHR|" + boObj.CategoryType;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveCategoryTypeMst", parasql);
        if (dalObj.success == true)
        {
            boObj.CategoryTypeId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
}