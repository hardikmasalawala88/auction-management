﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//=======================
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;

/// <summary>
/// Summary description for balAuctionTypeMst
/// </summary>
public class balAuctionTypeMst
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
	public balAuctionTypeMst()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "select * from dbo.AuctionTypeMst";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    public Boolean Insert(boAuctionTypeMst boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@AuctionTypeId|TINT|" + boObj.AuctionTypeId + "|@AuctionTypeName|VCHR|" + boObj.AuctionTypeName + "|@AuctionTypeDesc|VCHR|" + boObj.AuctionTypeDesc;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveAuctionTypeMst", parasql);
        if (dalObj.success == true)
        {
            boObj.AuctionTypeId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
}