﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DAL;
using System.Configuration;
/// <summary>
/// Summary description for balProductFullDetail
/// </summary>
public class balProductFullDetail
{
    string parasql = string.Empty;
    string sql = string.Empty;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    private DataSet ds;

	public balProductFullDetail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getProductSpec(boAddProduct boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ProductId|VCHR|" + boObj.ProductId ;
       
        ds = new DataSet();
        ds = dalObj.GetResults("uspgetProductSpecification", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }

    public DataSet getCurrentWinner(boProductBid boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ClientId|INT|" + boObj.ClientId;
        ds = new DataSet();
        ds = dalObj.GetResults("uspGetCurrentWinner", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
    public DataSet getRecentBidder(boAddProduct boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ProductId|VCHR|" + boObj.ProductId;

        ds = new DataSet();
        ds = dalObj.GetResults("uspGetRecentBidder", parasql);
        if (dalObj.success == true)
        {
            return ds;
        }
        else
            return null;
    }
}