﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Configuration;
//for use of string bulider in password
using System.Text;

/// <summary>
/// Summary description for balClientRegistration
/// </summary>
public class balClientRegistration
{
    private DataSet ds;
    string parasql = string.Empty;
    string sql = string.Empty;
    //Random random;
    string constr = ConfigurationManager.AppSettings["AMS"].ToString();
    
    public balClientRegistration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public DataSet Load()
    {
        clsDal dalObj = new clsDal(constr);
        sql = "Select * from dbo.ClientDetail";
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;
    }
    
    public Boolean Insert(boClientRegistration boObj)
    {

        clsDal dalObj = new clsDal(constr);
        parasql = "@ClientId|INT|" + boObj.ClientId + "|@ClientName|VCHR|" + boObj.ClientName;
        parasql += "|@ClientAddr|VCHR|" + boObj.ClientAddr + "|@ContactNo|VCHR|" + boObj.ContactNo;
        parasql += "|@EmailId|VCHR|" + boObj.EmailId + "|@Passwd|VCHR|"+boObj.Passwd;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveClientRegistration", parasql);
        if (dalObj.success == true)
        {
            boObj.ClientId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
    
    public Boolean UpdateClientDetail(boClientRegistration boObj)
    {

        clsDal dalObj = new clsDal(constr);
        parasql = "@ClientId|INT|" + boObj.ClientId + "|@ClientName|VCHR|" + boObj.ClientName + "|@Gender|TINT|" + boObj.Gender;
        parasql += "|@ClientAddr|VCHR|" + boObj.ClientAddr + "|@ContactNo|VCHR|" + boObj.ContactNo + "|@ShipAddr|VCHR|"+boObj.ShippingAddr;
       

        ds = new DataSet();
        ds = dalObj.GetResults("uspUpdateClientDetail", parasql);
        if (dalObj.success == true)
        {
            boObj.ClientId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }
    
    public Boolean ChangeProfilePasswd(boClientRegistration boObj, string pwd)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@Id|VCHR|" + boObj.ClientId + "|@Passwd|VCHR|" + boObj.Passwd + "|@NewPasswd|VCHR|" + pwd;

        ds = new DataSet();
        ds = dalObj.GetResults("uspSaveProfilePassword", parasql);
        if (dalObj.success == true)
        {
            //boObj.ClientId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return true;
        }
        else
            return false;
    }

    public string GetCode()
    {
        StringBuilder builder = new StringBuilder();
        //builder.Append(RandomString(2, true));
        builder.Append(RandomNumber(1000, 9999));

        return builder.ToString();
    }
    
    private int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }
    
    public DataSet getData(boClientRegistration boClientObj)
    {
        clsDal dalObj = new clsDal(constr);

        sql = "select ClientId,ClientName,Gender,ClientAddr,ShippingAddr,ContactNo,EmailId,ClientImage,Passwd";
        sql += " from dbo.ClientDetail";
        sql += " where ClientId =" + boClientObj.ClientId;
        ds = new DataSet();
        ds = dalObj.GetResults(sql);
        if (dalObj.success == true)
            return ds;
        else
            return null;

    }

    public void setImage(boAddProduct boObj)
    {
        string str;
        if (boObj.ClientId == 0)
        {
            str = "update EmployeeDetail set EmpImage = '" + boObj.Image1 + "' where EmpId = " + boObj.EmpId;
            setProperties(str);
        }
        else
        {
            str = "update ClientDetail set ClientImage = '" + boObj.Image1 + "' where ClientId = " + boObj.ClientId;
            setProperties(str);
        }
    
    }
    
    public Boolean setProperties(string str)
    {
        clsDal dalObj = new clsDal(constr);
        ds = new DataSet();
        ds = dalObj.GetResults(str);
        if (dalObj.success == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public DataSet FillClientPackageDetail(boClientRegistration boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ClientId|INT|" + boObj.ClientId;

        ds = new DataSet();
        ds = dalObj.GetResults("uspGetClientPacketPurchaseDetail", parasql);
        if (dalObj.success == true)
        {
            //boObj.ClientId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return ds;
        }
        else
            return null;
    }
    public DataSet FillClientBiddingDetail(boClientRegistration boObj)
    {
        clsDal dalObj = new clsDal(constr);
        parasql = "@ClientId|INT|" + boObj.ClientId;

        ds = new DataSet();
        ds = dalObj.GetResults("uspGetClientBiddingDetailOverViewPage", parasql);
        if (dalObj.success == true)
        {
            //boObj.ClientId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return ds;
        }
        else
            return null;
    }
}