﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boCategoryMst
/// </summary>
public class boCategoryMst
{
    int mCategoryTypeId;
    int mCategoryId;
    string mCategoryType;
    string mCategory;
    public boCategoryMst()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int CategoryTypeId
    {
        get { return mCategoryTypeId; }
        set { mCategoryTypeId = value; }
    }
    public string CategoryType
    {
        get { return mCategoryType; }
        set { mCategoryType = value; }
    }
    public int CategoryId
    {
        get { return mCategoryId; }
        set { mCategoryId = value; }
    }
    public String Category
    {
        get { return mCategory; }
        set { mCategory = value; }
    }
}