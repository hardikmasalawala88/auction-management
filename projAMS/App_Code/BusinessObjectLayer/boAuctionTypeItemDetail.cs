﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boAuctionTypeItemDetail
/// </summary>
public class boAuctionTypeItemDetail
{
    Int16  mAuctionTypeId;
    Int16  mCategoryId;
    int    mProductId;
    int    mMinAmt;
    string mStartDt;
    string mEndDt;
    string mStartTime;
    string mEndTime;
    int    mBuyNowAmt;
    int    mBidCnt;
    string mCharityACBank;
    string mCharityACName;
    string mCharityACNo;
    Int16  mBidMulti;

	public boAuctionTypeItemDetail()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public Int16 AuctionTypeId
    {
        get { return mAuctionTypeId; }
        set { mAuctionTypeId = value; }
    }
    public Int16 CategoryId
    {
        get { return mCategoryId; }
        set { mCategoryId = value; }
    }
    public int ProductId
    {
        get { return mProductId; }
        set { mProductId = value; }
    }
    public int MinAmt
    {
        get { return mMinAmt; }
        set { mMinAmt = value; }
    }
    public string StartDt
    {
        get { return mStartDt; }
        set { mStartDt = value; }
    }
    public string EndDt
    {
        get { return mEndDt; }
        set { mEndDt = value; }
    }
    public string StartTime
    {
        get { return mStartTime; }
        set { mStartTime = value; }
    }
    public string EndTime
    {
        get { return mEndTime; }
        set { mEndTime = value; }
    }
    public int BuyNowAmt
    {
        get { return mBuyNowAmt; }
        set { mBuyNowAmt = value; }
    }
    public int BidCnt
    {
        get { return mBidCnt; }
        set { mBidCnt = value; }
    }

    public string CharityACBank
    {
        get { return mCharityACBank; }
        set { mCharityACBank = value; }
    }
    public string CharityACName
    {
        get { return mCharityACName; }
        set { mCharityACName = value; }
    }
    public string CharityACNo
    {
        get { return mCharityACNo; }
        set { mCharityACNo = value; }
    }



   
    public Int16 BidMulti
    {
        get { return mBidMulti; }
        set { mBidMulti = value; }
    }
}