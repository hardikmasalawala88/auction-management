﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boProductBid
/// </summary>
public class boProductBid
{
    Int32 mBidId;
    Int32 mProductId;
    Int16 mCategoryId;
    int mClientId;
    int mBidAmt;
    Boolean mIsBuyNowAmt;
    Int16 mBuyQty;
   
	public boProductBid()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int16 CategoryId
    {
        get { return mCategoryId; }
        set { mCategoryId = value; }
    }
    public Int32 ProductId
    {
        get { return mProductId; }
        set { mProductId = value; }
    }
    public Int32 BidId
    {
        get { return mBidId; }
        set { mBidId = value; }
    }
    public int ClientId
    {
        get { return mClientId; }
        set { mClientId = value; }
    }
    public int BidAmt
    {
        get { return mBidAmt; }
        set { mBidAmt = value; }
    }
    public Boolean IsBuyNowAmt
    {
        get { return mIsBuyNowAmt; }
        set { mIsBuyNowAmt = value; }
    }
    public Int16 BuyQty
    {
        get { return mBuyQty; }
        set { mBuyQty = value; }
    }


}