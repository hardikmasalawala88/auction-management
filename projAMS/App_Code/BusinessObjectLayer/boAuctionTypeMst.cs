﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boAuctionTypeMst
/// </summary>
public class boAuctionTypeMst
{
    //m for memory so we identify this is memory variable
    int mAuctionTypeId;
    string mAuctionTypeName;
    string mAuctionTypeDesc;
	public boAuctionTypeMst()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int AuctionTypeId
    {
        get { return mAuctionTypeId; }
        set { mAuctionTypeId = value; }
    }
    public String AuctionTypeName
    {
        get { return mAuctionTypeName; }
        set { mAuctionTypeName = value; }
    }
    public String AuctionTypeDesc
    {
        get { return mAuctionTypeDesc; }
        set { mAuctionTypeDesc = value; }
    }
}