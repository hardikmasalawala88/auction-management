﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boSetAutoBid
/// </summary>
public class boSetAutoBid
{
	public boSetAutoBid()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    Int16 mCategoryId;
    int mProductId;
    int mClientId;
    int  mAmount;
    int mIncreaseBidAmt;
    String mBidFreq;
//    CategoryId	tinyint
//ProductId	int
//ClientId	int
//BidAmt	Small Mony
	
//IsBuyNowAmt	bit
//BuyQty	tiny int
//IsCancel	bit

    public Int16 CategoryId
    {
        get { return mCategoryId; }
        set { mCategoryId = value; }
    }
    public int ProductId
    {
        get { return mProductId; }
        set { mProductId = value; }
    }
    
    public int ClientId
    {
        get { return mClientId; }
        set { mClientId = value; }
    }

    public Int32 Amount
    {
        get { return mAmount; }
        set { mAmount = value; }
    }

    public String BidFreq
    {
        get { return mBidFreq; }
        set { mBidFreq = value; }
    }

    public int IncreaseBidAmt
    {
        get { return mIncreaseBidAmt; }
        set { mIncreaseBidAmt = value; }
    }
    
}