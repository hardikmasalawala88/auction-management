﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boPackagePurchase
/// </summary>
public class boPackagePurchase
{
    Int32 mPackagePurchaseId;
    Int32 mTotalBid;
    Int32 mRemaingBid;
    String mPaymentType;
    String mBankName;
    String mBankAc;
    Int32 mAmount;
    String mCardNo;
    int mClientId;
    Int16 mPacketId;

	public boPackagePurchase()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int32 PackagePurchaseId
    {
        get { return mPackagePurchaseId; }
        set { mPackagePurchaseId = value; }
    }

    public Int32 TotalBid
    {
        get { return mTotalBid; }
        set { mTotalBid = value; }
    }

    public Int32 RemaingBid
    {
        get { return mRemaingBid; }
        set { mRemaingBid = value; }
    }

    public String PaymentType
    {
        get { return mPaymentType; }
        set { mPaymentType = value; }
    }

    public String BankName
    {
        get { return mBankName; }
        set { mBankName = value; }
    }

    public String BankAc
    {
        get { return mBankAc; }
        set { mBankAc = value; }
    }

    public Int32 Amount
    {
        get { return mAmount; }
        set { mAmount = value; }
    }

    public String CardNo
    {
        get { return mCardNo; }
        set { mCardNo = value; }
    }

    public int ClientId
    {
        get { return mClientId; }
        set { mClientId = value; }
    }

    public Int16 PacketId
    {
        get { return mPacketId; }
        set { mPacketId = value; }
    }
}