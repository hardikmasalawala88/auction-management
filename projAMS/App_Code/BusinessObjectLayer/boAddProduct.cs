﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boAddProduct
/// </summary>
public class boAddProduct
{
    Int16 mCategoryId;
    Int32 mProductId;
    Int16 mAuctionTypeId;
    int mClientId;
    Int32 mEmpId;
    string mProductName;
    string mProdcutDesc;
    Int16 mQty;
    int mPrice;
    string mAdditionalInfo;
    string mWarranty;
    string mFeaturesAndBenefits;
    string mProductCondition;
    string mImage1;
    string mImage2;
    string mImage3;
    string mImage4;
    Int32 mShippingCharge;
    string mShippingDesc;
    Int16 mIsApproved;
    Int16 mIsConfirmed;
    //ProductMainDetailField=================================================
    string mCompanyName;
    string mModelNo;
    string mColor;
    string mOS;
    string mProcessor;
    string mOSversion;
    string mRAM;
    string  mSizeinch;
    string mResolution;
    string mPrimaryCamera;
    string mSecondaryCamera;
    string mScreenType;
    string mConnectivity;
    string mStorage;
    string mSpeaker;
    string mBattery;
    string mImage5;
    string mImage6;
    string mImage7;
    string mImage8;
    string mVideo;


    public boAddProduct()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //this called as properties
    public Int16 CategoryId
    {
        get { return mCategoryId; }
        set { mCategoryId = value; }
    }
    public Int32 ProductId
    {
        get { return mProductId; }
        set { mProductId = value; }
    }
    public Int16 AuctionTypeId
    {
        get { return mAuctionTypeId; }
        set { mAuctionTypeId = value; }
    }
    public int ClientId
    {
        get { return mClientId; }
        set { mClientId = value; }
    }
    public Int32 EmpId
    {
        get { return mEmpId; }
        set { mEmpId = value; }
    }
    public string ProductName
    {
        get { return mProductName; }
        set { mProductName = value; }
    }
    public string ProdcutDesc
    {
        get { return mProdcutDesc; }
        set { mProdcutDesc = value; }
    }
    public Int16 Qty
    {
        get { return mQty; }
        set { mQty = value; }
    }
    public int Price
    {
        get { return mPrice; }
        set { mPrice = value; }
    }
    public string AdditionalInfo
    {
        get { return mAdditionalInfo; }
        set { mAdditionalInfo = value; }
    }
    public string Warranty
    {
        get { return mWarranty; }
        set { mWarranty = value; }
    }
    public string FeaturesAndBenefits
    {
        get { return mFeaturesAndBenefits; }
        set { mFeaturesAndBenefits = value; }
    }
    public string ProductCondition
    {
        get { return mProductCondition; }
        set { mProductCondition = value; }
    }
    public string Image1
    {
        get { return mImage1; }
        set { mImage1 = value; }
    }
    public string Image2
    {
        get { return mImage2; }
        set { mImage2 = value; }
    }
    public string Image3
    {
        get { return mImage3; }
        set { mImage3 = value; }
    }
    public string Image4
    {
        get { return mImage4; }
        set { mImage4 = value; }
    }
    public Int32 ShippingCharge
    {
        get { return mShippingCharge; }
        set { mShippingCharge = value; }
    }
    public Int16 IsApproved
    {
        get { return mIsApproved; }
        set { mIsApproved = value; }
    }
    public string ShippingDesc
    {
        get { return mShippingDesc; }
        set { mShippingDesc = value; }
    }
    public Int16 IsConfirmed
    {
        get { return mIsConfirmed; }
        set { mIsConfirmed = value; }
    }

    //ProductMainDetailField=================================================
    public string CompanyName
    {
        get { return mCompanyName; }
        set { mCompanyName = value; }
    }
    public string ModelNo
    {
        get { return mModelNo; }
        set { mModelNo = value; }
    }
    public string Color
    {
        get { return mColor; }
        set { mColor = value; }
    }
    public string OS
    {
        get { return mOS; }
        set { mOS = value; }
    }
    public string Processor
    {
        get { return mProcessor; }
        set { mProcessor = value; }
    }
    public string OSversion
    {
        get { return mOSversion; }
        set { mOSversion = value; }
    }
    public string RAM
    {
        get { return mRAM; }
        set { mRAM = value; }
    }
    public string Sizeinch
    {
        get { return mSizeinch; }
        set { mSizeinch = value; }
    }
    public string Resolution
    {
        get { return mResolution; }
        set { mResolution = value; }
    }
    public string PrimaryCamera
    {
        get { return mPrimaryCamera; }
        set { mPrimaryCamera = value; }
    }
    public string SecondaryCamera
    {
        get { return mSecondaryCamera; }
        set { mSecondaryCamera = value; }
    }
    public string ScreenType
    {
        get { return mScreenType; }
        set { mScreenType = value; }
    }
    public string Connectivity
    {
        get { return mConnectivity; }
        set { mConnectivity = value; }
    }
    public string Storage
    {
        get { return mStorage; }
        set { mStorage = value; }
    }
    public string Speaker
    {
        get { return mSpeaker; }
        set { mSpeaker = value; }
    }
    public string Battery
    {
        get { return mBattery; }
        set { mBattery = value; }
    }
    public string Image5
    {
        get { return mImage5; }
        set { mImage5 = value; }
    }
    public string Image6
    {
        get { return mImage6; }
        set { mImage6 = value; }
    }
    public string Image7
    {
        get { return mImage7; }
        set { mImage7 = value; }
    }
    public string Image8
    {
        get { return mImage8; }
        set { mImage8 = value; }
    }
    public string Video
    {
        get { return mVideo; }
        set { mVideo = value; }
    }

}