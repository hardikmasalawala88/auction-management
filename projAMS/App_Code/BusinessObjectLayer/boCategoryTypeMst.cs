﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boCategoryTypeMst
/// </summary>
public class boCategoryTypeMst
{
    //m for memory so we identify this is memory variable
    int mCategoryTypeId;
    string mCategoryType;
	public boCategoryTypeMst()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int CategoryTypeId
    {
        get { return mCategoryTypeId; }
        set { mCategoryTypeId = value; }
    }
    public String CategoryType
    {
        get { return mCategoryType; }
        set { mCategoryType = value; }
    }
}