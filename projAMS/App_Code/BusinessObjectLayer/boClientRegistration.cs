﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boClientRegistration
/// </summary>
public class boClientRegistration
{
    //m for memory so we identify this is memory variable
    int mClientId;
    string mClientName;
    Int16 mGender;
    string mClientAddr;
    string mShippingAddr;
    string mContactNo;
    string mEmailId;
    string mClientImage;
    string mPasswd;
    Int16 mIsActive;
    Int16 mIsOnline;
	public boClientRegistration()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int ClientId
    {
        get { return mClientId; }
        set { mClientId = value; }
    }
    public string ClientName
    {
        get { return mClientName; }
        set { mClientName = value; }
    }
    public Int16 Gender
    {
        get { return mGender; }
        set { mGender = value; }
    }
    public string ClientAddr
    {
        get { return mClientAddr; }
        set { mClientAddr = value; }
    }
    public string ShippingAddr
    {
        get { return mShippingAddr; }
        set { mShippingAddr = value; }
    }
    public string ContactNo
    {
        get { return mContactNo; }
        set { mContactNo = value; }
    }
    public string EmailId
    {
        get { return mEmailId; }
        set { mEmailId = value; }
    }
    public string ClientImage
    {
        get { return mClientImage; }
        set { mClientImage = value; }
    }
    public string Passwd
    {
        get { return mPasswd; }
        set { mPasswd = value; }
    }
    public Int16 IsActive
    {
        get { return mIsActive; }
        set { mIsActive = value; }
    }
    public Int16 IsOnline
    {
        get { return mIsOnline; }
        set { mIsOnline = value; }
    }
}