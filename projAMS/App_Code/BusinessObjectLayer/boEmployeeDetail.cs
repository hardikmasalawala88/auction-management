﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boEmployeeDetail
/// </summary>
public class boEmployeeDetail
{
    Int32 mEmpId;
    string mEmpName;
    Int16 mGender;
    string mAddr;
    string mContactNo;
    string mDob;
    string mDoj;
    string mDesignation;
    string mEmpImage;
    string mEmailId;
    string mPasswd;
    Int16 mIsActive;
    string mLeaveDt;
    string mLoginDate;
    Int16 mIsOnline;

    public boEmployeeDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    public Int32 EmpId
    {
        get { return mEmpId; }
        set { mEmpId = value; }
    }
    public Int16 Gender
    {
        get { return mGender; }
        set { mGender = value; }
    }
    public Int16 IsActive
    {
        get { return mIsActive; }
        set { mIsActive = value; }
    }
    public Int16 IsOnline
    {
        get { return mIsOnline; }
        set { mIsOnline = value; }
    }
    public String EmpName
    {
        get { return mEmpName; }
        set { mEmpName = value; }
    }
    public String Addr
    {
        get { return mAddr; }
        set { mAddr = value; }
    }
    public String ContactNo
    {
        get { return mContactNo; }
        set { mContactNo = value; }
    }
    public String Dob
    {
        get { return mDob; }
        set { mDob = value; }
    }
    public String Doj
    {
        get { return mDoj; }
        set { mDoj = value; }
    }
    public String Designation
    {
        get { return mDesignation; }
        set { mDesignation = value; }
    }
    public String EmpImage
    {
        get { return mEmpImage; }
        set { mEmpImage = value; }
    }
    public String EmailId
    {
        get { return mEmailId; }
        set { mEmailId = value; }
    }
    public String Passwd
    {
        get { return mPasswd; }
        set { mPasswd = value; }
    }
    public String LeaveDt
    {
        get { return mLeaveDt; }
        set { mLeaveDt = value; }
    }
    public String LoginDate
    {
        get { return mLoginDate; }
        set { mLoginDate = value; }
    }
}