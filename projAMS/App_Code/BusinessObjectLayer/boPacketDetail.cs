﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for boPacketDetail
/// </summary>
public class boPacketDetail
{


    Int16 mPacketId;
    string mPacketName;
    Int32 mBids;
    Int16 mPrice;
    Int16 mTimes;
    string mDuration;

    public boPacketDetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Int32 Bids
    {
        get { return mBids; }
        set { mBids = value; }
    }
    public Int16 PacketId
    {
        get { return mPacketId; }
        set { mPacketId = value; }
    }
    public Int16 Price
    {
        get { return mPrice; }
        set { mPrice = value; }
    }
    public String PacketName
    {
        get { return mPacketName; }
        set { mPacketName = value; }
    }
    public Int16 Times
    {
        get { return mTimes; }
        set { mTimes = value; }
    }
    public String Duration
    {
        get { return mDuration; }
        set { mDuration = value; }
    }
}