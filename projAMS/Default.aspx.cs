﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;


public partial class _Default : System.Web.UI.Page
{
    public string path = "";
    balDefaultProductView balObjDP = new balDefaultProductView();
    balSetAutoBid balObjSetAutoBid = new balSetAutoBid();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (File.Exists(Server.MapPath("Web.config")))
            {
                path = "";
            }
            else if (File.Exists(Server.MapPath("../web.config")))
            {
                path = "../";
            }
            else if (File.Exists(Server.MapPath("../../web.config")))
            {
                path = "../../";
            }
            fillDLItems();
            fillDLUpcomingItem();
            fillDLFinishedItem();
        }


    }
    protected void fillDLItems()
    {
        DLItems.DataSource = balObjDP.getDataProductFullDetail();
        DLItems.DataBind();
    }

    protected void fillDLUpcomingItem()
    {
        DLUpcomingItem.DataSource = balObjDP.getDataUpcomingItem();
        DLUpcomingItem.DataBind();
    }

    protected void fillDLFinishedItem()
    {
        DLFinishedProduct.DataSource = balObjDP.getDataFinishedItem();
        DLFinishedProduct.DataBind();
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        fillDLItems();
        fillDLUpcomingItem();
        balObjSetAutoBid.CallAutoBid();
    }
    protected void DLItems_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label timelabel = (Label)e.Item.FindControl("Label1");

            string Startday = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[21].ToString());//21 Start Date
            string Endday = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[22].ToString());//22 End Date
            string Starttime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[23].ToString());//23 Start Time
            string Endtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[24].ToString());//24 End Time

            // Format the value as currency and redisplay it in the DataList.
            //timelabel.Text = (DateTime)time - DateTime.Now.TimeOfDay;
            //timelabel.Text = Startday.ToString(); //+ Starttime.ToString() + Endday.ToString() + Endtime.ToString();
            //TimeSpan dt = Convert.ToDateTime(Endtime) - Convert.ToDateTime(DateTime.Now);// +Endday.ToString() + Endtime.ToString();
            //timelabel.Text = Convert.ToDateTime(Starttime).ToString();
           
            string RDay = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[21].ToString());//21 Remainig Day
            string Rtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[22].ToString());//21 Remainig Time
            
                timelabel.Text = " " + RDay.ToString() + " Day " + Rtime.ToString();
             
            //else 
            //{
            //    timelabel.Text = Rtime.ToString();
            //}
           
        }
    }
    //protected void TimerDLUpcomingItem_Tick(object sender, EventArgs e)
    //{
       
    //}
    protected void DLUpcomingItem_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label timelabel = (Label)e.Item.FindControl("lblRTime");

            string RDay = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString());//21 Remainig Day
            string Rtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[4].ToString());//21 Remainig Time
            if (RDay != "0") 
            {
                timelabel.Text = " " + RDay.ToString() + " Day Remainig to Start";
            } 
            else 
            {
                timelabel.Text = Rtime.ToString()+" Time Remainig to Start";
            }
            
        }
    }
    protected void DLItems1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label timelabel = (Label)e.Item.FindControl("lblRTime1");

            string RDay = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[2].ToString());//21 Remainig Day
            string Rtime = Convert.ToString(((DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString());//21 Remainig Time
            if (RDay != "0")
            {
                timelabel.Text = " " + RDay.ToString() + " Day Remainig";
            }
            else
            {
                timelabel.Text = Rtime.ToString();
            }

        }
    }
    
    protected void lbtnPname_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "showD")
        {
            Session["showD"] = e.CommandArgument.ToString();
            Response.Redirect("~/StaticPage/ProductFullDetail.aspx");
        }
    }
    protected void lbtnUPCOMINGPID_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "showD")
        {
            Session["showD"] = e.CommandArgument.ToString();
            Response.Redirect("~/StaticPage/ProductFullDetail.aspx");
        }
    }
}